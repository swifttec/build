PROJROOT	= ..

include ../win32.mak.tools
include Makefile.defs

PROG	= $(TOOLSBIN)\$(PROGNAME).exe

all: $(PROG)

!if [$(TOOLSBIN)\mkobjnames $(OBJDIR) $(OBJDIR)\objnames.inc OBJS $(SRCS)]
!endif
!include $(OBJDIR)\objnames.inc


clean:
	-$(RM) -rf $(OBJDIR) $(TARGETS)

$(PROG): $(OBJS) ..\scripts\wrapper.bat ..\common\$(OBJDIR)\common.lib
	@if not exist $(TOOLSBIN) mkdir $(TOOLSBIN)
	cl -nologo -Fe$(PROG) $(OBJS) -link -libpath:../common/$(OBJDIR) common.lib
