#include <windows.h>
#include <stdio.h>

#pragma warning(disable: 4996)

#if (defined(WIN32) || defined(_WIN32)) && (_MSC_VER >= 1900)
	#define gets(x)	gets_s(x, sizeof(x)-1)
#endif


int
showDriveInfo(const char *drive)
		{
		DWORD	volumeSerialNumber, maximumComponentLength, fileSystemFlags;
		char	name[40], fsname[40];
		int		res=0;

		if (GetVolumeInformation(
		  drive,
		  name,
		  sizeof(name),
		  &volumeSerialNumber,
		  &maximumComponentLength,
		  &fileSystemFlags,
		  fsname,
		  sizeof(fsname)
		))
			{
			printf("Drive %s has label %s\n", drive, name);
			}
		else
			{
			res = GetLastError();
			if (res == ERROR_NOT_READY)
				{
				printf("No disk in drive %s or drive not ready.\n", drive);
				}
			else
				{
				char	msg[200];

				if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, res, 0, msg, sizeof(msg), 0))
					{
					printf("%s\n", msg);
					}
				else
					{
					printf("Failed to get mount point for %s - %d\n", drive, GetLastError());
					}
				}
			}

		return res;
		}


int
mountVolumeInDrive(const char *drive, const char *volume)
		{
		DWORD	volumeSerialNumber, maximumComponentLength, fileSystemFlags;
		char	name[40], fsname[40];
		bool	found=false;
		int		res=0;

		while (!found)
			{
			if (GetVolumeInformation(
			  drive,
			  name,
			  sizeof(name),
			  &volumeSerialNumber,
			  &maximumComponentLength,
			  &fileSystemFlags,
			  fsname,
			  sizeof(fsname)
			))
				{
				if (stricmp(name, volume) == 0)
					{
					found = true;
					break;
					}
				}
			else
				{
				res = GetLastError();
				if (res != ERROR_NOT_READY)
					{
					char	msg[200];

					if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, res, 0, msg, sizeof(msg), 0))
						{
						printf("%s\n", msg);
						}
					else
						{
						printf("Failed to get mount point for %s - %d\n", drive, GetLastError());
						}

					break;
					}
				}

			printf("Please insert volume %s in drive %s and press enter\n", volume, drive);
			printf("or enter q to quit.");
			gets(name);

			if (stricmp(name, "q") == 0)
				{
				res = ERROR_FILE_NOT_FOUND;
				break;
				}
			}

		return res;
		}


void
usage()
		{
		printf("usage: mountvol drive label\n");
		exit(1);
		}


int
main(int argc, char **argv)
		{
		char	drive[4];
		int		r;

		if (argc < 2) usage();
		drive[0] = argv[1][0];
		drive[1] = ':';
		drive[2] = '\\';
		drive[3] = 0;

		if (argc == 2) r = showDriveInfo(drive);
		else r = mountVolumeInDrive(drive, argv[2]);

		return r;
		}
