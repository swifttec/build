/*
**	rm.cpp		A simple rm command tool (unix remove) for the BDF
**				for windows.
*/

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#if (defined(WIN32) || defined(_WIN32)) && (_MSC_VER >= 1900)
	#define gets(x)	gets_s(x, sizeof(x)-1)
#endif

int		vflag=0;
int		qflag=0;
int		rflag=0;
int		Vflag=0;
int		sflag=0;
int		fflag=0;
int		iflag=0;
int		errcount=0;
int		dcount=0, derrors=0;
int		fcount=0, ferrors=0;

void
usage()
		{
		fprintf(stderr, "usage: rm [-rvf] file1 ...\n");
		fprintf(stderr, "where:\n");
		fprintf(stderr, "    -v   Verbose mode\n");
		fprintf(stderr, "    -q   Quiet mode - no messages except for summary (if -s flag supplied)\n");
		fprintf(stderr, "    -s   Print summary of deletes\n");
		fprintf(stderr, "    -i   Interactive mode - prompt before each delete\n");
		fprintf(stderr, "    -f   Forced delete mode - attempt to delete read-only entries\n");
		fprintf(stderr, "    -r   Recursive mode - delete all file in directories when found\n");
		exit(1);
		}


void
removefile(const char *file, bool all=false)
		{
		if (iflag && !all)
			{
			char	s[80];

			printf("Remove file %s (y/n)? ", file);
			if (gets(s) && *s != 'Y' && *s !='y') return;
			}

		if (vflag) printf("%s\n", file);

		BOOL	ok=FALSE;

		ok = DeleteFile(file);
		if (!ok)
			{
			if (fflag)
				{
				DWORD	attr;

				attr = GetFileAttributes(file);
				if (attr != INVALID_FILE_ATTRIBUTES)
					{
					attr &= ~(FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_READONLY | FILE_ATTRIBUTE_SYSTEM);
					SetFileAttributes(file, attr);

					ok = DeleteFile(file);
					}
				}
			}

		if (!ok)
			{
			if (!qflag) fprintf(stderr, "**** Can't delete %s ****\n", file);
			ferrors++;
			}
		else fcount++;
		}


void
removedir(const char *file, bool all=false)
		{
		char	tmp[MAX_PATH];

		if (iflag && !all)
			{
			char	s[80];

			printf("Remove directory %s (y/n)? ", file);
			if (gets(s) && *s != 'Y' && *s !='y') return;
			}

		if (rflag)
			{
			WIN32_FIND_DATA data;
			HANDLE 			dh;
			int				len=(int)strlen(file);
			
			sprintf(tmp, "%s\\*", file);

			dh = FindFirstFile(tmp, &data);
			if (dh != INVALID_HANDLE_VALUE)
				{
				do
					{
					// Skip . and ..
					if (strcmp(data.cFileName, ".") == 0 || strcmp(data.cFileName, "..") == 0) continue;

					strcpy(tmp, file);
					if (tmp[len-1] == '\\' || tmp[len-1] == '/')
						{
						strcpy(tmp + len, data.cFileName);
						}
					else
						{
						tmp[len] = '\\';
						strcpy(tmp + len + 1, data.cFileName);
						}

					if ((data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
						{
						removedir(tmp);
						}
					else
						{
						removefile(tmp);
						}
					}
				while (FindNextFile(dh, &data));

				FindClose(dh);
				}
			}

		if (vflag) printf("%s\n", file);

		BOOL	ok=FALSE;

		ok = RemoveDirectory(file);
		if (!ok)
			{
			if (fflag)
				{
				DWORD	attr;

				attr = GetFileAttributes(file);
				if (attr != INVALID_FILE_ATTRIBUTES)
					{
					attr &= ~(FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_READONLY | FILE_ATTRIBUTE_SYSTEM);
					SetFileAttributes(file, attr);

					ok = RemoveDirectory(file);
					}
				}
			}

		if (!ok)
			{
			if (!qflag) fprintf(stderr, "**** Can't delete %s ****\n", file);
			derrors++;
			}
		else dcount++;
		}


int
main(int argc, char **argv)
		{
		int		i;

		for (i=1;i<argc;i++)
			{
			if (argv[i][0] == '-')
				{
				char	*sp = argv[i]+1;

				while (*sp)
					{
					switch (*sp)
						{
						case 'q':	qflag = 1;	break;
						case 's':	sflag = 1;	break;
						case 'i':	iflag = 1;	break;
						case 'r':	rflag = 1;	break;
						case 'f':	fflag = 1;	break;
						case 'v':	vflag = 1;	break;
						default:	usage();	break;
						}
					sp++;
					}
				}
			else
				{
				DWORD	attr;

				attr = GetFileAttributes(argv[i]);
				if (attr != INVALID_FILE_ATTRIBUTES)
					{
					if ((attr & FILE_ATTRIBUTE_DIRECTORY) != 0) removedir(argv[i]);
					else removefile(argv[i]);
					}
				else if (!fflag)
					{
					// Do not need this error with -f
					if (!qflag) fprintf(stderr, "%s is not a file or directory.\n", argv[i]);
					errcount++;
					}
				}
			}

		if (sflag)
			{
			printf("Removed %d file%s and %d director%s\n", fcount, fcount==1?"":"s", dcount, dcount==1?"y":"ies");
			if (derrors) printf("Failed to remove %d director%s\n", derrors, derrors==1?"y":"ies");
			if (ferrors) printf("Failed to remove %d file%s\n", ferrors, ferrors==1?"":"s");
			//if (errcount) printf("%d non-existent file%s\n", errcount, errcount==1?"":"s");
			}

		return (ferrors + derrors + errcount);
		}
