PROJROOT	= ..

include ../win32.mak.tools
include Makefile.defs

PROG		= $(TOOLSBIN)\$(PROGNAME).exe
TARGETS		= $(PROG)

all: $(TARGETS)

!if [$(TOOLSBIN)\mkobjnames $(OBJDIR) $(OBJDIR)\localobjnames.inc OBJS $(SRCS)]
!endif

!include $(OBJDIR)\localobjnames.inc

clean:
	-$(RM) -rf $(OBJDIR) $(TARGETS)


$(PROG): $(OBJS) ..\common\$(OBJDIR)\common.lib
	@if not exist $(TOOLSBIN) mkdir $(TOOLSBIN)
	cl -nologo -Fe$@ $(OBJS) setargv.obj -link -libpath:..\common\$(OBJDIR) common.lib
