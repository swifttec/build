PROJROOT	= ..

include ..\win32.mak.tools
include Makefile.defs

PROG	= $(TOOLSBIN)\$(PROGNAME).exe
CFLAGS	= $(CFLAGS) -D_CRT_SECURE_NO_DEPRECATE

all: $(PROG)

!if [$(TOOLSBIN)\mkobjnames $(OBJDIR) $(OBJDIR)\localobjnames.inc LOCAL_OBJS $(SRCS)]
!endif
!include $(OBJDIR)\localobjnames.inc

OBJS=$(LOCAL_OBJS) $(BASE_OBJS) $(IO_OBJS) $(CMD_OBJS)

clean:
	-rd /s /q $(OBJDIR)
	-del /q $(TARGETS)


$(PROG): $(OBJS)
	@if not exist $(TOOLSBIN) mkdir $(TOOLSBIN)
	cl -nologo -Fe$(PROG) $(OBJS) setargv.obj
