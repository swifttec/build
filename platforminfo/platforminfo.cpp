/*
**	platforminfo.cpp
**
**	A simple utility used to get platform information from the system.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined(WIN32) || defined(_WIN32)
	#include <windows.h>

	#pragma warning(disable: 4996)
#else
	#include <unistd.h>
	#include <sys/utsname.h>

	#if !defined(linux) && !defined(_AIX) && !defined(__MACH__)
		#include <sys/systeminfo.h>
	#endif

	#if defined(__osf__)
		#include <syscall.h>
		
		extern "C" int gethostid(); // can't find it in the headers
	#endif
#endif


// Internal string class
// We can't use common library since we need to be purely standalone
// as in time we may be needed to define PLATFORM for the build.
class XString
		{
public:
		XString();
		XString(const char *s);
		~XString();

		XString &	operator=(const XString &s);
		XString &	operator=(const char *s);
		XString &	operator+=(const char *s);
		operator const char *() const	{ return m_str; }
		int		length() const			{ return (int)strlen(m_str); }
		bool		startsWith(const char *s) const;
		void		remove(int pos, int nchars);
		bool		operator==(const char *s)	{ return (strcmp(m_str, s) == 0); }

private:
		char	*m_str;
		int		m_capacity;
		};


XString::XString() :
	m_str(0),
	m_capacity(0)
		{
		*this = "";
		}

XString::XString(const char *s) :
	m_str(0),
	m_capacity(0)
		{
		*this = s;
		}


XString::~XString()
		{
		delete m_str;
		}


XString &
XString::operator=(const XString &s)
		{
		*this = s.m_str;
		return *this;
		}


XString &
XString::operator=(const char *s)
		{
		int	len;

		len = (int)strlen(s);

		if ((len + 1) > m_capacity)
			{
			m_capacity = len + 1;
			char	*pNewStr = new char[m_capacity];

			delete m_str;
			m_str = pNewStr;
			}

		strcpy(m_str, s);

		//printf("capacity=%d  length=%d\n", m_capacity, length());

		return *this;
		}


XString &
XString::operator+=(const char *s)
		{
		int	len=length();

		len += (int)strlen(s);

		//printf("len=%d (%d + %d), capacity=%d\n", len, length(), strlen(s), m_capacity);

		if ((len + 1) >= m_capacity)
			{
			m_capacity = len + 1;
			char	*pNewStr = new char[m_capacity];
			strcpy(pNewStr, m_str);
			delete m_str;
			m_str = pNewStr;
			}

		strcat(m_str, s);

		//printf("capacity=%d  length=%d\n", m_capacity, length());

		return *this;
		}


bool
XString::startsWith(const char *s) const
		{
		int	l=(int)strlen(s);
		bool res = false;

		if (l <= length())
			{
			if (strncmp(s, m_str, l) == 0) res = true;
			}

		return res;
		}


void
XString::remove(int pos, int nchars)
		{
		int	len=length();

		if (pos <= len)
			{
			char	*tp=&m_str[pos];
			char	*fp=&m_str[pos+nchars];

			while ((pos + nchars) < len)
				{
				*tp++ = *fp++;
				pos++;
				}

			*tp = 0;
			}
		}



#define OS_TYPE_UNKNOWN		0x00000000	// an unknown O/S

#define OS_TYPE_UNIX		0x80000000	// an generic unix variant
#define OS_TYPE_SUNOS		0x80000000	// Sun pre solaris 2
#define OS_TYPE_SOLARIS		0x80000000	// Sun solaris 2 onwards (SunOS 5.0 onwards)
#define OS_TYPE_LINUX		0x80000000	// Linux

#define OS_TYPE_OTHER		0x20000000

#define OS_TYPE_WINDOWS		0x40000000	// an generic windows variant
#define OS_TYPE_WIN31		0x40000001	// Windows 3.1 running Win32s
#define OS_TYPE_WIN95		0x40000002	// Windows 95
#define OS_TYPE_WIN98		0x40000004	// Windows 98
#define OS_TYPE_WINME		0x40000008	// Windows Me
#define OS_TYPE_WINNT351	0x40000010	// Windows NT 3.51
#define OS_TYPE_WINNT4		0x40000020	// Windows NT 4
#define OS_TYPE_WIN2K		0x40000040	// Windows 2000
#define OS_TYPE_WINXP		0x40000080	// Windows XP
#define OS_TYPE_WINDOTNET	0x40000100	// Windows .Net Server
#define OS_TYPE_WIN2003		0x40000200	// Windows 2000


#define	OS_FLAGS_UNIX			0x01
#define	OS_FLAGS_WINDOWS		0x02
#define	OS_FLAGS_WORKSTATION	0x04
#define	OS_FLAGS_SERVER			0x08
#define	OS_FLAGS_DC				0x10


XString		os_name;		// The O/S name string
XString		os_desc;		// The O/S description
int			os_type;		// The platform
int			os_majver;	// The major version number
int			os_minver;	// The minor version number
int			os_buildno;	// The build number
XString		os_nodename;	// The name of the machine
XString		os_version;	// The O/S version string
XString		os_release;	// The O/S release string
XString		os_machine;	// The machine type
XString		os_arch;		// The cpu architecture
XString		os_isalist;	// The instruction sets
XString		os_platform;	// the hardware platform
XString		os_hardware;	// The hardware description
XString		os_provider;	// The hardware provider
XString		os_serial;	// The hardware serial number
int			os_flags;		// Various flags
int			os_hostid;	// The host id

void
getosinfo()
		{
		char	tmp[256];

#if defined(WIN32) || defined(_WIN32)

		os_type = OS_TYPE_WINDOWS;
		os_name = "Windows";
		os_flags |= OS_FLAGS_WINDOWS;

		OSVERSIONINFOEX	ver;
		boolean		ok=false;
		boolean		extended=false;

		ver.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
		if (GetVersionEx((OSVERSIONINFO *)&ver))
			{
			ok = true;
			extended = true;
			}
		else
			{
			// Failed - try getting non-extended info
			ver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
			if (GetVersionEx((OSVERSIONINFO *)&ver))
				{
				ok = true;
				extended = false;
				}
			}

		if (ok)
			{
			os_majver = ver.dwMajorVersion;
			os_minver = ver.dwMinorVersion;
			os_buildno = ver.dwBuildNumber;

			switch (ver.dwPlatformId)
				{
				case VER_PLATFORM_WIN32s:
					// Win 3.1
					os_flags |= OS_FLAGS_WORKSTATION;
					os_type = OS_TYPE_WIN31;
					os_desc = "Windows 3.1";
					break;

				case VER_PLATFORM_WIN32_WINDOWS:
					// Win95/98/Me
					os_flags |= OS_FLAGS_WORKSTATION;
					switch (os_minver)
						{
						case 0:
							os_type = OS_TYPE_WIN95;
							os_desc = "Windows 95";
							if (ver.szCSDVersion[1] == 'C' || ver.szCSDVersion[1] == 'B') os_desc += " OSR2";
							break;

						case 10:
							os_type = OS_TYPE_WIN98;
							os_desc = "Windows 98";
							if (ver.szCSDVersion[1] == 'A') os_desc += " SE";
							break;

						case 90:
							os_type = OS_TYPE_WINME;
							os_desc = "Windows Me";
							break;
						}
					break;

				case VER_PLATFORM_WIN32_NT:
					switch (os_majver)
						{
						case 3:
							os_type = OS_TYPE_WINNT351;
							sprintf(tmp, "Windows NT %d.%d", os_majver, os_minver);
							os_desc = tmp;
							break;

						case 4:
							os_type = OS_TYPE_WINNT4;
							sprintf(tmp, "Windows NT %d.%d", os_majver, os_minver);
							os_desc = tmp;
							if (!extended && ver.szCSDVersion)
								{
								os_desc += " ";
								os_desc += ver.szCSDVersion;
								}
							break;

						case 5:
							switch (os_minver)
								{
								case 0:
									os_type = OS_TYPE_WIN2K;
									os_desc = "Windows 2000";
									break;

								case 1:
									os_type = OS_TYPE_WINXP;
									os_desc = "Windows XP";
									break;

								case 2:
									os_type = OS_TYPE_WIN2003;
									os_desc = "Windows 2003";
									break;
								}
							break;
						}

					if (extended)
						{
						switch (ver.wProductType)
							{
							case VER_NT_WORKSTATION:
								os_flags |= OS_FLAGS_WORKSTATION;
#ifdef NOT_YET
#endif // NOT_YET

								if ((ver.wSuiteMask & VER_SUITE_PERSONAL) != 0 && os_type == OS_TYPE_WINXP) os_desc += " Home";
								else os_desc += " Professional";
								break;

							case VER_NT_DOMAIN_CONTROLLER:
								os_flags |= OS_FLAGS_DC;
								// fall into server

							case VER_NT_SERVER:
								os_flags |= OS_FLAGS_SERVER;

								if (os_type == OS_TYPE_WINXP)
									{
									os_type = OS_TYPE_WINDOTNET;
									os_desc = "Windows .NET";
									if ((ver.wSuiteMask & VER_SUITE_ENTERPRISE) != 0) os_desc += " Enterprise";
									}
								else if (os_type == OS_TYPE_WIN2K)
									{
									os_desc = "Windows 2000";
									if ((ver.wSuiteMask & VER_SUITE_DATACENTER) != 0) os_desc += " DataCenter";
									else if ((ver.wSuiteMask & VER_SUITE_ENTERPRISE) != 0) os_desc += " Advanced";
									else if ((ver.wSuiteMask & VER_SUITE_SMALLBUSINESS) != 0) os_desc += " Small Business";
									else if ((ver.wSuiteMask & VER_SUITE_ENTERPRISE) != 0) os_desc += " Enterprise";
									}
								else if (os_type == OS_TYPE_WIN2003)
									{
									os_desc = "Windows 2003";
									if ((ver.wSuiteMask & VER_SUITE_ENTERPRISE) != 0) os_desc += " Enterprise";
									}

								os_desc += " Server";
								break;
							}

						if (ver.wServicePackMajor != 0)
							{
							sprintf(tmp, " SP%d", ver.wServicePackMajor);
							os_desc += tmp;

							if (ver.wServicePackMinor != 0)
								{
								sprintf(tmp, " .%d", ver.wServicePackMinor);
								os_desc += tmp;
								}
							}
						}
					else
						{
						/*
						XString	tmp;

						tmp = ca_registry_getString(HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Control\\ProductOptions", "ProductType");

						if (tmp.CompareNoCase("WINNT") == 0)
							{
							os_flags |= OS_FLAGS_WORKSTATION;
							os_desc += " Professional";
							}
						else if (tmp.CompareNoCase("LANMANNT") == 0)
							{
							os_flags |= OS_FLAGS_SERVER;
							os_desc += " Server";
							}
						else if (tmp.CompareNoCase("SERVERNT") == 0)
							{
							os_flags |= OS_FLAGS_SERVER;
							os_desc += " Advanced Server";
							}
						*/
						}
					break;
				}

			os_version = os_desc;
			if (os_version.startsWith("Windows ")) os_version.remove(0, 8);

			sprintf(tmp, "%d.%d", os_majver, os_minver);
			os_release = tmp;
			}

#if defined(_M_ALPHA)
		os_arch = "alpha";
#elif defined(_M_IX86)
		os_arch = "x86";
#elif defined(_M_IA64)
		os_arch = "ia64";
#elif defined(_M_MPPC)
		os_arch = "PowerMac";
#elif defined(_M_MRX000)
		os_arch = "MIPS";
#elif defined(_M_PPC)
		os_arch = "PowerPC";
#endif

		os_machine = os_arch;
		ULONG	n = 256;
		GetComputerName(tmp, &n);
		os_nodename = tmp;

		SYSTEM_INFO	si;

		GetSystemInfo(&si);
		switch (si.wProcessorArchitecture)
			{
			case PROCESSOR_ARCHITECTURE_UNKNOWN:
				os_arch = "unknown";
				break;

			case PROCESSOR_ARCHITECTURE_INTEL:
				os_arch = "x86";

				switch (si.wProcessorLevel)
					{
					case 3: os_machine = "i386";		break;
					case 4: os_machine = "i486";		break;
					case 5: os_machine = "i586";		break;
					case 6: os_machine = "i586pro";		break; // Pentium pro
					default: os_machine = "i686";		break;
					}

				break;

			case PROCESSOR_ARCHITECTURE_MIPS:
				os_arch = "mips";
				switch (si.wProcessorLevel)
					{
					case 4: 	os_machine = "MIPS_R4000";	break;
					default:	os_machine = "MIPS";		break;
					}
				break;

			case PROCESSOR_ARCHITECTURE_ALPHA:
				os_arch = "alpha";
				switch (si.wProcessorLevel)
					{
					case 21064: os_machine = "Alpha_21064";	break;
					case 21066: os_machine = "Alpha_21066";	break;
					case 21164: os_machine = "Alpha_21164";	break;
					default:	os_machine = "Alpha";		break;
					}
				break;

			case PROCESSOR_ARCHITECTURE_PPC:
				os_arch = "ppc";
				switch (si.wProcessorLevel)
					{
					case 1: os_machine = "PPC_601";	break;
					case 3: os_machine = "PPC_603";	break;
					case 4: os_machine = "PPC_604";	break;
					case 6: os_machine = "PPC_603+";	break;
					case 9: os_machine = "PPC_604+";	break;
					case 20: os_machine = "PPC_620";	break;
					default: os_machine = "PPC";	break;
					}
				break;

			case PROCESSOR_ARCHITECTURE_IA64:
				os_arch = "ia64";
				os_machine = "IA64";
				break;

			case PROCESSOR_ARCHITECTURE_IA32_ON_WIN64:
				os_arch = "ia32_on_ia64";
				os_machine = "IA64";
				break;

			case PROCESSOR_ARCHITECTURE_AMD64:
				os_arch = "x64";
				os_machine = "AMD64";
				break;
			}

		if (os_machine.length() == 0) os_machine = os_arch;
		if (os_isalist.length() == 0) os_isalist = os_machine;

#else
		struct utsname	u;

		os_type = OS_TYPE_UNIX;
		os_name = "Unix";
		os_flags |= OS_FLAGS_SERVER | OS_FLAGS_WORKSTATION | OS_FLAGS_UNIX;

		if (uname(&u) >= 0)
			{
			os_name = u.sysname;
			os_nodename = u.nodename;
			os_release = u.release;
			os_version = u.version;
			os_hardware = u.machine;
			}

#if !defined(linux) && !defined(_AIX) && !defined(__MACH__)
		char	buf[1024];

		if (sysinfo(SI_SYSNAME, buf, sizeof(buf)) > 0) os_name = buf;
		if (sysinfo(SI_HOSTNAME, buf, sizeof(buf)) > 0) os_nodename = buf;
		if (sysinfo(SI_RELEASE, buf, sizeof(buf)) > 0) os_release = buf;
		if (sysinfo(SI_VERSION, buf, sizeof(buf)) > 0) os_version = buf;
		if (sysinfo(SI_MACHINE, buf, sizeof(buf)) > 0) os_machine = buf;
		if (sysinfo(SI_ARCHITECTURE, buf, sizeof(buf)) > 0) os_arch = buf;
	#if !defined(__osf__)
		if (sysinfo(SI_ISALIST, buf, sizeof(buf)) > 0) os_isalist = buf;
		if (sysinfo(SI_PLATFORM, buf, sizeof(buf)) > 0) os_platform = buf;
	#endif
		if (sysinfo(SI_HW_PROVIDER, buf, sizeof(buf)) > 0) os_provider = buf;
		if (sysinfo(SI_HW_SERIAL, buf, sizeof(buf)) > 0) os_serial = buf;
#else
	#ifdef i386
		os_arch = "x86";
	#elif __MACH__
		os_arch = "x64";
	#elif __alpha
		os_arch = "alpha";
	#endif
#endif
		os_hostid = gethostid();

		int	m, n;

		if (sscanf((const char *)os_release, "%d.%d", &m, &n) == 2)
			{
			os_majver = m;
			os_minver = n;
			}

		if (os_name == "SunOS")
			{
			if (os_majver < 5)
				{
				os_type = OS_TYPE_SUNOS;
				os_desc = os_name;
				os_desc += " ";
				os_desc += os_release;
				}
			else
				{
				os_type = OS_TYPE_SOLARIS;

				if (os_minver < 7) sprintf(tmp, "Solaris %s", (const char *)os_release);
				else sprintf(tmp, "Solaris %d", os_minver);

				os_desc = tmp;
				}
			}

#endif

		}









struct CMDTAB
	{
	const char	*cmd;
	const char	*desc;
	bool		show;
	};

struct CMDTAB	cmdtab[] =
{
	{ "pointer",	"sizeof(void *)",		false },
	{ "char",		"sizeof(char)",			false },
	{ "wchar_t",	"sizeof(wchar_t)",		false },
	{ "short",		"sizeof(short)",		false },
	{ "int",		"sizeof(int)",			false },
	{ "long",		"sizeof(long)",			false },
	{ "longlong",	"sizeof(long long)",	false },
	{ "float",		"sizeof(float)",		false },
	{ "double",		"sizeof(double)",		false },

	{ "byteorder",	"Integer Byte Order",	false },

	{ "osname",		"O/S Name",				false },
	{ "osver",		"O/S Version",			false },
	{ "osrel",		"O/S Release",			false },
	{ "cpu",		"CPU Type",				false },
	{ "arch",		"CPU Architecture",		false },
	{ "nodename",	"Machine name",			false },

	// End of list marker
	{ 0,			0,						false }
};

int
main(int argc, char **argv)
		{
		bool		showall=false;
		int			i, res=0;
		struct CMDTAB	*cp;

		if (argc == 1) showall = true;

		for (i=1;i<argc;i++)
			{
			bool	found=false;

			for (cp=cmdtab;cp->cmd;cp++)
				{
				if (strcmp(argv[i], cp->cmd) == 0)
					{
					cp->show = true;
					found = true;
					}
				}

			if (!found)
				{
				fprintf(stderr, "Unknown field '%s' requested\n", argv[i]);
				res = 1;
				break;
				}
			}

		// Gather the info.
		getosinfo();

		if (res == 0)
			{
			bool		gotinfo=false;

			for (cp=cmdtab;cp->cmd;cp++)
				{
				if (showall) printf("%-20s = ", cp->desc);
				else if (gotinfo) putchar(' ');

				if (strcmp(cp->cmd, "pointer") == 0 && (showall || cp->show))
					{
					printf("%d", (int)sizeof(void *));
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "char") == 0 && (showall || cp->show))
					{
					printf("%d", (int)sizeof(char));
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "wchar_t") == 0 && (showall || cp->show))
					{
					printf("%d", (int)sizeof(wchar_t));
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "short") == 0 && (showall || cp->show))
					{
					printf("%d", (int)sizeof(short));
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "int") == 0 && (showall || cp->show))
					{
					printf("%d", (int)sizeof(int));
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "long") == 0 && (showall || cp->show))
					{
					printf("%d", (int)sizeof(long));
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "longlong") == 0 && (showall || cp->show))
					{
#if defined(_MSC_VER) && _MSC_VER < 1300
					printf("Not supported - use __int64 instead with %d bytes", (int)sizeof(__int64));
#else
					printf("%d", (int)sizeof(long long));
#endif
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "float") == 0 && (showall || cp->show))
					{
					printf("%d", (int)sizeof(float));
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "double") == 0 && (showall || cp->show))
					{
					printf("%d", (int)sizeof(double));
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "byteorder") == 0 && (showall || cp->show))
					{
					union
						{
#if defined(_MSC_VER) && _MSC_VER < 1300
						unsigned __int64	ull;
#else
						unsigned long long	ull;
#endif
						unsigned long		ul[2];
						unsigned short		us[4];
						unsigned char		bytes[8];
						} u;

#if defined(_MSC_VER) && _MSC_VER < 1300
					u.ull = 0x0807060504030201UL;
#else
					u.ull = 0x0807060504030201ULL;
#endif

					for (i=0;i<8;i++) printf("%x ", u.bytes[i]);
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "osname") == 0 && (showall || cp->show))
					{
					printf("%s", (const char *)os_name);
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "nodename") == 0 && (showall || cp->show))
					{
					printf("%s", (const char *)os_nodename);
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "osrel") == 0 && (showall || cp->show))
					{
					printf("%s", (const char *)os_release);
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "osver") == 0 && (showall || cp->show))
					{
					printf("%s", (const char *)os_version);
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "cpu") == 0 && (showall || cp->show))
					{
					printf("%s", (const char *)os_machine);
					gotinfo = true;
					}
				else if (strcmp(cp->cmd, "arch") == 0 && (showall || cp->show))
					{
					printf("%s", (const char *)os_arch);
					gotinfo = true;
					}

				if (showall) putchar('\n');
				}

			if (!showall && gotinfo) putchar('\n');
			}

		return res;
		}
