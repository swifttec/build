#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <io.h>

#pragma warning(disable: 4996)


char *extlist[] =
	{
	".exe",
	".com",
	".bat",
	".cmd",
	0
	};


int
checkForFileInPath(char *file, char *path, char *filename)
		{
		int	j, found=0;
		char	*dp;

		dp = strrchr(file, '.');
		for (j=0;extlist[j];j++)
			{
			if (dp == NULL || stricmp(dp, extlist[j]) == 0)
				{
				sprintf(filename, "%s\\%s%s", path, file, dp?"":extlist[j]);
				if (_access(filename, 0) == 0)
					{
					found = 1;
					break;
					}
				}
			}

		return found;
		}


int
main(int argc, char **argv)
		{
		int	i, found;
		char	*ep, *p, path[1024], filename[1024];

		for (i=1;i<argc;i++)
			{
			found = checkForFileInPath(argv[i], ".", filename);
			if (!found)
				{
				ep = getenv("PATH");

				while (ep && !found) 
					{
					p = strchr(ep, ';');

					if (p)
						{
						strncpy(path, ep, (p-ep));
						path[(p-ep)] = 0;
						}
					else
						{
						strcpy(path, ep);
						}

					found = checkForFileInPath(argv[i], path, filename);

					if (found || p == NULL) break;
					ep = p+1;
					}
				}

			if (!found)
				{
				printf("%s: Command not found\n", argv[i]);
				}
			else
				{
				printf("%s\n", filename);
				}
			}
		}
