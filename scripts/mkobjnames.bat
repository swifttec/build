@echo off
rem *********************************************************************************
rem Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
rem 
rem Permission is hereby granted, free of charge, to any person obtaining a copy
rem of this software and associated documentation files (the "Software"), to deal
rem in the Software without restriction, including without limitation the rights
rem to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
rem copies of the Software, and to permit persons to whom the Software is
rem furnished to do so, subject to the following conditions:
rem 
rem The above copyright notice and this permission notice shall be included in all
rem copies or substantial portions of the Software.
rem 
rem *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
rem IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
rem FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
rem AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
rem LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
rem OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
rem SOFTWARE.
rem *********************************************************************************


setlocal

set objdir=%1
shift

set outfile=%1
shift

set varname=%1
shift

if not defined objdir goto :usage
if not defined outfile goto :usage
if not defined varname goto :usage

set objlist=
if exist %outfile% del /f /q %outfile%

if not exist %objdir% mkdir %objdir%

:next
if "%1" == "" goto :end

set src=%1
shift

for %%f in (%src%) do set file=%%~nxf
set file=%file:.cpp=.obj%
set file=%file:.c=.obj%

set objlist=%objlist% %objdir%\%file%
echo %objdir%\%file%: %src% >> %outfile%
goto :next


:usage
echo Usage: mkobjnames objdir outfile varname src1 src2 ....
exit /b 1

:end
echo %varname%=%objlist% >> %outfile%
exit /b 0
