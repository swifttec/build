#!perl
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************


use strict;
use POSIX qw(strftime);

sub main
		{
		my	$res=1;
		my	@lines;
		my	$linecount=0;

		if (!defined($ARGV[0]))
			{
			print STDERR "usage: updateversionhistory filename\n";
			exit 1;
			}
		elsif (open(IN, $ARGV[0]))
			{
			my $line;

			while ($line = <IN>)
				{
				while ($line =~ /^(.*)[\r\n]+$/)
					{
					$line = $1;
					}

				$lines[$linecount++] = $line;
				}

			close(IN);
			$res = 0;
			}

		if ($linecount == 0)
			{
			# This is a new file
			$lines[$linecount++] = "Release Date = " . strftime "%d/%m/%Y", localtime;
			$lines[$linecount++] = "";
			}
		else
			{
			if (!($lines[0] =~ /^\s*Release.*Date.*=/))
				{
				$linecount += 2;

				my $pos=$linecount;

				while ($pos-- > 0)
					{
					$lines[$pos] = $lines[$pos-2];
					}
				$lines[1] = "";
				}

			$lines[0] = "Release Date = " . strftime "%d/%m/%Y", localtime;
			}

		open(OUT, ">$ARGV[0]");
		for (my $i=0;$i<$linecount;$i++)
			{
			print OUT "$lines[$i]\n";
			}
		close(OUT);

		return 0;
		}


# Last line
exit main();
