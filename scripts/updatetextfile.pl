#!perl
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************


use strict;

my @lines1;
my @lines2;
my $count1=0;
my $count2=0;
my $file1=@ARGV[0];
my $file2=@ARGV[1];
my $write=0;

$file1 =~ s/\\/\//g;
$file2 =~ s/\\/\//g;

if (-f $file1)
	{
	# print "Reading $file1\n";

	open(IN, $file1);
	while (<IN>)
		{
		$lines1[$count1++] = $_;
		}
	close(IN);
	}

if (-f $file2)
	{
	# print "Reading $file2\n";

	open(IN, $file2);
	while (<IN>)
		{
		$lines2[$count2++] = $_;
		}
	close(IN);

	if ($count1 == $count2)
		{
		for (my $i=0;$i<$count1;$i++)
			{
			if ($lines1[$i] ne $lines2[$i])
				{
				$write = 1;
				last;
				}
			}
		}
	else
		{
		$write = 1;
		}
	}
else
	{
	$write = 1;
	}

if ($write)
	{
	print "Updating $file2\n";

	open(OUT, ">$file2");
	for (my $i=0;$i<$count1;$i++)
		{
		print OUT $lines1[$i];
		}
	close(OUT);
	}
