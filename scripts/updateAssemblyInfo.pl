#!perl
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************


use strict;

my %info;

sub updateAssemblyInfo
	{
	my ($filename) = @_;

	if (open(IN, $filename))
		{
		my @lines;
		my $linecount=0;
		my $pver=$info{"PRODUCT_MAJOR_VERSION"}.".".$info{"PRODUCT_MINOR_VERSION"}.".".$info{"PRODUCT_REVISION"}.".".$info{"PRODUCT_BUILD"};
		my $bver=$info{"BUILD_MAJOR_VERSION"}.".".$info{"BUILD_MINOR_VERSION"}.".".$info{"BUILD_REVISION"}.".".$info{"BUILD_BUILD"};
		my $changed=0;

		while (<IN>)
			{
			while (/[\r\n]{1}$/)
				{
				chop;
				}

			my $oldline=$_;

			if (/^(.*AssemblyVersion\(").*("\).*)$/)
				{
				$_ = "$1$pver$2" if ($pver ne "...");
				}
			elsif (/^(.*AssemblyInformationalVersion\(").*("\).*)$/)
				{
				$_ = "$1$pver$2" if ($pver ne "...");
				}
			elsif (/^(.*AssemblyFileVersion\(").*("\).*)$/)
				{
				if ($bver ne "...")
					{
					$_ = "$1$bver$2";
					}
				else
					{
					$_ = "$1$pver$2" if ($pver ne "...");
					}
				}
			elsif (/^(.*AssemblyTitle\(").*("\).*)$/)
				{
				my $v=$info{"BUILD_TITLE"};

				$_ = "$1$v$2" if ($v ne "");
				}
			elsif (/^(.*AssemblyDescription\(").*("\).*)$/)
				{
				my $v=$info{"BUILD_DESCRIPTION"};

				$_ = "$1$v$2" if ($v ne "");
				}
			elsif (/^(.*AssemblyCompany\(").*("\).*)$/)
				{
				my $v=$info{"PRODUCT_COMPANY"};

				$_ = "$1$v$2" if ($v ne "");
				}
			elsif (/^(.*AssemblyProduct\(").*("\).*)$/)
				{
				my $v=$info{"PRODUCT_NAME"};

				$_ = "$1$v$2" if ($v ne "");
				}
			elsif (/^(.*AssemblyCopyright\(").*("\).*)$/)
				{
				my $v=$info{"PRODUCT_COPYRIGHT"};

				$_ = "$1$v$2" if ($v ne "");
				}
			elsif (/^(.*AssemblyTrademark\(").*("\).*)$/)
				{
				my $v=$info{"PRODUCT_TRADEMARK"};

				$_ = "$1$v$2" if ($v ne "");
				}

			$lines[$linecount++] = $_;
			$changed++ if ($_ ne $oldline);
			}

		close(IN);

		if ($changed)
			{
			print "Updating $filename\n";

			open(OUT, ">$filename");
			for (my $i=0;$i<$linecount;$i++)
				{
				print OUT "$lines[$i]\n";
				}
			close(OUT);
			}
		}
	}



sub showinfo
	{
	foreach my $p (sort keys %info)
		{
		print "$p = $info{$p}\n";
		}
	}

sub readinfo
	{
	my ($filename) = @_;

	if (open(IN, $filename))
		{
		while (<IN>)
			{
			chop;

			next if (/^#/);
			if (/^(\S+)=(.*)$/)
				{
				$info{$1} = $2;
				}
			}

		close(IN);
		}
	}



sub main
	{
	my $folder="";

	if (@ARGV[0] eq "")
		{
		print "No project folder specified\n";
		return 1;
		}

	$folder = @ARGV[0];

	if (! -d $folder)
		{
		print "$folder is not a valid folder\n";
		return 1;
		}

	readinfo("product.info");
	readinfo("$folder/build.info");

	if (-f "$folder/My Project/AssemblyInfo.vb")
		{
		updateAssemblyInfo("$folder/My Project/AssemblyInfo.vb");
		}

	if (-f "$folder/Properties/AssemblyInfo.cs")
		{
		updateAssemblyInfo("$folder/Properties/AssemblyInfo.cs");
		}

	return 0;
	}


# Last
exit main();
