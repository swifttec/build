#!perl
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************


use strict;

my @vlist;
my %product;

sub customsort
		{
		my $am=0;
		my $av=0;
		my $ar=0;
		my $bm=0;
		my $bv=0;
		my $br=0;

		if ($a =~ /^(\d+)\.(\d+)\.(\d+)\.txt/)
			{
			$am = $1;
			$av = $2;
			$ar = $3;
			}
		elsif ($a =~ /^(\d+)\.(\d+)\.txt/)
			{
			$am = $1;
			$av = $2;
			}
		elsif ($a =~ /^(\d+)\.txt/)
			{
			$am = $1;
			}

		if ($b =~ /^(\d+)\.(\d+)\.(\d+)\.txt/)
			{
			$bm = $1;
			$bv = $2;
			$br = $3;
			}
		elsif ($b =~ /^(\d+)\.(\d+)\.txt/)
			{
			$bm = $1;
			$bv = $2;
			}
		elsif ($b =~ /^(\d+)\.txt/)
			{
			$bm = $1;
			}

		$bm <=> $am || $bv <=> $av || $br <=> $ar;
		}


sub scanfiles
		{
		my ($vhdir)=@_;
		my @files;
		my $idx=0;

		opendir(DIR, $vhdir);
		foreach my $entry (readdir(DIR))
			{
			if ($entry =~ /^\d+.*\.txt$/)
				{
				$files[$idx++] = $entry;
				}
			}
		closedir(DIR);

		$idx = 0;
		foreach my $file (sort customsort @files)
			{
			if ($file =~ /^(.*)\.txt$/)
				{
				my $version=$1;

				$vlist[$idx++] = $version;
				}
			}
		}


sub getReleaseDate
		{
		my ($file)=@_;
		my $res="";

		open(IN, $file);
		while (<IN>)
			{
			chop;

			if (/^Release\s*Date\s*=\s*(.*)$/)
				{
				$res = $1;
				last;
				}
			}
		close(IN);

		return $res;
		}


sub generateText
		{
		my ($vhdir, $file)=@_;

		print "Generating $file\n";
		open(OUT, ">$file");
		print OUT qq($product{"TITLE"}
=============================================
$product{"DESCRIPTION"}

);

		foreach my $version (@vlist)
			{
			my $released="Unknown";
			my $releasename="";
			my $contents="";

			open(IN, "$vhdir/$version.txt");
			while (<IN>)
				{
				chop;

				if (/^Release\s*Date\s*=\s*(.*)$/)
					{
					$released = $1;
					}
				elsif (/^Release\s*Name\s*=\s*(.*)$/)
					{
					$releasename = " ($1)";
					}
				else
					{
					if ($_ ne "" || $contents ne "")
						{
						$contents .= "$_\n";
						}
					}
				}
			close(IN);

			my $header = "Release $version$releasename - released $released";

			print OUT "$header\n";
			my $c=length($header);
			for (my $i=0;$i<$c;$i++)
				{
				print OUT "-";
				}
			print OUT "\n$contents\n";
			}

		close(OUT);
		}


sub generateHTML
		{
		my ($vhdir, $file)=@_;
		my $fullhtml=1;

		print "Generating $file\n";
		open(OUT, ">$file");
		print OUT qq(<html>
<head>
<style type="text/css">
.vhTable
	{
	border-collapse: collapse;
	border: 1 navy solid;
	margin: 0 0 0 0;
	padding: 0 0 0 0;
	font-family: Arial;
	font-size: 10pt;
	}

.vhTableHeaderRow
	{
	background: #000080;
	color: #ffffff;
	font-weight: bold;
	}

.vhTableDataRow
	{
	}

.vhTableVersionHeaderCell
	{
	padding: 4 4 4 4;
	text-align: center;
	}

.vhTableDescriptionHeaderCell
	{
	padding: 4 4 4 4;
	text-align: left;
	}

.vhTableVersionDataCell
	{
	padding: 4 4 4 4;
	border: 1 navy solid;
	background: #F2F8FF;
	font-weight: bold;
	text-align: center;
	}

.vhTableDescriptionDataCell
	{
	padding: 4 4 4 4;
	border: 1 navy solid;
	text-align: left;
	}

html, body
	{
	font-family: Arial;
	font-size: 10pt;
	}

</style>
</head>
<body>
<h1>$product{"TITLE"}</h1>
<p>
$product{"DESCRIPTION"}
</p>
<h2>Version History</h2>
) if ($fullhtml);
		print OUT qq(<table class="vhTable">\n);
		print OUT qq(<tr class="vhTableHeaderRow">\n);
		print OUT qq(<td class="vhTableVersionHeaderCell">Version</td>\n);
		print OUT qq(<td class="vhTableVersionHeaderCell">Released</td>\n);
		print OUT qq(<td class="vhTableDescriptionHeaderCell">Description</td>\n);
		print OUT qq(</tr>\n);
		foreach my $version (@vlist)
			{
			my $released="Unknown";
			my $releasename="";
			my $contents="";

			open(IN, "$vhdir/$version.txt");
			while (<IN>)
				{
				chop;
				if (/^Release\s*Date\s*=\s*(.*)$/)
					{
					$released = $1;
					}
				elsif (/^Release\s*Name\s*=\s*(.*)$/)
					{
					$releasename = "<br><small><i>($1)</i></small>";
					}
				else
					{
					if ($_ ne "" || $contents ne "")
						{
						if (/^\*(.*)$/)
							{
							$contents .= qq(<li>$1</li>\n);
							}
						else
							{
							$contents .= qq($_<br>\n);
							}
						}
					}
				}
			close(IN);

			print OUT qq(<tr class="vhTableDataRow">\n);
			print OUT qq(<td class="vhTableVersionDataCell">$version$releasename</td>\n);
			print OUT qq(<td class="vhTableVersionDataCell">$released</td>\n);
			print OUT qq(<td class="vhTableDescriptionDataCell">\n$contents</td>\n);

			print OUT qq(</tr>\n);
			}
		print OUT qq(</table>\n);

		print OUT qq(
<br><br>
<small><i>$product{"COPYRIGHT"}</i></small>
</body>
</html>
) if ($fullhtml);
		close(OUT);
		}



sub main
		{
		my $vhdir="versionhistory";

		if (-d $vhdir)
			{
			if (-f "product.info")
				{
				open(IN, "product.info");
				while (<IN>)
					{
					chop;
					if (/^PRODUCT_(.*)=(.*)$/)
						{
						$product{$1} = $2;
						}
					}

				close(IN);
				}

			scanfiles($vhdir);
			if ($#vlist >= 0)
				{
				generateText($vhdir, "versionhistory.txt");
				generateHTML($vhdir, "versionhistory.html");
				}
			}

		return 0;
		}


# Last thing
exit main();
