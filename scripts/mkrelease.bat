@echo off
rem *********************************************************************************
rem Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
rem 
rem Permission is hereby granted, free of charge, to any person obtaining a copy
rem of this software and associated documentation files (the "Software"), to deal
rem in the Software without restriction, including without limitation the rights
rem to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
rem copies of the Software, and to permit persons to whom the Software is
rem furnished to do so, subject to the following conditions:
rem 
rem The above copyright notice and this permission notice shall be included in all
rem copies or substantial portions of the Software.
rem 
rem *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
rem IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
rem FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
rem AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
rem LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
rem OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
rem SOFTWARE.
rem *********************************************************************************


setlocal

if not defined MASTERBUILD_KEEPVER set MASTERBUILD_KEEPVER=0
if not defined MASTERBUILD_DOVERSIONHISTORY set MASTERBUILD_DOVERSIONHISTORY=1

if not "%SW_BUILD_RELEASE_X86%%SW_BUILD_RELEASE_X64%%SW_BUILD_DEBUG_X86%%SW_BUILD_DEBUG_X64%" == "" goto :after_buildtypes
set SW_BUILD_RELEASE_X86=Release x86
set SW_BUILD_RELEASE_X64=Release x64
set SW_BUILD_DEBUG_X86=
set SW_BUILD_DEBUG_X64=

:after_buildtypes
set AUTOBUILD=0
set MKRELEASE_PREINCBUILD=
set MKRELEASE_POSTINCBUILD=
set MKPACKAGE_EULA_FILE=..\common\eula.rtf
set MKPACKAGE_TARGETDIR=..\install
set INSTALLER_TYPE=
set INSTALLER_PREFIX=
set INSTALLER_SUFFIX=
set INSTALLER_REDIST=

for %%f in (%CD:\= %) do set folder=%%f
for %%a in (%*) do call :parse_arg %%a

if not defined MKRELEASE_DOBUILD set MKRELEASE_DOBUILD=1
if not defined MKRELEASE_PREINCBUILD set MKRELEASE_PREINCBUILD=0
if not defined MKRELEASE_POSTINCBUILD set MKRELEASE_POSTINCBUILD=1

set SOLUTION=%SW_BUILD_SOLUTION%

if defined SOLUTION goto :after_find_solution
set starttime=%time%

for %%f in (%CD:\= %) do set folder=%%f

set SOLUTION=%folder%.sln
:after_find_solution

rem The Visual Studio build environment
call ..\setbuildenv

rem Update the build number
if %MKRELEASE_PREINCBUILD% == 1 call ..\build\bin\getbuildinfo --file product.info --prefix PRODUCT --inc-build

if exist pre-mkrelease.bat call pre-mkrelease.bat
if exist ..\build\local\scripts\pre-mkrelease.bat call ..\build\local\scripts\pre-mkrelease.bat
if exist ..\build\local\scripts\pre-mkrelease-%COMPUTERNAME%.bat call ..\build\local\scripts\pre-mkrelease-%COMPUTERNAME%.bat

rem Get the build info from product.info
call :do_getproductinfo

rem Update any assemblies
for /f %%f in ('perl ..\build\scripts\getallfolders.pl') do call :do_update_assembly %%f

rem When autobuild is enabled skip the questions
set MASTERBUILD_CLEAN=0
if %AUTOBUILD% == 1 goto :step_build

:step_versionhistory
if %MASTERBUILD_DOVERSIONHISTORY% == 1 call ..\build\scripts\mkhistory

:step_clean
if %MASTERBUILD_CLEAN% == 1 call :do_clean

:step_build
if %AUTOBUILD% == 1 perl ..\build\scripts\updateversionhistoryfile.pl versionhistory\%PRODUCT_MAJOR_VERSION%.%PRODUCT_MINOR_VERSION%.%PRODUCT_REVISION%.txt

perl ..\build\scripts\buildversionhistory.pl
rem git commit product.info versionhistory\* -m "Auto-commit of info and version files for v%PRODUCT_VERSION%"

call :do_getsourceinfo
if errorlevel 1 exit /b 1

if %MKRELEASE_DOBUILD% == 1 call :do_build
if errorlevel 1 exit /b 1

call ..\build\scripts\mkpackage
if errorlevel 1 exit /b 1

if exist post-mkrelease.bat call post-mkrelease.bat
if exist ..\build\local\scripts\post-mkrelease.bat call ..\build\local\scripts\post-mkrelease.bat
if exist ..\build\local\scripts\post-mkrelease-%COMPUTERNAME%.bat call ..\build\local\scripts\post-mkrelease-%COMPUTERNAME%.bat

if %MKRELEASE_POSTINCBUILD% == 1 call ..\build\bin\getbuildinfo --file product.info --prefix PRODUCT --inc-build

if %AUTOBUILD% == 1 goto :step_exit
set /p ans=Do you want to finalise this release (closeversion)? [y/n] 
if /i "%ans%" == "y" call ..\build\scripts\closeversion noupdatehistory

:step_exit
exit /b 0





rem Called on autobuild
:auto_build
if %MASTERBUILD_KEEPVER% == 0 goto :step_build

call ..\build\bin\getbuildinfo --file product.info --prefix PRODUCT --inc-revision
call :do_getproductinfo
goto :step_clean



rem ###################################################################
rem Parse command line arg
rem ###################################################################
:parse_arg
set arg=%1

if /i "%arg%" == "auto" set AUTOBUILD=1
if /i "%arg%" == "sign" set SIGNAPPS=1
if /i "%arg%" == "nosign" set SIGNAPPS=0
if /i "%arg%" == "keepver" set MASTERBUILD_KEEPVER=1
if /i "%arg%" == "nopreincbuild" set MKRELEASE_PREINCBUILD=0
if /i "%arg%" == "preincbuild" set MKRELEASE_PREINCBUILD=1
if /i "%arg%" == "nopostincbuild" set MKRELEASE_POSTINCBUILD=0
if /i "%arg%" == "postincbuild" set MKRELEASE_POSTINCBUILD=1

if /i "%arg%" == "noincbuild" set MKRELEASE_POSTINCBUILD=0
if /i "%arg%" == "noincbuild" set MKRELEASE_PREINCBUILD=0

if /i "%arg%" == "full" set INSTALLER_TYPE=full
if /i "%arg%" == "full" set INSTALLER_PREFIX=Setup
if /i "%arg%" == "full" set INSTALLER_REDIST=0

if /i "%arg%" == "patch" set INSTALLER_TYPE=patch
if /i "%arg%" == "patch" set INSTALLER_PREFIX=Patch
if /i "%arg%" == "patch" set INSTALLER_REDIST=0

goto :eof






rem ################################################################################
rem # Do a single devenv command
rem ################################################################################
:do_devenv
set devaction=%1
set devconfig=%2
set devplatform=%3

echo.
echo %devaction% %devconfig%
set devstarttime=%time%

if defined VS100COMNTOOLS goto :use_devenv
echo msbuild /nologo %SOLUTION% /p:Configuration=%devconfig% /p:Platform=%devplatform% /t:%devaction% 
msbuild /nologo %SOLUTION% /p:Configuration=%devconfig% /p:Platform=%devplatform% /t:%devaction% 
goto :after_build

:use_devenv
echo devenv /nologo %SOLUTION% /%devaction% "%devconfig%|%devplatform%"
devenv /nologo %SOLUTION% /%devaction% "%devconfig%|%devplatform%"

:after_build
set devendtime=%time%

echo %devaction% %devconfig% Start time: %devstarttime%
echo %devaction% %devconfig% End time  : %devendtime%
if errorlevel 1 exit /b 1
goto :eof



rem ################################################################################
rem # Add to list function
rem ################################################################################
:addtolist
set list=%list% %1
goto :eof



rem ################################################################################
rem # Build
rem ################################################################################
:do_build

if defined SW_BUILD_RELEASE_X64 call :do_devenv build %SW_BUILD_RELEASE_X64%
if errorlevel 1 exit /b 1

if defined SW_BUILD_RELEASE_X86 call :do_devenv build %SW_BUILD_RELEASE_X86%
if errorlevel 1 exit /b 1

if defined SW_BUILD_DEBUG_X64 call :do_devenv build %SW_BUILD_DEBUG_X64%
if errorlevel 1 exit /b 1

if defined SW_BUILD_DEBUG_X86 call :do_devenv build %SW_BUILD_DEBUG_X86%
if errorlevel 1 exit /b 1

if not defined SIGNAPPS goto :after_signing
if not defined SIGNCERT goto :after_signing
if not defined SIGNPASS goto :after_signing
if %SIGNAPPS% == 0 goto :after_signing
if not exist "%SIGNCERT%" goto :after_signing

set signlist=

:do_sign_release
if not defined SOURCE_RELEASE goto :do_sign_release_x86

set list=
for %%a in (%SOURCE_RELEASE%\*.exe) do call :addtolist "%%a"
if defined list set signlist=%signlist% %SOURCE_RELEASE%\*.exe

set list=
for %%a in (%SOURCE_RELEASE%\*.dll) do call :addtolist "%%a"
if defined list set signlist=%signlist% %SOURCE_RELEASE%\*.dll


:do_sign_release_x86
if not defined SOURCE_RELEASE_X86 goto :do_sign_release_x64

set list=
for %%a in (%SOURCE_RELEASE_X86%\*.exe) do call :addtolist "%%a"
if defined list set signlist=%signlist% %SOURCE_RELEASE_X86%\*.exe

set list=
for %%a in (%SOURCE_RELEASE_X86%\*.dll) do call :addtolist "%%a"
if defined list set signlist=%signlist% %SOURCE_RELEASE_X86%\*.dll


:do_sign_release_x64
if not defined SOURCE_RELEASE_X64 goto :do_sign_release_final

set list=
for %%a in (%SOURCE_RELEASE_X64%\*.exe) do call :addtolist "%%a"
if defined list set signlist=%signlist% %SOURCE_RELEASE_X64%\*.exe

set list=
for %%a in (%SOURCE_RELEASE_X64%\*.dll) do call :addtolist "%%a"
if defined list set signlist=%signlist% %SOURCE_RELEASE_X64%\*.dll


:do_sign_release_final
if not defined signlist goto :after_signing
echo Signing apps (%PRODUCT_COMPANY%) - %signlist%

call ..\build\scripts\signtool sign /f "%SIGNCERT%" /p "%SIGNPASS%" /t %SIGNTIMESTAMPURL% %signlist%

:after_signing
goto :eof


rem ################################################################################
rem # getproductinfo
rem ################################################################################
:do_getproductinfo
call ..\build\bin\getbuildinfo --readonly --file product.info --prefix PRODUCT --out bat:buildinfo.bat
call buildinfo.bat
del /q buildinfo.bat
goto :eof


rem ################################################################################
rem # getsourceinfo
rem ################################################################################
:do_getsourceinfo
if not exist source.info goto :no_source_info

call ..\build\bin\getbuildinfo --readonly --file source.info --prefix SOURCE --out bat:sourceinfo.bat
call sourceinfo.bat
del /q sourceinfo.bat
goto :eof


rem ################################################################################
rem # no source info
rem ################################################################################
:no_source_info
echo source.info not found
exit /b 1


rem ################################################################################
rem # update assembly
rem ################################################################################
:do_update_assembly
if not exist %1 goto :eof
pushd %1
echo Updating assemblies in %1
for /d %%d in (*) do perl ..\build\scripts\updateAssemblyInfo.pl "%%d"
popd

goto :eof


