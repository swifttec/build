@echo off
rem *********************************************************************************
rem Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
rem 
rem Permission is hereby granted, free of charge, to any person obtaining a copy
rem of this software and associated documentation files (the "Software"), to deal
rem in the Software without restriction, including without limitation the rights
rem to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
rem copies of the Software, and to permit persons to whom the Software is
rem furnished to do so, subject to the following conditions:
rem 
rem The above copyright notice and this permission notice shall be included in all
rem copies or substantial portions of the Software.
rem 
rem *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
rem IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
rem FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
rem AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
rem LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
rem OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
rem SOFTWARE.
rem *********************************************************************************

rem Use this script to finalise a release and update versions etc

setlocal

set UPDATE_ACTION=
set UPDATE_HISTORY=1

rem work out the 
for %%f in (%CD:\= %) do set folder=%%f

rem parse the args
for %%a in (%*) do call :parse_arg %%a

rem The Visual Studio build environment
call ..\setbuildenv

rem Get the build info from product.info
call :do_getproductinfo

set VERSION_TAG=%PRODUCT_PACKAGE%_%PRODUCT_VERSION%

set allfolders=
for /f %%f in ('perl ..\build\scripts\getallfolders.pl') do call :do_add_allfolder %%f

echo This will finalise the release for %PRODUCT_PACKAGE% version %PRODUCT_VERSION%.
echo The following folders will be checked in and tagged with %VERSION_TAG%
echo.
echo %allfolders%
echo.
set /p ans=Do you want to continue? 
if /i not "%ans%" == "y" goto :eof

rem Run the pre-update script
if defined CLOSEVERSION_PRE_UPDATE_SCRIPT call %CLOSEVERSION_PRE_UPDATE_SCRIPT%

rem Finalise the history
if %UPDATE_HISTORY% == 1 call ..\build\scripts\mkhistory

rem Do the git commit for all folders
for %%f in (%allfolders%) do call :do_checkin %%f

if exist ..\build\local\scripts\closeversion-pre-updateversions.bat call ..\build\local\scripts\closeversion-pre-updateversions.bat
if exist closeversion-pre-updateversions.bat call closeversion-pre-updateversions.bat

rem Update the revision in the product file
if defined UPDATE_ACTION goto :do_updateversion

echo %PRODUCT_NAME% %PRODUCT_VERSION% build %PRODUCT_BUILD%
set /p ans=Update product version? [m=Major, v=Minor, r=Revision] 

if /i "%ans%" == "m" set UPDATE_ACTION=--inc-major
if /i "%ans%" == "v" set UPDATE_ACTION=--inc-minor
if /i "%ans%" == "r" set UPDATE_ACTION=--inc-revision


:do_updateversion
if not defined UPDATE_ACTION set UPDATE_ACTION=--inc-revision
..\build\bin\getbuildinfo --file product.info --prefix PRODUCT %UPDATE_ACTION% --inc-build
call :do_getproductinfo

rem git add product.info
rem git commit -m "auto-update product/version to %PRODUCT_VERSION%"
rem git push

if exist ..\build\local\scripts\closeversion-post-updateversions.bat call ..\build\local\scripts\closeversion-post-updateversions.bat
if exist closeversion-post-updateversions.bat call closeversion-post-updateversions.bat

goto :eof



rem ###################################################################
rem Parse command line arg
rem ###################################################################
:parse_arg
set arg=%1

if /i "%arg%" == "auto" set UPDATE_ACTION=--inc-revision
if /i "%arg%" == "major" set UPDATE_ACTION=--inc-major
if /i "%arg%" == "minor" set UPDATE_ACTION=--inc-minor
if /i "%arg%" == "revision" set UPDATE_ACTION=--inc-revision
if /i "%arg%" == "noupdatehistory" set UPDATE_HISTORY=0

goto :eof



rem ################################################################################
rem # getproductinfo
rem ################################################################################
:do_getproductinfo
call ..\build\bin\getbuildinfo --readonly --file product.info --prefix PRODUCT --out bat:buildinfo.bat
call buildinfo.bat
del /q buildinfo.bat
goto :eof




:do_add_allfolder
if "%1" == "" goto :eof
if "%1" == "#" goto :eof

set folder=%1
set folder=%folder:/=\%

if defined allfolders set allfolders=%allfolders% 
set allfolders=%allfolders% %folder%
goto :eof



:do_checkin
set folder=%1
set autostash=0

echo Auto-committting and tagging sources in %folder%
pushd %folder%

if "%folder%" == "." goto :after_set_autostash
if not exist product.info goto :after_set_autostash
for /f "usebackq" %%g in (`git status --porcelain product.info`) do set autostash=1
:after_set_autostash

rem echo autostash=%autostash%
rem pause

if %autostash% == 0 goto :do_auto_commit
rem echo git stash push -m autostash_productinfo product.info
git stash push -m autostash_productinfo product.info
rem pause

:do_auto_commit
git commit -a -m "Auto-commit for final release of %PRODUCT_NAME% v%PRODUCT_VERSION%"
git tag -a %VERSION_TAG% -m "Final release of %PRODUCT_NAME% v%PRODUCT_VERSION%"
git push --tags

if %autostash% == 0 goto :after_stash_pop
rem echo git stash pop -q
git stash pop -q
rem pause
:after_stash_pop

popd

goto :eof

