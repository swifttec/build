#!/usr/bin/perl
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************


use strict;

package main;

my @projlist;
my $projcount = 0;


sub dirname
	{
	my ($file) = @_;
	my $parts=0;
	my @plist;

	foreach my $p (split(/[\/\\]/, $file))
		{
		my $done=0;

		if ($p eq ".")
			{
			$done = 1;
			}
		elsif ($p eq "..")
			{
			if ($parts > 0)
				{
				my $l = $plist[$parts-1];

				if ($plist[$parts-1] ne "..")
					{
					$parts -= 1;
					$done = 1;
					}
				}
			}

		if (!$done)
			{
			$plist[$parts++] = $p;
			}
		}

	my $res="";

	for (my $i=0;$i<$parts-1;$i++)
		{
		$res .= "/" if ($i != 0);
		$res .= $plist[$i];
		}

	$res = "." if ($res eq "");

	return $res;
	}


sub fixpath
	{
	my ($file) = @_;
	my $parts=0;
	my @plist;

	foreach my $p (split(/[\/\\]/, $file))
		{
		my $done=0;

		if ($p eq ".")
			{
			$done = 1;
			}
		elsif ($p eq "..")
			{
			if ($parts > 0)
				{
				my $l = $plist[$parts-1];

				if ($plist[$parts-1] ne "..")
					{
					$parts -= 1;
					$done = 1;
					}
				}
			}

		if (!$done)
			{
			$plist[$parts++] = $p;
			}
		}

	my $res="";

	for (my $i=0;$i<$parts;$i++)
		{
		$res .= "/" if ($i != 0);
		$res .= $plist[$i];
		}

	return $res;
	}


sub findProject
	{
	my ($file) = @_;
	my $found = 0;
	my $pProject=0;

	for (my $i=0;$i<$projcount;$i++)
		{
		$pProject = $projlist[$i];

		if ($pProject->filename() eq $file)
			{
			$found = 1;
			last;
			}
		}

	if (!$found)
		{
		$pProject = new Project();
		$pProject->load($file);
		$projlist[$projcount++] = $pProject;
		}

	return $pProject;
	}


sub checkSolution
	{
	my ($solution) = @_;

	print "Checking $solution\n";

	open(IN, $solution);
	while (<IN>)
		{
		chop;

		if (/^Project\("{(.*)}"\)\s*=\s*"(.*)",\s*"(.*)",\s*"{(.*)}"/)
			{
			my $slnid = $1;
			my $projname = $2;
			my $projfile = $3;
			my $projid = lc $4;
			my $pProject=findProject("./".fixpath($projfile));


			if ($pProject->id() ne $projid)
				{
				print "$projname UUID mismatch - have $projid, expected ".$pProject->id()."\n";
				}
			}

		}
	close(IN);
	}


sub main
	{
	foreach my $file (<*.sln>)
		{
		checkSolution($file);
		}

	return 0;
	}

exit main();



#################################

package Project;

sub new
	{
	my $class = shift;
	my $this = { };
	bless $this, $class;
	return $this;
	}


sub load
	{
	my ($this, $file) = @_;
	my $reffile="";
	my $refid="";
	my $pcount=0;
	my @plist;
	my @ulist;
	my $dir=main::dirname($file);

	# print "Loading $file in $dir\n";

	$this->{m_filename} = $file;

	open(PROJ, $file);
	while (<PROJ>)
		{
		if (/<ProjectGuid>{(.*)}<\/ProjectGuid>/)
			{
			$this->{m_id} = lc $1;
			# print " ID = ".$this->id()."\n";
			}
		elsif (/<ProjectReference Include="(.*)">/)
			{
			my $path= $plist[$pcount] = main::fixpath("$dir/$1");
			# print "    RF = $path\n";
			}
		elsif (/<Project>{(.*)}<\/Project>/)
			{
			$ulist[$pcount] = lc $1;
			# print "    RI = $1\n";
			}
		elsif (/<\/ProjectReference>/)
			{
			$pcount++;
			}
		}

	close(PROJ);


	# Deal with subprojects
	my $seenfilename=0;

	for (my $i=0;$i<$pcount;$i++)
		{
		my $p=main::findProject($plist[$i]);

		if ($p->id() ne $ulist[$i])
			{
			if (!$seenfilename)
				{
				$seenfilename = 1;
				print "\n  <<<<$file>>>>\n";
				}
			
			print "  Project UUID mismatch in $file for $plist[$i] - have ".$ulist[$i].", expected ".$p->id()."\n";
			$this->fixProjectReference($ulist[$i], $p->id());
			}

		}
	}


sub id
	{
	my ($this) = @_;

	return $this->{m_id};
	}


sub filename
	{
	my ($this) = @_;

	return $this->{m_filename};
	}


sub
fixProjectReference
	{
	my ($this, $oldid, $newid) = @_;
	my $count=0;
	my @lines;

	open(FILE, $this->filename());
	while (<FILE>)
		{
		while (/[\r\n]+$/)
			{
			chop;
			}

		my $line = $_;

		if (/^(.*<Project>{)(.*)(}<\/Project>.*)$/)
			{
			if (lc $2 eq lc $oldid)
				{
				$lines[$count++] = "$1$newid$3";
				}
			else
				{
				$lines[$count++] = $line;
				}
			}
		else
			{
			$lines[$count++] = $line;
			}
		}
	close(FILE);

	# print "Updating ".$this->filename()."\n";
	# <STDIN>;
	open(FILE, ">".$this->filename());
	for (my $i=0;$i<$count;$i++)
		{
		print FILE $lines[$i]."\n";
		}
	close(FILE);
	# print "Done\n";
	# <STDIN>;
	}
