#!perl
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************


use strict;

sub main
		{
		my	$res=1;

		if (!defined($ARGV[0]))
			{
			print STDERR "usage: mkeula eula-source-file\n";
			}
		elsif (open(IN, $ARGV[0]))
			{
			my $line;

			while ($line = <IN>)
				{
				while ($line =~ /^(.*)%([A-Z_]+)\%(.*)$/)
					{
					$line = "$1$ENV{$2}$3";
					}

				print "$line";
				}

			close(IN);
			$res = 0;
			}
		else
			{
			print STDERR "Can't open $ARGV[0] for reading\n";
			}

		return 0;
		}


# Last line
exit main();
