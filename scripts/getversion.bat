@echo off
rem *********************************************************************************
rem Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
rem 
rem Permission is hereby granted, free of charge, to any person obtaining a copy
rem of this software and associated documentation files (the "Software"), to deal
rem in the Software without restriction, including without limitation the rights
rem to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
rem copies of the Software, and to permit persons to whom the Software is
rem furnished to do so, subject to the following conditions:
rem 
rem The above copyright notice and this permission notice shall be included in all
rem copies or substantial portions of the Software.
rem 
rem *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
rem IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
rem FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
rem AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
rem LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
rem OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
rem SOFTWARE.
rem *********************************************************************************


setlocal

set fetchtag=
set wantedtag=%1

if not defined wantedtag goto :error_no_tag

call :do_getproductinfo

if "%wantedtag%" == "master" set fetchtag=master

git tag -l > taglist.tmp
for /f %%f in (taglist.tmp) do call :do_checktag %%f
del taglist.tmp

if not defined fetchtag set fetchtag=%wantedtag%

echo Checking out code using %fetchtag%

rem Do a local pull first in case .allfolders was updated
git pull

echo Attempting to checkout version %fetchtag%
call :do_checkout %fetchtag%

echo.
echo Updating all folders
call pullall

echo Using %fetchtag% to fetch other folders
for /f %%f in (.allfolders) do call :do_folder_checkout %%f
goto :eof


:do_folder_checkout
set param=%1

rem Ignore comments
if "%param%" == "#" goto :eof

set folder=%param:/=\%

echo.

pushd %folder%
echo Attempting to checkout version %fetchtag% in %folder%
call :do_checkout %fetchtag%
popd

echo.
echo.
echo #############################################################
echo #############################################################
echo ## IMPORTANT - if you checked out a TAG you must create    ##
echo ## a branch before doing any work otherwise your work will ##
echo ## lost if you commit and then switch back to the master   ##
echo ## branch. If necessary use                                ##
echo ##                                                         ##
echo ##     ..\build\scripts\branchall branchname               ##
echo ##                                                         ##
echo #############################################################
echo #############################################################
echo.
echo.
goto :eof



rem ################################################################################
rem # Errors
rem ################################################################################
:error_no_tag
echo ERROR - No version tag specified
goto :eof

:error_version_not_found
echo ERROR - Version/Tag %wantedtag% NOT found
goto :eof



rem ################################################################################
rem # getproductinfo
rem ################################################################################
:do_getproductinfo
call ..\build\bin\getbuildinfo --readonly --file product.info --prefix PRODUCT --out bat:buildinfo.bat
call buildinfo.bat
del /q buildinfo.bat
goto :eof




:do_checktag
set tag=%1
if defined fetchtag goto :eof

if "%tag%" == "%wantedtag%" set fetchtag=tags/%tag%
if "%tag%" == "%PRODUCT_PACKAGE%_%wantedtag%" set fetchtag=%tag%

goto :eof



:do_checkout
set tag=%1

git checkout %fetchtag%

rem if %tag% == master goto :do_checkout_master
rem git checkout tags/%fetchtag%
goto :eof



:do_checkout_master
git checkout %fetchtag%
goto :eof
