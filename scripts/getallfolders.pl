#!/usr/bin/perl
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************


use strict;

my $dependscount=0;
my @depends;

sub hasDepends
	{
	my ($folder)=@_;
	my $res=0;

	for (my $i=0;$i<$dependscount;$i++)
		{
		if ($folder eq $depends[$i])
			{
			$res = 1;
			last;
			}
		}

	return $res;
	}


sub addDepends
	{
	my ($folder)=@_;

	if (!hasDepends($folder))
		{
		$depends[$dependscount++] = $folder;
		}
	}


sub getFolderDependencies
	{
	my ($folder)=@_;

	if ($folder ne "" && -f "$folder/.allfolders")
		{
		my @flist;
		my $fcount=0;

		open(IN, "$folder/.allfolders");
		while(<IN>)
			{
			chop;

			next if (/^\s*#/ || /^\s*$/);

			if ($_ ne ".")
				{
				$flist[$fcount++] = $_;
				}
			}
		close(IN);

		for (my $i=0;$i<$fcount;$i++)
			{
			getFolderDependencies($flist[$i]);
			}
		}

	addDepends($folder);
	}

my $windows=0;

$windows = 1 if ($ENV{"OS"} =~ /^Windows/);

getFolderDependencies(".");

if ($windows)
	{
	for (my $i=0;$i<$dependscount;$i++)
		{
		my $folder=$depends[$i];

		$folder =~ s/\//\\/g;
		print qq($folder\n);
		}

	}
else
	{
	for (my $i=0;$i<$dependscount;$i++)
		{
		my $folder=$depends[$i];

		print qq($folder\n);
		}
	}
