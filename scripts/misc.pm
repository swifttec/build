#!/usr/bin/perl
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************


package misc;
use strict;
use filename;

sub stringToDMY
		{
		my ($ds)=@_;

		return dateStringToDMY($ds);
		}


sub dateStringToDMY
		{
		my ($ds)=@_;

		my $day;
		my $month;
		my $year;
		my $valid = 1;

		if ($ds =~ /^(\d+)\/(\d+)\/(\d+)$/)
			{
			$day = $1;
			$month = $2;
			$year = $3;

			if ($year < 0)
				{
				$valid = 0;
				}
			elsif ($year < 100)
				{
				my $thisyear = todaysYear();
				my $yd = int($thisyear / 100) * 100;
				my $ym = $thisyear % 100;

				if ($ym < $year)
					{
					$ym += 100;
					$yd -= 100;
					}

				if (($ym - $year) > 50)
					{
					$year += $yd + 100;
					}
				else
					{
					$year += $yd;
					}
				}
			}
		elsif ($ds =~ /^(\d+)\/(\d+)$/)
			{
			$day = $1;
			$month = $2;
			$year = todaysYear();
			}
		else
			{
			$valid = 0;
			}

		$valid = 0 if ($month < 1 || $month > 12);
		$valid = 0 if ($day < 1 || $day > 31);

		return ($day, $month, $year, $valid);
		}


sub dateStringToKey
		{
		my ($ds)=@_;
		my ($day, $month, $year, $valid)=stringToDMY($ds);
		my $key="";

		if ($valid)
			{
			$key = dmyToKey($day, $month, $year);
			}

		return $key;
		}


sub dateKeyToDMY
		{
		my ($ds)=@_;
		my $day;
		my $month;
		my $year;
		my $valid=0;
		
		if ($ds =~ /^(\d{4})(\d{2})(\d{2})/)
			{
			$year = $1;
			$month = $2;
			$day = $3;
			$valid = 1;
			}

		$valid = 0 if ($month < 1 || $month > 12);
		$valid = 0 if ($day < 1 || $day > 31);

		return ($day, $month, $year, $valid);
		}


sub dateKeyToString
		{
		my ($ds)=@_;
		my ($day, $month, $year, $valid)=dateKeyToDMY($ds);
		my $res="";
		
		if ($ds =~ /^(\d{4})(\d{2})(\d{2})/)
			{
			$year = $1;
			$month = $2;
			$day = $3;
			$valid = 1;
			}

		$valid = 0 if ($month < 1 || $month > 12);
		$valid = 0 if ($day < 1 || $day > 31);

		if ($valid)
			{
			$res = sprintf("%02d/%02d/%04d", $day, $month, $year);
			}

		return $res;
		}


sub dmyToString
		{
		my ($day, $month, $year)=@_;

		return sprintf("%02d/%02d/%04d", $day, $month, $year);
		}


sub dmyToKey
		{
		my ($day, $month, $year)=@_;

		return sprintf("%04d%02d%02d", $year, $month, $day);
		}


sub mkfolder
		{
		my ($folder, $perm) = @_;

		if (!-d $folder)
			{
			my $parent;
			$parent = filename::dirname($folder);
			if (!-d $parent && $parent ne "" && $parent ne $folder)
				{
				mkfolder($parent, $perm);
				}

			mkdir($folder, $perm);
			}
		}


sub todaysYear
		{
		my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime;

		return $year+1900;
		}


sub todaysDateKey
		{
		my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime;

		return sprintf("%04d%02d%02d", $year+1900, $mon+1, $mday);
		}


sub nowAsTimestamp
		{
		my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime;

		return sprintf("%04d%02d%02d%02d%02d%02d", $year+1900, $mon+1, $mday, $hour, $min, $sec);
		}


sub fullMonthName
		{
		my ($idx)=@_;

		return ("", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")[$idx];
		}


sub abbrMonthName
		{
		my ($idx)=@_;

		return ("", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")[$idx];
		}


sub abbrDayName
		{
		my ($idx)=@_;

		return ("", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun")[$idx];
		}


sub moneyToString
		{
		my ($amount, $alwayspence, $currency)=@_;
		my $res;

		my $pound=0;
		my $pence=0;

		if ($amount =~ /^\s*(\d+)\.(\d+)\s*$/)
			{
			my ($whole, $fraction)=($1, $2);
			$pound = $whole;

			if (length($fraction) == 1)
				{
				$fraction .= "0";
				}
			elsif (length($fraction) > 2)
				{
				$fraction =~ /^(\d{2})/;
				$fraction = $1;
				}

			$pence = $fraction;
			}
		elsif ($amount =~ /^\s*(\d+)\s*$/)
			{
			$pound = $1;
			$pence = 0;
			}

		#print "amount = $amount\n";
		#print "pound = $pound\n";
		#print "pence = $pence\n";

		$res .= $currency;
		$res .= $pound;
		if ($pence || $alwayspence)
			{
			$res .= ".";
			$res .= sprintf("%02d", $pence);
			}

		return $res;
		}



sub stringToMoney
		{
		my ($str)=@_;
		my $res=0;

		if ($str =~ /^\s*(\d+)\.(\d+)\s*$/)
			{
			my ($whole, $fraction)=($1, $2);
			$res = $whole * 100;

			if (length($fraction) == 1)
				{
				$fraction .= "0";
				}
			elsif (length($fraction) > 2)
				{
				$fraction =~ /^(\d{2})/;
				$fraction = $1;
				}

			$res = "$whole.$fraction";
			}
		elsif ($str =~ /^\s*(\d+)\s*$/)
			{
			my ($whole)=($1);
			$res = $whole;
			}

		return $res;
		}


sub backupFile
		{
		my ($path, $nbackups)=@_;
		my $datadir=filename::dirname($path);
		my $filebase=filename::basename($path);

		misc::mkfolder(filename::dirname($path), 0777);

		$nbackups = 1 if ($nbackups eq "");
		$nbackups = 9 if ($nbackups > 9);

		if ($nbackups > 1)
			{
			my $datafile="$datadir/$filebase.bk$nbackups";

			unlink($datafile);
			for (my $i=($nbackups-1);$i>0;$i--)
				{
				my $j=$i+1;
				my $df1="$datadir/$filebase.bk$i";
				my $df2="$datadir/$filebase.bk$j";

				rename($df1, $df2);
				}

			$datafile = "$datadir/$filebase";
			rename($datafile, "$datadir/$filebase.bk1");
			}
		elsif ($nbackups == 1)
			{
			my $datafile="$datadir/$filebase.bak";

			unlink($datafile);

			$datafile = "$datadir/$filebase";
			rename($datafile, "$datadir/$filebase.bak");
			}
		}


sub lockfile
		{
		my ($filename, $delay, $force)=@_;
		my $lockfile = "$filename.lock";
		my $res=0;

		$delay = 30 if ($delay eq "");

		if ($misc::lockcount{$lockfile} == 0)
			{
			for (my $i=0;$i<$delay;$i++)
				{
				last if (!-f $lockfile);
				sleep(1);
				}

			if (! -f $lockfile || $force)
				{
				if (-f $lockfile)
					{
					$res = 2;
					}
				else
					{
					$res = 1;
					}

				misc::mkfolder(filename::dirname($lockfile), 0777);

				my $now=localtime;
				open(OUT, ">$lockfile");
				print OUT "$now\n";
				close(OUT);
				}
			}

		$misc::lockcount{$lockfile}++ if ($res);

		return $res;
		}


sub unlockfile
		{
		my ($filename)=@_;
		my $lockfile = "$filename.lock";
		my $res = 0;

		$misc::lockcount{$lockfile}-- if ($misc::lockcount{$lockfile} > 0);
		if ($misc::lockcount{$lockfile} == 0)
			{
			unlink($lockfile);
			$res = 1;
			}

		return $res;
		}



sub rmfolder
		{
		my ($folder)=@_;

		if (-d $folder)
			{
			opendir(DIR, $folder);
			my @entries=readdir(DIR);
			closedir(DIR);

			foreach my $e (@entries)
				{
				next if ($e eq "." || $e eq "..");

				my $path="$folder/$e";

				if (-d $path)
					{
					rmfolder($path);
					}
				else
					{
					unlink($path);
					}
				}

			rmdir($folder);
			}
		}

my @klist=
	(
	"B:0",
	"KB:1",
	"MB:1",
	"GB:2",
	"TB:2",
	"PB:2",
	"EB:3",
	"ZB:3",
	"YB:3",
	"BB:4",
	""
	);

sub sizeToString
		{
		my ($size)=@_;
		my $idx=0;

		while ($klist[$idx] ne "" && int($size / 1000) != 0)
			{
			$idx++;
			$size /= 1024;
			}

		my ($s, $p) = split(/:/, $klist[$idx]);

		return sprintf("%.*f%s", $p, $size, $s);
		}


1;
