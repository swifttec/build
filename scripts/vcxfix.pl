#!perl
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************


sub randhex
		{
		my $res="";

		$n = rand(16);
		if ($n < 10) { $res = chr(48+$n); }
		else { $res = chr(65+$n-10); }

		return $res;
		}


sub uuid
		{
		# {49401A6E-2A60-44BB-A6FA-73AC20190DB7}

		my $res = "{";

		for (my $i=0;$i<8;$i++) { $res .= randhex(); }
		$res .= "-";
		for (my $i=0;$i<4;$i++) { $res .= randhex(); }
		$res .= "-";
		for (my $i=0;$i<4;$i++) { $res .= randhex(); }
		$res .= "-";
		for (my $i=0;$i<4;$i++) { $res .= randhex(); }
		$res .= "-";
		for (my $i=0;$i<12;$i++) { $res .= randhex(); }

		$res .= "}";

		return $res;
		}


sub fix
		{
		my ($filename) = @_;

		if (-f $filename)
			{
			my $oldfilename="$filename.bak";
			my $newfilename=$filename;
			my $overwrite=0;

			print "Fixing $filename\n";

			my $oldfilename="$filename.bak";
			my $newfilename=$filename;

			if (-f "$filename.bak") 
				{
				unlink("$filename.bak");
				}

			rename($filename, "$filename.bak");

			if (-f $newfilename)
				{
				print "$newfilename already exists\n";
				return;
				}

			open(IN, "$oldfilename");
			open(OUT, ">$newfilename");

			while ($line = <IN>)
				{
				chomp($line);
				if ($line =~ /^(.*<ProjectGuid>).*(<\/ProjectGuid>.*)$/ || $line =~ /^(.*<UniqueIdentifier>).*(<\/UniqueIdentifier>.*)$/)
					{
					$line = $1.uuid().$2;
					}

				print OUT "$line\n";
				}

			close(OUT);
			close(IN);

			unlink($oldfilename);
			}
		else
			{
			print "$file is not a file\n";
			}
		}


foreach $file (glob "*.vcxproj *.vcxproj.filters *.csproj *.vbproj")
	{
	fix($file);
	}
