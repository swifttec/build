#!/bin/bash
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************

#
# This is a generic script that uses it's name and tries to find
# the path for the program and execute it. We can also add common
# paths to the pathlist in order to avoid developers having all
# paths in their path in order to perform a build.

prog=`basename $0`

# This splits the users PATH
pathlist=`echo $PATH | sed 's/:/ /g'`

# This is the list of the extra paths
extrapaths="/bin /usr/bin /usr/local/bin /sbin /usr/sbin /usr/java* /opt/*/bin"

for p in $pathlist $extrapaths
do
	#echo Checking $p
	if [ "$p" != "." -a -d "$p" -a -x "$p/$prog" ]; then
		exec "$p/$prog" $*
		exit 0
	fi
done

echo "$prog not found"
exit 1
