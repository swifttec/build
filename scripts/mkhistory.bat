@echo off
rem *********************************************************************************
rem Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
rem 
rem Permission is hereby granted, free of charge, to any person obtaining a copy
rem of this software and associated documentation files (the "Software"), to deal
rem in the Software without restriction, including without limitation the rights
rem to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
rem copies of the Software, and to permit persons to whom the Software is
rem furnished to do so, subject to the following conditions:
rem 
rem The above copyright notice and this permission notice shall be included in all
rem copies or substantial portions of the Software.
rem 
rem *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
rem IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
rem FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
rem AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
rem LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
rem OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
rem SOFTWARE.
rem *********************************************************************************


setlocal

call :do_getproductinfo
call :do_version_history
goto :eof



rem ################################################################################
rem # getproductinfo
rem ################################################################################
:do_getproductinfo
call ..\build\bin\getbuildinfo --readonly --file product.info --prefix PRODUCT --out bat:buildinfo.bat
call buildinfo.bat
del /q buildinfo.bat
goto :eof




rem ################################################################################
rem # Create/edit version history
rem ################################################################################
:do_version_history
if exist versionhistory goto :do_vh2
mkdir versionhistory

:do_vh2
set HISTORY_VERSION=%PRODUCT_MAJOR_VERSION%.%PRODUCT_MINOR_VERSION%.%PRODUCT_REVISION%
set vhfile=versionhistory\%HISTORY_VERSION%.txt
set vhpath=%CD%\%vhfile%
set vhnew=0

if not exist %vhfile% set vhnew=1

perl ..\build\scripts\updateversionhistoryfile.pl %vhfile%

:edit_vhfile
echo Callling GVIM to edit version history entry
call gvim -f %vhfile%

if %vhnew% == 1 git add %vhfile%

rem Generate the version history files
:do_vh3
perl ..\build\scripts\buildversionhistory.pl

goto :eof





