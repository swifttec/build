@echo off
rem *********************************************************************************
rem Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
rem 
rem Permission is hereby granted, free of charge, to any person obtaining a copy
rem of this software and associated documentation files (the "Software"), to deal
rem in the Software without restriction, including without limitation the rights
rem to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
rem copies of the Software, and to permit persons to whom the Software is
rem furnished to do so, subject to the following conditions:
rem 
rem The above copyright notice and this permission notice shall be included in all
rem copies or substantial portions of the Software.
rem 
rem *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
rem IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
rem FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
rem AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
rem LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
rem OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
rem SOFTWARE.
rem *********************************************************************************


setlocal

set filepath=%1
set prog=%0
set progname=
set progdir=

for %%f in (%prog%) do set progname=%%~nxf
for %%f in (%prog%) do set progdir=%%~pf

set filename=
set filedir=
for %%f in (%filepath%) do set filename=%%~nf
for %%f in (%filepath%) do set filedir=%%~pf

set dstr=%DATE%
set tstr=%TIME: =0%
set timestamp=%dstr:~6,4%%dstr:~3,2%%dstr:~0,2%_%tstr:~0,2%%tstr:~3,2%%tstr:~6,2%
set tempfolder=%temp%\mc%timestamp%

rem echo tempfolder=%tempfolder%
md %tempfolder%
pushd %tempfolder%
"C:\Program Files (x86)\Windows Kits\8.1\bin\x86\mc.exe" %filepath%
popd
perl %progdir%\updatetextfile.pl %tempfolder%\%filename%.h %filedir%\%filename%.h
rd /s /q %tempfolder%
