@echo off
rem *********************************************************************************
rem Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
rem 
rem Permission is hereby granted, free of charge, to any person obtaining a copy
rem of this software and associated documentation files (the "Software"), to deal
rem in the Software without restriction, including without limitation the rights
rem to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
rem copies of the Software, and to permit persons to whom the Software is
rem furnished to do so, subject to the following conditions:
rem 
rem The above copyright notice and this permission notice shall be included in all
rem copies or substantial portions of the Software.
rem 
rem *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
rem IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
rem FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
rem AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
rem LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
rem OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
rem SOFTWARE.
rem *********************************************************************************


setlocal

if not defined BUILDDIR set BUILDDIR=..\build
if not defined MKPACKAGE_BUILD_FULL set MKPACKAGE_BUILD_FULL=1
if not defined MKPACKAGE_BUILD_PATCH set MKPACKAGE_BUILD_PATCH=0
if not defined MKPACKAGE_BUILD_NOREDIST set MKPACKAGE_BUILD_NOREDIST=0

if exist pre-mkpackage.bat call pre-mkpackage.bat
if exist ..\build\local\scripts\pre-mkpackage.bat call ..\build\local\scripts\pre-mkpackage.bat
if exist ..\build\local\scripts\pre-mkpackage-%COMPUTERNAME%.bat call ..\build\local\scripts\pre-mkpackage-%COMPUTERNAME%.bat

if not defined PRODUCT_PACKAGE call :do_getproductinfo

call :do_getsourceinfo

if not defined MKPACKAGE_EULA_FILE goto :after_eula
if exist %MKPACKAGE_EULA_FILE% perl %BUILDDIR%\scripts\mkeula.pl %MKPACKAGE_EULA_FILE% > eula.rtf
:after_eula

rem echo MKPACKAGE_BUILD_FULL = %MKPACKAGE_BUILD_FULL%
rem echo MKPACKAGE_BUILD_PATCH = %MKPACKAGE_BUILD_PATCH%
rem echo MKPACKAGE_BUILD_NOREDIST = %MKPACKAGE_BUILD_NOREDIST%
rem pause

if exist install rd /s /q install
for %%p in (*.iss) do call :mkpkg %%p

del install\*.tmp 2>nul:

if exist post-mkpackage.bat call post-mkpackage.bat
if exist ..\build\local\scripts\post-mkpackage.bat call ..\build\local\scripts\post-mkpackage.bat
if exist ..\build\local\scripts\post-mkpackage-%COMPUTERNAME%.bat call ..\build\local\scripts\post-mkpackage-%COMPUTERNAME%.bat

exit /b 0
goto :eof


:mkpkg
set issfile=%1
set PRODUCT_PACKAGE=%issfile:.iss=%

if "%PRODUCT_PACKAGE%" == "" goto :eof

call :do_build_all_packages

goto :eof







:do_build_all_packages

rem Build Setup (full) package
if %MKPACKAGE_BUILD_FULL% == 0 goto :do_build_patch_package

set INSTALLER_TYPE=full
set INSTALLER_PREFIX=Setup
set INSTALLER_SUFFIX=
set INSTALLER_REDIST=1
call :do_build_package

rem Build Patch package
:do_build_patch_package
if %MKPACKAGE_BUILD_PATCH% == 0 goto :do_build_noredist_package

set INSTALLER_TYPE=patch
set INSTALLER_PREFIX=Patch
set INSTALLER_SUFFIX=
set INSTALLER_REDIST=0
call :do_build_package

rem Build noredist package
:do_build_noredist_package
if %MKPACKAGE_BUILD_NOREDIST% == 0 goto :do_build_other_package

set INSTALLER_TYPE=noredist
set INSTALLER_PREFIX=Setup
set INSTALLER_SUFFIX=-noredist
set INSTALLER_REDIST=0
call :do_build_package

rem Build other package type
:do_build_other_package

goto :eof





rem ################################################################################
rem # getproductinfo
rem ################################################################################
:do_getproductinfo
call %BUILDDIR%\bin\getbuildinfo --readonly --file product.info --prefix PRODUCT --out bat:buildinfo.bat
call buildinfo.bat
del /q buildinfo.bat
goto :eof


rem ################################################################################
rem # getsourceinfo
rem ################################################################################
:do_getsourceinfo
if not exist source.info goto :no_source_info

call %BUILDDIR%\bin\getbuildinfo --readonly --file source.info --prefix SOURCE --out bat:sourceinfo.bat
call sourceinfo.bat
del /q sourceinfo.bat
goto :eof


:no_source_info
echo source.info not found
exit /b 1



rem ################################################################################
rem # Build setup package from ISS file
rem ################################################################################
:do_build_package
if "%PRODUCT_PACKAGE%" == "temp" goto :eof

rem echo INSTALLER_TYPE=%INSTALLER_TYPE%
rem echo PRODUCT_PACKAGE=%PRODUCT_PACKAGE%
rem pause

perl ..\build\iss\issupdate.pl "%PRODUCT_PACKAGE%.iss" > temp.iss

if not exist install md install

if not defined PRODUCT_PACKAGE_VERSION set PRODUCT_PACKAGE_VERSION=%PRODUCT_MAJOR_VERSION%.%PRODUCT_MINOR_VERSION%.%PRODUCT_REVISION%

set iscc=
if exist "C:\Program Files (x86)\Inno Setup 5\iscc.exe" set iscc=C:\Program Files (x86)\Inno Setup 5\iscc.exe
if exist "C:\Program Files\Inno Setup 5\iscc.exe" set iscc=C:\Program Files\Inno Setup 5\iscc.exe
if exist "C:\Program Files (x86)\Inno Setup 6\iscc.exe" set iscc=C:\Program Files (x86)\Inno Setup 6\iscc.exe
if exist "C:\Program Files\Inno Setup 6\iscc.exe" set iscc=C:\Program Files\Inno Setup 6\iscc.exe
if exist "%iscc%" "%iscc%" /O"install" /f"%INSTALLER_PREFIX%%PRODUCT_PACKAGE%-%PRODUCT_PACKAGE_VERSION%%INSTALLER_SUFFIX%" temp.iss
if errorlevel 1 exit /b 1

echo Package built successfully.
del temp.iss

if not defined SIGNSETUP goto :after_signing
if not defined SIGNCERT goto :after_signing
if not defined SIGNPASS goto :after_signing
if %SIGNSETUP% == 0 goto :after_signing
if not exist "%SIGNCERT%" goto :after_signing

call ..\build\scripts\signtool sign /f "%SIGNCERT%" /p "%SIGNPASS%" /t %SIGNTIMESTAMPURL% "install\%INSTALLER_PREFIX%%PRODUCT_PACKAGE%-%PRODUCT_PACKAGE_VERSION%%INSTALLER_SUFFIX%.exe"

:after_signing
if not defined MKPACKAGE_TARGETDIR goto :eof
if not exist "%MKPACKAGE_TARGETDIR%" mkdir "%MKPACKAGE_TARGETDIR%"
copy /y "install\%INSTALLER_PREFIX%%PRODUCT_PACKAGE%-%PRODUCT_PACKAGE_VERSION%%INSTALLER_SUFFIX%.exe" "%MKPACKAGE_TARGETDIR%\%INSTALLER_PREFIX%%PRODUCT_PACKAGE%-%PRODUCT_PACKAGE_VERSION%%INSTALLER_SUFFIX%.exe"
goto :eof
