@echo off
rem *********************************************************************************
rem Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
rem 
rem Permission is hereby granted, free of charge, to any person obtaining a copy
rem of this software and associated documentation files (the "Software"), to deal
rem in the Software without restriction, including without limitation the rights
rem to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
rem copies of the Software, and to permit persons to whom the Software is
rem furnished to do so, subject to the following conditions:
rem 
rem The above copyright notice and this permission notice shall be included in all
rem copies or substantial portions of the Software.
rem 
rem *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
rem IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
rem FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
rem AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
rem LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
rem OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
rem SOFTWARE.
rem *********************************************************************************


setlocal

if not "%SW_BUILD_RELEASE_X86%%SW_BUILD_RELEASE_X64%%SW_BUILD_DEBUG_X86%%SW_BUILD_DEBUG_X64%" == "" goto :after_buildtypes
set SW_BUILD_RELEASE_X86=Release x86
set SW_BUILD_RELEASE_X64=Release x64

:after_buildtypes

set SOLUTION=%SW_BUILD_SOLUTION%

if defined SOLUTION goto :after_find_solution
set starttime=%time%

for %%f in (%CD:\= %) do set folder=%%f

set SOLUTION=%folder%.sln
:after_find_solution

rem The Visual Studio build environment
call ..\setbuildenv

if "%1" == "" call :do_build
if "%1" == "build" call :do_build
if "%1" == "clean" call :do_clean

set endtime=%time%

echo Start time: %starttime%
echo End time  : %endtime%

goto :eof


rem ################################################################################
rem # Do a single devenv command
rem ################################################################################
:do_devenv
set devaction=%1
set devconfig=%2
set devplatform=%3

echo.
echo %devaction% %devconfig%
set devstarttime=%time%

if defined VS100COMNTOOLS goto :use_devenv
echo msbuild /nologo %SOLUTION% /p:Configuration=%devconfig% /p:Platform=%devplatform% /t:%devaction% 
msbuild /nologo %SOLUTION% /p:Configuration=%devconfig% /p:Platform=%devplatform% /t:%devaction% 
goto :after_build

:use_devenv
echo devenv /nologo %SOLUTION% /%devaction% "%devconfig%|%devplatform%"
devenv /nologo %SOLUTION% /%devaction% "%devconfig%|%devplatform%"

:after_build
set devendtime=%time%

echo %devaction% %devconfig% Start time: %devstarttime%
echo %devaction% %devconfig% End time  : %devendtime%
if errorlevel 1 exit /b 1
goto :eof


rem ################################################################################
rem # Build
rem ################################################################################
:do_build

if defined SW_BUILD_RELEASE_X64 call :do_devenv build %SW_BUILD_RELEASE_X64%
if errorlevel 1 exit /b 1

if defined SW_BUILD_RELEASE_X86 call :do_devenv build %SW_BUILD_RELEASE_X86%
if errorlevel 1 exit /b 1

if defined SW_BUILD_DEBUG_X64 call :do_devenv build %SW_BUILD_DEBUG_X64%
if errorlevel 1 exit /b 1

if defined SW_BUILD_DEBUG_X86 call :do_devenv build %SW_BUILD_DEBUG_X86%
if errorlevel 1 exit /b 1

goto :eof


rem ################################################################################
rem # Cleanup previous builds
rem ################################################################################
:do_clean

if defined SW_BUILD_RELEASE_X64 call :do_devenv clean %SW_BUILD_RELEASE_X64%
if errorlevel 1 exit /b 1

if defined SW_BUILD_RELEASE_X86 call :do_devenv clean %SW_BUILD_RELEASE_X86%
if errorlevel 1 exit /b 1

if defined SW_BUILD_DEBUG_X64 call :do_devenv clean %SW_BUILD_DEBUG_X64%
if errorlevel 1 exit /b 1

if defined SW_BUILD_DEBUG_X86 call :do_devenv clean %SW_BUILD_DEBUG_X86%
if errorlevel 1 exit /b 1

echo.
echo Cleaning output folders
for %%f in (Debug-x86 Debug-x64 Release-x86 Release-x64 DebugMfc-x86 DebugMfc-x64 ReleaseMfc-x86 ReleaseMfc-x64) do if exist %%f rd /s /q %%f
goto :eof


