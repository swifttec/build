@echo off
rem *********************************************************************************
rem Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
rem 
rem Permission is hereby granted, free of charge, to any person obtaining a copy
rem of this software and associated documentation files (the "Software"), to deal
rem in the Software without restriction, including without limitation the rights
rem to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
rem copies of the Software, and to permit persons to whom the Software is
rem furnished to do so, subject to the following conditions:
rem 
rem The above copyright notice and this permission notice shall be included in all
rem copies or substantial portions of the Software.
rem 
rem *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
rem IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
rem FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
rem AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
rem LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
rem OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
rem SOFTWARE.
rem *********************************************************************************


setlocal

set ASK_ACTION=1
set SHOW_VERSION=1
set UPDATE_MAJOR=0
set UPDATE_MINOR=0
set UPDATE_REVISION=0
set UPDATE_BUILD=0
set DO_GIT=0


for %%a in (%*) do call :parse_arg %%a

call :do_getproductinfo

if exist product.info call :do_updateversions

goto :eof





rem ###################################################################
rem Parse command line arg
rem ###################################################################
:parse_arg
set arg=%1
set ASK_ACTION=0

if /i "%arg%" == "auto" set UPDATE_BUILD=1
if /i "%arg%" == "major" set UPDATE_MAJOR=1
if /i "%arg%" == "minor" set UPDATE_MINOR=1
if /i "%arg%" == "revision" set UPDATE_REVISION=1
if /i "%arg%" == "git" set DO_GIT=1
if /i "%arg%" == "keepver" set MASTERBUILD_KEEPVER=1

goto :eof


rem ################################################################################
rem # getproductinfo
rem ################################################################################
:do_getproductinfo
call ..\build\bin\getbuildinfo --readonly --file product.info --prefix PRODUCT --out bat:buildinfo.bat
call buildinfo.bat
del /q buildinfo.bat
goto :eof




rem ################################################################################
rem # askaction
rem ################################################################################
:do_askaction
set SHOW_VERSION=0
set ans=

echo %PRODUCT_NAME% %PRODUCT_VERSION% build %PRODUCT_BUILD%
set /p ans=Update product version? [m=Major, v=Minor, r=Revision] 

if /i "%ans%" == "m" set UPDATE_MAJOR=1
if /i "%ans%" == "v" set UPDATE_MINOR=1
if /i "%ans%" == "r" set UPDATE_REVISION=1
goto :eof



rem ################################################################################
rem # updateversions
rem ################################################################################
:do_updateversions
set OLDVER=%PRODUCT_NAME% %PRODUCT_VERSION% build %PRODUCT_BUILD%

if %ASK_ACTION% == 1 call :do_askaction

set UPDATE_ACTION=
if %UPDATE_MAJOR% == 1 set UPDATE_ACTION=--inc-major
if %UPDATE_MINOR% == 1 set UPDATE_ACTION=--inc-minor
if %UPDATE_REVISION% == 1 set UPDATE_ACTION=--inc-revision

if defined UPDATE_ACTION ..\build\bin\getbuildinfo --file product.info --prefix PRODUCT %UPDATE_ACTION% --inc-build
call :do_getproductinfo

if defined UPDATE_ACTION (
	if %DO_GIT% == 1 (
		git add product.info
		git commit -m "auto-update product/version info"
		git push
	)

	echo %OLDVER% =^> %PRODUCT_NAME% %PRODUCT_VERSION% build %PRODUCT_BUILD%
) else if %SHOW_VERSION% == 1 (
	echo %PRODUCT_NAME% %PRODUCT_VERSION% build %PRODUCT_BUILD%
)
