#!/usr/bin/perl
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************


package filename;
use strict;

sub init
		{
		# DOS or UNIX?
		if (defined($ENV{WINDIR}))
			{
			$filename::pathsep = "\\";
			}
		else
			{
			$filename::pathsep = "/";
			}
		}


sub dirname
		{
		my ($path) = @_;
		my $dir=".";

		if ($path =~ /^(.*)([\/\\]{1})[^\/\\]+$/)
			{
			my $parent=$1;
			my $sep=$2;

			$dir = $parent;
			if ($parent =~ /^\w{1}:$/ || $parent eq "")
				{
				$dir .= $sep;
				}
			}
		elsif ($path =~ /^\w{1}:([\/\\]{1})$/ || $path eq "/" || $path eq "\\")
			{
			$dir = $path;
			}

		return $dir;
		}


sub basename
		{
		my ($path) = @_;
		my $res=$path;

		if ($path =~ /^.*[\/\\]{1}([^\/\\]+)$/)
			{
			$res = $1;
			}
		elsif ($path =~ /^\w{1}:([^\/\\]+)$/)
			{
			$res = $1;
			}

		return $res;
		}


sub extname
		{
		my ($path) = @_;
		my $filename=basename($path);
		my $res="";

		$res = $1 if ($filename =~ /\.([^.]+)$/);
		
		return $res;
		}


sub concat
		{
		my ($path1, $path2, $sep)=@_;
		my $path="";
		my $count=0;

		$sep = $filename::pathsep if ($sep eq "");

		if ($path1 =~ /^[\/\\]+/)
			{
			$path = $sep;
			}

		if ($path1 eq "" && $path2 =~ /^[\/\\]+/)
			{
			$path = $sep;
			}

		foreach my $p (split(/[\/\\]/, $path1))
			{
			if ($p ne "")
				{
				$path .= $sep if ($count++);
				$path .= $p;
				}
			}

		foreach my $p (split(/[\/\\]/, $path2))
			{
			if ($p ne "")
				{
				$path .= $sep if ($count++);
				$path .= $p;
				}
			}

		return $path;
		}


#
# MAIN
#
init();

1;
