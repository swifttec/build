@echo off
rem *********************************************************************************
rem Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
rem 
rem Permission is hereby granted, free of charge, to any person obtaining a copy
rem of this software and associated documentation files (the "Software"), to deal
rem in the Software without restriction, including without limitation the rights
rem to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
rem copies of the Software, and to permit persons to whom the Software is
rem furnished to do so, subject to the following conditions:
rem 
rem The above copyright notice and this permission notice shall be included in all
rem copies or substantial portions of the Software.
rem 
rem *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
rem IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
rem FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
rem AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
rem LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
rem OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
rem SOFTWARE.
rem *********************************************************************************

rem
rem Generic program wrapper which wraps a program based on the current platform
rem If this wrapper script is called X it will attempt to execute the program
rem X-platform which is found by calling uname -sr and replacing any spaces with
rem underlines.

setlocal
set prog=%0
set prog=%prog:.bat=%

rem echo Program is %prog%

rem Do the equivalent of basename on the directories
for %%f in (%prog%) do set progname=%%~nxf
for %%f in (%prog%) do set progdir=%%~dpf

rem echo Progdir is %progdir%
rem echo Progname is %progname%

if not defined OS goto :no_OS
if not defined PROCESSOR_ARCHITECTURE goto :no_PROCESSOR_ARCHITECTURE

set arch=%PROCESSOR_ARCHITECTURE%
if %arch% == AMD64 set arch=x64

set program=%progdir%%OS%-%arch%\%progname%
if not exist %program%.exe set program=%progdir%%OS%-%arch%-%SW_BUILD_ENV%\%progname%
if not exist %program%.exe set program=%progdir%%OS%-%arch%-VC6\%progname%
if not exist %program%.exe set program=%progdir%%OS%-%arch%-VS6\%progname%
if not exist %program%.exe set program=%progdir%%OS%-%arch%-VC7\%progname%
if not exist %program%.exe set program=%progdir%%OS%-%arch%-VS7\%progname%
if not exist %program%.exe set program=%progdir%%OS%-%arch%-VC8\%progname%
if not exist %program%.exe set program=%progdir%%OS%-%arch%-VC9\%progname%
if not exist %program%.exe set program=%progdir%%OS%-%arch%-VC10\%progname%
if not exist %program%.exe set program=%progdir%%OS%-%arch%-VC11\%progname%
if not exist %program%.exe set program=%progdir%%OS%-%arch%-VC12\%progname%
if not exist %program%.exe set program=%progdir%%OS%-%arch%-VC14\%progname%
if not exist %program%.exe set program=%progdir%\%progname%
if not exist %program%.exe goto :no_program

rem Finally run the platform-independent program
%program% %*
exit /b %errorlevel%
goto :eof


:no_OS
echo THe environment variable OS is not defined
exit /b 1


:no_PROCESSOR_ARCHITECTURE
echo THe environment variable PROCESSOR_ARCHITECTURE is not defined
exit /b 1


:no_program
echo The program %progname%.exe cannot be found.
exit /b 1
