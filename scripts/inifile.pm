#!/usr/bin/perl
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************


package inifile;
use strict;
use filename;
use misc;

sub read
		{
		my ($datafile)=@_;
		my %data;

        lockfile($datafile);
		if (open(IN, "$datafile"))
			{
			my $idx=0;
			my $section="";
			my $innotes=0;
			my $notes="";

			# Read the data file.
			while (<IN>)
				{
				chop;

				if ($innotes)
					{
					if (/^{END:(\S+)}/)
						{
						my $param = $1;
						my $key;

						if ($section ne "")
							{
							$key = "$section:$param";
							}
						else
							{
							$key = $param;
							}

						$notes =~ s/%0D/\n/g;
						$notes =~ s/\r\n/\n/g;
						$data{$key} = $notes;
						$innotes = 0;
						next;
						}

					$notes .= "\n" if ($notes ne "");
					$notes .= $_;
					next;
					}

				if (/^{BEGIN:(\S+)}/)
					{
					$innotes = 1;
					$notes = "";
					next;
					}

				if (/^\[([^\]]+)\]/)
					{
					$section = $1;
					}

				if (/^([^=]*)=(.*)$/)
					{
					my $param = $1;
					my $value = $2;
					my $key = "$section:$param";

					if ($section ne "")
						{
						$key = "$section:$param";
						}
					else
						{
						$key = $param;
						}

					$data{$key} = $value;

					next;
					}
				}
			close(IN);
			}

        unlockfile($datafile);

		return %data;
		}


sub lockfile
		{
		my ($filename)=@_;

		return misc::lockfile($filename, 30, 1);
		}


sub unlockfile
		{
		my ($filename)=@_;

		return misc::unlockfile($filename, 30, 1);
		}


sub write
		{
		my ($path, %data, $nbackups)=@_;

		misc::mkfolder(filename::dirname($path), 0777);

		lockfile($path);

		misc::backupFile($path, $nbackups);

		if (open(OUT, ">$path"))
			{
			my %sections;
			my $nosection=0;

			foreach my $entry (keys %data)
				{
				if ($entry =~ /:/)
					{
					my ($section, $param) = split(/:/, $entry);

					$sections{$section} = 1;
					}
				else
					{
					$nosection = 1;
					}
				}

			if ($nosection)
				{
				foreach my $entry (keys %data)
					{
					next if ($entry =~ /:/);

					my $value=$data{$entry};
					my $param=$entry;

					if ($value =~ /\n/ || $value =~ /\r/)
						{
						$value =~ s/\r\n/\n/g;
						$value =~ s/\r/\n/g;
						$value =~ s/%0D/\n/g;
						print OUT "{BEGIN:$param}\n$value\n{END:$param}\n";
						}
					else
						{
						print OUT "$param=$value\n";
						}
					}
				}

			foreach my $id (sort keys %sections)
				{
				print OUT "\n[$id]\n";

				foreach my $entry (keys %data)
					{
					if ($entry =~ /:/)
						{
						my ($section, $param) = split(/:/, $entry);
						my $value=$data{$entry};

						next if ($section ne $id);

						if ($value =~ /\n/ || $value =~ /\r/)
							{
							$value =~ s/\r\n/\n/g;
							$value =~ s/\r/\n/g;
							$value =~ s/%0D/\n/g;
							print OUT "{BEGIN:$param}\n$value\n{END:$param}\n";
							}
						else
							{
							print OUT "$param=$value\n";
							}
						}
					}
				}

			close(OUT);
			}
		else
			{
			print qq(failed to create $path<br>\n);
			}

		unlockfile($path);
		}



1;
