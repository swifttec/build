#!/bin/sh
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************

#
# This script wraps calls to uname and other system utilities to give a consistent
# means of getting the platform information for the build process.

OSNAME=`uname -s`
OSID=0

case $OSNAME in
	AIX)
		OSID=100
		;;

	Darwin)
		OSID=200
		;;

	Linux)
		OSNAME=`grep '^NAME=' /etc/os-release | cut -c6- | sed -e 's/^"//' -e 's/"$//'`
		case $OSNAME in
			Red*)
				OSID=301
				;;

			Cent*)
				OSID=302
				;;

			openSUSE*)
				OSID=303
				;;

			Raspbian*)
				OSID=304
				;;
		esac
		;;

	*)
		OSID=300
		;;
esac

# Finally echo the platform info as a string without spaces.
echo $OSID
