#!/bin/bash
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************

# xlink - Smart x-platform linker


build_dll()
	{
	echo Building dynamic library $target
	echo objects = $objects
	}


build_lib()
	{
	echo Building static library $target
	echo objects = $objects
	}


build_exe()
	{
	echo Building program $target
	echo objects = $objects
	}


# Check our args
type=$1
target=$2
shift 2
objects=$*

if [ "$target" == "" ] || [ "$objects" == "" ]; then
	echo usage: xlink type target objects
	exit 1
fi

if [ "$type" == "dll" ]; then
	build_dll
elif [ "$type" == "lib" ]; then
	build_lib
elif [ "$type" != "exe" ]; then
	build_exe
else
	echo Unknown type $type
	exit 1
fi

echo type = $type
echo target = $target
