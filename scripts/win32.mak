# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************

#
# Windows makefile for scripts
#
PROJROOT= ..

include ../win32.mak.tools

SCRIPTS	= \
	$(TOOLSBIN)\mkobjnames.bat \
	$(TOOLSBIN)\mkdir.bat \
	$(TOOLSBIN)\zip.bat \
	$(TOOLSBIN)\doxygen.bat \
	$(TOOLSBIN)\parsevcproj.bat \
	$(TOOLSBIN)\parsevcproj.pl \


all: $(SCRIPTS)

clean:


$(TOOLSBIN)\mkdir.bat: mkdir.bat
	@-if not exist $(TOOLSBIN) mkdir $(TOOLSBIN)
	@echo Installing $@
	@-if exist $@ del /f $@
	@copy $** $@

$(TOOLSBIN)\zip.bat: zip.bat
	@-if not exist $(TOOLSBIN) mkdir $(TOOLSBIN)
	@echo Installing $@
	@-if exist $@ del /f $@
	@copy $** $@

$(TOOLSBIN)\doxygen.bat: doxygen.bat
	@-if not exist $(TOOLSBIN) mkdir $(TOOLSBIN)
	@echo Installing $@
	@-if exist $@ del /f $@
	@copy $** $@

$(TOOLSBIN)\mkobjnames.bat: mkobjnames.bat
	@-if not exist $(TOOLSBIN) mkdir $(TOOLSBIN)
	@echo Installing $@
	@-if exist $@ del /f $@
	@copy $** $@
	

$(TOOLSBIN)\installfiles.bat: installfiles.bat
	@-if not exist $(TOOLSBIN) mkdir $(TOOLSBIN)
	@echo Installing $@
	@-if exist $@ del /f $@
	@copy $** $@

$(TOOLSBIN)\parsevcproj.bat: parsevcproj.bat
	@echo Installing $@
	@-if not exist $(TOOLSBIN) mkdir $(TOOLSBIN)
	@-if exist $@ del /f $@
	@copy $** $@
	

$(TOOLSBIN)\parsevcproj.pl: parsevcproj.pl
	@echo Installing $@
	@-if not exist $(TOOLSBIN) mkdir $(TOOLSBIN)
	@-if exist $@ del /f $@
	@copy $** $@

