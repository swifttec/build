#!/bin/bash
# *********************************************************************************
# Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# *********************************************************************************


do_getbuildinfo()
	{
	../../build/bin/getbuildinfo --readonly --out sh:buildinfo.sh
	. ./buildinfo.sh
	rm ./buildinfo.sh
	}


do_parsearg()
	{
	ASK_ACTION=0

	case $1 in
		major)
			UPDATE_MAJOR=1
			;;

		minor)
			UPDATE_MINOR=1
			;;

		revision)
			UPDATE_REVISION=1
			;;

		git)
			DO_GIT=1
			;;
	esac
	}


do_updateversions()
	{
	OLDVER="$BUILD_LIBNAME $BUILD_VERSION (build $BUILD_BUILD)"

	if [ $ASK_ACTION == 1 ]; then
		SHOW_VERSION=0
		echo "$BUILD_LIBNAME $BUILD_VERSION (build $BUILD_BUILD)"
		read -p "Update build version? (m=Major, v=Minor, r=Revision) ? " ans
		case $ans in
			m)
				UPDATE_MAJOR=1
				;;

			v)
				UPDATE_MINOR=1
				;;

			r)
				UPDATE_REVISION=1
				;;
		esac
	fi

	if [ $UPDATE_MAJOR == 1 ]; then
		../../build/bin/getbuildinfo --inc-major --inc-build
		UPDATE_BUILD=1
	elif [ $UPDATE_MINOR == 1 ]; then
		../../build/bin/getbuildinfo --inc-minor --inc-build
		UPDATE_BUILD=1
	elif [ $UPDATE_REVISION == 1 ]; then
		../../build/bin/getbuildinfo --inc-revision --inc-build
		UPDATE_BUILD=1
	fi

	if [ $UPDATE_BUILD == 1 ]; then
		do_getbuildinfo

		if [ $DO_GIT == 1 ]; then
			git commit -m "auto-update build version to $BUILD_VERSION" build.info
			git push
		fi

		echo "$OLDVER => $BUILD_LIBNAME $BUILD_VERSION (build $BUILD_BUILD)"
	elif [ $SHOW_VERSION == 1 ]; then
		echo "$BUILD_LIBNAME $BUILD_VERSION (build $BUILD_BUILD)"
	fi
	}


ASK_ACTION=1
SHOW_VERSION=1
UPDATE_MAJOR=0
UPDATE_MINOR=0
UPDATE_REVISION=0
UPDATE_BUILD=0
DO_GIT=0

for arg in $*
do
	do_parsearg $arg
done

do_getbuildinfo

if [ -f build.info ]; then
	do_updateversions
fi

