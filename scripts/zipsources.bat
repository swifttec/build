@echo off
rem *********************************************************************************
rem Copyright (c) 2006-2019 Simon Sparkes T/A SwiftTec
rem 
rem Permission is hereby granted, free of charge, to any person obtaining a copy
rem of this software and associated documentation files (the "Software"), to deal
rem in the Software without restriction, including without limitation the rights
rem to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
rem copies of the Software, and to permit persons to whom the Software is
rem furnished to do so, subject to the following conditions:
rem 
rem The above copyright notice and this permission notice shall be included in all
rem copies or substantial portions of the Software.
rem 
rem *** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
rem IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
rem FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
rem AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
rem LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
rem OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
rem SOFTWARE.
rem *********************************************************************************

rem Use this script to make a zip of the sources for the current project

setlocal

set ZIPFILE=%1
if not defined ZIPFILE goto :error_no_zipfile

rem work out the 
for %%f in (%CD:\= %) do set myfolder=%%f

rem parse the args
for %%a in (%*) do call :parse_arg %%a

rem The Visual Studio build environment
call ..\setbuildenv

rem Get the build info from product.info
call :do_getproductinfo

set VERSION_TAG=%PRODUCT_PACKAGE%_%PRODUCT_VERSION%
set TMPFOLDER=%TEMP%\%VERSION_TAG%

echo TMPFOLDER=%TMPFOLDER%

set allfolders=
if exist .allfolders for /f %%f in (.allfolders) do call :do_add_allfolder %%f
if not defined allfolders set allfolders=.
call :do_add_allfolder build

rem Do the git commit for all folders
for %%f in (%allfolders%) do call :do_checkin %%f

rem Find 7z
set ZIP=%ProgramFiles%\7-Zip\7z.exe
if not exist "%ZIP%" set ZIP=%SystemDrive%\Program Files\7-Zip\7z.exe
if not exist "%ZIP%" set ZIP=%SystemDrive%\Program Files (x86)\7-Zip\7z.exe
if not exist "%ZIP%" set ZIP=c:\Program Files\7-Zip\7z.exe
if not exist "%ZIP%" goto :no_zip


if exist %ZIPFILE% del %ZIPFILE%

rem Make the ZIP file
"%ZIP%" a %ZIPFILE% -tzip %TMPFOLDER%

rem Clean up the temp folder
rd /s /q %TMPFOLDER%

goto :eof

:no_zip
echo Cannot find command-line 7-zip (7z.exe)
exit /b 1

goto :eof



rem ################################################################################
rem # getproductinfo
rem ################################################################################
:do_getproductinfo
call ..\build\bin\getbuildinfo --readonly --file product.info --prefix PRODUCT --out bat:buildinfo.bat
call buildinfo.bat
del /q buildinfo.bat
goto :eof




:do_add_allfolder
if "%1" == "" goto :eof
if "%1" == "#" goto :eof

set folder=%1
set folder=%folder:/=\%
set folder=%folder:..\=%
if "%folder%" == "." set folder=%myfolder%

if defined allfolders set allfolders=%allfolders% 
set allfolders=%allfolders% %folder%
goto :eof



:do_checkin
set folder=%1

echo Adding sources in %folder%
pushd ..

set roptions=/xj /s /e /purge /xd generated-docs ipch fltkdll-Debug* fltkdll-Release* fltkdlld fltkpng-Debug* fltkpng-Release* fltkjpeg-Debug* fltkjpeg-Release* fltkzlib-Debug* fltkzlib-Release* release* debug* windows_nt* .git .vs data logs install obj bin /xf *.swp *.sdf *.suo *.bak *.log *.tlog *.VC.* *.pdb

if "%folder%" == "3rdParty" set roptions=/xj /s /e /purge /xd .git

robocopy %folder% %TMPFOLDER%\%folder% %roptions%

popd

goto :eof



:error_no_zipfile
echo ERROR - no zipfile specified
goto :eof
