MASTERROOT		= ..
PROJROOT        = .
MASTERROOTFILES	= ../Makefile ../Makefile.defs ../gitall ../mkrelease

include $(MASTERROOT)/build/defs/Makefile.defs
include $(PROJROOT)/makein/make.defs

include Makefile.defs

all: $(MAKEIN) $(MASTERROOTFILES)
	$(SILENT)$(MAKEIN) -m $(MAKE) -f Makefile $(DIRS)

clean: $(MAKEIN)
	$(SILENT)$(MAKEIN) -m $(MAKE) -f Makefile -t clean $(DIRS)
	$(SILENT)rm -rf bin

../mkrelease: masterroot/mkrelease
	$(SILENT)cp masterroot/mkrelease ..

../gitall: masterroot/gitall
	$(SILENT)cp masterroot/gitall ..

../Makefile: masterroot/Makefile
	$(SILENT)cp masterroot/Makefile ..

../Makefile.defs: masterroot/mkdefs
	$(SILENT)masterroot/mkdefs .. > ../Makefile.defs

# Need to include this after the main targets are defined.
include $(PROJROOT)/makein/make.build
