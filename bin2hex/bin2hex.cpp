/*
**	rm.cpp		A simple uname command tool (unix remove) for the BDF
**				for windows.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>



void
usage()
		{
		fprintf(stderr, "usage: bin2hex binary_number [padwidth]\n");
		exit(1);
		}


int
main(int argc, char **argv)
		{
		if (argc < 2) usage();

		int		w=0;
		int		v=0;
		int		l;
		char	*s, nstr[32];

		s = argv[1];
		while (*s)
			{
			if (*s != '0' && *s != '1') usage();
			v <<= 1;
			v |= (*s=='1')?1:0;
			s++;
			}

		if (argc > 2) w = atoi(argv[2]);
		sprintf(nstr, "%x", v);

		l = (int)strlen(nstr);
		while (l < w)
			{
			putchar('0');
			l++;
			}
		puts(nstr);

		return 0;
		}
