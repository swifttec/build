PROJROOT = ..
MASTERROOT = ..\..

!include $(PROJROOT)\defs\win32.defs
!include win32.mak.defs

all: $(MAKEIN)

clean:
	@-rd /s /q $(OBJDIR)

!include win32.mak.build
