/*
** makein.cpp	Utility script to simply making in a directory
**				or set of directories.
**
** This program was originally built for use in windows where recursive
** makes we difficult due to the limitations of batch files, but has
** since been extended to include unix to make large recursive makefile
** structure for large projects easier to create.
**
** This program was orignally called "dmake" but this was changed due to
** a name clash with sun's distributed make program.
**
** NOTE: We cannot use the facilities of common since we may be made
** before common is built.
*/

#if defined(WIN32) || defined(_WIN32)
	#include <windows.h>
	#include <io.h>
#else
	#include <unistd.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

const char	*makefile="";
const char	*makeprog="";
const char	*makeopts="";
char		old_dir[1024];
int			verbose=1;
int			doit=1;
int			level=0;
int			debug=0;

/*
** Gets the current working directory
*/
void
getCurrentDirectory(char *dir, int dirsize)
		{
#if defined(WIN32) || defined(_WIN32)
		if (!GetCurrentDirectory(dirsize, dir)) *dir = 0;
#else
		if (getcwd(dir, dirsize) == NULL) *dir = 0;
#endif
		}

/**
*** Set the current working directory
***
*** @return	true is successful, false otherwise.
**/
int
setCurrentDirectory(const char *dir)
		{
		int	result=0;

#if defined(WIN32) || defined(_WIN32)
		if (!SetCurrentDirectory(dir)) result = GetLastError();
#else
		if (chdir(dir) != 0) result = errno;
#endif

		return result;
		}


static void
printf_and_flush(const char *fmt, ...)
		{
		va_list	ap;

		va_start(ap, fmt);
		vprintf(fmt, ap);
		va_end(ap);

		fflush(stdout);
		}

static void
fprintf_and_flush(FILE *f, const char *fmt, ...)
		{
		va_list	ap;

		va_start(ap, fmt);
		vfprintf(f, fmt, ap);
		va_end(ap);

		fflush(f);
		}

#define printf	printf_and_flush
#define fprintf	fprintf_and_flush


void
usage()
		{
		fprintf(stderr, "usage: makein [-m makeprog] [-f makefile] [-t target] dir1 ...\n");
		exit(1);
		}


int
domake(const char *directory, int ntgts, char **tlist)
		{
		char	targets[4096];
		char	cmdline[4096];
		char	dmake_dir[1024];
		int		i, r=0;
		char	tmp[1024];

		getCurrentDirectory(tmp, sizeof(tmp));
		if ((r = setCurrentDirectory(directory)) == 0)
			{
			*cmdline = 0;
			*targets = 0;

			if (*makeprog)
				{
				strcat(cmdline, makeprog);
				}
			else
				{
#if defined(WIN32) || defined(_WIN32)
				strcat(cmdline, "nmake -nologo");
#else
				strcat(cmdline, "make");
#endif
				}

			if (*makeopts)
				{
				strcat(cmdline, " ");
				strcat(cmdline, makeopts);
				}

			if (*makefile)
				{
				strcat(cmdline, " -f ");
				strcat(cmdline, makefile);
				}
			else
				{
#if defined(WIN32) || defined(_WIN32)
				if (access("win32.mak", 0) == 0)
					{
					strcat(cmdline, " -f win32.mak");
					}
#endif
				}

			for (i=0;i<ntgts;i++)
				{
				strcat(cmdline, " ");
				strcat(cmdline, tlist[i]);

				if (*targets) strcat(targets, " ");
				strcat(targets, tlist[i]);
				}

			if (verbose)
				{
				printf("\n%-*s========== Making%s%s in %s%s%s ==========\n", level*2, "", *targets?" ":"", targets, old_dir, *old_dir?"/":"", directory);
				}

			if (debug) printf("%s\n", cmdline);

#if defined(WIN32) || defined(_WIN32)
			if (*old_dir) sprintf(dmake_dir, "%s/%s", old_dir, directory);
			else sprintf(dmake_dir, "%s", directory);

			r = SetEnvironmentVariable("MAKEIN_DIR", dmake_dir);
#else
			if (*old_dir) sprintf(dmake_dir, "MAKEIN_DIR=%s/%s", old_dir, directory);
			else sprintf(dmake_dir, "MAKEIN_DIR=%s", directory);

			putenv(dmake_dir);
#endif

			r = system(cmdline);

#if !defined(WIN32) && !defined(_WIN32)
			r = r / 256;
#endif

			if (debug) printf("r = %d\n", r);
			setCurrentDirectory(tmp);

			if (verbose)
				{
				printf("%-*s========== Make in %s%s%s exited with status %d ==========\n", level*2, "", old_dir, *old_dir?"/":"", directory, r);
				}
			}
		else
			{
			fprintf(stderr, "ERROR: Can't change to directory %s\n", directory);
			}

		return r;
		}


int
main(int argc, char **argv)
		{
		char	*dirlist[256], *tgtlist[256];
		int		i, r, ndirs=0, ntgts=0;

		for (i=1;i<argc;i++)
			{
			if (strcmp(argv[i], "-q") == 0) verbose = 0;
			else if (strcmp(argv[i], "-n") == 0) doit = 0;
			else if (strcmp(argv[i], "-d") == 0) debug = 1;
			else if (strcmp(argv[i], "-m") == 0)
				{
				if (++i >= argc) usage();
				makeprog = argv[i];
				}
			else if (strcmp(argv[i], "-f") == 0)
				{
				if (++i >= argc) usage();
				makefile = argv[i];
				}
			else if (strcmp(argv[i], "-o") == 0)
				{
				if (++i >= argc) usage();
				makeopts = argv[i];
				}
			else if (strcmp(argv[i], "-t") == 0)
				{
				if (++i >= argc) usage();
				tgtlist[ntgts++] = argv[i];
				}
			else
				{
				dirlist[ndirs++] = argv[i];
				}
			}

		char	*ep=getenv("MAKEIN_DIR");

		if (ep) strcpy(old_dir, ep);
		else *old_dir = 0;

		for (i=0,r=0;r==0&&i<ndirs;i++)
			{
			r = domake(dirlist[i], ntgts, tgtlist);
			}

		if (debug) printf("exiting with %d\n", r);
		return r;
		}
