@echo off

setlocal
if not exist masterroot/setbuildenv.bat goto :no_masterroot

copy /y masterroot\setbuildenv.bat ..

rem ######################################################################
rem General build script
rem ######################################################################

if not defined SW_BUILD_ENV call :do_setbuildenv
if not defined SW_BUILD_ENV goto :no_build_env

rem Use ANT in preference to make
if exist win32.mak goto :use_nmake

nmake -nologo %*
exit /b %errorlevel%


rem ######################################################################
rem NMAKE build - called if win32.mak is found
rem ######################################################################
:use_nmake
if exist win32.mak nmake -nologo -f win32.mak %*
exit /b %errorlevel%


:no_build_env
echo **** The build environment is not correctly set and the build cannot continue. ****
exit /b 1


:no_masterroot
echo **** masterroot not found or invalid. Please execute the build folder ****
exit /b 1


:do_setbuildenv
set SETBUILDENVDIR=

pushd .

:again
rem echo Looking in %CD%
if exist setbuildenv.bat set SETBUILDENVDIR=%CD%
if defined SETBUILDENVDIR goto :done

set lastcd=%CD%
cd ..
if %cd% == %lastcd% goto :done

goto :again

:done
popd

if not defined SETBUILDENVDIR goto :no_build_env

echo Calling setbuildenv.bat in %SETBUILDENV%
call %SETBUILDENVDIR%\setbuildenv %SW_MAKE_ENV%
