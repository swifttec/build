[Code]
function isArchX64: Boolean;
begin
  Result := Is64BitInstallMode and (ProcessorArchitecture = paX64);
end;

function isArchIA64: Boolean;
begin
  Result := Is64BitInstallMode and (ProcessorArchitecture = paIA64);
end;

function isArchX86: Boolean;
begin
  Result := not isArchX64 and not isArchIA64;
end;

function arch(Param: String): String;
begin
	case ProcessorArchitecture of
		paX86:	Result := 'x86';
		paX64:	Result := 'x64';
		paIA64:	Result := 'ia64';
	else
		Result := 'unknown';
	end;

	Result := 'x86';
end;

function isWindowsVersion(Major, Minor: Integer): Boolean;
var
  Version: TWindowsVersion;
begin
  GetWindowsVersionEx(Version);
  Result :=
    ((Version.Major = Major) and (Version.Minor >= Minor));
end;

function isWindowsVersionOrNewer(Major, Minor: Integer): Boolean;
var
  Version: TWindowsVersion;
begin
  GetWindowsVersionEx(Version);
  Result :=
    (Version.Major > Major) or
    ((Version.Major = Major) and (Version.Minor >= Minor));
end;

function isWindowsXPOrNewer: Boolean;
begin
  Result := isWindowsVersionOrNewer(5, 1);
end;

function isWindowsVistaOrNewer: Boolean;
begin
  Result := isWindowsVersionOrNewer(6, 0);
end;

function isWindows7 : Boolean;
begin
  Result := isWindowsVersion(6, 1);
end;

function isWindows7_x64 : Boolean;
begin
  Result := isWindowsVersion(6, 1) and isArchX64();
end;

function isWindows7OrNewer: Boolean;
begin
  Result := isWindowsVersionOrNewer(6, 1);
end;

function isWindows8OrNewer: Boolean;
begin
  Result := isWindowsVersionOrNewer(6, 2);
end;

function isWindows10OrNewer: Boolean;
begin
  Result := isWindowsVersionOrNewer(10, 0);
end;
