#!perl

use strict;

sub main
		{
		my	$res=1;
		my	$onlysetup=0;

		if (!defined($ARGV[0]))
			{
			print STDERR "usage: issupdate issfile\n";
			}
		elsif (open(IN, $ARGV[0]))
			{
			my $line;
			my $insetup=0;

			while ($line = <IN>)
				{
				chop $line;

				if ($line =~ /^\[(\w+)\]/)
					{
					$insetup = (!$onlysetup || $1 eq "Setup");
					}
				elsif ($insetup)
					{
					while ($line =~ /^(.*)\${([^}]+)}(.*)$/)
						{
						$line = "$1$ENV{$2}$3";
						}
					}

				print "$line\n";
				}

			close(IN);
			$res = 0;
			}
		else
			{
			print STDERR "Can't open $ARGV[0] for reading\n";
			}

		return 0;
		}


# Last line
exit main();
