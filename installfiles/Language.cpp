// Language.cpp: implementation of the Language class.
//
//////////////////////////////////////////////////////////////////////

#include "installfiles.h"
#include "Language.h"

#if defined(WIN32) || defined(_WIN32)
	#pragma warning(disable: 4996)
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Language	Language::table[] =
{
//	Short	Alternative		Full				Code	
	Language("AFR",	"AFR",	"Afrikaans",			1078	),
	Language("ALB",	"ALB",	"Albanian",			1052	),
	Language("ARA",	"ARA",	"Arabic (Saudi Arabia)",	1025	),
	Language("ARI",	"ARI",	"Arabic (Iraq)",		2049	),
	Language("ARE",	"ARE",	"Arabic (Egypt)",		3073	),
	Language("ARL", "ARL",	"Arabic (Libya)",		4097	),
	Language("ARG",	"ARG",	"Arabic (Algeria)",		5121	),
	Language("ARM",	"ARM",	"Arabic (Morocco)",		6145	),
	Language("ART",	"ART",	"Arabic (Tunisia)",		7169	),
	Language("ARO",	"ARO",	"Arabic (Oman)",		8193	),
	Language("ARY",	"ARY",	"Arabic (Yemen)",		9217	),
	Language("ARS",	"ARS",	"Arabic (Syria)",		10241	),
	Language("ARJ",	"ARJ",	"Arabic (Jordan)",		11265	),
	Language("ARN",	"ARN",	"Arabic (Lebanon)",		12289	),
	Language("ARK",	"ARK",	"Arabic (Kuwait)",		13313	),
	Language("ARU",	"ARU",	"Arabic (U.A.E.)",		14337	),
	Language("ARB",	"ARB",	"Arabic (Bahrain)",		15361	),
	Language("ARQ",	"ARQ",	"Arabic (Qatar)",		16385	),
	Language("BAS",	"BAS",	"Basque",			1069	),
	Language("BYE",	"BYE",	"Byelorussian",			1059	),
	Language("BUL",	"BUL",	"Bulgarian",			1026	),
	Language("CAT",	"CAT",	"Catalan",			1027	),
	Language("CHT",	"CHT",	"Chinese (Taiwan)",		1028	),
	Language("CHS",	"CHS",	"Chinese (PRC)",		2052	),
	Language("CHH",	"CHH",	"Chinese (Hong Kong S.A.R.)",	3076	),
	Language("CHG",	"CHG",	"Chinese (Singapore)",		4100	),
	Language("CRO",	"CRO",	"Croatian",			1050	),
	Language("CZE",	"CZE",	"Czech",			1029	),
	Language("DAN",	"DAN",	"Danish",			1030	),
	Language("DUT",	"DUT",	"Dutch (Standard)",		1043	),
	Language("DUB",	"DUB",	"Dutch (Belgium)",		2067	),
	Language("USA",	"ENU",	"English (US)",			1033	),
	Language("ENG",	"ENG",	"English (UK)",			2057	),
	Language("CAN",	"CAN",	"English (Canada)",		4105	),
	Language("NZL",	"NZL",	"English (New Zealand)",	5129	),
	Language("AUS",	"AUS",	"English (Australia)",		3081	),
	Language("IRE",	"IRE",	"English (Ireland)",		6153	),
	Language("STH", "STH",	"English (South Africa)",	7177	),
	Language("EST",	"EST",	"Estonian ",			1061	),
	Language("FAR",	"FAR",	"Faroese",			1080	),
	Language("FRS",	"FRS",	"Farsi",			1065	),
	Language("FIN",	"FIN",	"Finnish",			1035	),
	Language("FRN",	"FRA",	"French (Standard)",		1036	),
	Language("FRB",	"FRB",	"French (Belgium)",		2060	),
	Language("FRS",	"FRS",	"French (Switzerland)",		4108	),
	Language("FRC",	"FRC",	"French (Canada)",		3084	),
	Language("FRX",	"FRX",	"French (Luxembourg)",		5132	),
	Language("GRM",	"DEU",	"German (Standard)",		1031	),
	Language("GRS",	"GRS",	"German (Switzerland)",		2055	),
	Language("GRA",	"GRA",	"German (Austria)",		3079	),
	Language("GRX",	"GRX",	"German (Luxembourg)",		4103	),
	Language("GRL",	"GRL",	"German (Liechtenstein)",	5127	),
	Language("GRE",	"GRE",	"Greek",			1032	),
	Language("HEB",	"HEB",	"Hebrew",			1037	),
	Language("HUN",	"HUN",	"Hungarian",			1038	),
	Language("ICE",	"ICE",	"Icelandic",			1039	),
	Language("IND",	"IND",	"Indonesian",			1057	),
	Language("ITA",	"ITA",	"Italian",			1040	),
	Language("ITS",	"ITS",	"Italian (Switzerland)",	2064	),
	Language("JPN",	"JPN",	"Japanese",			1041	),
	Language("KRN",	"KRN",	"Korean",			1042	),
	Language("KRJ",	"KRJ",	"Korean (Johab)",		2066	),
	Language("LAT",	"LAT",	"Latvian",			1062	),
	Language("LIT",	"LIT",	"Lithuanian",			1063	),
	Language("NOB",	"NOB",	"Norwegian (Bokm�l)",		1044	),
	Language("NOR",	"NOR",	"Norwegian (Nynorsk)",		2068	),
	Language("POL",	"POL",	"Polish",			1045	),
	Language("PRT",	"PRT",	"Portuguese (Portugal)",	2070	),
	Language("PRB",	"PRB",	"Portuguese (Brazil)",		1046	),
	Language("ROM",	"ROM",	"Romanian",			1048	),
	Language("RUS",	"RUS",	"Russian",			1049	),
//	Language("SER",	"SER",	"Serbian (Latin)",		0	),
	Language("SLK",	"SLK",	"Slovak",			1051	),
	Language("SLV",	"SLV",	"Slovenian",			1060	),
	Language("SPX",	"SPX",	"Spanish (Mexico)",		2058	),
	Language("SPA",	"SPA",	"Spanish (Traditional Sort)",	1034	),
	Language("SPM",	"SPM",	"Spanish (Modern Sort)",	3082	),
	Language("SWE",	"SWE",	"Swedish",			1053	),
	Language("THA",	"THA",	"Thai",				1054	),
	Language("TUR",	"TUR",	"Turkish",			1055	),
	Language("UKR",	"UKR",	"Ukrainian",			1058	),
	Language(0, 0, 0, 0 )
};


// Find a language by code
Language *
Language::find(int code)
		{
		Language	*lp;

		for (lp=table;lp->_shortname;lp++)
			{
			if (lp->_code == code) return lp;
			}

		return NULL;
		}


// Find a language by string
Language *
Language::find(const char *code)
		{
		Language	*lp;

		for (lp=table;lp->_shortname;lp++)
			{
			if (strcasecmp(code, lp->_shortname) == 0 || strcasecmp(code, lp->_fullname) == 0 || strcasecmp(code, lp->_altname) == 0) return lp;
			}

		return find(atoi(code));
		}


// Print the language table
void
Language::printTable()
		{
		Language    *lp = Language::table;

		while (lp->_shortname)
			{
			printf("%-3s %-32s %5d\n", lp->shortname(), lp->fullname(), lp->code());
			lp++;
			}
		}
