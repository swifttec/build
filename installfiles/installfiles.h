/*
** installfiles.h	Common header file
*/

// TODO: reference additional headers your program requires here
#include "Xtypes.h"
#include "XString.h"
#include "XTime.h"
#include "XStringArray.h"
#include "XPointerArray.h"
#include "XTokenizer.h"
#include "XFilename.h"

#include "XFolder.h"
#include "XFileInfo.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

#if !defined(WIN32) && !defined(_WIN32)
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <unistd.h>
#else
	#include <io.h>
#endif
//#include <direct.h>

#include "Language.h"
