// Language.h: interface for the Language class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LANGUAGE_H__580D95D5_9EEB_4333_ACB1_E2EC12F44269__INCLUDED_)
#define AFX_LANGUAGE_H__580D95D5_9EEB_4333_ACB1_E2EC12F44269__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class Language  
		{
public:
		Language(const char *s, const char *a, const char *f, int c)	{ _shortname=s; _altname=a; _fullname=f; _code=c; }

		const char *	shortname() const	{ return _shortname; }
		const char *	altname() const		{ return _altname; }
		const char *	fullname() const	{ return _fullname; }
		int				code() const		{ return _code; }

public:
		static Language *	find(int code);
		static Language *	find(const char *str);
		static void			printTable();

public:
		static Language	table[];

private:
		const char	*_shortname;	// The short name for the language (e.g. USA)
		const char	*_altname;		// The alternate short name for the language (e.g. ENU)
		const char	*_fullname;		// The full name for the language (English)
		int			_code;			// The code number
		};

#endif // !defined(AFX_LANGUAGE_H__580D95D5_9EEB_4333_ACB1_E2EC12F44269__INCLUDED_)
