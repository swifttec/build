PROJROOT	= ..

include ../win32.mak.tools
include Makefile.defs

PROGNAME_1	= $(PROGNAME)
PROGNAME_2	= cp
PROG_1		= $(TOOLSBIN)\$(PROGNAME_1).exe
PROG_2		= $(TOOLSBIN)\$(PROGNAME_2).exe

TARGETS		= \
	$(PROG_1) \
	$(PROG_2) \


all: $(TARGETS)

!if [$(TOOLSBIN)\mkobjnames $(OBJDIR) $(OBJDIR)\localobjnames.inc LOCAL_OBJS $(SRCS)]
!endif

!include $(OBJDIR)\localobjnames.inc

OBJS=$(LOCAL_OBJS) $(BASE_OBJS) $(IO_OBJS) $(CMD_OBJS)

clean:
	-$(RM) -rf $(OBJDIR) $(TARGETS)


$(PROG_1): $(OBJS) ..\common\$(OBJDIR)\common.lib
	@if not exist $(TOOLSBIN) mkdir $(TOOLSBIN)
	cl -nologo -Fe$@ $(OBJS) setargv.obj -link -libpath:..\common\$(OBJDIR) common.lib

$(PROG_2): $(PROG_1)
	copy $(PROG_1) $@
