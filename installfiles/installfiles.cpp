/**
*** cp.cpp  A unix-like copy command
**/

#include "installfiles.h"


//using namespace X_BASE_NAMESPACE;
//using namespace X_IO_NAMESPACE;


#ifndef _WIN32
	#define O_BINARY	0
	#define _S_IREAD	S_IREAD
	#define _S_IWRITE	S_IWRITE
#else
	#pragma warning(disable: 4996)
#endif

#define PROG_MODE_CP		0
#define PROG_MODE_INSTALLFILES	1

int	prog_mode=0;	// Program mode


bool	iflag=false;	// Interactive flag, prompt before overwrite if true
bool	vflag=false;	// Verbose mode
bool	dflag=false;	// Target is a directory
bool	pflag=false;	// Create parent directories as required.
bool	fflag=false;	// Attempt to overwrite read-only files
bool	kflag=false;	// Keep date/time of file on overwrite
bool	rflag=false;	// Recursive flag - if from is a directory it is copied recursively
bool	cflag=false;	// Copy contents of specified directory (used with -r)

bool	bflag=false;	// Installfiles Mode: Banner flag for  - prints banner
//bool	sflag=false;	// Installfiles Mode: Create a symlink, don't do a copy
//bool	lflag=false;	// Installfiles Mode: Create a link, don't do a copy
bool	tflag=false;	// Installfiles Mode: Only copy the file if changed (t=test file)

int	errorcount=0;	// The number of errors encountered

#if defined(WIN32) || defined(_WIN32)
	// These O/S have the stat64 and lstat64 interface
	typedef struct _stati64 x_stat_t;
	#define x_stat		::_tstati64
	#define x_lstat	::_tstati64
#else
	// These O/S have the stat and lstat interface
	typedef struct stat x_stat_t;
	#define x_stat		::stat
	#define x_lstat	::lstat
#endif



/**
*** Test to see if the given filename is a file
**/
int
x_file_mode(const XString &filename)
		{
		int			res=0666;

#if defined(WIN32) || defined(_WIN32)
		XFileInfo	finfo(filename);

		res = (finfo.isReadOnly()?0555:0777);
#else
		x_stat_t	st;

		if (x_stat(filename, &st) >= 0)
			{
			res = st.st_mode;
			}

#endif

		return res;
		}




/*
** The "copy" function
**
** Copies from a file to another file.
*/
void
copy(XString from, XString to)
		{
		XTime	fromWriteTime;
		XTime	fromCreateTime;
		XTime	fromAccessTime;
		// int	fr;
		int64_t	nbytes=0;
		int	keptdate=0;

		XFileInfo	frominfo(from);

		if (!frominfo.isFile(from))
			{
			printf("%s is not a file\n", (const char *)from);
			errorcount++;
			return;
			}

		XFileInfo	toinfo(to);
		XFileInfo::getFileTimes(from, &fromWriteTime, &fromCreateTime, &fromAccessTime);

		if (toinfo.isFile(to))
			{
			// printf("tflag=%d\n", tflag);
			// printf("size: from %d to %d\n", frominfo.getSize(), toinfo.getSize());
			// printf("time: from %d to %d\n", frominfo.getLastModificationTime().toUnixTime(), toinfo.getLastModificationTime().toUnixTime());

			if (tflag
				&& toinfo.getSize() == frominfo.getSize()
				&& toinfo.getLastModificationTime() >= frominfo.getLastModificationTime()
				)
				{
				// Files are the same - ignore
				return;
				}

			if (kflag)
				{
				// We want to keep the original time on the file. (introduced to fool nmake)
				XFileInfo::getFileTimes(to, &fromWriteTime, &fromCreateTime, &fromAccessTime);
				keptdate = 1;
				}

			if (iflag)
				{
				XString	line;

				printf("Overwrite %s ? ", (const char *)to);
				line.readLine(stdin);
				if (line.length() == 0 || (line[0] != 'Y' && line[0] != 'y')) return;
				}
			}

		// Need to make a copy
		int	in, out;
		bool	ok=false;

		in = open(from, O_RDONLY|O_BINARY, 0);
		if (in < 0)
			{
			printf("Can't open %s for reading - errno=%d\n", (const char *)from, errno);
			errorcount++;
			return;
			}
		
		if (XFileInfo::isFile(to))
			{
			if (!XFileInfo::isWriteable(to))
				{
				if (!fflag)
					{
					printf("%s is read-only\n", (const char *)to);
					errorcount++;
					return;
					}

				// Try to make it writeable
				chmod(to, 0666);
				}

			/*
			if (unlink(to) < 0)
				{
				UString msg;

				msg.Format(_T("unlink failed [%d]"), GetLastError());
				errorcount++;
				return;
				}
			*/
			}

		out = open(to, O_WRONLY|O_TRUNC|O_CREAT|O_BINARY, x_file_mode(from));
		if (out < 0 && fflag)
			{
			unlink(to);
			out = open(to, O_WRONLY|O_TRUNC|O_CREAT|O_BINARY, x_file_mode(from));
			}

		if (out < 0)
			{
			close(in);

			printf("Can't open %s for writing - errno=%d\n", (const char *)to, errno);
			errorcount++;
			return;
			}

		// OK now do the copy
	#define COPYBUFSIZE 0x8000
		int	n;
		char	*buf;
		
		buf = new char[COPYBUFSIZE];

		if (vflag)
			{
			if (prog_mode == PROG_MODE_CP) printf("%s -> %s", (const char *)from, (const char *)to);
			else
				{
				if (bflag) printf("  %s", (const char *)from);
				else printf("Installing %s in %s", (const char *)from, (const char *)XFilename::dirname(to));
				}
			}

		for (;;)
			{
			n = read(in, buf, COPYBUFSIZE);
			if (n < 0)
				{
				printf("%sFailed to read from %s - errno=%d\n", vflag?"\n":"", (const char *)from, errno);
				errorcount++;
				break;
				}

			if (n > 0)
				{
				int r = write(out, buf, n);
				if (r != n)
					{
					printf("%sFailed to write to %s - errno=%d\n", vflag?"\n":"", (const char *)to, errno);
					errorcount++;
					break;
					}
				
				nbytes += n;
					}
			
			if (n != COPYBUFSIZE)
				{
				ok = true;
				break;
				}
			}

		close(in);
		close(out);

		if (ok)
			{
#if defined(WIN32) || defined(_WIN32)
			// Need to be writeable to set the file times on Windows
			chmod(to, 666);
			XFileInfo::setFileTimes(to, &fromWriteTime, &fromCreateTime, &fromAccessTime);
#else
			XFileInfo::setFileTimes(to, &fromWriteTime, &fromCreateTime, &fromAccessTime);
#endif

			chmod(to, x_file_mode(from));

			if (vflag)
				{
				const char	*qual[] = { " bytes", "K", "M" };
				double	div[] = { 1, 1024.0, 1024.0*1024.0 };
				int	qi=0;

				for (qi=0;qi<2;qi++)
					{
					if (((double)nbytes/div[qi]) < 1024.0) break;
					}

				printf(qi==0?" (%.0f%s)%s\n":" (%.1f%s)%s\n", (double)nbytes/div[qi], qual[qi], keptdate?"****":"");
				}
			}

		delete buf;
		}


// Check the directory exists and create it if not (only with
// the appropriate flags)
bool
checkDir(const XString &dname, bool createFlag)
		{
		XString	dir=XFilename::fix(dname, XFilename::PathTypePlatform);

		if (!XFileInfo::isDirectory(dir) && createFlag)
			{
			// If the user has requested a directory be made AND the is a file
			// of the same name AND the user has specified -f try removing the file.
			if (XFileInfo::isFile(dir) && fflag)
				{
				if (vflag) printf("Removing file %s\n", (const char *)XString(dir));
				unlink(XString(dir));
				}

			x_status_t	r;

			if ((r = XFolder::create(dir, pflag)) != 0)
				{
				printf("Can't create directory %s - errno=%d\n", (const char *)XString(dir), r);
				exit(1);
				}
			else if (vflag) printf("Creating directory %s\n", (const char *)XString(dir));
			}

		return XFileInfo::isDirectory(dir);
		}


/*
** Called to copy a file to a directory
** This function will ensure the directory exists.
*/
void
copyFileToDir(const XString &filename, const XString &destdir, int level=0)
		{
		XString    fromfile = XFilename::fix(filename, XFilename::PathTypePlatform);
		XString    tofile = XFilename::concatPaths(XFilename::fix(destdir, XFilename::PathTypePlatform), XFilename::basename(filename));
		
		checkDir(destdir, dflag);
		if (XFileInfo::isDirectory(fromfile) && rflag)
			{
			XFileInfo	de;
			XFolder	dir;

			// Make sure we can copy to the directory
			if (!(cflag && level == 0) && !checkDir(tofile, true)) return;

			if (dir.open(fromfile))
				{
				while (dir.read(de) == 0)
					{
					if (de.getName() == "." || de.getName() == "..") continue;
					
					if (cflag && level == 0)
						{
						// User has specified the -c flag
						// so we want to copy the contents of the directory
						// to the specified directory and not create a sub-directory
						// at the top level.
						copyFileToDir(de.getFullPathname(), destdir, level+1);
						}
					else copyFileToDir(de.getFullPathname(), tofile, level+1);
					}
				}
			}
		else
			{
			copy(fromfile, tofile);
			}
		}


/*
** Copy file to destination dir, taking care of any language
** substitution provided in langlist
*/
void
do_copy(const XString &filename, const XString &todir, XPointerArray &langlist)
		{
		XString    fromfile, destdir;
		char		lcstr[32];
		Language    *lp;
		int	    l;

		if (langlist.count() == 0) copyFileToDir(filename, todir);
		else
			{
			for (l=0;l<langlist.count();l++)
				{
				lp = (Language *)langlist[l];
				fromfile = filename;
				destdir = todir;

				while (fromfile.contains("[LANG]")) fromfile.Replace("[LANG]", lp->shortname());
				while (fromfile.contains("[ALTLANG]")) fromfile.Replace("[ALTLANG]", lp->altname());
				while (fromfile.contains("[LANGNAME]")) fromfile.Replace("[LANGNAME]", lp->fullname());
				sprintf(lcstr, "%d", lp->code());
				while (fromfile.contains("[LANGCODE]")) fromfile.Replace("[LANGCODE]", lcstr);
				sprintf(lcstr, "%04x", lp->code());
				while (fromfile.contains("[LANGCODEHEX]")) fromfile.Replace("[LANGCODEHEX]", lcstr);

				while (destdir.contains("[LANG]")) destdir.Replace("[LANG]", lp->shortname());
				while (destdir.contains("[ALTLANG]")) destdir.Replace("[ALTLANG]", lp->altname());
				while (destdir.contains("[LANGNAME]")) destdir.Replace("[LANGNAME]", lp->fullname());
				sprintf(lcstr, "%d", lp->code());
				while (destdir.contains("[LANGCODE]")) destdir.Replace("[LANGCODE]", lcstr);
				sprintf(lcstr, "%04x", lp->code());
				while (destdir.contains("[LANGCODEHEX]")) destdir.Replace("[LANGCODEHEX]", lcstr);

				if (!checkDir(destdir, dflag))
					{
					printf("%s is not a directory\n", (const char *)destdir);
					exit(1);
					}

				copyFileToDir(fromfile, destdir);
				}
			}
		}




void
copy_usage()
		{
		printf("usage:\n");
		printf("\tcp [-ivf] source-file target-file\n");
		printf("\tcp [options] [--langlist langlist] source-file ... target-dir\n");
		printf("where options are\n");
		printf("    -i               Interactive mode, prompt when overwriting files\n");
		printf("    -v               Verbose mode\n");
		printf("    -f               Attempt to force overwrite on read-only files/dirs\n");
		printf("    -d               Force target to be directory. Will be created if necessary\n");
		printf("    -r               Recursively copy a directory.\n");
		printf("    -c               Copy contents of top-level directory for recursive copy\n");
		printf("                     rather than creating a sub-directory of the same name\n");
		printf("    --langlist list  Specify langlist and expansion of language codes\n");
		printf("                     [LANG] & [LANGCODE]. (e.g. --langlist USA,FRN,ITA)\n");
		printf("    --print-langtab  Print langlist\n");
		exit(1);
		}



int
copy_main(int argc, char* argv[])
		{
		int		i=1;
		char		*s;
		XString	target;
		XPointerArray	langlist;
		XStringArray	dirlist;
		char		*todir;

		prog_mode = PROG_MODE_CP;

		// Check for options
		while (i < argc && argv[i][0] == '-')
			{
			if (strcmp(argv[i], "--print-langtab") == 0)
				{
				// Print the language list
				Language::printTable();
				exit(0);
				}
			else if (strcmp(argv[i], "--langlist") == 0)
				{
				if (++i < argc)
					{
					XTokenizer	tok(argv[i], " \t,;:");

					while (tok.hasMoreTokens())
						{
						XString    lang;
						Language    *lp;

						lang = tok.nextToken();
						if ((lp = Language::find((const char *)lang)) != NULL) langlist.add(lp);
						else
							{
							printf("Unknown language '%s'\n", (const char *)lang);
							exit(1);
							}
						}
					
					dflag = true;
					}
				else copy_usage();
				}
			else
				{
				for (s=argv[i]+1;*s;s++)
					{
					switch (*s)
						{
						case 'i':	iflag=1;	break;
						case 'v':	vflag=1;	break;
						case 'f':	fflag=1;	break;
						case 'd':	dflag=1;	break;
						case 'k':	kflag=1;	break;
						case 'p':	pflag=1;	break;
						case 'r':	rflag=1;	break;
						case 'c':	cflag=1;	break;
						default:	copy_usage();	break;
						}
					}
				}

			i++;
			}

		// We need at least 2 more args for source and target
		int filecount=argc-i-1;

		if (filecount <= 0) copy_usage();

		todir = argv[argc-1];
		if (filecount > 1 || XFileInfo::isDirectory(todir) || dflag)
			{
			// Either the target is already a directory,
			// or there is more than one file to copy
			// or the user has requested that the target be a directory

			// Copy files to a directory
			while (i < argc-1)
				{
				do_copy(argv[i], todir, langlist);
				i++;
				}
			}
		else
			{
			// Copy source file to target file
			if (!checkDir(XFilename::dirname(argv[i+1]), pflag))
				{
				printf("The directory for %s does not exist\n", argv[i+1]);
				exit(1);
				}
			else copy(argv[i], argv[i+1]);
			}

		return errorcount?1:0;
		}





void
install_usage()
		{
		printf("usage: installfiles [-brc] [--langlist list] [--banner type] dir [files]\n");
		printf("where\n");
		printf("    -b               Print banner\n");
		printf("    -t               Test files before copying and only copy if changed\n");
		printf("    -r               Recursive copy of directories\n");
		printf("    -c               Copy contents of top-level directory for recursive copy\n");
		printf("                     rather than creating a sub-directory of the same name\n");
		printf("    --banner type    Print a banner with the string 'Installling type files...\n");
		printf("    --langlist list  Specify langlist and expansion of language codes\n");
		printf("                     [LANG] & [LANGCODE]. (e.g. --langlist USA,FRN,ITA)\n");
		printf("    --print-langtab  Print langlist\n");
		exit(1);
		}


int
install_main(int argc, char* argv[])
		{
		XPointerArray	langlist;
		XString	arg, dir, file;
		int		argno=1, gotdir=0;
		int		doneBanner=0;
		XString	bannerType;

		prog_mode = PROG_MODE_INSTALLFILES;

		if (argc <= 1) install_usage();
		
		// Set up flags for install mode (verbose, force)
		vflag = true;
		fflag = true;
		pflag = true;
		dflag = true;

		for (argno=1;argno<argc;argno++)
			{
			arg = argv[argno];

			if (arg == "--help" || arg == "-?" || arg == "/?") install_usage();
			else if (strcmp(argv[argno], "--print-langtab") == 0)
				{
				// Print the language list
				Language::printTable();
				exit(0);
				}
			else if (strcmp(argv[argno], "--banner") == 0)
				{
				bflag = true;
				if (++argno < argc) bannerType = argv[argno];
				else install_usage();
				}
			else if (strcmp(argv[argno], "--langlist") == 0)
				{
				if (bannerType.isEmpty()) bannerType = "language";
				if (++argno < argc)
					{
					XTokenizer	tok(argv[argno], " \t,;:");

					while (tok.hasMoreTokens())
						{
						XString    lang;
						Language    *lp;

						lang = tok.nextToken();
						if ((lp = Language::find(lang)) != NULL) langlist.add(lp);
						else
							{
							printf("Unknown language '%s'\n", (const char *)lang);
							exit(1);
							}
						}
					}
				else install_usage();
				}
			else if (arg[0] == '-')
				{
				const char  *sp = (const char *)arg + 1;

				while (*sp)
					{
					switch (*sp)
						{
						case 'b':	bflag = true;	break;
						case 'r':	rflag = true;	break;
						case 'c':	cflag = true;	break;
						case 't':	tflag = true;	break;

						default:
							install_usage();
							break;
						}
					
					sp++;
					}
				}
			else if (!gotdir)
				{
				gotdir = 1;
				dir = argv[argno];
				}
			else
				{
				if (bflag && !doneBanner)
					{
					doneBanner = 1;

					printf("##########################################################################\n");
					
					printf("## Installing");
					if (!bannerType.isEmpty()) printf(" %s", (const char *)bannerType);
					printf(" files into %s", (const char *)dir);
					if (langlist.count() > 0)
						{
						Language    *lp;

						printf(" [");
						for (int l=0;l<langlist.count();l++)
							{
							lp = (Language *)langlist[l];
							printf("%s%s", l?",":"", lp->shortname());
							}
						printf("]");
						}
					
					printf("\n##########################################################################\n");
					}

				do_copy(argv[argno], dir, langlist);
				}
			}

		return errorcount?1:0;
		}






int
main(int argc, char *argv[])
		{
		XString    prog, ext;
		int	    r;

		// Because cp and installfiles functionality are embedded in the same program
		// we need to figure out which we are and call the appropriate _main function.
		XFilename::split(XFilename::basename(argv[0]), prog, ext);

		// printf("prog='%s'\n", (const char *)prog);
		if (prog.equalsIgnoreCase("installfiles") || prog.equalsIgnoreCase("insfiles")) r = install_main(argc, argv);
		else r = copy_main(argc, argv);

		return r;
		}
