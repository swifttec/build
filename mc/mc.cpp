/*
** mc.cpp	Unix equivilent to the Windows Message Compiler
**
** We need to use this for the cbase library so we cannot use
** any of the CA classes otherwise bootstrapping a new platform
** will be difficult.
*/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

// Common includes
#include "XString.h"


#define ANSI	0
#define UNICODE	1

// Types
typedef struct
		{
		XString	name;		// Internal name
		int		value;		// Internal value
		XString	extname;	// External name
		int		count;
		} NameRecord;


class StatusCode
		{
public:
		StatusCode(int id, int nstr);
		~StatusCode();

		int				id() const		{ return _id; }

		void			setText(int idx, const XString &text)	{ _textArray[idx] = text; }
		const XString &	getText(int idx)						{ return _textArray[idx]; }

private:
		unsigned int	_id;		// Internal value
		XString			*_textArray;	// External name
		};

StatusCode::StatusCode(int id, int nstr) :
	_id(id)
		{
		_textArray = new XString[nstr];
		}

StatusCode::~StatusCode()
		{
		delete [] _textArray;
		}



// Globals
int		intype=ANSI;
int		outtype=UNICODE;
int		custflag=0;
XString	hdrext="h";
XString	hdrdir=".";
XString	rcdir=".";
int		bflag=0;
int		sflag=0;
int		verbose=0;
int		defoutputbase=16;

int		warnCount = 0;
int		errorCount = 0;
int		lineno = 0;
XString	filename;
XString	cast;

NameRecord	**severityList=0;
NameRecord	**facilityList=0;
NameRecord	**languageList=0;
int			severityCount=0;
int			facilityCount=0;
int			languageCount=0;
int			severityCapacity=0;
int			facilityCapacity=0;
int			languageCapacity=0;

StatusCode	**codeList=0;
int			codeCount=0;
int			codeCapacity=0;

NameRecord *
findNameRecord(NameRecord **&list, int &count, const char *name)
		{
		NameRecord	*np;
		int			i, found=0;

		for (i=0;i<count;i++)
			{
			np = list[i];
			if (np->name.CompareNoCase(name) == 0)
				{
				found = 1;
				break;
				}
			}

		if (!found) np = NULL;

		return np;
		}


void
addNameRecord(NameRecord **&list, int &count, int &capacity, const char *name, int value, const char *extname)
		{
		NameRecord	*np;
		int			i, found=0;

		for (i=0;i<count;i++)
			{
			np = list[i];
			if (np->name.CompareNoCase(name) == 0)
				{
				found = 1;
				break;
				}
			}

		if (!found)
			{
			if (capacity == 0)
				{
				capacity = 8;
				list = new NameRecord *[capacity];
				}
			else
				{
				NameRecord	**oldlist = list;
				capacity += 8;
				list = new NameRecord *[capacity];
				memcpy(list, oldlist, count * sizeof(NameRecord *));
				delete oldlist;
				}

			list[count++] = np = new NameRecord;
			}

		if (np != NULL)
			{
			np->name = name;
			np->value = value;
			np->extname = extname;
			np->count = 0;
			}
		}


void
addSeverity(const char *name, int value, const char *extname)
		{
		addNameRecord(severityList, severityCount, severityCapacity, name, value, extname);
		}


void
addFacility(const char *name, int value, const char *extname)
		{
		addNameRecord(facilityList, facilityCount, facilityCapacity, name, value, extname);
		}


void
addLanguage(const char *name, int value, const char *extname)
		{
		addNameRecord(languageList, languageCount, languageCapacity, name, value, extname);
		}


void
clearList(NameRecord **&list, int &count)
		{
		int		i;

		for (i=0;i<count;i++) delete list[i];
		count = 0;
		}


NameRecord *
findSeverity(const char *name)
		{
		return findNameRecord(severityList, severityCount, name);
		}


NameRecord *
findFacility(const char *name)
		{
		return findNameRecord(facilityList, facilityCount, name);
		}


int
findLanguageIdx(const char *name)
		{
		NameRecord	*np;
		int			i;

		for (i=0;i<languageCount;i++)
			{
			np = languageList[i];
			if (np->name == name) return i;
			}

		return -1;
		}


int
valueOf(const char *s)
		{
		int		v;

		if (strncmp(s, "0x", 2) == 0) sscanf((const char *)s, "0x%x", &v);
		else v = atoi(s);

		return v;
		}


char *
numstr(int n, int b)
		{
		static char	nstr[32];

		if (b == 10) sprintf(nstr, "%d", n);
		else sprintf(nstr, "0x%X", n);

		return nstr;
		}


void
addStatusCode(FILE *hdr, int msgid, const XString &name, const XString &text, const XString &comment, int base, int langidx)
		{
		if (codeCount == 0)
			{
			// Write out the definitions of facilities and severities
			NameRecord	*np;
			int			i, first;

			for (i=0,first=1;i<facilityCount;i++)
				{
				np = facilityList[i];
				if (!np->extname.isEmpty())
					{
					if (first)
						{
						fprintf(hdr, "\n/*\n** Define the facilty codes\n*/\n");
						first = 0;
						}
					fprintf(hdr, "#define %s\t%s\n", (const char *)np->extname, numstr(np->value, defoutputbase));
					}
				}

			for (i=0,first=1;i<severityCount;i++)
				{
				np = severityList[i];
				if (!np->extname.isEmpty())
					{
					if (first)
						{
						fprintf(hdr, "\n/*\n** Define the severity codes\n*/\n");
						first = 0;
						}
					fprintf(hdr, "#define %s\t%s\n", (const char *)np->extname, numstr(np->value, defoutputbase));
					}
				}

			fprintf(hdr, "\n\n");
			}

		XString	caststr;

		if (!cast.isEmpty())
			{
			caststr = "(";
			caststr += cast;
			caststr += ")";
			}

		if (!comment.isEmpty()) fprintf(hdr, "%s\n", (const char *)comment);
		fprintf(hdr, "//\n");
		fprintf(hdr, "// MessageId: %s\n", (const char *)name);
		fprintf(hdr, "//\n");
		fprintf(hdr, "// MessageText:\n");
		fprintf(hdr, "//\n");
		XString	tmp=text;

		while (!tmp.isEmpty())
			{
			int	p;

			p = tmp.first('\n');
			if (p < 0)
				{
				fprintf(hdr, "//  %s\n", (const char *)tmp);
				break;
				}
			else
				{
				fprintf(hdr, "//  %s\n", (const char *)tmp.left(p));
				tmp.remove(0, p+1);
				}
			}

		fprintf(hdr, "//\n");
		fprintf(hdr, "#define %s\t(%s%s)\n\n", (const char *)name, (const char *)caststr, numstr(msgid, base));

		StatusCode	*pCode;
		int			i, found=0;

		for (i=0;i<codeCount;i++)
			{
			pCode = codeList[i];
			if (pCode->id() == msgid)
				{
				found = 1;
				break;
				}
			}

		if (!found)
			{
			if (codeCapacity == 0)
				{
				codeCapacity = 8;
				codeList = new StatusCode *[codeCapacity];
				}
			else
				{
				StatusCode	**oldlist = codeList;
				codeCapacity += 8;
				codeList = new StatusCode *[codeCapacity];
				memcpy(codeList, oldlist, codeCount * sizeof(StatusCode *));
				delete oldlist;
				}

			codeList[codeCount++] = pCode = new StatusCode(msgid, languageCount);
			}

		if (pCode != NULL)
			{
			if (langidx >= 0) 
				{
				pCode->setText(langidx, text);
				languageList[langidx]->count++;
				}
			}
		}


int
scan(FILE *in, FILE *hdr)
		{
		XString		comment, msgtxt, symname, line;
		int			msgid=0;
		int			inmsg=0;
		int			facility = 0;
		int			severity = 0;
		int			res=0;
		int			outputbase = defoutputbase;
		int			langidx=-1;

		warnCount = 0;
		errorCount = 0;
		lineno = 0;

		while (line.readLine(in))
			{
			line.stripChars("\r\n \t");
			lineno++;

			if (line.startsWith(";"))
				{
				if (!comment.isEmpty()) comment += "\n";
				comment += line.mid(1);
				continue;
				}

			if (inmsg)
				{
				// Keep reading until we find a single dot.
				if (line == ".")
					{
					int		code;

					code = (severity << 30) | (facility << 16) | (custflag << 29) | (msgid & 0xffff);
					addStatusCode(hdr, code, symname, msgtxt, comment, outputbase, langidx);

					inmsg = 0;
					msgtxt = "";
					comment = "";
					}
				else
					{
					//if (!msgtxt.isEmpty()) msgtxt += " ";
					msgtxt += line;
					msgtxt += "\n";
					}
				}
			else if (!line.isEmpty())
				{
				// We expect something in the form:
				//   param = value
				// and it may include a brackets which go over multiple lines
				XString	param, value;
				int		p;

				p = line.first("=");
				if (p <= 0)
					{
					printf("Syntax error at line %d of %s\n", lineno, (const char *)filename);
					printf("[%s]\n", (const char *)line);
					res = 1;
					break;
					}

				param = line.left(p);
				param.stripChars(" \t", XString::both);

				// printf("PARAM = '%s'\n", (const char *)param);

				value = line.mid(p+1);
				p = value.first('(');
				if (p >= 0)
					{
					value.remove(0, p+1);
					while ((p = value.last(')')) < 0)
						{
						if (!line.readLine(in))
							{
							printf("EOF reached whilst looking for closing bracket\n");
							return 1;
							}
						lineno++;
						line.stripChars("\r\n \t");
						value += line;
						}

					value.remove(p);
					}

				value.replaceAll('\t', ' ');
				while (value.indexOf("  ") >= 0) value.replaceAll("  ", " ");
				value.stripChars(" \t", XString::both);

				// printf("VALUE = '%s'\n", (const char *)value);

				if (param == _T("MessageIdTypedef"))
					{
					cast = value;
					}
				else if (param == "SeverityNames" || param == "FacilityNames" || param == "LanguageNames")
					{
					if (param == "SeverityNames") clearList(severityList, severityCount);
					else if (param == "FacilityNames") clearList(facilityList, facilityCount);
					else if (param == "LanguageNames") clearList(languageList, languageCount);

					XString	iname, ename, ivalue;
					int		pequals, pcolon;

					// Expect something in the form X=Y:Z with optional spacing and probabbly repeated
					// several times. We don't have the tokenizer class available here so we'll have
					// to split it manually (groan).
					while (!value.isEmpty())
						{
						pequals = value.first('=');
						pcolon = value.first(':');
						if (pequals < 0 || pcolon < 0 || pequals > pcolon)
							{
							printf("Syntax error for param %s near line %d\n", (const char *)param, lineno);
							}

						iname = value.left(pequals);
						ivalue = value.mid(pequals+1, pcolon-pequals-1);
						value.remove(0, pcolon+1);

						value.stripChars(" ", XString::both);
						p = value.first(' ');
						if (p > 0)
							{
							ename = value.left(p);
							value.remove(0, p+1);
							}
						else
							{
							ename = value;
							value.empty();
							}
						
						ename.stripChars(" ", XString::both);

						// printf("  iname=%s\n", (const char *)iname);
						// printf("  ivalue=%s\n", (const char *)ivalue);
						// printf("  ename=%s\n", (const char *)ename);

						if (param == "SeverityNames") addSeverity(iname, valueOf(ivalue), ename);
						else if (param == "FacilityNames") addFacility(iname, valueOf(ivalue), ename);
						else if (param == "LanguageNames") addLanguage(iname, valueOf(ivalue), ename);
						}
					}
				else if (param == "MessageId")
					{
					++msgid;
					if (!value.isEmpty()) msgid = valueOf(value);
					}
				else if (param == "Language")
					{
					langidx = findLanguageIdx(value);
					if (langidx < 0)
						{
						printf("Can't find language %s at line %d\n", (const char *)value, lineno);
						return 1;
						}

					inmsg = 1;
					}
				else if (param == "Severity")
					{
					if (!value.isEmpty())
						{
						NameRecord	*np;

						np = findSeverity(value);
						if (np == NULL)
							{
							printf("Can't find severity %s at line %d\n", (const char *)value, lineno);
							return 1;
							}

						severity = np->value;
						}
					}
				else if (param == "Facility")
					{
					if (!value.isEmpty())
						{
						NameRecord	*np;

						np = findFacility(value);
						if (np == NULL)
							{
							printf("Can't find facility %s at line %d\n", (const char *)value, lineno);
							return 1;
							}

						facility = np->value;
						}
					}
				else if (param == "SymbolicName") symname = value;
				else if (param == "OutputBase")
					{
					int	n=atoi(value);

					if (n == 10 || n == 16) outputbase = n;
					else
						{
						printf("Invalid output base %s at line %d\n", (const char *)value, lineno);
						return 1;
						}
					}
				else
					{
					printf("Syntax error at line %d\n", lineno);
					return 1;
					}
				}
			}

		if (!comment.isEmpty())
			{
			fprintf(hdr, "%s\n", (const char *)comment);
			}

		return res;
		}


extern "C" int
compStatusCode(const void *p1, const void *p2)
		{
		StatusCode	**pCode1=(StatusCode **)p1;
		StatusCode	**pCode2=(StatusCode **)p2;
		int			r=0;

		if ((*pCode1)->id() > (*pCode2)->id()) r = 1;
		else if ((*pCode1)->id() < (*pCode2)->id()) r = -1;

		return r;
		}


void
writenum(int fd, int n, int nbytes)
		{
		unsigned char	uc;

		while (nbytes > 0)
			{
			uc = n & 0xff;
			n >>= 8;
			write(fd, &uc, 1);
			nbytes--;
			}
		}


int
mkbinfile(const char *binfile, int sections, int langidx)
		{
		XString	file;
		int		fd, i;

		file = binfile;
		file += ".bin";

		if (rcdir != ".")
			{
			file.insert(0, "/");
			file.insert(0, rcdir);
			}

		fd = open(file, O_WRONLY|O_TRUNC|O_CREAT, 0666);
		if (fd < 0)
			{
			printf("Can't create %s\n", (const char *)file);
			return 1;
			}

		if (verbose) printf("Writing %s\n", (const char *)file);

		writenum(fd, sections, 4);

		int		textoffset = 4 + (sections * 12);
		int		firstid=0, currid=0, currsection=0, firsttextoffset=0;

		for (i=0;i<codeCount;i++)
			{
			StatusCode	*pCode=codeList[i];

			if (i == 0)
				{
				currid = firstid = pCode->id();
				firsttextoffset = textoffset;
				}
			else
				{
				if (pCode->id() != currid + 1)
					{
					if (verbose) printf("    [%x .. %x] - %d bytes\n", firstid, currid, textoffset-firsttextoffset);

					lseek(fd, 4 + (currsection * 12), SEEK_SET);
					writenum(fd, firstid, 4);
					writenum(fd, currid, 4);
					writenum(fd, firsttextoffset, 4);

					firstid = pCode->id();
					firsttextoffset = textoffset;
					currsection++;
					}
				}
			currid = pCode->id();

			XString	text=pCode->getText(langidx);
			int		nbytes, pad=0, tlen=text.length() + 1;

			if ((tlen % 2) == 1) pad = 1;

			lseek(fd, textoffset, SEEK_SET);
			if (outtype == UNICODE)
				{
				const wchar_t	*wp = (const wchar_t *)text;

				nbytes = 4 + (tlen * 2);
				if ((nbytes % 4) != 0) pad = 4 - (nbytes % 4);
				nbytes += pad;

				writenum(fd, nbytes, 2);
				writenum(fd, 1, 2);

				while (*wp)
					{
					writenum(fd, *wp++, 2);
					}
				writenum(fd, 0, 2);
				}
			else
				{
				nbytes = 4 + tlen;
				if ((nbytes % 4) != 0) pad = 4 - (nbytes % 4);
				nbytes += pad;

				writenum(fd, nbytes, 2);
				writenum(fd, 0, 2);
				write(fd, (const char *)text, text.length()+1);
				}

			if (pad) writenum(fd, 0, pad);

			// printf("Wrote %d bytes at offset %d\n", nbytes, textoffset);
			textoffset += nbytes;
			}

		if (verbose) printf("    [%x .. %x] - %d bytes\n", firstid, currid, textoffset-firsttextoffset);

		lseek(fd, 4 + (currsection * 12), SEEK_SET);
		writenum(fd, firstid, 4);
		writenum(fd, currid, 4);
		writenum(fd, firsttextoffset, 4);

		if (verbose) printf("    Total of %d messages, %d bytes\n", codeCount, textoffset);

		return 0;
		}


int
mkrcfiles(const XString &rcfile)
		{
		if (codeCount == 0)
			{
			printf("No codes to write\n");
			return 1;
			}

		int		i, sections=0, firstid, currid;

		// First sort the status messages
		qsort(codeList, codeCount, sizeof(StatusCode *), compStatusCode);

		// Analyze the code list to find out how many sections we have
		currid = firstid = codeList[0]->id();
		sections = 1;

		for (i=1;i<codeCount;i++)
			{
			StatusCode	*pCode=codeList[i];

			if (pCode->id() != currid + 1)
				{
				firstid = pCode->id();
				sections++;
				}
			currid = pCode->id();
			}

		XString	file(rcfile);
		FILE	*rc;

		if (rcdir != ".")
			{
			file.insert(0, "/");
			file.insert(0, rcdir);
			}

		if ((rc = fopen(file, "w")) == NULL)
			{
			printf("Can't create %s\n", (const char *)file);
			return 1;
			}

		if (verbose) printf("Generating %s\n", (const char *)file);

		for (i=0;i<languageCount;i++)
			{
			int	lang, sublang;

			sublang = languageList[i]->value >> 10;
			lang = languageList[i]->value & 0x3ff;

			if (languageList[i]->count) 
				{
				fprintf(rc, "LANGUAGE 0x%x,0x%x\n", lang, sublang);
				fprintf(rc, "1 11 %s.bin\n", (const char *)languageList[i]->extname);
				mkbinfile(languageList[i]->extname, sections, i);
				}
			}

		fclose(rc);

		return 0;
		}


int
compile(const XString &file)
		{
		FILE	*in, *hdr;

		if ((in = fopen(file, "r")) == NULL)
			{
			fprintf(stderr, "Can't open %s\n", (const char *)file);
			return 1;
			}

		if (verbose)
			{
			printf("Compiling %s\n", (const char *)file);
			}

		// Save the name in the global var for error reporting
		filename = file;

		// Work out the directory for the header file.
		XString	hdrfile(file), rcfile;
		int		p;

		p = hdrfile.last('/');
		if (p > 0) hdrfile.remove(0, p+1);

		p = hdrfile.last('.');
		hdrfile.remove(p+1);

		rcfile = hdrfile;

		hdrfile += hdrext;
		rcfile += "rc";

		XString basehdrfile = hdrfile;

		if (hdrdir != ".")
			{
			hdrfile.insert(0, "/");
			hdrfile.insert(0, hdrdir);
			}

		if (verbose) printf("Generating %s\n", (const char *)hdrfile);

		hdr = fopen(hdrfile, "w");
		if (hdr == NULL)
			{
			printf("Can't create %s\n", (const char *)hdrfile);
			return 1;
			}

		// Write the status info to the hdr file.
		fprintf(hdr, "/*\n");
		fprintf(hdr, "** %s - generated status codes\n", (const char *)basehdrfile);
		fprintf(hdr, "**\n");
		fprintf(hdr, "** These values specify the allowed range of status codes for various aspects\n");
		fprintf(hdr, "** of the project.\n");
		fprintf(hdr, "**\n");
		fprintf(hdr, "**\n");
		fprintf(hdr, "** Generic messages to all projects\n");
		fprintf(hdr, "**\n");
		fprintf(hdr, "**\n");
		fprintf(hdr, "**  Values are 32 bit values layed out as follows:\n");
		fprintf(hdr, "**\n");
		fprintf(hdr, "**   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1\n");
		fprintf(hdr, "**   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0\n");
		fprintf(hdr, "**  +---+-+-+-----------------------+-------------------------------+\n");
		fprintf(hdr, "**  |Sev|C|R|     Facility          |               Code            |\n");
		fprintf(hdr, "**  +---+-+-+-----------------------+-------------------------------+\n");
		fprintf(hdr, "**\n");
		fprintf(hdr, "**  where\n");
		fprintf(hdr, "**\n");
		fprintf(hdr, "**      Sev - is the severity code\n");
		fprintf(hdr, "**\n");
		fprintf(hdr, "**          00 - Success\n");
		fprintf(hdr, "**          01 - Informational\n");
		fprintf(hdr, "**          10 - Warning\n");
		fprintf(hdr, "**          11 - Error\n");
		fprintf(hdr, "**\n");
		fprintf(hdr, "**      C - is the Customer code flag\n");
		fprintf(hdr, "**\n");
		fprintf(hdr, "**      R - is a reserved bit\n");
		fprintf(hdr, "**\n");
		fprintf(hdr, "**      Facility - is the facility code\n");
		fprintf(hdr, "**\n");
		fprintf(hdr, "**      Code - is the facility's status code\n");
		fprintf(hdr, "*/\n\n");

		// Now scan the lines of the file
		scan(in, hdr);

		fclose(hdr);
		fclose(in);

		// Create the RC files
		mkrcfiles(rcfile);

		return 0;
		}


void
usage()
		{
	   	printf("Usage: mc [options] file\n");
		printf("where options are:\n");
       	printf("    -a      input file is ANSI (default).\n");
		printf("    -u      input file is Unicode.\n");
		printf("    -v      verbose output.\n");
		printf("    -A      messages in generated file should be ANSI.\n");
		printf("    -U      messages in generated file should be Unicode (default).\n");
		printf("    -c      sets the Customer bit in all the message Ids.\n");
		printf("    -d      uses decimal as the default base for values\n");
		printf("    -e ext  Specify the extension for the header file.\n");
		printf("    -h dir  Specify the directory in which the include file will be created\n");

		/*
		usage: MC [-?aAcdnosuUvw] [-m maxmsglen] [-h dirspec] [-e extension] [-r dirspec] [-x dbgFileSpec] filename.mc
		-? - displays this message
		-b - .BIN filename should have .mc filename_ included for uniqueness.
					  From 1 - 3 chars.
					 Default is .\
		-m maxmsglen - generate a warning if the size of any message exceeds
					  maxmsglen characters.
		-n - terminates all strings with null's in the message tables.
		-o - generate OLE2 header file (use HRESULT definition instead of
			status code definition)
		-r pathspec - gives the path of where to create the RC include file
					 and the binary message resource files it includes.
					 Default is .\
		-s - insert symbolic name as first line of each message.
		-w - warns if message text contains non-OS/2 compatible inserts.
		-x pathspec - gives the path of where to create the .dbg C include
						file that maps message Ids to their symbolic name.
		filename.mc - gives the names of a message text file
					 to compile.
		Generated files have the Archive bit cleared.
*/

		exit(1);
		}


int
main(int argc, char **argv)
		{
		int			i, r;
		XString		arg, file;

		for (i=1;i<argc;i++)
			{
			arg = argv[i];

			if (arg.length() > 0 && arg[0] == '-')
				{
				if (arg == "-a") intype = ANSI;
				else if (arg == "-u") intype = UNICODE;
				else if (arg == "-A") outtype = ANSI;
				else if (arg == "-U") outtype = UNICODE;
				else if (arg == "-v") verbose = 1;
				else if (arg == "-c") custflag = 1;
				else if (arg == "-d") defoutputbase = 10;
				else if (arg == "-h")
					{
					if (++i < argc) hdrdir = argv[i];
					else usage();
					}
				else if (arg == "-r")
					{
					if (++i < argc) rcdir = argv[i];
					else usage();
					}
				else usage();
				}
			else file = arg;
			}

		if (file.length() == 0 || !file.endsWith(".mc")) usage();

		if (verbose)
			{
			printf("CA Message Compiler for Unix/Linux version 0.1\n");
			}

		// Setup default names
		addSeverity("Success", 0, "");
		addSeverity("Informational", 1, "");
		addSeverity("Warning", 2, "");
		addSeverity("Error", 3, "");

		addFacility("System", 0xff, "");
		addFacility("Application", 0xfff, "");

		addLanguage("English", 1, "msg00001");

		r = compile(file);

		// Cleanup
		clearList(severityList, severityCount);
		delete severityList;

		clearList(facilityList, facilityCount);
		delete facilityList;

		clearList(languageList, languageCount);
		delete languageList;

		for (i=0;i<codeCount;i++) delete codeList[i];
		delete codeList;

		return r;
		}
