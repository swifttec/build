/*
** dumpmsgbin.cpp	Dump the contents of a message compiler binary file
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>


int
readint(int fd, int &n)
		{
		char	buf[4];
		int		r;

		r = read(fd, buf, 4);
		if (r == 4)
			{
			r = 0;
			n = (buf[0] & 0xff) | ((buf[1] & 0xff) << 8) | ((buf[2] & 0xff) << 16) | ((buf[3] & 0xff) << 24);
			}
		else
			{
			r = -1;
			n = 0;
			}

		return r;
		}


int
readshort(int fd, short &n)
		{
		char	buf[2];
		int		r;

		r = read(fd, buf, 2);
		if (r == 2)
			{
			r = 0;
			n = (buf[0] & 0xff) | ((buf[1] & 0xff) << 8);
			}
		else
			{
			r = -1;
			n = 0;
			}

		return r;
		}


int
readUnicodeString(int fd, wchar_t *ws)
		{
		short	uc;

		while (readshort(fd, uc) == 0)
			{
			*ws++ = uc;
			if (uc == 0) break;
			}

		return 0;
		}


int
readAsciiString(int fd, char *s)
		{
		char	c;

		while (read(fd, &c, 1) == 1)
			{
			*s++ = c;
			if (c == 0) break;
			}

		return 0;
		}


int
main(int argc, char **argv)
		{
		int		fd, i;
		int		nsections;
		int		pos;

		if (argc < 2)
			{
			printf("usage: dumpmsgbin file\n");
			exit(1);
			}

		fd = open(argv[1], O_RDONLY, 0);
		if (fd < 0)
			{
			printf("Can't open %s\n", argv[1]);
			exit(1);
			}

		readint(fd, nsections);

		printf("There are %d sections\n", nsections);
		pos = 4;
		for (i=0;i<nsections;i++)
			{
			int		id, firstid, lastid, offset;

			lseek(fd, pos, SEEK_SET);
			readint(fd, firstid);
			readint(fd, lastid);
			readint(fd, offset);
			pos += 12;

			printf("  0x%08X - 0x%08X at offset %d\n", firstid, lastid, offset);

			for (id=firstid;id<=lastid;id++)
				{
				short	nbytes, flags;

				lseek(fd, offset, SEEK_SET);
				readshort(fd, nbytes);
				readshort(fd, flags);

				printf("    nbytes=%d  flags=0x%x\n", nbytes, flags);

				if ((flags & 1) != 0)
					{
					static wchar_t	wstr[1024];

					readUnicodeString(fd, wstr);

					printf("    0x%08X = UNICODE %ls\n", id, wstr);
					}
				else
					{
					static char	str[1024];

					readAsciiString(fd, str);

					printf("    0x%08X = ASCII %s\n", id, str);
					}

				offset += nbytes;
				}
			}

		close(fd);

		return 0;
		}
