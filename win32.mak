MASTERROOT		= ..
PROJROOT        = .
MASTERROOTFILES	= ..\setbuildenv.bat ..\mkall.bat ..\cleanall.bat ..\gitall ..\gitall.bat

!include $(MASTERROOT)\build\defs\win32.defs
!include $(PROJROOT)\makein\win32.mak.defs
!include Makefile.defs


all: $(MAKEIN) $(MASTERROOTFILES)
	$(SILENT)$(MAKEIN) $(DIRS)

clean: $(MAKEIN)
	$(SILENT)$(MAKEIN) -t clean $(DIRS)
	$(SILENT)rd /s /q bin


..\setbuildenv.bat: masterroot\setbuildenv.bat
	$(SILENT)copy /y masterroot\setbuildenv.bat ..

..\gitall: masterroot\gitall
	$(SILENT)copy /y masterroot\gitall ..

..\gitall.bat: masterroot\gitall.bat
	$(SILENT)copy /y masterroot\gitall.bat ..

..\mkall.bat: masterroot\mkall.bat
	$(SILENT)copy /y masterroot\mkall.bat ..

..\cleanall.bat: masterroot\cleanall.bat
	$(SILENT)copy /y masterroot\cleanall.bat ..

..\Makefile.defs: masterroot\mkdefs
	$(SILENT)masterroot\mkdefs .. > ../Makefile.defs

# Need to include this after the main targets are defined.
!include $(PROJROOT)\makein\win32.mak.build
