/*
**	XString.h	A standalone snapshot of XString
*/

#ifndef __XString_h__
#define __XString_h__

#include "XStringObject.h"

class XString
		{
public:
		/// Used for specifying case comparision operations
		enum caseCompare { exact, ignoreCase };

		/// Used for string stripping operations
		enum stripType { leading, trailing, both };

public:
		/// Default constructor - a blank string
		XString();
		virtual ~XString();

		/// Constructor - copy of the given char string
		XString(const char *s);

		/// Constructor - copy of the given unicode string
		XString(const wchar_t *s);

		/// Copy constructor (reference)
		XString(const XString &s);

		/// Copy constructor (pointer)
		/// XString(const XString *s);

		/// Copy exactly n chars from cs (including nulls)
		XString(const TCHAR *s, int n);

		/// Create a blank string with the given initial capacity nc
		XString(int ic);			

		/// Constructs a string containing the single character c.
		XString(TCHAR c);			

		/// Constructs a string containing the character c repeated N times.
		XString(TCHAR c, int n);		

public:
		/// Returns the length of the string
		int		length() const;

		/// Returns the reference count of the string (the number of XString objects sharing the same underlying string object)
		int		refcount() const;

		/// Returns the capacity of the string (i.e. the amount of space available before the string is expanded by rellaocating memory)
		int		capacity() const;

		/// Give this string object a hint about the expected capacity that will be required
		int		capacity(int nc);

		/// Returns true if this is a zero lengthed string (i.e., the null string).
		bool		isNull() const		{ return length() == 0; }	

		/// Returns true if this is a zero lengthed string (i.e., the null string).
		bool		isEmpty() const		{ return length() == 0; }	

		/// Returns TRUE if self contains no bytes with the high bit set.
		bool		isAscii() const;					

		/// Reset the string to an empty string
		void		reset()				{ *this = _T(""); }

		/// Reset the string to an empty string
		void		empty()				{ *this = _T(""); }

		/// Reset the string to an empty string
		void		clear()				{ *this = _T(""); }

		// Comparison Operators
		friend bool	operator==(const XString &s1, const char *s2     )	{ return (s1.compareTo(s2) == 0); }
		friend bool	operator==(const XString &s1, const wchar_t *s2     )	{ return (s1.compareTo(s2) == 0); }
		friend bool	operator==(const char *s1,      const XString &s2)	{ return (s2.compareTo(s1) == 0); }
		friend bool	operator==(const wchar_t *s1,      const XString &s2)	{ return (s2.compareTo(s1) == 0); }
		friend bool	operator==(const XString &s1, const XString &s2)	{ return (s1.compareTo(s2) == 0); }
		friend bool	operator!=(const XString &s1, const TCHAR *s2     )	{ return (s1.compareTo(s2) != 0); }
		friend bool	operator!=(const TCHAR *s1,      const XString &s2)	{ return (s2.compareTo(s1) != 0); }
		friend bool	operator!=(const XString &s1, const XString &s2)	{ return (s1.compareTo(s2) != 0); }

		// Logical equality and inequality. Case sensitivity is exact. This function is incompatible with const TCHAR*
		// strings with embedded nulls. This function may be incompatible with const TCHAR* MBCS strings.

		friend bool	operator< (const XString &s1, const char *s2     )	{ return (s1.compareTo(s2) <  0); }
		friend bool	operator< (const XString &s1, const wchar_t *s2     )	{ return (s1.compareTo(s2) <  0); }
		friend bool	operator< (const char *s1,      const XString &s2)	{ return (s2.compareTo(s1) >= 0); }
		friend bool	operator< (const wchar_t *s1,      const XString &s2)	{ return (s2.compareTo(s1) >= 0); }
		friend bool	operator< (const XString &s1, const XString &s2)	{ return (s1.compareTo(s2) <  0); }
		friend bool	operator> (const XString &s1, const char *s2     )	{ return (s1.compareTo(s2) >  0); }
		friend bool	operator> (const XString &s1, const wchar_t *s2     )	{ return (s1.compareTo(s2) >  0); }
		friend bool	operator> (const char *s1,      const XString &s2)	{ return (s2.compareTo(s1) <= 0); }
		friend bool	operator> (const wchar_t *s1,      const XString &s2)	{ return (s2.compareTo(s1) <= 0); }
		friend bool	operator> (const XString &s1, const XString &s2)	{ return (s1.compareTo(s2) >  0); }
		friend bool	operator<=(const XString &s1, const char *s2     )	{ return (s1.compareTo(s2) <= 0); }
		friend bool	operator<=(const XString &s1, const wchar_t *s2     )	{ return (s1.compareTo(s2) <= 0); }
		friend bool	operator<=(const char *s1,      const XString &s2)	{ return (s2.compareTo(s1) >  0); }
		friend bool	operator<=(const wchar_t *s1,      const XString &s2)	{ return (s2.compareTo(s1) >  0); }
		friend bool	operator<=(const XString &s1, const XString &s2)	{ return (s1.compareTo(s2) <= 0); }
		friend bool	operator>=(const XString &s1, const char *s2     )	{ return (s1.compareTo(s2) >= 0); }
		friend bool	operator>=(const XString &s1, const wchar_t *s2     )	{ return (s1.compareTo(s2) >= 0); }
		friend bool	operator>=(const char *s1,      const XString &s2)	{ return (s2.compareTo(s1) <  0); }
		friend bool	operator>=(const wchar_t *s1,      const XString &s2)	{ return (s2.compareTo(s1) <  0); }
		friend bool	operator>=(const XString &s1, const XString &s2)	{ return (s1.compareTo(s2) >= 0); }

		// Assignment Operators
		const XString &	operator=(TCHAR);
		const XString &	operator=(const char *);
		const XString &	operator=(const wchar_t *);
		const XString &	operator=(const XString &);
		const XString &	operator=(const XString *s)	{ *this = *s; return *this; }

		// Append Operators
		XString &	operator+=(TCHAR);
		XString &	operator+=(const char *);
		XString &	operator+=(const wchar_t *);
		XString &	operator+=(const XString &);
		XString &	operator+=(const XString *);

		TCHAR		operator[](int i) const;
		TCHAR		operator()(int i) const;
		XString	operator()(int start, int len) const;
		int		index(TCHAR ch, int pos=0, caseCompare cc=XString::exact) const;
		int		index(const TCHAR *pat, int pos=0, caseCompare cc=XString::exact) const;
		int		index(const XString &pat, int pos=0, caseCompare cc=XString::exact) const;
		int		index(const TCHAR *pat, int patlen, int i, caseCompare cmp) const;
		int		index(const XString &pat, int patlen, int i, caseCompare cmp) const;

		int		rindex(TCHAR ch, int pos, caseCompare cc) const;
		int		rindex(const TCHAR *pat, int pos, caseCompare cc) const;
		int		rindex(const XString &pat, int pos, caseCompare cc) const;
		int		rindex(const TCHAR *pat, int patlen, int i, caseCompare cmp) const;
		int		rindex(const XString &pat, int patlen, int i, caseCompare cmp) const;

		int		first(TCHAR c) const;
		int		first(TCHAR c, int) const;
		int		first(const TCHAR* str) const;
		int		first(const TCHAR* str, int N) const;

		int		last(TCHAR c) const;
		int		last(TCHAR c, int n) const;
		int		last(const TCHAR *str) const;
		int		last(const TCHAR *str, int n) const;

		/// Get a pointer to the string data (useful for strcpy, etc)
		const TCHAR *	data() const;

		/// Type conversion to a TCHAR string pointer. (This is a const operation)
		operator const TCHAR *() const	{ return data(); }

		/// Type conversion to a const void pointer. (This is a const operation)
		operator const void *() const	{ return data(); }

	#if defined(UNICODE) || defined(_UNICODE)
		// Type conversion to char string from a unicode based XString. (This is a non-const operation)
		operator const char *();
	#else
		// Type conversion to unicode string pointer from a char based XString. (This is a non-const operation)
		operator const wchar_t *();
	#endif

		// Collation
		int	compareTo(const char *str, caseCompare=XString::exact) const;
		int	compareTo(const wchar_t *str, caseCompare=XString::exact) const;
		int	compareTo(const XString &str, caseCompare=XString::exact) const;

		XString	strip(stripType type=trailing, TCHAR c=' ');
		XString	strip(stripType type, const TCHAR *s);

		void		stripChars(const TCHAR *s=_T(" \t"), stripType type=trailing);

		// Modify methods: (insert, remove, replace etc)
		XString &	insert(int pos, TCHAR c)	{ return insert(pos, &c, 1); }
		XString &	insert(int pos, const TCHAR* cs);
		XString &	insert(int pos, const TCHAR* cs, int N);
		XString &	insert(int pos, const XString& str);
		XString &	insert(int pos, const XString& str, int N);

		XString &	remove(int pos);
		XString &	remove(int pos, int N);

		XString &	replace(int pos, int N, const TCHAR* cs);
		XString &	replace(int pos, int N1,const TCHAR* cs, int N2);
		XString &	replace(int pos, int N, const XString& str);
		XString &	replace(int pos, int N1,const XString& str, int N2);

		XString &	prepend(const TCHAR* cs);
		XString &	prepend(const TCHAR* cs, int n);
		XString &	prepend(TCHAR c, int n);
		XString &	prepend(const XString& str);
		XString &	prepend(const XString& cstr, int n);
		XString &	prepend(const XString *cstr)		{ return prepend(*cstr); }
		XString &	prepend(const XString *cstr, int n)	{ return prepend(*cstr, n); }

		XString &	append(const char *s);	
		XString &	append(const wchar_t *s);	
		XString &	append(const TCHAR *s, int n);
		XString &	append(TCHAR c, int n=1);
		XString &	append(const XString &cstr);
		XString &	append(const XString &cstr, int n);
		XString &	append(const XString *cstr)		{ return append(*cstr); }
		XString &	append(const XString *cstr, int n)	{ return append(*cstr, n); }

		friend XString operator+(const XString &s1, const XString &s2);
		//friend XString operator+(const XString &s,  const TCHAR *cs);
		//friend XString operator+(const TCHAR *cs, XString &s);

		// Misc functions
		void		toLower();
		void		toUpper();
		uint32		hash(caseCompare cmp=exact) const;

		void		resize(int n);

		// Returns the number of bytes necessary to store the object using the global function:
		int		binaryStoreSize() const	{ return length()+sizeof(int); }


		bool		contains(const TCHAR* str, caseCompare = XString::exact) const;
		bool		contains(const XString& cs, caseCompare = XString::exact) const;

		// Extra (non-standard but useful) functions
		void		expandEnvVars();	// Expands env. vars in the string

		XString	left(int n) const		{ return (*this)(0, n); }
		XString	right(int n) const		{ return (*this)(length()-n, n); }
		XString	mid(int pos) const		{ return (*this)(pos, length()-pos); }
		XString	mid(int pos, int n) const	{ return (*this)(pos, n); }

		// Quote and unquote the string
		void		quote(TCHAR qchar='"', bool onlyIfWhiteSpace=true, TCHAR backslash=0);
		void		unquote(TCHAR qchar='"', TCHAR backslash=0);
		XString	toQuotedXString(TCHAR qchar='"', bool onlyIfWhiteSpace=true, TCHAR backslash='\\');
		XString	toUnquotedXString(TCHAR qchar='"', TCHAR backslash='\\');


public: // Java XString methods

		/// Returns the character at the specified index.
		TCHAR		charAt(int index) const					{ return (*this)[index]; }

		/// Compares two strings lexicographically, ignoring case considerations.
		int		compareToIgnoreCase(const XString &str) const		{ return compareTo(str, ignoreCase); }

		/// Concatenates the specified string to the end of this string.
		XString &	concat(const XString &str)				{ *this += str; return *this; }

		/// Tests if this string ends with the specified suffix.
		bool		endsWith(const XString &suffix, bool ignore_case=false) const;

		/// Compares this string to the specified object.
		virtual bool	equals(const XString &str)				{ return compareTo(str, exact) == 0; }
		virtual bool	equals(const XString *str)				{ return compareTo(*str, exact) == 0; }

		/// Compares this XString to another XString, ignoring case considerations.
		bool		equalsIgnoreCase(const XString &str) const		{ return compareTo(str, ignoreCase) == 0; }

		// Convert this XString into bytes according to the platform's default character encoding, storing the result into a new byte array.
		// byte[] getBytes()

		// Deprecated. This method does not properly convert characters into bytes. As of JDK 1.1, the preferred way to do this is via the getBytes(XString enc) method, which takes a character-encoding name, or the getBytes() method, which uses the platform's default encoding.
		// void getBytes(int srcBegin, int srcEnd, byte[] dst, int dstBegin)

		// Convert this XString into bytes according to the specified character encoding, storing the result into a new byte array.
		// byte[] getBytes(XString enc)

		// Copies characters from this string into the destination character array.
		// void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)

		/// Returns the hashcode for this string.
		virtual uint32	hashCode()						{ return hash(); }

		/// Returns the index within this string of the first occurrence of the specified character.
		int		indexOf(TCHAR ch) const					{ return first(ch); }

		/// Returns the index within this string of the first occurrence of the specified character, starting the search at the specified index.
		int		indexOf(const XString &str) const			{ return index(str); }

		/// Returns the index within this string of the first occurrence of the specified substring.
		int		indexOf(TCHAR ch, int pos) const			{ return index(ch, pos); }

		/// Returns the index within this string of the first occurrence of the specified substring, starting at the specified index.
		int		indexOf(const XString &str, int pos) const		{ return index(str, pos); }

		/// Returns the index within this string of the last occurrence of the specified character.
		int		lastIndexOf(TCHAR ch) const				{ return last(ch); }

		/// Returns the index within this string of the last occurrence of the specified character, searching backward starting at the specified index.
		int		lastIndexOf(TCHAR ch, int fromIndex) const		{ return rindex(ch, fromIndex, exact); }

		/// Returns the index within this string of the rightmost occurrence of the specified substring.
		int		lastIndexOf(const XString &str) const			{ return rindex(str, length()-1, exact); }

		/// Returns the index within this string of the last occurrence of the specified substring.
		int		lastIndexOf(const XString &str, int fromIndex) const	{ return rindex(str, fromIndex, exact); }

		/// Tests if two string regions are equal.
		bool		regionMatches(bool ignore_case, int toffset, const XString &other, int ooffset, int len) const;

		/// Tests if two string regions are equal.
		bool		regionMatches(int toffset, const XString &other, int ooffset, int len) const;

		/// Returns a new string resulting from replacing all occurrences of oldChar in this string with newChar.
		XString	replace(char oldChar, char newChar) const		{ XString tmp = *this ; tmp.Replace(oldChar, newChar); return tmp; }

		/// Tests if this string starts with the specified prefix.
		bool		startsWith(const XString &prefix) const			{ return startsWith(prefix, 0); }

		/// Tests if this string starts with the specified prefix beginning a specified index.
		bool		startsWith(const XString &prefix, int toffset, bool ignore_case=false) const;

		/// Returns a new string that is a substring of this string.
		XString		substring(int beginIndex) const				{ return (*this)(beginIndex, length()-beginIndex); }

		/// Returns a new string that is a substring of this string.
		XString		substring(int beginIndex, int endIndex) const		{ return (*this)(beginIndex, endIndex-beginIndex); }

		/// Converts all of the characters in this XString to lower case using the rules of the default locale, which is returned by Locale.getDefault.
		XString &	toLowerCase()	{ toLower(); return *this; }

		/// This object (which is already a string!) is itself returned.
		virtual XString	toXString()	{ return *this; }

		/// Converts all of the characters in this XString to upper case using the rules of the default locale, which is returned by Locale.getDefault.
		XString &	toUpperCase()	{ toUpper(); return *this; }

		/// Removes white space from both ends of this string.
		XString &	trim()		{ stripChars(_T(" \t"), both); return *this; }


public: // Microsoft CString methods

		/// Return the length of the string
		int		GetLength() const		{ return length(); }

		bool		IsEmpty() const			{ return isNull(); }
		void		Empty()				{ *this = _T(""); }
		TCHAR		GetAt(int n) const		{ return (*this)[n]; }
		void		SetAt(int n, TCHAR c);
		int		Compare(const char *s) const		{ return compareTo(s, exact); }
		int		Compare(const wchar_t *s) const		{ return compareTo(s, exact); }
		int		CompareNoCase(const TCHAR *s) const	{ return compareTo(s, ignoreCase); }
		XString	Mid(int pos) const		{ return mid(pos); }
		XString	Mid(int pos, int n) const	{ return mid(pos, n); }
		XString	Left(int n) const		{ return left(n); }
		XString	Right(int n) const		{ return right(n); }
		void		MakeLower()			{ toLower(); }
		void		MakeUpper()			{ toUpper(); }
		void		MakeReverse();
		XString	SpanIncluding(const TCHAR *charSet) const;
		XString	SpanExcluding(const TCHAR *charSet) const;

		// Replace all occurences of oldchar with newchar
		int		replaceAll(TCHAR oldchar, TCHAR newchar);
		int		Replace(TCHAR oldchar, TCHAR newchar)			{ return replaceAll(oldchar, newchar); }
		int		ReplaceAll(TCHAR oldchar, TCHAR newchar)		{ return replaceAll(oldchar, newchar); }
		
		// Replace all occurences of oldstr with newstr
		int		replaceAll(const TCHAR *oldstr, const TCHAR *newstr, caseCompare cmp=exact);
		int		Replace(const TCHAR *oldstr, const TCHAR *newstr)	{ return replaceAll(oldstr, newstr, exact); }
		int		ReplaceAll(const TCHAR *oldstr, const TCHAR *newstr)	{ return replaceAll(oldstr, newstr, exact); }

		// Remove all occurences of char in string
		int		Remove(TCHAR c);
		int		Insert(int pos, TCHAR c)	{ insert(pos, &c, 1); return length(); }
		int		Insert(int pos, const TCHAR *s)	{ insert(pos, s); return length(); }
		int		Delete(int pos, int n=1)	{ remove(pos, n); return length(); }

		// Strip the specified characters from the string
		void		TrimLeft(TCHAR c)			{ TCHAR	s[2]; s[0] = c; s[1] = 0; stripChars(s, leading); }
		void		TrimLeft(const TCHAR *s=_T(" \t\n"))	{ stripChars(s, leading); }
		void		TrimRight(TCHAR c)			{ TCHAR	s[2]; s[0] = c; s[1] = 0; stripChars(s, trailing); }
		void		TrimRight(const TCHAR *s=_T(" \t\n"))	{ stripChars(s, trailing); }

		// Find characters
		int		Find(TCHAR ch) const		{ return first(ch); }
		int		Find(TCHAR *str) const		{ return index(str, (int)t_strlen(str), 0, exact); }
		int		Find(TCHAR ch, int pos) const	{ return index(ch, pos); }
		int		Find(TCHAR *str, int pos) const	{ return index(str, (int)t_strlen(str), pos, exact); }
		int		ReverseFind(TCHAR ch) const	{ return last(ch); }
		int		FindOneOf(TCHAR *charSet) const	{ return first(charSet); };

		// Direct access to buffer
		TCHAR *		GetBuffer(int minlen);
		TCHAR *		GetBufferSetLength(int len);
		void		ReleaseBuffer(int len=-1);
		void		FreeExtra();
		void		LockBuffer();
		void		UnlockBuffer();
		bool		BufferLocked() const		{ return (_strobj != NULL && _strobj->_refcount < 0); }

public: // File + Stream methods

		// Read a line from the file. Strip the \n chars from the end if stripNL is true
		// Returns false on EOF, true if OK
		bool		readLine(FILE *f, bool stripNL=true);	


	#if defined(NEVER) && (defined(__AFX_H__) || defined(_AFXDLL) || defined(AFXAPI))
	// Microsoft MFC support
public:
		// MFC Cstring cast
		operator CString();

		// MFC CString assignment operator
		const XString &	operator=(const CString &s);

		// MFC CString append operator
		XString &		operator+=(const CString &s);

		// MFC CString Constructor
		XString::XString(const CString &s);

		// MFC CString comparisions
		friend bool	operator==(const XString &s1, const CString &s2     )	{ return (s1.compareTo((const TCHAR *)s2) == 0); }
		friend bool	operator==(const CString &s1, const XString &s2     )	{ return (s2.compareTo((const TCHAR *)s1) == 0); }
		
		friend bool	operator!=(const XString &s1, const CString &s2     )	{ return (s1.compareTo((const TCHAR *)s2) != 0); }
		friend bool	operator!=(const CString &s1, const XString &s2     )	{ return (s2.compareTo((const TCHAR *)s1) != 0); }
		
		friend bool	operator<(const XString &s1, const CString &s2     )	{ return (s1.compareTo((const TCHAR *)s2) < 0); }
		friend bool	operator<(const CString &s1, const XString &s2     )	{ return (s2.compareTo((const TCHAR *)s1) >= 0); }
		friend bool	operator>(const XString &s1, const CString &s2     )	{ return (s1.compareTo((const TCHAR *)s2) > 0); }
		friend bool	operator>(const CString &s1, const XString &s2     )	{ return (s2.compareTo((const TCHAR *)s1) <= 0); }

		friend bool	operator<=(const XString &s1, const CString &s2     )	{ return (s1.compareTo((const TCHAR *)s2) <= 0); }
		friend bool	operator<=(const CString &s1, const XString &s2     )	{ return (s2.compareTo((const TCHAR *)s1) >= 0); }
		friend bool	operator>=(const XString &s1, const CString &s2     )	{ return (s1.compareTo((const TCHAR *)s2) >= 0); }
		friend bool	operator>=(const CString &s1, const XString &s2     )	{ return (s2.compareTo((const TCHAR *)s1) <= 0); }

	#endif // defined(__AFX_H__) || defined(_AFXDLL) || defined(AFXAPI)



public: // STATIC methods
		static int	initialCapacity(int ic=15);
		static int	maxWaste(int mw=15);
		static int	resizeIncrement(int ri=16);
		static char *	strnew(const char *s);


private:
		XString(const TCHAR *s1, int n1, const TCHAR *s2, int n2);
		void	addRef();
		void	dropRef();
		void	prepareToUpdate();

private: // STATIC methods
		static XStringObject *	getEmptyXString();

private: // STATIC members
		static XStringObject	*__emptyXString;

protected:
		XStringObject	*_strobj;	// A pointer to the string object
		};


/*********************************************************************************
** Functions
*********************************************************************************/
XString	toLower(const XString& str);
XString	toUpper(const XString& str);
uint32	hash(const XString &str);
XString	x_string_quote(const XString &str, TCHAR qchar='"', bool onlyIfWhiteSpace=true, TCHAR backslash=0);
XString	x_string_unquote(const XString &str, TCHAR qchar='"', TCHAR backslash=0);

#endif
