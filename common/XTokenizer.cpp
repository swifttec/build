/****************************************************************************
**	XTokenizer.cpp		A String tokenizer class
****************************************************************************/



#include "XTokenizer.h"


/**
*** @brief		Construct a XTokenizer object according to the given parameters.
***
*** Construct a XTokenizer object according to the given parameters.
***
*** The XTokenizer can be used any set of characters as delimiter characters 
*** by specifying the <i>sepchars</i> parameter.
***
*** The <i>skipdups</i> parameter tells the tokenizer whether or not to ignore 
*** two or more delimiters or not. If true (default) then a string followed by any
*** number of delimiters followed by another string will return only two tokens.
*** If <i>skipdups</i> is false then one token is returned for each delimiter
*** found, with empty strings being returned where duplicate delimiters were found.
***
*** Example:
***
*** <b>Delimiter:</b> ':'<br>
*** <b>String:</b> A::B:C:<br>
*** <b>skipdups=true:</b> [A] [B] [C]<br>
*** <b>skipdups=false:</b> [A] [] [B] [C] []<br>
*** 
*** If stdquotes is true	the standard quote pairs "..." and '...' are added.
***
*** 
*** @param s		The initial string to tokenize. [default: empty string]
*** @param sepchars	A string containing the chars that are considered to be token delimiters. [default: " \\t\\n"]
*** @param skipdups	A bool defining the action to be taken when 2 delimiter characters are found. [default: true]
*** @param stdquotes	A bool defining if the standard quote pairs "..." and '...' are to be added. [default: false]
*** @param meta		The character which is used to prefix quotes and separators to allow them to be ignored.
**/
XTokenizer::XTokenizer(const TCHAR *s, const TCHAR *sepchars, bool skipdups, bool stdquotes, TCHAR meta)
	{
	_string = XString::strnew(s);
	_chars = XString::strnew(sepchars);
	_skip = skipdups;
	_doit = true;
	_meta = meta;
	reset();

	if (stdquotes)
	    {
	    addQuotes(_T('"'), _T('"'));
	    addQuotes(_T('\''), _T('\''));
	    }
	}

// Destructor
XTokenizer::~XTokenizer()
	{
	clear();	// Cleans up allocated strings
	clearQuotes();	// Cleans up allocated strings
	delete [] _string;
	delete [] _chars;
	}


/**
** This method clears the internal token array and forces the
** the tokenizer to re-tokenize the current string on the next call
** that requests a token.
**
** @brief	clear the internal token array
**/
void
XTokenizer::clear()
	{
	// Remove and delete all strings in the vector
	_tokenVec.clear();

	reset();
	_doit = true;
	}


/**
** This method clears the internal quotes array and forces the
** the tokenizer to re-tokenize the current string on the next call
** that requests a token.
**
** @brief	clear the internal quotes array.
**/
void
XTokenizer::clearQuotes()
	{
	// Remove and delete all strings in the vector
	for (int i=0;i<_quoteVec.size();i++)
	    {
	    TCHAR	*s = (TCHAR *)_quoteVec[i];
	    delete [] s;
	    }
	_quoteVec.clear();
	_doit = true;
	}


/**
** This method resets the tokenizer to start returning tokens from the beginning
** of the internal token array, but does not force a re-tokenization of the
** current string. This can be used when the current set of tokens need to
** be re-read from the beginning using the nextToken() method or the () operator
**
** @brief	Reset token retrieval
**/
void
XTokenizer::reset()
	{
	_curr = 0;
	}


/**
** This method adds a pair of delimiting quotes to the internal
** quote array. The quote pairs are used allow quoted strings
** that contain delimiters to be ignored.
**
** E.g. If a string to be tokenized using spaces as the delimiter
** is specified, but anything contained within double-quotes should
** be taken as a single token then using addQuotes('"', '"') will make
** the tokenizer work correctly.
**
** The string: <b>This "is a" test</b><br>
** would give the following tokens:<br>
** No quotes: [This] ["is] [a"] [test]<br>
** With quotes: [This] ["is a"] [test]
**
** @param qs	The quote character marking the start of a token
** @param qe	The quote character marking the end of a token
** @brief 	Add a pair of delimiting quotes
**/
void
XTokenizer::addQuotes(TCHAR qs, TCHAR qe)
	{
	TCHAR	qstr[3];

	qstr[0] = qs;
	qstr[1] = qe;
	qstr[2] = 0;
	_quoteVec.add(XString::strnew(qstr));
	_doit = true;
	}


/**
** This private method does the hard work of splitting up the string into tokens
** and putting them into the internal arrays
** 
** @brief Do the actual tokenizing
**/
void			
XTokenizer::tokenize()
	{
	int		quoteEnum;
	TCHAR		*s;
	int		n;
	XString	tmpstr;

	clear();
	for (s=_string;*s;)
	    {
	    if (_skip)
		{
		// Skip initial separators
		s += strspn(s, _chars);
		if (!*s) break;
		}

	    // Check to a quoted token
	    quoteEnum = 0;
	    TCHAR    *searchFor = _chars;

	    for (quoteEnum=0;quoteEnum<_quoteVec.size();quoteEnum++)
		{
		TCHAR *qs = (TCHAR *)_quoteVec[quoteEnum];

		if (*qs == *s)
		    {
		    searchFor = qs+1;
		    s++;
		    break;
		    }
		}

	    tmpstr.empty();

	    // Get the number of non-separator chars 
	    n = (int)strcspn(s, searchFor);

	    // If we have a meta char specified the char we've just found
	    // was prefixed by the meta char we must continue our search
	    while (_meta && n > 0 && s[n-1] == _meta) // && s[n+1])
		{
		int	p, mcount=0;

		// Count the number of meta chars prefixing this
		// If it's even we're not really prefixed and we've found the real
		// quote!
		for (p=n-1;p>=0&&s[p]==_meta;--p) mcount++;
		if ((mcount % 2) == 0) break;

		n++;
		if (!s[n]) break;
		tmpstr.insert(tmpstr.length(), s, n);
		s += n;
		n = (int)strcspn(s, searchFor);
		}

	    tmpstr.insert(tmpstr.length(), s, n);

	    if (_meta)
		{
		int	p, pos=0;

		while ((p=tmpstr.index(_meta, pos)) >= 0)
		    {
		    tmpstr.remove(p, 1);
		    p++;
		    pos = p;
		    }
		}

	    // Allocate space for and copy the string and place into the vector
	    _tokenVec.add(tmpstr);

	    s += n;
	    if (!*s) break;

	    // Skip over endQuote and delimiter
	    bool	skippedDelimiter = false;

	    s++;
	    if (searchFor != _chars) 
		{
		// We've skipped over the end quote and
		// should now be pointing at the delimiter
		// so we must move forward one if we can
		n = (int)strspn(s, _chars);
		if (n > 0) 
		    {
		    s++;
		    skippedDelimiter = true;
		    }
		}
	    else skippedDelimiter = true;

	    // If we've now got to the end of the string AND we were in not
	    // skip mode we must add an empty string to the token list
	    if (!*s && !_skip && skippedDelimiter) _tokenVec.add(_T(""));
	    }

	_doit = false;
	}



/**
** Test to see if there are more tokens available
**
** @return true if there are more tokens available, false otherwise
**/
bool
XTokenizer::hasMoreTokens()
	{
	if (_doit) tokenize();
	return (_curr < _tokenVec.size());
	}


/**
** This method gets the next token from the internal list. 
**
** If there are more tokens available for the current string
** the str parameter is assigned the value of the next token
** and the value true is returned.
**
** If there are no more tokens available the str parameter is unchanged
** and the value false is returned.
**
** @brief Get the next token
** @return true if there was a token available, false otherwise (str is unchanged)
** @param str	If a token is available 
**/
bool
XTokenizer::nextToken(XString &str)
	{
	if (_doit) tokenize();
	if (_curr < _tokenVec.size())
	    {
	    str = _tokenVec[_curr++];
	    return true;
	    }

	return false;
	}


/**
** This method gets the next token from the internal list. 
**
** If there are more tokens available for the current string
** the str parameter is assigned the value of the next token
** and the value true is returned.
**
** If there are no more tokens available the str parameter is unchanged
** and an exception is thrown
**
** @brief	Get the next token
** @exception	XTokenizerException	Thrown if no more tokens are available.
**/
XString
XTokenizer::nextToken()
	{
	XString	tmp;

	if (_doit) tokenize();
	if (_curr < _tokenVec.size()) tmp = _tokenVec[_curr++];
	else throw 1;

	return tmp;
	}


/**
** Return the number of tokens available
**/
int
XTokenizer::count()
	{
	if (_doit) tokenize();
	return _tokenVec.size();
	}


/**
*** @brief Set a new string for tokenizing
***
*** The setString method allows a new string to be tokenized without
*** the overhead of destructing and constructing a XTokenizer object.
***
*** @param str	String to be tokenized
**/
void
XTokenizer::setString(const TCHAR *str)
	{
	delete [] _string;
	_string = XString::strnew(str);
	_doit = true;
	}


/**
*** This method allows the caller to retrieve a specific token via a number rather than having to
*** step through the tokens to get to the required one. If the specified index is out of range a
*** IndexOutOfBoundsException is thrown.
***
*** @param n	A zero based index specifiying the token required.
*** @exception	IndexOutOfBoundsException if n is less than zero or greater than the number of available tokens
*** @return	the nth token or a blank string
*** @brief 	Get the nth token
*** @exception	IndexOutOfBoundsException	Thrown if n is out of range of the number
***						of tokens available.
**/
XString
XTokenizer::getToken(int n)
	{
	if (_doit) tokenize();
	if (n < 0 || n >= _tokenVec.size()) throw 1;
	return _tokenVec[n];
	}
