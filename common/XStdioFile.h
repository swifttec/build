/**
*** @file		XStdioFile.h
*** @brief		Basic file handling class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the XStdioFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __XStdioFile_h__
#define __XStdioFile_h__

#include "XString.h"

/**
*** @brief	Basic file handling class.
***
*** XStdioFile is a basic file handling class which hides the 32/64 bit implementation
*** of the underlying operating system by providing a 64-bit interface.
*** XStdioFile does not do any data caching or buffering an provides an interface
*** that is as close to the underlying O/S as possible.
***
*** @author		Simon Sparkes
**/
class XStdioFile
		{
public:
		/// Construct a file object ready to be opened.
		XStdioFile();

		/// Virtual destructor
		virtual ~XStdioFile();

		/**
		*** @brief	Opens the file
		**/
		int				open(const XString &filename, const XString &mode);

		/**
		*** @brief	Closes the file
		**/
		int				close();

		/// Return the open status of the file.
		bool				isOpen() const;

		/// Returns the current size of the file.
		int			size();

		/// Cast to a FILE
		operator FILE *()	{ return (FILE *)m_hFile; }

		/// Flush output if possible
		void				flush();

private: // Members
		FILE		*m_hFile;			///< A handle to the file
		};




#endif
