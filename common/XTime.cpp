/****************************************************************************
**	XTime.cpp	A class to represent a time in seconds and microseconds 
**			since 1st Jan 1900
****************************************************************************/


#include <math.h>
#if !defined(WIN32) && !defined(_WIN32)
#include <sys/time.h>
#endif
#include "XTime.h"
#include "XDate.h"

// The number of seconds between 1/1/1900 and 1/1/1970
// Our time and Unix time
#if defined(WIN32) || defined(_WIN32)
static int
gettimeofday(struct timeval *tp, void *p)
		{
		SYSTEMTIME  st;
		FILETIME    ft;
		int64_t	    t;

		UNREFERENCED_PARAMETER(p);

		GetSystemTime(&st);
		SystemTimeToFileTime(&st, &ft);

		t = (((int64_t)ft.dwHighDateTime << 32) | (int64_t)ft.dwLowDateTime) - 116444736000000000;
		tp->tv_sec = (int)(t / 10000000);
		tp->tv_usec = (int)((t % 10000000) / 10);

		return 0;
		}
#endif


XTime::~XTime()
		{
		}


// Construct with today's date
XTime::XTime() :
    m_seconds(0),
    m_nanoseconds(0)
		{
		update();
		}


// Get the time now
void
XTime::update()
		{
		struct timeval	tv;

		gettimeofday(&tv, NULL);
		*this = tv;
		}


// Construct the time from a Unix clock
XTime::XTime(time_t clock) :
    m_seconds(0),
    m_nanoseconds(0)
		{
		*this = clock;
		}


XTime::XTime(const FILETIME &ft) :
    m_seconds(0),
    m_nanoseconds(0)
		{
		*this = ft;
		}


XTime::XTime(const SYSTEMTIME &ft) :
    m_seconds(0),
    m_nanoseconds(0)
		{
		*this = ft;
		}



// Construct the time from a timeval
XTime::XTime(const struct timeval &tv) :
    m_seconds(0),
    m_nanoseconds(0)
		{
		*this = tv;
		}


// Construct from a double
XTime::XTime(double v, bool isVariantTime)
		{
		if (isVariantTime)
			{
			m_seconds = (int64_t)(v * SECONDS_IN_DAY);
			m_nanoseconds = 0;
			}
		else
			{
			double	w, f;

			// Split v into whole(w) and fractional(f) parts
			f = modf(v, &w);

			m_seconds = (int64_t)w;
			m_nanoseconds = (uint32_t)(f * 1000000000.0);
			}

		calculateDST();
		}


XTime::XTime(const XDate &dt)
		{
		*this = dt;
		}


XTime::XTime(const struct tm &tm)
		{
		*this = tm;
		}


XTime::XTime(int64_t secs, uint32_t nsecs, TimeType tt)
		{
		m_nanoseconds = nsecs;
		switch (tt)
			{
			case UnixTime:
				m_seconds = secs + SECONDS_XTIME_UNTIL_UNIXTIME;
				break;

			case WindowsFileTime:
				{
				int64_t	t;

				t = secs - (SECONDS_FILETIME_UNTIL_XTIME * 10000000UL);
				m_seconds = t / 10000000UL;
				m_nanoseconds += (int)(t % 10000000UL) * 100;
				}
				break;

			default:
				m_seconds = secs;
				break;
			}

		while (m_nanoseconds > 1000000000)
			{
			m_seconds++;
			m_nanoseconds -= 1000000000;
			}

		calculateDST();
		}



// set the time from a timeval
XTime &
XTime::operator=(const struct timeval &tv)
		{
		m_seconds = (int64_t)tv.tv_sec + SECONDS_XTIME_UNTIL_UNIXTIME;
		m_nanoseconds = tv.tv_usec * 1000;

		calculateDST();

		return *this;
		}


// set the time
XTime &
XTime::operator=(time_t clock)
		{
		m_seconds = (int64_t)clock + SECONDS_XTIME_UNTIL_UNIXTIME;
		m_nanoseconds = 0;

		calculateDST();

		return *this;
		}


// set the time
XTime &
XTime::operator=(double v)
		{
		double	w, f;

		// Split v into whole(w) and fractional(f) parts
		f = modf(v, &w);

		m_seconds = (int64_t)w;
		m_nanoseconds = (uint32_t)(f * 1000000000.0);

		calculateDST();

		return *this;
		}


// Set the time from a FILETIME
// Microsoft documentation states that the FILETIME structure
// is a 64-bit value representing the number of 100-nanosecond intervals since January 1, 1601. 
XTime &
XTime::operator=(const FILETIME &ft)
		{
		// t is the number of 100-nanosecond intervals since January 1, 1900
		int64_t	 t;

		t = (((int64_t)ft.dwHighDateTime << 32) | (int64_t)ft.dwLowDateTime) - (SECONDS_FILETIME_UNTIL_XTIME * 10000000UL);
		m_seconds = t / 10000000UL;
		m_nanoseconds = (int)(t % 10000000UL) * 100;

		calculateDST();

		return *this;
		}


FILETIME
XTime::toFILETIME() const
		{
		FILETIME	ft;

		if (!toFILETIME(ft)) throw 1;

		return ft;
		}


bool
XTime::toFILETIME(FILETIME &v) const
		{
		bool	res=true;
		int64_t	t;

		t = ((int64_t)m_seconds * 10000000UL) + ((int64_t)m_nanoseconds / 100UL) + (SECONDS_FILETIME_UNTIL_XTIME * 10000000UL);

		if (t >= 0)
			{
			v.dwHighDateTime = (u_long)(t >> 32);
			v.dwLowDateTime = (u_long)(t & 0xffffffff);
			}
		else res = false;

		return res;
		}



// Set the time from a SYSTEMTIME
// Microsoft documentation states that the SYSTEMTIME structure
// is a 64-bit value representing the number of 100-nanosecond intervals since January 1, 1601. 
XTime &
XTime::operator=(const SYSTEMTIME &ft)
		{
		XDate	d(ft.wDay, ft.wMonth, ft.wYear);

		if (d.isValid()
			&& ft.wHour < 24
			&& ft.wMinute < 60
			&& ft.wSecond < 62	// Can have leap seconds
			)
			{
			m_seconds = (int64_t)(d.julian() - JULIAN_DAY_31ST_DEC_1899) * SECONDS_IN_DAY + (int64_t)(ft.wHour * 3600) + (int64_t)(ft.wMinute * 60) + (int64_t)ft.wSecond;
			m_nanoseconds = ft.wMilliseconds * 1000000UL;
			}
		else throw 1;

		return *this;
		}


SYSTEMTIME
XTime::toSYSTEMTIME() const
		{
		SYSTEMTIME	ft;

		if (!toSYSTEMTIME(ft)) throw 1;

		return ft;
		}


/*
System time members as follows:
typedef struct _SYSTEMTIME {  WORD wYear;  WORD wMonth;  WORD wDayOfWeek;  WORD wDay;  WORD wHour;  WORD wMinute;  WORD wSecond;  WORD wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME;

wYear Current year. The year must be greater than 1601.  Windows Server 2003, Windows XP:  The year cannot be greater than 30827.
wMonth Current month; January = 1, February = 2, and so on. 
wDayOfWeek Current day of the week; Sunday = 0, Monday = 1, and so on. 
wDay Current day of the month. 
wHour Current hour. 
wMinute Current minute. 
wSecond Current second. 
wMilliseconds Current millisecond. 
*/
bool
XTime::toSYSTEMTIME(SYSTEMTIME &v) const
		{
		bool	res=true;
		XDate	d(*this);
		int		y=d.year();

		if (y > 1601 && y < 30828)
			{
			v.wYear = (WORD)d.year();
			v.wMonth = (WORD)d.month();
			v.wDayOfWeek = (WORD)d.dayOfWeek();
			v.wDay = (WORD)d.day();
			v.wHour = (WORD)hour();
			v.wMinute = (WORD)minute();
			v.wSecond = (WORD)second();
			v.wMilliseconds = (WORD)millisecond();
			}
		else res = false;

		return res;
		}



// Set the time from a struct tm
XTime &
XTime::operator=(const struct tm &tm)
		{
		XDate	d(tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900);

		if (d.isValid()
			&& tm.tm_hour >= 0 && tm.tm_hour < 24
			&& tm.tm_min >= 0 && tm.tm_min < 60
			&& tm.tm_sec >= 0 && tm.tm_sec < 62	// Can have leap seconds
			)
			{
			m_seconds = (int64_t)(d.julian() - JULIAN_DAY_31ST_DEC_1899) * SECONDS_IN_DAY + (int64_t)(tm.tm_hour * 3600) + (int64_t)(tm.tm_min * 60) + (int64_t)tm.tm_sec;
			m_nanoseconds = 0;
			}
		else throw 1;

		calculateDST();

		return *this;
		}


struct tm
XTime::toTM() const
		{
		struct tm	tm;

		if (!toTM(tm)) throw 1;

		return tm;
		}


bool
XTime::toTM(struct tm &v) const
		{
		bool	res=true;
		XDate	d(*this);
		int		y=d.year();

		if (y >= 1900)
			{
			v.tm_sec  = second();			// Seconds after minute (0 - 59). 
			v.tm_min = minute();			// Minutes after hour (0 - 59). 
			v.tm_hour = hour();				// Hours since midnight (0 - 23). 
			v.tm_mday = d.day();			// Day of month (1 - 31). 
			v.tm_mon = d.month() - 1;		// Month (0 - 11; January = 0). 
			v.tm_year = d.year() - 1900;	// Year (current year minus 1900). 
			v.tm_wday = d.dayOfWeek();		// Day of week (0 - 6; Sunday = 0). 
			v.tm_yday = d.dayOfYear();		// Day of year (0 - 365; January 1 = 0). 
			v.tm_isdst = isDST();			// Always 0 for gmtime. 
			}
		else res = false;

		return res;
		}




time_t
XTime::toUnixTime() const
		{
		time_t	t;

		if (!toUnixTime(t)) throw 1;

		return t;
		}


bool
XTime::toUnixTime(time_t &t) const
		{
		bool	res=true;
		int64_t	actual, maxtime;

		switch (sizeof(time_t))
			{
			case 4:		maxtime = 0x7fffffffUL;			break;
			case 8:		maxtime = 0x7fffffffffffffffUL;	break;

			// Assume 4 bytes for time_t
			default:	maxtime = 0x7fffffffUL;			break;
			}

		actual  = m_seconds - SECONDS_XTIME_UNTIL_UNIXTIME;
		if (actual < 0 || actual > maxtime)
			{
			// The time_t result is out of range
			res = false;
			}
		else t = (time_t)(m_seconds - SECONDS_XTIME_UNTIL_UNIXTIME);

		return res;
		}




struct timeval
XTime::toTimeVal() const
		{
		struct timeval	tv;

		if (!toTimeVal(tv)) throw 1;

		return tv;
		}


bool
XTime::toTimeVal(struct timeval &tv) const
		{
		bool	res=true;
		time_t	t;

		if (toUnixTime(t))
			{
#if defined(WIN32) || defined(_WIN32)
			tv.tv_sec = (long)t;
#else
			tv.tv_sec = t;
#endif
			tv.tv_usec = microsecond();
			}
		else res = false;

		return res;
		}



// Set the time from a struct tm
XTime &
XTime::operator=(const XDate &d)
		{
		if (d.isValid())
			{
			m_seconds = (int64_t)(d.julian() - JULIAN_DAY_31ST_DEC_1899) * SECONDS_IN_DAY;
			m_nanoseconds = 0;
			}
		else throw 1;

		calculateDST();

		return *this;
		}


XDate
XTime::toDate() const
		{
		XDate	d;

		if (!toDate(d)) throw 1;

		return d;
		}


bool
XTime::toDate(XDate &v) const
		{
		v = XDate(*this);

		return true;
		}


bool
operator==(const XTime &a, const XTime &b)
		{
		return (a.m_seconds == b.m_seconds && a.m_nanoseconds == b.m_nanoseconds);
		}


bool
operator!=(const XTime &a, const XTime &b)
		{
		return (a.m_seconds != b.m_seconds || a.m_nanoseconds != b.m_nanoseconds);
		}


bool
operator<=(const XTime &a, const XTime &b)
		{
		return (a.m_seconds <= b.m_seconds || (a.m_seconds == b.m_seconds && a.m_nanoseconds <= b.m_nanoseconds));
		}


bool
operator>=(const XTime &a, const XTime &b)
		{
		return (a.m_seconds >= b.m_seconds || (a.m_seconds == b.m_seconds && a.m_nanoseconds >= b.m_nanoseconds));
		}


bool
operator<(const XTime &a, const XTime &b)
		{
		return (a.m_seconds < b.m_seconds || (a.m_seconds == b.m_seconds && a.m_nanoseconds < b.m_nanoseconds));
		}


bool
operator>(const XTime &a, const XTime &b)
		{
		return (a.m_seconds > b.m_seconds || (a.m_seconds == b.m_seconds && a.m_nanoseconds > b.m_nanoseconds));
		}



// Return the hour (0-23)
int
XTime::hour() const
		{
		return (int)((m_seconds/3600) % 24);
		}

// Return the minute (0-59)
int
XTime::minute() const
		{
		return (int)((m_seconds/60) % 60);
		}

// Return the second (0-59)
int
XTime::second() const
		{
		return (int)(m_seconds % 60);
		}

// Return the fractional portion of the time in milliseconds
int
XTime::millisecond() const
		{
		return m_nanoseconds / 1000000;
		}

// Return the fractional portion of the time in microseconds
int
XTime::microsecond() const
		{
		return m_nanoseconds / 1000;
		}

// Return the fractional portion of the time in nanoseconds
int
XTime::nanosecond() const
		{
		return m_nanoseconds;
		}

// Return the day (1-31)
int
XTime::day() const
		{
		return XDate(*this).day();
		}

// Return the day (1-12)
int
XTime::month() const
		{
		return XDate(*this).month();
		}

// Return the year
int
XTime::year() const
		{
		return XDate(*this).year();
		}

// Return the dayOfWeek (0=Sunday, 1=Monday, ....)
int
XTime::dayOfWeek() const
		{
		return XDate(*this).dayOfWeek();
		}

// Return the day of year (1-366 if dayOneIsZero is false) or (0-365 if dayOneIsZero is true)
int
XTime::dayOfYear(bool dayOneIsZero) const
		{
		return XDate(*this).dayOfYear(dayOneIsZero);
		}

// Return the julian day number
int
XTime::julian() const
		{
		return (int)(m_seconds / SECONDS_IN_DAY) + JULIAN_DAY_31ST_DEC_1899;
		}



// Return the time as a double where the whole part is the number of seconds
// since midnight 31st Dec 1899 and the fraction part is the fraction of the second
double
XTime::toDouble() const
		{
		return (double)m_seconds + ((double)m_nanoseconds / 1000000000.0);
		}

// Return the time as a 64-bit integer where the representing the number of seconds
// since midnight 31st Dec 1899.
int64_t
XTime::toInt64(TimeType tt) const
		{
		int64_t	res=0;

		switch (tt)
			{
			case UnixTime:
				res = m_seconds - SECONDS_XTIME_UNTIL_UNIXTIME;
				break;

			case WindowsFileTime:
				res = ((int64_t)m_seconds * 10000000UL) + ((int64_t)m_nanoseconds / 100UL) + (SECONDS_FILETIME_UNTIL_XTIME * 10000000UL);
				break;

			default:
				res = m_seconds;
				break;
			}

		return res;
		}

// Return the Julian Day Number for this time
int
XTime::toJulian() const
		{
		return julian();
		}



/// Return the name of the timezone
XString
XTime::tzName(int dst) const
		{
		XString	s;

		s = (dst > 0)?"":"GMT";

		return s;
		}



void
XTime::calculateDST()
		{
		// For a standard XTime we do nothing
		}


int
XTime::dst() const
		{
		return 0;
		}



/*
From the cftime man page

     %%      same as %
     %a      locale's abbreviated weekday name
     %A      locale's full weekday name
     %b      locale's abbreviated month name
     %B      locale's full month name
     %c      locale's appropriate date and time representation
  Default
     %C      locale's date and time representation as produced by
             date(1)
 Standard-conforming
     %C      century number (the year divided by  100  and  trun-
             cated  to  an  integer  as a decimal number [1,99]);
             single digits are preceded by 0; see standards(5)
     %d      day of month [1,31]; single digits are preceded by 0
     %D      date as %m/%d/%y
     %e      day of month [1,31]; single digits are preceded by a
             space
     %h      locale's abbreviated month name
     %H      hour (24-hour clock) [0,23]; single digits are  pre-
             ceded by 0
     %I      hour (12-hour clock) [1,12]; single digits are  pre-
             ceded by 0
     %j      day number of year [1,366]; single digits  are  pre-
             ceded by 0
     %k      hour (24-hour clock) [0,23]; single digits are  pre-
             ceded by a blank
     %l      hour (12-hour clock) [1,12]; single digits are  pre-
             ceded by a blank
     %m      month number [1,12]; single digits are preceded by 0
     %M      minute [00,59]; leading zero is  permitted  but  not
             required
     %n      insert a newline
     %p      locale's equivalent of either a.m. or p.m.
     %r      appropriate time  representation  in  12-hour  clock
             format with %p
     %R      time as %H:%M
     %S      seconds [00,61]
     %t      insert a tab
     %T      time as %H:%M:%S
     %u      weekday as a decimal number [1,7], with 1 represent-
             ing Sunday
     %U      week number of year as  a  decimal  number  [00,53],
             with Sunday as the first day of week 1
     %V      week number of the year as a decimal number [01,53],
             with  Monday  as  the first day of the week.  If the
             week containing 1 January has four or more  days  in
             the  new  year, then it is considered week 1; other-
             wise, it is week 53 of the previous  year,  and  the
             next week is week 1.
     %w      weekday as a decimal number [0,6], with 0 represent-
             ing Sunday
     %W      week number of year as  a  decimal  number  [00,53],
             with Monday as the first day of week 1
     %x      locale's appropriate date representation
     %X      locale's appropriate time representation
     %y      year within century [00,99]
     %Y      year, including the century (for example 1993)
     %Z      time zone name or abbreviation, or no  bytes  if  no
             time zone information exists




XString
XTime::format(const XString &fmt)
		{
		XString	outstr;
		XString	formatString(fmt);
		int			c, v, p=0, q;
		XDate		calendar(*this);
		XLocale	&loc=XLocale::getCurrentLocale();
		char		tmp[80];

		while ((q = formatString.indexOf('%', p)) >= 0)
			{
			// If we have any chars copy them into outstr
		  	if (q > p) outstr += formatString.substring(p, q);

			// Now do the substitution on the %
			q++;
			if (q >= (int)formatString.length()) break;

			c = formatString.charAt(q++);
			switch (c)
				{
				case '%':	// same as %
					outstr += _T("%");
					break;

				case 'a':	// locale's abbreviated weekday name
					outstr += loc.dayName(calendar.dayOfWeek(), true);
					break;

				case 'A':	// locale's full weekday name
					outstr += loc.dayName(calendar.dayOfWeek(), false);
					break;

				case 'b':	// locale's abbreviated month name
					outstr += loc.monthName(calendar.month()-1, true);
					break;

				case 'B':	// locale's full month name
					outstr += loc.monthName(calendar.month()-1, false);
					break;

				case 'c':	// locale's appropriate date and time representation
					outstr += format(_T("%a %b %d %T %Y"));
					break;

				case 'C':	// Default: locale's date and time representation as produced by date(1)
					outstr += format(_T("%a %b %e %T %Z %Y"));
					break;

				case 'D':	// date as %m/%d/%y
					outstr += format(_T("%m/%d/%y"));
					break;

				case 'd':	// day of month [1,31]; single digits are preceded by 0
				case 'e':	// day of month [1,31]; single digits are preceded by a space
					sprintf(tmp, c=='d'?"%02d":"%2d", calendar.dayOfMonth());
					outstr += tmp;
					break;

				case 'g':	// Week-based year within century [00,99].
					sprintf(tmp, "%02d", calendar.year() % 100);
					outstr += tmp;
					break;

				case 'G':	// Week-based year, including the century [0000,9999].
					sprintf(tmp, "%04d", calendar.year() % 10000);
					outstr += tmp;
					break;

				case 'h':	// locale's abbreviated month name
					outstr += loc.monthName(calendar.month()-1, true);
					break;

				case 'H':	// hour (24-hour clock) [0,23]; single digits are  preceded by 0
				case 'k':	// hour (24-hour clock) [0,23]; single digits are  preceded by a blank
					sprintf(tmp, c=='H'?"%02d":"%2d", hour());
					outstr += tmp;
					break;

				case 'I':	// hour (12-hour clock) [1,12]; single digits are  preceded by 0
				case 'l':	// hour (12-hour clock) [1,12]; single digits are  preceded by a blank
					v = hour() % 12;

					if (v == 0) v = 12;
					sprintf(tmp, c=='I'?"%02d":"%2d", v);
					outstr += tmp;
					break;

				case 'j':	// day number of year [1,366]; single digits  are  preceded by 0
					sprintf(tmp, "%03d", calendar.dayOfYear());
					outstr += tmp;
					break;

				case 'm':	// month number [1,12]; single digits are preceded by 0
					sprintf(tmp, "%02d", calendar.month());
					outstr += tmp;
					break;

				case 'M':	// minute [00,59]; leading zero is  permitted  but  not required
					sprintf(tmp, "%02d", minute());
					outstr += tmp;
					break;

				case 'n':	// insert a newline
					outstr += "\n";
					break;

				case 'p':	// locale's equivalent of either a.m. or p.m.
					outstr += isAM()?loc.amString(true):loc.pmString(true);
					break;

				case 'P':	// locale's equivalent of either a.m. or p.m.
					outstr += isAM()?loc.amString(false):loc.pmString(false);
					break;

				case 'r':	// appropriate time  representation  in  12-hour  clock format with %p
					outstr += format("%I:%M:%S %p");
					break;

				case 'R':	// time as %H:%M
					outstr += format("%H:%M");
					break;

				case 'S':	// seconds [00,61]
					sprintf(tmp, "%02d", second());
					outstr += tmp;
					break;

				case 't':	// insert a tab
					outstr += _T("\t");
					break;

				case 'T':	// time as %H:%M:%S
					outstr += format("%H:%M:%S");
					break;

				case 'u':	// weekday as a decimal number [1,7], with 1 represent- ing Sunday
					v = calendar.dayOfWeek();
					if (v == 0) v = 7;
					sprintf(tmp, "%d", v);
					outstr += tmp;
					break;

				case 'U':	// week number of year as  a  decimal  number  [00,53], with Sunday as the first day of week 1
					sprintf(tmp, "%02d", calendar.weekOfYear());
					outstr += tmp;
					break;

				case 'V':	// week number of the year as a decimal number [01,53], with  Monday  as  the first day of the week.  If the week containing 1 January has four or more  days  in the  new  year, then it is considered week 1; other- wise, it is week 53 of the previous  year,  and  the next week is week 1.
					sprintf(tmp, "%02d", calendar.weekOfYear());
					outstr += tmp;
					break;

				case 'w':	// weekday as a decimal number [0,6], with 0 represent- ing Sunday
					sprintf(tmp, "%d", calendar.dayOfWeek());
					outstr += tmp;
					break;

				case 'W':	// week number of year as  a  decimal  number  [00,53], with Monday as the first day of week 1
					sprintf(tmp, "%02d", calendar.weekOfYear());
					outstr += tmp;
					break;

				case 'x':	// locale's appropriate date representation
					outstr += format("%d");
					outstr += loc.dateSeparator();
					outstr += format("%m");
					outstr += loc.dateSeparator();
					outstr += format("%Y");
					break;

				case 'X':	// locale's appropriate time representation
					outstr += format("%H");
					outstr += loc.timeSeparator();
					outstr += format("%M");
					outstr += loc.timeSeparator();
					outstr += format("%S");
					break;

				case 'y':	// year within century [00,99]
					sprintf(tmp, "%02d", calendar.year() % 100);
					outstr += tmp;
					break;

				case 'Y':	// year, including the century (for example 1993)
					sprintf(tmp, "%d", calendar.year());
					outstr += tmp;
					break;

				case 'z':	// time zone name or abbreviation, or no  bytes  if  no time zone information exists
				case 'Z':	// time zone name or abbreviation, or no  bytes  if  no time zone information exists
					{
					outstr += tzName(dst());
					}

					break;

				default:
					// Don't know so keep it!
					outstr += "%";
					outstr += c;
					break;
				}

			p = q;
			}

		// Add any remaining chars
		outstr += formatString.substring(p);

		return outstr;
		}
		*/
