/****************************************************************************
** XFilename.h	A data I/O block
****************************************************************************/



#ifndef __base_XFilename_h__
#define __base_XFilename_h__

#include "XStringArray.h"

// Forward declarations

#if defined(WIN32) || defined(_WIN32)
	#define X_PATH_SEPARATOR			'\\'
	#define X_PATH_SEPARATOR_STRING	"\\"
#else
	#define X_PATH_SEPARATOR			'/'
	#define X_PATH_SEPARATOR_STRING	"/"
#endif

/**
*** A generic file handle.
***
*** @ingroup	core
*** @author		Simon Sparkes
**/
class XFilename
		{
public:
		/// An enumeration of basic file handle types
		enum PathType
			{
			PathTypeNoChange=0,	///< Path separator should not be changed
			PathTypePlatform=1,	///< Platform specific path
			PathTypeUnix=2,		///< Path using unix separators - i.e. forward slash
			PathTypeWindows=3	///< Path using windows separators - i.e. backslash
			};

public:
		/// Default constructor
		XFilename(const XString &path="", PathType=PathTypeNoChange);

		/// Virtual destructor
		virtual ~XFilename();

		/// Copy constructor.
		XFilename(const XFilename &other);

		/// Assignment operator - XFilename
		XFilename &	operator=(const XFilename &other);

		/// Assignment operator - XString
		XFilename &	operator=(const XString &other);


		/// Returns true if the path is a relative path. Blank paths always return false.
		bool				isRelativePath() const;

		/// Returns true if the path is a absolute path (e.g. starts with /). Blank paths always return false.
		bool				isAbsolutePath() const;

		/// Returns the file extension (e.g. x.cpp) will return cpp
		XString			extension() const;

		/// Returns the directory part of a filename (e.g. /a/b/c -> /a/b)
		XString			dirname() const;

		/// Returns the base part of a filename (e.g. /a/b/c -> c)
		XString			basename() const;

		/// Returns the drive part of a filename (e.g. x:/a/b/c -> x)
		XString			drivename() const;

		/// Returns the share part of a filename.
		XString			sharename() const;

		/// Returnd the base and extension of the filename.
		void				split(XString &base, XString &extension) const;

		/// Returns the type of this path
		PathType			type() const	{ return m_type; }

public:
		/**
		*** Fixes a filename to ensure that it is valid and contains no extra components
		*** and all the separators are correct.
		***
		*** For the windows platform:
		***	C:    		-> C:\\<br>
		***	C:\\xxx\\	-> C:\\xxx
		***
		*** For the Unix platforms
		***	/a/	-> /a<br>
		***	/a//b	-> /a/b
		***
		*** If the pathtype is anything other than PathTypeNoChange all path
		*** separators are changed to the type specified. If the pathtype is 
		*** PathTypeNoChange (the default) the separators in the filename are
		*** left unchanged.
		***
		*** @param	path	The pathname to fix.
		*** @param	type	The type of path separator to use.
		**/
		static XString		fix(const XString &path, PathType type=PathTypeNoChange);

		/**
		*** Concatenate 2 paths.
		*** Assumes that 2nd path is relative, if not results are unpredicatable!
		***
		*** This function will handle both dos and unix paths. Specify a type
		*** if a specific concatenation is required, otherwise the default
		*** is to use the platform default. (Unix=/, Dos=\)
		***
		*** @param path1	The first path
		*** @param path2	The path to append
		*** @param ptype	The path type
		**/
		static XString		concatPaths(const XString &path1, const XString &path2, PathType ptype=PathTypePlatform);

		/// Returns true if the path is a relative path. Blank paths always return false.
		static bool			isRelativePath(const XString &path);

		/// Returns true if the path is a absolute path (e.g. starts with /). Blank paths always return false.
		static bool			isAbsolutePath(const XString &path);

		/**
		*** Return the extension of the given file name.
		*** 
		*** The extension is any characters after (but not including) the last period
		*** of the filename.
		***
		*** e.g.	fred.c		-> extension=c
		***			fred		-> extension=
		***
		*** If the filename includes a directory part this will be ignored.
		***
		*** e.g.	a/fred.c	-> extension=c
		**/
		static XString		extension(const XString &file);

		/**
		*** Returns the directory component of a path.
		***
		*** The dirname method takes a pointer to a character string
		*** that  contains a pathname, and returns a pointer to a string
		*** that is a pathname of the parent directory of that file.
		*** Trailing '/' characters in the path are not counted as part
		*** of the path.
		*** 
		*** If path does not contain a '/',  then  dirname()  returns  a
		*** pointer  to  the  string "." .  If path is a null pointer or
		*** points to an empty string, dirname() returns  a  pointer  to
		*** the string "." .
		***
		*** <b>Addition to Dos/Windows version</b><br>
		*** If the path is a DOS drive root path (e.g x:\) this is returned
		**/
		static XString		dirname(const XString &file);

		/**
		*** Returns the last component of a path (normally filename)
		*** like the standard basename, except that we return a XString
		*** 
		*** Note that this function will parse both DOS and Unix format
		*** filenames.
		**/
		static XString		basename(const XString &file);
		
		/**
		*** This function returns the drive component of a path.
		*** This only makes sense in windows/dos, but left for compatibiliy reasons
		*** and will return an empty string under Unix.
		**/
		static XString		drivename(const XString &file);

		/**
		*** This function returns the share component of a path
		*** This only makes sense in windows/dos, but left for compatibiliy reasons
		*** and returns a blank in a non-dos environment.
		**/
		static XString		sharename(const XString &file);

		/**
		*** Split a path into it's component parts
		**/
		static int			splitPath(const XString &path, XStringArray &sa);

		/**
		*** This function splits the given file name into its base and extension.
		*** 
		*** The extension is any characters after (but not including) the last period
		*** of the filename. The base consists of all characters before
		*** (but not including) the last period. If not period appears in the base will
		*** be the same as the filename, and the extension will be blank.
		***
		*** e.g.	fred.c		-> base=fred	extension=c
		***			fred		-> base=fred	extension=
		***
		*** If the filename includes a directory part this will be ignored.
		***
		*** e.g.	a/fred.c	-> base=fred	extension=c
		**/
		static void			split(const XString &file, XString &base, XString &extension);


		/**
		*** Optimize a path by reducing duplicate path separators
		*** . and .. where possible
		**/
		static XString		optimizePath(const XString &path, PathType type);

		/**
		*** Joins path components to make a path
		**/
		static XString		joinPath(XStringArray &sa, PathType type);

		/**
		*** Returns the absolute path name for the given path
		*** in relation to the specified directory (default current)
		***
		*** If the neither the directory nor the path passed to this function are absolute
		*** it is impossible to generate an absolute path and the result will be the same
		*** as calling concatPaths.
		***
		*** @param path		The path name
		*** @param dir		The directory name used to generate a relative path
		*** @param type		The path type
		**/
		static XString		absolutePath(const XString &path, const XString &dir, PathType type);


		/**
		*** Returns the relative path name for the given path
		*** in relation to the specified directory (default .)
		***
		*** There are certain conditions when it is impossible to calculate a relative
		*** path. These are described below:
		***
		*** When the path is absolute and the directory is relative it is impossible
		*** to determine the relative path between them. Under these circumstances the
		*** orginal path is returned.
		***
		*** When the path and the directory are on different physical drives (e.g. c: and d:)
		*** there is no relative path between them. Under these circumstances the
		*** orginal path is returned.
		***
		*** @param p		The path name (can be absolute)
		*** @param d		The directory name used to generate an absolute path
		*** @param type		The path type
		**/
		static XString		relativePath(const XString &p, const XString &d, PathType type);

private:	 // Members
		PathType	m_type;		///< The type of path
		XString	m_path;		///< The path string
		};


#endif
