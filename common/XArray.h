/****************************************************************************
**	XArray.h	A generic array of anything common to all array objects
****************************************************************************/


#ifndef __base_X_Array_h__
#define __base_X_Array_h__

#include "XBuffer.h"

/**
*** A generic array class which is self sizing. If an element is accessed 
*** at an index out of range the array is automatically resized to incorporate
*** that index and it's reference is then returned.
*** 
*** All memory is automatically created and return to the system on destruction
***
*** @brief		A generic array class
*** @ingroup	core
*** @author		Simon Sparkes
***/
class XArray
		{
protected:
		/// Protected constructor
		XArray(int size, int elemsize, int incsize);

public:
		/// Virtual destructor
		virtual ~XArray();

		/// Return the size of the array
		virtual int	size() const	{ return _nelems; }

		/// Return the size of the array
		virtual int	count() const	{ return size(); }

		/// Clear/empty array of all elements
		virtual void	clear();

		// Remove the one or more elements
		virtual void	remove(int pos, int count=1)	{ removeElements(pos, count); }

		/// MFC compatibility - clear()
		virtual void	RemoveAll()	{ clear(); }

		/// MFC compatibility - size()
		virtual int	GetSize()	{ return size(); }

		/// MFC compatibility - remove one or more elements
		virtual void	RemoveAt(int pos, int count=1)	{ removeElements(pos, count); }

		// Insert the one or more elements blank elements
		virtual void	insertElements(int pos, int count=1);

		// Remove the one or more elements
		virtual void	removeElements(int pos, int count=1);

protected:
		/// Ensure that the array has sufficient spaces for the specified number of elements
		void	ensureCapacity(size_t nelems);

protected:
		XBuffer	_data;		// The array data
		int		_nelems;	// The number of elements in the array
		int		_elemsize;	// The size of each element
		};

#endif
