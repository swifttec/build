/****************************************************************************
**	FileInfo.h	A class to hold the information for a file entry
****************************************************************************/


#ifndef __io_X_FileInfo_h__
#define __io_X_FileInfo_h__

#include "Xtypes.h"

#include "XString.h"
#include "XTime.h"

#include "XFileAttributes.h"

//#include <sys/stat.h>


#if defined(WIN32) || defined(_WIN32)
typedef int		x_mode_t;
typedef int		x_uid_t;
typedef int		x_gid_t;
#else
typedef mode_t	x_mode_t;
typedef uid_t	x_uid_t;
typedef gid_t	x_gid_t;
#endif

#if defined(WIN32) || defined(_WIN32)

#else // if defined(WIN32) || defined(_WIN32)

// Unix Code
#include <dirent.h>


#define INVALID_FILE_ATTRIBUTES				0xffffffff

#endif // if defined(WIN32) || defined(_WIN32)



/**
*** A class holding information for a file.
***
*** A XFileInfo object represents the information about a file
*** that can be gathered from the file system. The information
*** is gathered from system calls such as stat (unix) and
*** GetFileAttributes(windows).
***
*** Because Windows and Unix represent files very differently
*** effort has been put in to this class to represent the
*** information in a platform independent way, yet without
*** losing information for each platform.
***
*** Internally the XFileInfo object uses a set of validate flags
*** to determine which information about the file is valid. Using this technique
*** the XFileInfo object only gathers information about attributes accessed thus
*** potentially reducing the number of calls to the underlying O/S.
***
*** @ingroup	io
*** @see		XFolder
*** @author		Simon Sparkes
**/
class XFileInfo
		{
friend class XFolder;

public:
		enum ValidateType
			{
			FileNone=0x00,
			FileAttributes=0x01,
			FileTime=0x04,
			FileSize=0x08,
			FileName=0x10,
			FileAll=0x1f
			};

public:
		/// Default constructor
		XFileInfo(const XString &filename=_T(""));

		// Copy constructor
		XFileInfo(const XFileInfo &v);

		virtual ~XFileInfo();

		// Assignment operator
		const XFileInfo &	operator=(const XFileInfo &v);

		/// Comparison operator (XString) - compares equality with name name (no directory)
		bool operator==(const XString &s)					{ return (_name == s); }

		/// Return the file name
		const XString &	getName() const					{ return _name; }

		/// Return the directory name
		const XString &	getDirectory() const			{ return _directory; }

		/// Return the directory name in Dos format (8.3)
		const XString &	getDosDirectory()				{ validate(FileName); return _dosdir; }

		// Return the full path name
		XString			getFullPathname() const;

		// Return the full path name
		XString			getDosFullPathname() const;

		// Return the filename in dos format (8.3) on WIN32 platform
		// returns standard filename in Unix
		XString 			getDosName()					{ validate(FileName); return _dosname; }

		// File mode (see mknod(2))
		XString			getUnixModeString();

		// Inode number
		ino_t				getInode()						{ validate(FileAttributes); return (ino_t)_inode; }

		// ID of device containing a directory entry for this file
		dev_t				getDirectoryDeviceNo()			{ validate(FileAttributes); return _dirdevno; }

		// ID of device This entry is defined only for char special or block special files
		dev_t				getDeviceNo()					{ validate(FileAttributes); return _devno; }

		// Number of links
		uint32_t				getLinkCount()					{ validate(FileAttributes); return _linkcount; }

		// User/Group IDs
		//x_uid_t			getOwner()						{ validate(FileAttributes); return _uid; }
		//x_gid_t			getGroup()						{ validate(FileAttributes); return _gid; }

		// File size (always 64-bit for future proofing)
		uint64_t		getSize()						{ validate(FileSize); return _size; }

		// Get various file times
		XTime				getLastAccessTime()				{ validate(FileTime); return _lastAccessTime; }
		XTime				getLastModificationTime()		{ validate(FileTime); return _lastModificationTime; }
		XTime				getLastWriteTime()				{ validate(FileTime); return _lastModificationTime; }
		XTime				getCreateTime()					{ validate(FileTime); return _createTime; }
		XTime				getLastStatusChangeTime()		{ validate(FileTime); return _lastStatusChangeTime; }

		// Get the Dos file attributes
		XFileAttributes	getAttributes()					{ validate(FileAttributes); return _attributes; }
		XString			getAttributesString();

		// Some useful shortcuts
		bool						isValid();
		bool						hasAttributes(x_fileattr_t wanted, bool wantAny=true);
		bool						isHidden()			{ return (hasAttributes(XFileAttributes::FileAttrHidden)); }
		bool						isSystem()			{ return (hasAttributes(XFileAttributes::FileAttrSystem)); }
		bool						isReadOnly()		{ return (hasAttributes(XFileAttributes::FileAttrReadOnly)); }
		bool						isArchive()			{ return (hasAttributes(XFileAttributes::FileAttrArchive)); }
		bool						isNormal()			{ return (hasAttributes(XFileAttributes::FileAttrNormal)); }
		bool						isTemporary()		{ return (hasAttributes(XFileAttributes::FileAttrTemporary)); }
		bool						isSparse()			{ return (hasAttributes(XFileAttributes::FileAttrSparse)); }
		bool						isCompressed()		{ return (hasAttributes(XFileAttributes::FileAttrCompressed)); }
		bool						isOffline()			{ return (hasAttributes(XFileAttributes::FileAttrOffline)); }
		bool						isNotIndexed()		{ return (hasAttributes(XFileAttributes::FileAttrNotIndexed)); }
		bool						isEncrypted()		{ return (hasAttributes(XFileAttributes::FileAttrEncrypted)); }
		bool						isReparsePoint()	{ return (hasAttributes(XFileAttributes::FileAttrReparsePoint)); }
		bool						isSymbolicLink()	{ return (hasAttributes(XFileAttributes::FileAttrSymbolicLink)); }


		// Read/Write/Execute etc
		bool						isSticky()			{ return (hasAttributes(XFileAttributes::FileAttrSticky)); }
		bool						isSetUserID()		{ return (hasAttributes(XFileAttributes::FileAttrSetUserID)); }
		bool						isSetGroupID()		{ return (hasAttributes(XFileAttributes::FileAttrSetGroupID)); }
		bool						isOwnerRead()		{ return (hasAttributes(XFileAttributes::FileAttrOwnerRead)); }
		bool						isOwnerWrite()		{ return (hasAttributes(XFileAttributes::FileAttrOwnerWrite)); }
		bool						isOwnerExecute()	{ return (hasAttributes(XFileAttributes::FileAttrOwnerExecute)); }
		bool						isGroupRead()		{ return (hasAttributes(XFileAttributes::FileAttrGroupRead)); }
		bool						isGroupWrite()		{ return (hasAttributes(XFileAttributes::FileAttrGroupWrite)); }
		bool						isGroupExecute()	{ return (hasAttributes(XFileAttributes::FileAttrGroupExecute)); }
		bool						isWorldRead()		{ return (hasAttributes(XFileAttributes::FileAttrWorldRead)); }
		bool						isWorldWrite()		{ return (hasAttributes(XFileAttributes::FileAttrWorldWrite)); }
		bool						isWorldExecute()	{ return (hasAttributes(XFileAttributes::FileAttrWorldExecute)); }

		// File Type
		XFileAttributes::FileType	getType()			{ validate(FileAttributes); return _attributes.type(); }
		bool						isDevice()			{ validate(FileAttributes); return _attributes.isDevice(); }
		bool						isFifo()			{ validate(FileAttributes); return _attributes.isFifo(); }
		bool						isPipe()			{ validate(FileAttributes); return _attributes.isPipe(); }
		bool						isCharDevice()		{ validate(FileAttributes); return _attributes.isCharDevice(); }
		bool						isBlockDevice()		{ validate(FileAttributes); return _attributes.isBlockDevice(); }
		bool						isSocket()			{ validate(FileAttributes); return _attributes.isSocket(); }
		bool						isDirectory()		{ validate(FileAttributes); return _attributes.isDirectory(); }
		bool						isFile()			{ validate(FileAttributes); return _attributes.isFile(); }
		bool						isDoor()			{ validate(FileAttributes); return _attributes.isDoor(); }
		bool						isXenixNamedFile()	{ validate(FileAttributes); return _attributes.isXenixNamedFile(); }
		bool						isNetworkSpecial()	{ validate(FileAttributes); return _attributes.isNetworkSpecial(); }

		// Refresh the file info
		virtual void		refresh();

		// For symbolic links we can read the link value
		XString			getLinkValue();

		virtual uint_t	getInfoForFile(const XString &filename);


public:	// STATIC methods
		/// Returns true if the specified filename is a file.
		static bool				isFile(const XString &filename);

		/// Returns true if the specified filename is a folder/directory.
		static bool				isDirectory(const XString &filename);

		/// Returns true if the specified filename is a folder/directory.
		static bool				isFolder(const XString &filename);

		/// Returns true if the specified filename is a symbolic link.
		static bool				isSymbolicLink(const XString &filename);

		/// Returns true if the specified filename is readable.
		static bool				isReadable(const XString &filename);

		/// Returns true if the specified filename is writeable.
		static bool				isWriteable(const XString &filename);

		/// Returns true if the specified filename is executeable.
		static bool				isExecutable(const XString &filename);	

		/// Returns the size of the specified file in bytes or -1 on error.
		static uint64_t	size(const XString &filename);

		/**
		*** Retrieves the various file times for the specified file.
		***
		*** This function gets the file times for the specified path.
		*** The time parameters are optional and if they are specifed
		*** as NULL then that time element is not retrieved.
		***
		*** Note that Unix does not have the concept of creation time
		*** so it is simulated using last modification time.
		***
		*** @param path				The filename to set the times on.
		*** @param lastModTime		If not null a pointer to a Time object to receive
		***							the last modification time of the file.
		*** @param createTime		If not null a pointer to a Time object to receive
		***							the creation time of the file.
		*** @param lastAccessTime	If not null a pointer to a Time object to receive
		***							the last access time of the file.
		**/
		static uint_t		getFileTimes(const XString &path, XTime *lastModTime, XTime *createTime, XTime *lastAccessTime);

		/**
		*** This function sets the file times for the specified path.
		*** The time parameters are optional and if they are specifed
		*** as NULL then that time element is not modified.
		***
		*** Note that Unix does not have the concept of creation time
		*** so it is ignored.
		***
		*** @brief	Retrieves the various file times from the file
		***
		*** @param path				The filename to set the times on.
		*** @param lastModTime		If not null a pointer to a Time object to containing
		***							the last modification time of the file.
		*** @param createTime		If not null a pointer to a Time object to containing
		***							the creation time of the file.
		*** @param lastAccessTime	If not null a pointer to a Time object to containing
		***							the last access time of the file.
		**/
		static uint_t		setFileTimes(const XString &path, XTime *lastModTime, XTime *createTime, XTime *lastAccessTime);

protected:
		virtual void		reset();
		virtual void		validate(ValidateType type);

		virtual bool		getAttributeInfo(const XString &filename);
		virtual bool		getTimeInfo(const XString &filename);
		virtual bool		getSizeInfo(const XString &filename);
		virtual bool		getNameInfo(const XString &filename);

protected:
		XString			_name;					///< The filename (no path)
		XString			_directory;				///< The directory in which the entry is found
		XString			_dosname;				///< The filename (no path) in Dos (8.3) format
		XString			_dosdir;				///< The directory in which the entry is found in Dos format
		int				_validData;				///< Flags indicating which parts of the file info have been validated.
		XFileAttributes	_attributes;			///< The file attributes
		uint64_t		_size;					///< The actual file size
		uint64_t		_inode;					///< The inode number
		dev_t			_devno;					///< The device number (valid only for devices)
		dev_t			_dirdevno;				///< The device number of the directories device
		uint32_t		_linkcount;				///< The number of links to this file
		XTime			_createTime;			///< The creation time of the file.
		XTime			_lastAccessTime;		///< The time the file was last accessed
		XTime			_lastModificationTime;	///< The time the file was last modified
		XTime			_lastStatusChangeTime;	///< The time the status of the file last changed
		};

#endif
