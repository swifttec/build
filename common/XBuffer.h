/****************************************************************************
**	Buffer.h	A general purpose memory buffer
****************************************************************************/


#ifndef __base_Buffer_h__
#define __base_Buffer_h__

#include "Xtypes.h"

/**
*** General purpose memory buffer class.
***
*** A general purpose memory buffer that can be resized
*** copied to/from and used directly in most memory functions
*** via the casts.
*** 
*** A XBuffer has the concept of a size and a capacity. Rather than resizing the buffer
*** every time data is appended to the buffer or the size is changed the buffer is
*** only reallocated if the new size will be greater than the current buffer capacity
*** 
*** The new capacity is calculated according to the increment value:
***		- 0  = set capacity to size requested
***		- < 0 = double capacity until enough space
***		- > 0 = increment capacity by _increment until enough space
***
*** The increment algorithm can be changed using the setIncrement method.
***
*** <b>NOTE:</b>
*** There is no protection against doing a memcpy into the
*** buffer when it has insufficient space. Use the read/write
*** functions in preference to memcpy
***
*** @ingroup core
**/
class XBuffer
		{
public:
		/**
		*** Default constructor - construct a buffer of the specified size using the specified increment value.
		***
		*** Default constructor creates a buffer with a size and capacity of zero,
		*** an increment size of 32, and a read-only flag of false. The default parameters
		*** can be overridden as required.
		*** @param size		The initial size of the buffer
		*** @param increment	The initial increment value
		**/
		XBuffer(size_t size=0, int increment=32);	


		/**
		*** Buffer copy constructor
		***
		*** Copies size bytes from the memory location specified by ptr in the buffer.
		**/
		XBuffer(const void *ptr, size_t size, int increment=32);	

		/// Virtual destructor
		virtual ~XBuffer();

		/// Copy constructor - makes a copy of the XBuffer specified.
		XBuffer(XBuffer &);

		/// Assignment operator - makes a copy of the XBuffer specified
		const XBuffer &	operator=(const XBuffer &v);

		/// Compare to memory buffer (using memcpy)
		int			compareTo(const void *ptr, size_t size);

		/// Compare to another Buffer (using memcpy)
		int			compareTo(XBuffer &v)	{ return v.compareTo(_buf, _size); }

		/// Clear the buffer
		void		clear()				{ setSize(0); }

		/// Clear the buffer contents (reset to zero bytes), but keep the size the same
		void		clearContents()		{ setContents(0); }

		/// Set the buffer contents to contain all bytes as v
		void		setContents(char v);

		// Comparison Operators
		friend bool	operator==(XBuffer &s1, XBuffer &s2)	{ return (s1.compareTo(s2) == 0); }
		friend bool	operator!=(XBuffer &s1, XBuffer &s2)	{ return (s1.compareTo(s2) != 0); }
		friend bool	operator< (XBuffer &s1, XBuffer &s2)	{ return (s1.compareTo(s2) <  0); }
		friend bool	operator> (XBuffer &s1, XBuffer &s2)	{ return (s1.compareTo(s2) >  0); }
		friend bool	operator<=(XBuffer &s1, XBuffer &s2)	{ return (s1.compareTo(s2) <= 0); }
		friend bool	operator>=(XBuffer &s1, XBuffer &s2)	{ return (s1.compareTo(s2) >= 0); }

		/** 
		*** Set/change the size of the buffer.
		***
		*** Sets the data size of the buffer (as returned by size()) to the size specified.
		*** The capacity of the buffer will be adjusted to ensure that it is possible to store
		*** the specified number of bytes.
		***
		*** @param size	The new size required.
		*** @param keep	If true preserve the old data, otherwise the old data may be discarded.
		*** @param zero	If true any new part of the buffer will be intialised to zero.
		***				If both keep and zero flags are true the entire buffer will be intialised
		***				to zero. If zero is false no initialisation of the buffer is done.
		*** @param trim	If true set the buffer capacity to the exact size.
		**/
		void		setSize(size_t size, bool keep=true, bool zero=true, bool trim=false);

		/// Return the size of the data in the buffer in bytes.
		size_t		size() const		{ return _size; }

		/// Return the capacity of the buffer in bytes.
		size_t		capacity() const	{ return _capacity; }

		/// Return the increment value for this buffer
		int			increment() const	{ return _increment; }

		// Set the increment value
		void		setIncrement(int amount)	{ _increment = amount; }

		/**
		*** Write into the buffer from the specified memory location adjusting size if required.
		***
		*** @param	from	The memory location to copy data from.
		*** @param	nbytes	The number of bytes of data to copy.
		*** @param	offset	The offset into this buffer at which to start copying data to.
		***
		*** @return 		The number of bytes copied
		**/
		size_t		write(const void *from, size_t nbytes, size_t offset);


		/**
		*** Read data from the buffer into the specified memory location.
		***
		*** @param	to		The memory location to copy data to. <b>This buffer must contain
		***					sufficient space to copy nbytes of data to.</b>
		*** @param	nbytes	The number of bytes of data to copy.
		*** @param	offset	The offset into this buffer at which to start copying data from.
		***
		*** @return 		The number of bytes copied.
		**/
		size_t		read(void *to, size_t nbytes, size_t offset);


		/// Return a pointer to the buffer
		void *		buffer() const			{ return (void *)_buf; }

		/// Cast into unsigned char pointer
		operator unsigned char *() const	{ return (unsigned char *)_buf; }

		/// Cast into char pointer
		operator char *() const				{ return (char *)_buf; }

		/// Cast into void pointer
		operator void *() const				{ return (void *)_buf; }

		/// Cast into void * pointer
		operator void **() const			{ return (void **)_buf; }

		/// Index operator
		char& operator[](int i) const;

private:
		char	*_buf;		///< A pointer to the buffer
		size_t	_size;		///< The reported size of the buffer
		size_t	_capacity;	///< The capacity (actual size) of the buffer
		int		_increment;	///< The amount to increment capacity by
		};


#endif
