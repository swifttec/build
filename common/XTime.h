/****************************************************************************
**	Time.h		A class to represent a time in seconds and microseconds since 1st Jan 1900
****************************************************************************/


#ifndef __base_XTime_h__
#define __base_XTime_h__


#include "Xtypes.h"
#include <time.h>
#include "XString.h"

#ifndef __base_XDate_h__
#include "XDate.h"
#endif

// Forward declarations
class XDate;


// Special dates as Julian day numbers
#define JULIAN_DAY_1ST_JAN_1601		2305814
#define JULIAN_DAY_31ST_DEC_1899	2415020
#define JULIAN_DAY_1ST_JAN_1900		2415021
#define JULIAN_DAY_1ST_JAN_1970		2440588

// Special time periods in seconds used in calculations between various time formats
// on different platforms
//#define SECONDS_1900_UNTIL_1970	 2208988800UL
//#define SECONDS_1601_UNTIL_1900	 9435484800UL
//#define SECONDS_1601_UNTIL_1970	11644473600UL

/// The number of seconds between zero in XTime and zero in Unix time
#define SECONDS_XTIME_UNTIL_UNIXTIME	 2209075200UL

/// The number of seconds between zero in windows FILETIME and 
/// zero in XTime
#define SECONDS_FILETIME_UNTIL_XTIME	 9435398400UL

// Just plain useful!
#define SECONDS_IN_DAY		86400
#define SECONDS_IN_HOUR		3600

/**
*** A class to represent a time in seconds and nanoseconds.
***
***	The XTime class is a portable representation of time. It has 2 components
*** - The time in seconds since midnight on 31st December 1899 (held as a 64-bit integer).
*** - The time fraction in nanoseconds (held as a 32-bit unsigned integer)
***
*** The XTime makes no adjustments for timezone and works in GMT time.
***
*** @author Simon Sparkes
*** @ingroup core
**/
class XTime
		{
public:
		/**
		*** An enumeration used to determine the time type when performing a
		*** conversion from another type.
		**/
		enum TimeType
			{
			InternalTime,			///< The internal time format (time in seconds since 00:00:00 on 31 Dec 1899)
			UnixTime,				///< A standard unix time_t time (time in seconds since 00:00:00 on 1 Jan 1970).
			WindowsFileTime			///< A windows FILETIME as a 64-bit number (the number of 100-nanosecond intervals since 00:00:00 on January 1, 1601).
			};

public:
		virtual ~XTime();

		/// Construct the time object with the current time
		XTime();

		/// Construct from a unix timeval
		XTime(const struct timeval &tv);

		/// Construct from unix time
		XTime(time_t clock);

		/// Construct from a windows FILETIME
		XTime(const FILETIME &ft);

		/// Construct from a windows SYSTEMTIME
		XTime(const SYSTEMTIME &ft);

		/// Construct from a XDate - time will be set to midnight (00:00) on the
		/// date specified by the XDate object.
		XTime(const XDate &ft);

		/// Construct from a struct tm (unix)
		XTime(const struct tm &tm);


		/**
		*** Construct from a double (either a XTime double or a windows variant time)
		***
		*** @param	clock			The time value. This is handled according to the isVariantTime parameter.
		*** @param	isVariantTime	If false (the default) the double is treated as a XTime double where
		***							the whole part of the double is the number of seconds since midnight on
		***							31/12/1899 and the fractional part represents the fraction of the time.
		***
		***							If isVariantTime is true then the double is treated as a Windows variant
		***							time where the whole part of the double represents the number of days
		***							since 31/12/1899 and the fraction part represents the fractional part
		***							of the day since midnight (e.g. 0.25=6am, 0.5=12pm, etc)
		***							When converting from a variant time the fractional part of seconds is ignored
		***							and always set to zero.
		**/
		XTime(double clock, bool isVariantTime=false);


		/**
		*** Construct from a int64 and uint32.
		***
		*** @param	secs			The seconds value. This is handled according to the type parameter.
		*** @param	nsecs			The nanoseconds value.
		*** @param	type			Specifies the type of time being assigned. This can have any of the values
		***							from the TimeType enumeration.
		**/
		XTime(int64_t secs, uint32_t nsecs, TimeType tt=InternalTime);

/**
*** @name Assignment operators
**/
//@{
		/// Assignmemt from a unix timeval
		XTime &	operator=(const struct timeval &tv);

		/// Assignment from a unix time
		XTime &	operator=(time_t clock);

		/// Assignment from a double (XTime format only)
		XTime &	operator=(double v);

		/// Assignment from a windows FILETIME
		XTime &	operator=(const FILETIME &v);

		/// Assignment from a windows SYSTEMTIME
		XTime &	operator=(const SYSTEMTIME &v);

		/// Assignment from a XDate
		XTime &	operator=(const XDate &v);

		/// Assignment from a struct tm (unix)
		XTime &	operator=(const struct tm &v);
//@}


/**
*** @name Member access
**/
//@{

		/// Return the hour (0-23)
		virtual int		hour() const;

		/// Return the minute (0-59)
		virtual int		minute() const;

		/// Return the second (0-59)
		virtual int		second() const;

		/// Return the fractional portion of the time in milliseconds
		virtual int		millisecond() const;

		/// Return the fractional portion of the time in microseconds
		virtual int		microsecond() const;

		/// Return the fractional portion of the time in nanoseconds
		virtual int		nanosecond() const;

		/// Return the day (1-31)
		virtual int		day() const;

		/// Return the day (1-12)
		virtual int		month() const;

		/// Return the year
		virtual int		year() const;

		/// Return the dayOfWeek (0=Sunday, 1=Monday, ....)
		virtual int		dayOfWeek() const;

		/// Return the day of year (1-366 if dayOneIsZero is false) or (0-365 if dayOneIsZero is true)
		virtual int		dayOfYear(bool dayOneIsZero=false) const;

		/// Return the julian day number
		virtual int		julian() const;

//@}


/**
*** @name Time Conversions
**/
//@{
		/// Return the time as a double where the whole part is the number of seconds
		/// since midnight 31st Dec 1899 and the fraction part is the fraction of the second
		virtual double	toDouble() const;

		/// Return the time as a 64-bit integer where the representing the number of seconds
		/// since midnight 31st Dec 1899.
		virtual int64_t	toInt64(TimeType tt=InternalTime) const;

		/// Return the Julian Day Number for this time
		virtual int		toJulian() const;
		
		/**
		*** Return the time as a unix time_t (seconds since midnight 1st Jan 1970).
		***
		*** @throws	XTimeConversionException	If the result will not fit within a time_t variable.
		**/
		time_t			toUnixTime() const;

		/**
		*** Get the time as a unix time_t (seconds since midnight 1st Jan 1970).
		***
		*** This form of the toUnixTime method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @return		true if the conversion succeeds, false if it fails.
		**/
		bool			toUnixTime(time_t &t) const;

		/**
		*** Return the time as a unix struct timeval (seconds since midnight 1st Jan 1970).
		***
		*** @throws	XTimeConversionException	If the result will not fit within a struct timeval variable.
		**/
		struct timeval	toTimeVal() const;

		/**
		*** Get the time as a unix struct timeval (seconds since midnight 1st Jan 1970).
		***
		*** This form of the toTimeVal method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @return		true if the conversion succeeds, false if it fails.
		**/
		bool			toTimeVal(struct timeval &tv) const;

		/**
		*** Return the time as a windows FILETIME (the number of 100-nanosecond intervals since January 1, 1601)
		***
		*** @throws	XTimeConversionException	If the result will not fit within a FILETIME variable.
		**/
		FILETIME		toFILETIME() const;

		/**
		*** Get the time as a windows FILETIME (the number of 100-nanosecond intervals since January 1, 1601)
		***
		*** This form of the toFILETIME method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @return		true if the conversion succeeds, false if it fails.
		**/
		bool		    toFILETIME(FILETIME &v) const;


		/**
		*** Return the time as a windows SYSTEMTIME
		***
		*** @throws	XTimeConversionException	If the result will not fit within a SYSTEMTIME variable.
		**/
		SYSTEMTIME		toSYSTEMTIME() const;

		/**
		*** Get the time as a windows SYSTEMTIME
		***
		*** This form of the toSYSTEMTIME method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @return		true if the conversion succeeds, false if it fails.
		**/
		bool		    toSYSTEMTIME(SYSTEMTIME &v) const;


		/**
		*** Return the time as a struct tm (unix)
		***
		*** @throws	XTimeConversionException	If the result will not fit within a struct tm variable.
		**/
		struct tm			toTM() const;

		/**
		*** Get the time as a struct tm (unix)
		***
		*** This form of the toTM method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @return		true if the conversion succeeds, false if it fails.
		**/
		bool		    toTM(struct tm &v) const;


		/**
		*** Return the time as a XDate
		***
		*** @throws	XTimeConversionException	If the result will not fit within a XDate variable.
		**/
		XDate			toDate() const;

		/**
		*** Get the time as a struct tm (unix)
		***
		*** This form of the toTM method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @return		true if the conversion succeeds, false if it fails.
		**/
		bool		    toDate(XDate &v) const;


		/// Cast the time to a 64-bit integer.
		/// @see toInt64
		operator int64_t() const				{ return toInt64(); }

		/// Cast the time to a double
		/// @see toDouble
		operator double() const				{ return toDouble(); }

		/// Cast the time to a unix timeval
		/// @see toTimeVal
		operator struct timeval() const		{ return toTimeVal(); }

		/// Cast the time to a windows FILETIME
		/// @see toFILETIME
		operator FILETIME() const			{ return toFILETIME(); }

		/// Cast the time to a windows SYSTEMTIME
		/// @see toSYSTEMTIME
		operator SYSTEMTIME() const			{ return toSYSTEMTIME(); }

		/// Cast the time to a struct tm
		/// @see toTM
		operator struct tm() const			{ return toTM(); }

		/// Cast the time to a XDate
		/// @see toDate
		operator XDate() const				{ return toDate(); }

//@}


		/// Update this object to hold the current time.
		void			update();

		/// Return true if the time is AM
		bool			isAM() const		{ return (hour() < 12); }

		/// Return true if the time is PM
		bool			isPM() const		{ return (hour() >= 12); }

		/**
		*** Check to see if the time is in DST (Daylight Savings Time)
		***
		*** @return	0 if DST is not in effect, 1 if DST is in effect or
		***			-1 if DST information is unavailable
		**/
		virtual int		dst() const;

		/// Returns true is DST is in effect
		bool			isDST() const		{ return (dst() > 0); }

		/// Return the name of the timezone
		virtual XString	tzName(int dst) const;

		// Comparision operators
		friend bool	operator==(const XTime &a, const XTime &b);
		friend bool	operator!=(const XTime &a, const XTime &b);
		friend bool	operator<=(const XTime &a, const XTime &b);
		friend bool	operator>=(const XTime &a, const XTime &b);
		friend bool	operator<(const XTime &a, const XTime &b);
		friend bool	operator>(const XTime &a, const XTime &b);


		/// Format the time into a string
		//XString			format(const XString &fmt);

public:
		/// Return the current time.
		static XTime	now()		{ XTime	t; return t; }

protected: // Methods
		virtual void	calculateDST();

protected: // Members
		int64_t		m_seconds;		///< Time in seconds since 00:00:00 Jan 1 1900 GMT
		uint32_t		m_nanoseconds;	///< Time fraction in nanoseconds
		};


#endif
