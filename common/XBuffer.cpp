/****************************************************************************
** Buffer.cpp		General-purpose buffer
****************************************************************************/


#include "XBuffer.h"

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

XBuffer::XBuffer(size_t size, int increment) :
    _buf(0),
    _size(0),
    _capacity(0),
    _increment(increment)
		{
		setSize(size, true, true, false);
		}


// Memory copy constructor
XBuffer::XBuffer(const void *buf, size_t size, int increment) :
    _buf(0),
    _size(0),
    _capacity(0),
    _increment(increment)
		{
		setSize(size, false, false, false);
		memcpy(_buf, buf, size);
		}


// Copy constructor
XBuffer::XBuffer(XBuffer &v) :
    _buf(0),
    _size(0),
    _capacity(0),
    _increment(0)
		{
		*this = v;
		}


// Destructor
XBuffer::~XBuffer()
		{
		if (_buf != NULL) free(_buf);
		_buf = NULL;
		}


// Assignment operator
const XBuffer &
XBuffer::operator=(const XBuffer &v)
		{
		_increment = v._increment;
		setSize(v.size(), false, false, false);
		memcpy(_buf, v, v.size());

		return *this;
		}


/* 
** Set the size of the buffer
**
** size	The new size
** keep	If true preserve the old data, otherwise the old data may be discarded
** zero	If true init the buffer to zero, otherwise not initialisation done
** trim	If true set the buffer capacity to the exact size
*/
void
XBuffer::setSize(size_t size, bool keep, bool zero, bool trim)
		{
		char		*newbuf = NULL;
		size_t		oldsize = _size;
		size_t		wanted = size;

		// Don't need to do anything!
		if (size == oldsize)
			{
			if (!(trim && size != _capacity)) return;
			}

		// Force a size of at least one to make sure we get a buffer
		if (wanted < 1) wanted = 1;

		if (wanted > _capacity)
			{
			// Calculate the new capacity according to the _increment selected
			// 0  = set capacity to size requested
			// <0 = double capacity until enough space
			// >0 = increment capacity by _increment until enough space
			if (_increment == 0 || trim) _capacity = wanted;
			else if (_increment < 0)
				{
				// Double in size until capacity reached
				if (_capacity == 0) _capacity = 1;
				while (wanted > _capacity) _capacity *= 2;
				}
			else
				{
				if (wanted > _capacity) 
					{
					_capacity = ((wanted + _increment - 1) / _increment)*_increment;
					}
				}

			newbuf = (char *)realloc(_buf, _capacity);
			if (newbuf == NULL)
				{
				throw 1;
				}

			// Save the new (possibly unchanged) buffer
			_buf = newbuf;
			}
		else if (wanted < _capacity && trim)
			{
			_capacity = wanted;
			newbuf = (char *)realloc(_buf, _capacity);
			if (newbuf == NULL)
				{
				throw 1;
				}

			// Save the new (possibly unchanged) buffer
			_buf = newbuf;
			}

		// Clear out any new memory
		if (zero)
			{
			if (!keep)
				{
				// Data preservation is false, zero is true
				// Clear the buffer
				memset(_buf, 0, _capacity);
				}
			else if (wanted > oldsize)
				{
				// Only clear out the new parts of the buffer
				memset(_buf+oldsize, 0, wanted-oldsize);
				}
			}

		_size = size;
		}


// write to the buffer from another buffer adjusting size if required
// returns the number of bytes copied
size_t
XBuffer::write(const void *from, size_t nbytes, size_t offset)
		{
		if (nbytes > 0)
			{
			if ((nbytes + offset) > _size) setSize(nbytes + offset, true, false, false);
			memcpy(_buf+offset, from, nbytes);
			}

		return nbytes;
		}


// read from the buffer to another buffer
// returns the number of bytes copied
size_t
XBuffer::read(void *to, size_t nbytes, size_t offset)
		{
		if ((nbytes + offset) > _size) nbytes = _size - offset;

		if (nbytes > 0) memcpy(to, _buf+offset, nbytes);

		return nbytes;
		}


// Compare the buffer to the ptr/len provided
int
XBuffer::compareTo(const void *ptr, size_t len)
		{
		size_t	l1, l2, minlen;
		int		r;

		l1 = _size;
		l2 = len;
		minlen = (l1<l2)?l1:l2;

		r = memcmp(_buf, ptr, minlen);

		if (r != 0) return r;
		if (l1 == l2) return 0;
		return (l2>l1?-1:1);
		}


void
XBuffer::setContents(char v)
		{
		memset(_buf, v, _capacity);
		}


char&
XBuffer::operator[](int i) const
		{
		if (i < 0 || (size_t)i > _capacity) throw 1;
		return _buf[i];
		}
		
#ifdef BUFFER_LOAD_SAVE
/**
*** Load this buffer by reading from the given file descriptor
**/
int
XBuffer::load(File &file, int nbytes, int offset)
		{
		int	r=nbytes;

		if (nbytes > 0)
			{
			// Make sure we have enough space
			if ((nbytes + offset) > _size) setSize(nbytes + offset, true, false, false);
			r = file.read(_buf+offset, nbytes);
			if (r != nbytes && r >= 0) setSize(r + offset, true, false, false);
			}

		return r;
		}


/**
*** Save this buffer by writing to the given file descriptor
**/
int
XBuffer::save(File &file, int nbytes, int offset)
		{
		if ((nbytes + offset) > _size) nbytes = _size - offset;

		if (nbytes > 0) nbytes = file.write(_buf+offset, nbytes);

		return nbytes;
		}


/**
*** Load this buffer by reading from the given file descriptor
**/
int
XBuffer::load(int fd, int nbytes, int offset)
		{
		int	r=nbytes;

		if (nbytes > 0)
			{
			// Make sure we have enough space
			if ((nbytes + offset) > _size) setSize(nbytes + offset, true, false, false);
			r = ::read(fd, _buf+offset, nbytes);
			if (r != nbytes && r >= 0) setSize(r + offset, true, false, false);
			}

		return r;
		}


/**
*** Save this buffer by writing to the given file descriptor
**/
int
XBuffer::save(int fd, int nbytes, int offset)
		{
		if ((nbytes + offset) > _size) nbytes = _size - offset;

		if (nbytes > 0) nbytes = ::write(fd, _buf+offset, nbytes);

		return nbytes;
		}


#endif // BUFFER_LOAD_SAVE
