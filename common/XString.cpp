/*
**	XString.cpp	A class combining the best of RWCXString (Rogue Wave)
**			CXString (Microsoft MFC) and Java XString classes
*/

#include "XString.h"

#include <stdio.h>
#include <memory.h>
#include <stdarg.h>
#include <stdlib.h>
#if !defined(WIN32) && !defined(_WIN32)
	#include <unistd.h>
#else
	#pragma warning(disable: 4996)
#endif
#include <string.h>
#include <ctype.h>


#define LOCK()		XStringObject::LOCK()
#define UNLOCK()	XStringObject::UNLOCK()


/**
**	<b>x_string_newCharXString</b> converts the unicode string parameter <i>wstr</i> 
**	and copies it into a new char string by allocating memory by calling <b>new</b>.
**
**	@brief	Create a char string from a wide char (unicode) string
**	@param	wstr A pointer to a unicode string
**	@return	A pointer to a char string
**/
char *
x_string_newCharXString(const wchar_t *wstr)
	{
	int	clen;
	char	*cstr;

#if defined(WIN32) || defined(_WIN32)
	clen = WideCharToMultiByte(CP_ACP, WC_SEPCHARS|WC_COMPOSITECHECK, wstr, -1, NULL, 0, NULL, NULL);
#else
	clen = wcstombs(NULL, wstr, 0);
#endif
	cstr = new char[clen+1];
#if defined(WIN32) || defined(_WIN32)
	WideCharToMultiByte(CP_ACP,	WC_SEPCHARS|WC_COMPOSITECHECK, wstr, -1, cstr, clen+1, NULL, NULL);
#else
	clen = wcstombs(cstr, wstr, clen+1);
#endif

	return cstr;
	}


/**
** 	Make a copy of the char string using memory allocated by calling <B>new</B>.
**	The caller is responsible for calling <B>delete</B> to free up memory
**
**	@brief		Copy a string using new
**	@param		str the string to be copied
**	@return		A pointer to a copy of the string
**	@exception	MemoryAllocationException - if the call to new fails
**/
char *
x_string_newCharXString(const char *str)
	{
	char    *q;

	if (!str) return 0;

	q = new char[strlen(str)+1];
	strcpy(q, str);

	return q;
	}


/**
**	<b>x_string_newUnicodeXString</b> converts the char string parameter <i>cstr</i> 
**	and copies it into a new unicode string by allocating memory by calling <b>new</b>.
**
**	@brief	Create a char string from a wide char (unicode) string
**	@param	cstr A pointer to a char string
**	@return	A pointer to a unicode string
**/
wchar_t *
x_string_newUnicodeXString(const char *cstr)
	{
	int	len = (int)strlen(cstr);
	wchar_t	*wstr = new wchar_t[len+1];

#if defined(WIN32) || defined(_WIN32)
	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, cstr, -1, wstr, len+1);
#else
	mbstowcs(wstr, cstr, len+1);
#endif
	
	return wstr;	
	}


/**
** 	Make a copy of the wchar_t string using memory allocated by calling <B>new</B>.
**	The caller is responsible for calling <B>delete</B> to free up memory
**
**	@brief		Copy a string using new
**	@param		str the string to be copied
**	@return		A pointer to a copy of the string
**	@exception	MemoryAllocationException - if the call to new fails
**/
wchar_t *
x_string_newUnicodeXString(const wchar_t *str)
	{
	wchar_t    *q;

	if (!str) return 0;

	q = new wchar_t[wcslen(str)+1];
	wcscpy(q, str);

	return q;
	}


//--------------------------------------------------------------------------------
// XString implementation
//--------------------------------------------------------------------------------

// PRIVATE STATIC
// This is a special static function to create and maintain an empty string
// We use it because we cannot guarantee the order of static construction
XStringObject	*XString::__emptyXString=NULL;

XStringObject *
XString::getEmptyXString()
	{
	LOCK();
	if (__emptyXString == NULL) 
	    {
	    __emptyXString = new XStringObject(_T(""));
	    __emptyXString->_refcount = 0;
	    }
	UNLOCK();

	return __emptyXString;
	}


// PRIVATE
// Make sure that we can write to this XString by making sure that
// we are the only reference to the XStringObject
void	
XString::prepareToUpdate()
	{
	LOCK();
	if (_strobj->_refcount > 1 || _strobj == __emptyXString) _strobj = new XStringObject(_strobj);
        UNLOCK();
	}


/**
** Sets the minimum initial capacity of an XString, and returns the old value. The initial setting is 15 bytes. Larger
** values will use more memory, but result in fewer resizes when concatenating or reading strings. Smaller values will
** waste less memory, but result in more resizes.
*/
int
XString::initialCapacity(int v)
	{
	int	old = XStringObject::__initsize;

	XStringObject::__initsize = v;

	return old;
	}


/**
** Sets the maximum amount of unused space allowed in a string should it shrink, and returns the old value. The initial
** setting is 15 bytes. If more than mw bytes are wasted, then excess space will be reclaimed.
*/
int
XString::maxWaste(int v)
	{
	int	old = XStringObject::__maxwaste;

	XStringObject::__maxwaste = v;

	return old;
	}


/**
** Sets the resize increment when more memory is needed to grow a string. Returns the old value. The initial setting
** is 16 bytes.
*/
int
XString::resizeIncrement(int v)
	{
	int	old = XStringObject::__incsize;

	XStringObject::__incsize = v;

	return old;
	}


// Constructor: Blank string
XString::XString() :
    _strobj(0)
	{
	LOCK();
	_strobj = getEmptyXString();
	_strobj->addRef();
	UNLOCK();
	}


// Constructor: Copy of char string s
XString::XString(const char *s) :
    _strobj(0)
	{
	if (!s || !*s)
	    {
	    LOCK();
	    _strobj = getEmptyXString();
	    _strobj->addRef();
	    UNLOCK();
	    }
	else
	    {
#if defined(UNICODE) || defined(_UNICODE)
	    // conversion to wchar_t required
	    wchar_t *tmp = x_string_newUnicodeXString(s);
	    _strobj = new XStringObject(tmp);
	    delete [] tmp;
#else
	    // Same type - no conversion required
	    _strobj = new XStringObject(s);
#endif
	    }
	}


// Constructor: Copy of wchar_t string s
XString::XString(const wchar_t *s) :
    _strobj(0)
	{
	if (!s || !*s)
	    {
	    LOCK();
	    _strobj = getEmptyXString();
	    _strobj->addRef();
	    UNLOCK();
	    }
	else
	    {
#if defined(UNICODE) || defined(_UNICODE)
	    // Same type - no conversion required
	    _strobj = new XStringObject(s);
#else
	    // conversion to wchar_t required
	    char *tmp = x_string_newCharXString(s);
	    _strobj = new XStringObject(tmp);
	    delete [] tmp;
#endif
	    }
	}


// Copy Constructor (reference)
XString::XString(const XString &s) :
    _strobj(0)
	{
	LOCK();
	dropRef();
	if (!s.BufferLocked())
	    {
	    _strobj = s._strobj;
	    addRef();
	    }
	else _strobj = new XStringObject((const TCHAR *)s);
	UNLOCK();
	}


/* Copy Constructor (pointer)
XString::XString(const XString *s) :
    _strobj(0)
	{
	LOCK();
	dropRef();
	if (!s->BufferLocked())

	    {

	    _strobj = s->_strobj;

	    addRef();

	    }

	else _strobj = new XStringObject((const TCHAR *)*s);

	UNLOCK();
	}
*/

// Constructor: Blank string with given initial capacity
XString::XString(int ic) :
    _strobj(0)
	{
	_strobj = new XStringObject(0, ic);
	}



// Constructor: One TCHAR string as character c
XString::XString(TCHAR c) :
    _strobj(0)
	{
	_strobj = new XStringObject(1);
	_strobj->_buf[0] = c;
	_strobj->_buf[1] = '\0';
	}

// Constructor: n TCHAR string as character c n times
XString::XString(TCHAR c, int n) :
    _strobj(0)
	{
	if (n <= 0)
	    {
	    LOCK();
	    _strobj = getEmptyXString();
	    _strobj->addRef();
	    UNLOCK();
	    }
	else
	    {
	    _strobj = new XStringObject(n);
	    _strobj->setChars(c, 0, n);
	    _strobj->_buf[n] = 0;
	    }
	}

// Constructor: copy exactly n chars from s, including nulls
XString::XString(const TCHAR *s, int n) :
    _strobj(0)
	{
	if (n <= 0)
	    {
	    LOCK();
	    _strobj = getEmptyXString();
	    _strobj->addRef();
	    UNLOCK();
	    }
	else
	    {
	    _strobj = new XStringObject(n);

	    memcpy(_strobj->_buf, s, n * sizeof(TCHAR));
	    _strobj->_buf[n] = '\0';
	    }
	}


// Private Constructor: copy exactly n chars from s, including nulls
XString::XString(const TCHAR *s1, int n1, const TCHAR *s2, int n2) :
    _strobj(0)
	{
	if ((n1+n2) <= 0)
	    {
	    LOCK();
	    _strobj = getEmptyXString();
	    _strobj->addRef();
	    UNLOCK();
	    }
	else
	    {
	    _strobj = new XStringObject(n1+n2);

	    memcpy(_strobj->_buf, s1, n1 * sizeof(TCHAR));
	    memcpy(_strobj->_buf + n1, s2, n2 * sizeof(TCHAR));
	    _strobj->_buf[n1+n2] = '\0';
	    }
	}


// Destructor
XString::~XString()
	{
	// If we are locked do an unlock so that dropRef will delete the strobj
	if (BufferLocked()) _strobj->_refcount = 1;
	dropRef();
	}


void
XString::addRef()
	{
	LOCK();
	if (!BufferLocked()) _strobj->addRef();
	UNLOCK();
	}


void
XString::dropRef()
	{
	LOCK();
	if (_strobj && !BufferLocked())
	    {
	    if (_strobj->dropRef() == 0)
		{
		// If the refcount is now 0 nobody is interested anymore
		delete _strobj;
		if (_strobj == __emptyXString) __emptyXString = 0;
		}
	    _strobj = 0;
	    }
	UNLOCK();
	}


/// Assignment operator - XString set to a 1 character string of value <b>c</b>
const XString &	
XString::operator=(TCHAR c)
	{
	TCHAR	s[2];

	s[0] = c;
	s[1] = '\0';
	*this = s;

	return *this;
	}


/// Assignment operator - XString set to be a copy of the given char string
const XString &
XString::operator=(const char *s)
	{
	TCHAR	*tmp;

#if defined(UNICODE) || defined(_UNICODE)
	// conversion to wchar_t required
	tmp = x_string_newUnicodeXString(s);
#else
	// Same type - no conversion required
	tmp = (TCHAR *)s;
#endif

	LOCK();
	if (!BufferLocked())
	    {
	    dropRef();

	    if (!s || !*s)
		{
		_strobj = getEmptyXString();
		_strobj->addRef();
		}
	    else
		{
		_strobj = new XStringObject(tmp);
		}
	    }
	else
	    {
	    // Since we are require to keep the buffer exclusive we handle
	    // the assignment differently
	    int	l = (int)t_strlen(tmp);

	    _strobj->ensureCapacity(l);
	    t_strcpy(_strobj->_buf, tmp);
	    _strobj->_length = l;
	    }
	UNLOCK();

#if defined(UNICODE) || defined(_UNICODE)
	delete [] tmp;
#endif

	return *this;
	}


/// Assignment operator - XString set to be a copy of the given unicode string
const XString &
XString::operator=(const wchar_t *s)
	{
	TCHAR	*tmp;

#if defined(UNICODE) || defined(_UNICODE)
	// Same type - no conversion required
	tmp = (TCHAR *)s;
#else
	// conversion to wchar_t required
	tmp = x_string_newCharXString(s);
#endif

	LOCK();
	if (!BufferLocked())
	    {
	    dropRef();

	    if (!s || !*s)
		{
		_strobj = getEmptyXString();
		_strobj->addRef();
		}
	    else 
		{
		_strobj = new XStringObject(tmp);
		}
	    }
	else
	    {
	    // Since we are require to keep the buffer exclusive we handle
	    // the assignment differently
	    int	l = (int)t_strlen(tmp);

	    _strobj->ensureCapacity(l);
	    t_strcpy(_strobj->_buf, tmp);
	    _strobj->_length = l;
	    }
	UNLOCK();

#if !defined(UNICODE) && !defined(_UNICODE)
	delete [] tmp;
#endif

	return *this;
	}


/// Assignment operator - XString set to be a copy of the given string
const XString &	
XString::operator=(const XString &s)
	{
	// Someone is trying to assign me to myself!
	// So do nothing
	if (&s != this)
	    {
	    LOCK();
	    if (!BufferLocked() && !s.BufferLocked())
		{
		dropRef();
		_strobj = s._strobj;
		addRef();
		}
	    else
		{
		// Since we are require to keep the buffer exclusive we handle
		// the assignment differently

		prepareToUpdate();


		_strobj->ensureCapacity(s._strobj->_length);
		memcpy(_strobj->_buf, s._strobj->_buf, (s._strobj->_length + 1) * sizeof(TCHAR));
		_strobj->_length = s._strobj->_length;
		}
	    UNLOCK();
	    }

	return *this;
	}


/// Append operator - XString appended with the given string
XString &	
XString::operator+=(const XString &s)
	{
	LOCK();
	*this += s._strobj->_buf;
	UNLOCK();

	return *this;
	}


/// Append operator - XString appended with the given string
XString &	
XString::operator+=(const XString *s)
	{
	LOCK();
	*this += s->_strobj->_buf;
	UNLOCK();

	return *this;
	}


/// Append operator - XString appended with the given char
XString &	
XString::operator+=(TCHAR c)
	{
	TCHAR	s[2];

	s[0] = c;
	s[1] = '\0';
	*this += s;

	return *this;
	}


/// Append operator - XString appended with the given char string
XString &
XString::operator+=(const char *s)
	{
	LOCK();
	prepareToUpdate();

	// We are alone!
#if defined(UNICODE) || defined(_UNICODE)
	// conversion to wchar_t required
	wchar_t *tmp = x_string_newUnicodeXString(s);
	_strobj->append(tmp);
	delete [] tmp;
#else
	// Same type - no conversion required
	_strobj->append(s);
#endif

	UNLOCK();

	return *this;
	}


/// Append operator - XString appended with the given unicode string
XString &
XString::operator+=(const wchar_t *s)
	{
	LOCK();
	prepareToUpdate();

	// We are alone!
#if defined(UNICODE) || defined(_UNICODE)
	// Same type - no conversion required
	_strobj->append(s);
#else
	// conversion to wchar_t required
	char *tmp = x_string_newCharXString(s);
	_strobj->append(tmp);
	delete [] tmp;
#endif

	UNLOCK();

	return *this;
	}


/**
** Index operator - Returns the <i>i</i>th character of the string
** @brief Index operator
*/
TCHAR
XString::operator[](int i) const
	{
	TCHAR	c=0;

	LOCK();
	if (i>=0&&i<=_strobj->_length) c = _strobj->_buf[i];
	UNLOCK();

	return c;
	}


/**
** Index operator - Returns the <i>i</i>th character of the string
** @brief Index operator
*/
TCHAR
XString::operator()(int i) const
	{
	TCHAR	c=0;

	LOCK();
	if (i>=0&&i<=_strobj->_length) c = _strobj->_buf[i];
	UNLOCK();

	return c;
	}

/**
** Substring operator - Returns a string which is part of the current string
** as specified by the parameters
**
** @param start Zero based start index
** @param len	Maximum number of characters to take
**
** @brief Substring operator
*/
XString
XString::operator()(int start, int len) const
	{
	int	l = length();

	if (start < 0) start = 0;
	if (start >= l) return XString();
	if (len >  (l-start)) len = l-start;

	return XString(_strobj->_buf+start, len);
	}


XString &	XString::append(const char *s)		{ *this += s; return *this; }
XString &	XString::append(const wchar_t *s)		{ *this += s; return *this; }
XString &	XString::append(const XString &cstr)	{ *this += cstr; return *this; }


XString &
XString::append(const TCHAR *s, int n)
	{
	if (n > 0)
	    {
	    LOCK();
	    prepareToUpdate();

	    // We are alone, so go ahead with copy
	    _strobj->append(s, n);

	    UNLOCK();
	    }

	return *this;
	}


XString &
XString::append(TCHAR c, int n)
	{
	if (n > 0)
	    {
	    LOCK();
	    prepareToUpdate();

	    // We are alone, so go ahead with copy
	    _strobj->ensureCapacity(_strobj->_length + n);
	    // memset(_strobj->_buf+_strobj->_length, c, n);
	    _strobj->setChars(c, _strobj->_length, n);
	    _strobj->_length += n;
	    _strobj->_buf[_strobj->_length] = '\0';

	    UNLOCK();
	    }

	return *this;
	}



XString &
XString::append(const XString &cstr, int n)
	{
	if (n > 0)
	    {
	    LOCK();
	    if (n > cstr.length()) n = cstr.length();
	    append(cstr._strobj->_buf, n);
	    UNLOCK();
	    }

	return *this;
	}



// Prepend a copy of the null-terminated character string pointed to by cs to self. Returns a reference to self. This
// function is incompatible with cs strings with embedded nulls. This function may be incompatible with cs
// MBCS strings.
XString &
XString::prepend(const TCHAR* cs)
	{
	return prepend(cs, (int)t_strlen(cs));
	}


// Prepend a copy of the character string cs to self. Exactly N bytes are copied, including any embedded nulls.
// Hence, the buffer pointed to by cs must be at least N bytes long. Returns a reference to self. 
XString &
XString::prepend(const TCHAR* cs, int n)
	{
	insert(0, cs, n);

	return *this;
	}


// Prepend N copies of character c to self. Returns a reference to self.
XString &
XString::prepend(TCHAR c, int n)
	{
	TCHAR		*tmp = new TCHAR[n+1];

#if defined(UNICODE) || defined(_UNICODE)
	// Can't use memset
	TCHAR	*bp = tmp;

	for (int i=0;i<n;i++) *bp++ = c;
#else
	memset(tmp, c, n);
#endif
	tmp[n] = 0;
	XString	&self = prepend(tmp, n);
	delete [] tmp;

	return self;
	}


// Prepends a copy of the string str to self. Returns a reference to self.
XString &
XString::prepend(const XString &str)
	{
	return prepend(str._strobj->_buf, str.length());
	}


// Prepend the first N bytes or the length of str (whichever is less) of str to self. Returns a reference to self.
XString &
XString::prepend(const XString &str, int n)
	{
	return prepend(str._strobj->_buf, n<str.length()?n:str.length());
	}


// Returns the length of the string
int		
XString::length() const		
	{ 
	return _strobj->_length; 
	}


// Returns the reference count of the string (the number of XString objects sharing the same underlying string object)
int		
XString::refcount() const	
	{ 
	return _strobj->_refcount; 
	}


// Returns the capacity of the string (i.e. the amount of space available before the string is expanded by rellaocating memory)
int		
XString::capacity() const	
	{
	return _strobj->_capacity;
	}


// Provide a hint about capacity!
int
XString::capacity(int nc)
	{
	_strobj->ensureCapacity(nc);
	return _strobj->_capacity;
	}


// Return pointer to string
const TCHAR *	
XString::data() const	
	{
	return _strobj->_buf; 
	}





// Returns an int less than, greater than, or equal to zero, according to the result of calling the standard C library
// function memcmp() on self and the argument str. Case sensitivity is according to the caseCompare argument, and
// may be XString::exact or XString::ignoreCase. If caseCompare is XString::exact, then this
// function works for all string types. Otherwise, this function is incompatible with MBCS strings. This function
// is incompatible with const TCHAR* strings with embedded nulls. This function may be incompatible with
// const TCHAR* MBCS strings.

int
XString::compareTo(const char *str, caseCompare cc) const
	{
	XString	tmp(str);

	return compareTo(tmp, cc);
	}

int
XString::compareTo(const wchar_t *str, caseCompare cc) const
	{
	XString	tmp(str);

	return compareTo(tmp, cc);
	}

int
XString::compareTo(const XString &str, caseCompare cc) const
	{
	const TCHAR	*s1, *s2;
	int		i, r, l1, l2, minlen;
	int		c1, c2;

	s1 = _strobj->_buf;
	l1 = length();
	s2 = str._strobj->_buf;
	l2 = str.length();
	minlen = (l1<l2)?l1:l2;

	if (cc == exact)
	    {
	    // Case dependent match
	    r = memcmp(s1, s2, minlen * sizeof(TCHAR));
	    if (r != 0) return r;
	    }
	else
	    {
	    // Case independent match
	    for (i=0;i<minlen;i++)
		{
		c1 = tolower(*s1++);
		c2 = tolower(*s2++);
		if (c1 != c2) return (c1>c2?1:-1);
		}
	    }

	// at this point the strings are equal up to the length of the shorter one.
	if (l1 == l2) return 0;
	return (l2>l1?-1:1);
	}


/*
XString
operator+(const XString &s, const TCHAR *cs)
	{
	XString	tmp(s);

	tmp += cs;
	return tmp;
	//return XString(s._strobj->_buf, s.length(), cs, (int)strlen(cs));
	}            


XString
operator+(const TCHAR *cs, XString &s)
	{
	XString	tmp(cs);

	tmp += s;
	return tmp;
	// return XString(cs, (int)strlen(cs), s._strobj->_buf, s.length());
	}
*/

XString
operator+(const XString &s1, const XString &s2)
	{
	XString	tmp(s1);

	tmp += s2;
	return tmp;
	// return XString(s1._strobj->_buf, s1.length(), s2._strobj->_buf, s2.length());
	}


// Insert a copy of the null-terminated string cs into self at byte position pos, thus expanding the string. Returns a
// reference to self. This function is incompatible with cs strings with embedded nulls. This function may be
// incompatible with cs MBCS strings.
XString &
XString::insert(int pos, const TCHAR *cs)
	{
	return insert(pos, cs, (int)t_strlen(cs));
	}


// Insert a copy of the first N bytes of cs into self at byte position pos, thus expanding the string. Exactly N bytes are
// copied, including any embedded nulls. Hence, the buffer pointed to by cs must be at least N bytes long. Returns
// a reference to self.
XString &
XString::insert(int pos, const TCHAR *cs, int n)
	{
	if (pos >= 0 && pos <= length() && n > 0)
	    {
	    TCHAR	*tmpbuf=NULL;
	    int		tmplen;

	    LOCK();
	    prepareToUpdate();

	    // Work out how much we must preserve if any
	    tmplen = _strobj->_length - pos;
	    if (tmplen > 0)
		{
		tmpbuf = new TCHAR[tmplen];
		memcpy(tmpbuf, _strobj->_buf + pos, tmplen * sizeof(TCHAR)); 
		}

	    // Make sure we have enough space
	    _strobj->ensureCapacity(_strobj->_length + n);
	    memcpy(_strobj->_buf + pos, cs, n * sizeof(TCHAR));

	    // Add in the preserved data
	    if (tmplen > 0)
		{
		memcpy(_strobj->_buf + pos + n, tmpbuf, tmplen * sizeof(TCHAR));
		delete [] tmpbuf;
		}

	    _strobj->_length += n;
	    _strobj->_buf[_strobj->_length] = '\0';

	    UNLOCK();
	    }

	return *this;
	}


// Insert a copy of the string str into self at byte position pos. Returns a reference to self.
XString &
XString::insert(int pos, const XString &str)
	{
	return insert(pos, str._strobj->_buf, str.length());
	}


// Insert a copy of the first N bytes or the length of str (whichever is less) of str into self at byte position pos.
// Returns a reference to self.
XString &
XString::insert(int pos, const XString &str, int n)
	{
	return insert(pos, str._strobj->_buf, n<str.length()?n:str.length());
	}


// Removes the bytes from the byte position pos, which must be no greater than length(), to the end of string.
// Returns a reference to self.
XString &	
XString::remove(int pos)
	{
	if (pos >= 0 && pos <= length())
	    {
	    LOCK();
	    prepareToUpdate();

	    // Just truncate the string and keep the capacity
	    _strobj->_length = pos;
	    _strobj->_buf[_strobj->_length] = '\0';

	    UNLOCK();
	    }

	return *this;
	}


// Removes N bytes or to the end of string (whichever comes first) starting at the byte position pos, which must be
// no greater than length(). Returns a reference to self.
XString&
XString::remove(int pos, int n)
	{
	if (pos >= 0 && pos <= length() && n > 0)
	    {
	    TCHAR	*from, *to;
	    int		count;

	    LOCK();
	    if (n+pos > _strobj->_length) n = _strobj->_length-pos;

	    prepareToUpdate();

	    // Just change the string and keep the capacity as it
	    to = _strobj->_buf + pos;
	    from = _strobj->_buf + pos + n;
	    count = _strobj->_length - (pos + n);

	    // Copy the bytes from the top of the string
	    while (count > 0)
		{
		*to++ = *from++;
		count--;
		}

	    _strobj->_length -= n;
	    _strobj->_buf[_strobj->_length] = '\0';

	    UNLOCK();
	    }

	return *this;
	}


// Replaces N bytes or to the end of string (whichever comes first) starting at byte position pos, which must be no
// greater than length(), with a copy of the null-terminated string cs. Returns a reference to self. This function is
// incompatible with cs strings with embedded nulls. This function may be incompatible with cs MBCS
// strings.
XString&
XString::replace(int pos, int n, const TCHAR* cs)
	{
	return replace(pos, n, cs, (int)t_strlen(cs));
	}


// Replaces N1 bytes or to the end of string (whichever comes first) starting at byte position pos, which must be no
// greater than length(), with a copy of the string cs. Exactly N2 bytes are copied, including any embedded
// nulls. Hence, the buffer pointed to by cs must be at least N2 bytes long. Returns a reference to self.
XString&
XString::replace(int pos, int n1 ,const TCHAR *cs, int n2)
	{
	if (pos >= 0 && pos <= length() && n1 >= 0 && n2 >= 0)
	    {
	    LOCK();
	    prepareToUpdate();

	    if (pos + n1 > _strobj->_length) n1 = _strobj->_length - pos;

	    if (n1 > n2)
		{
		// Remove some chars first
		remove(pos, n1-n2);
		}
	    else if (n1 < n2)
		{
		// insert some room by calling insert
		insert(pos, cs, n2-n1);
		}

	    // Now copy the data in
	    memcpy(_strobj->_buf + pos, cs, n2 * sizeof(TCHAR));

	    _strobj->_buf[_strobj->_length] = '\0';

	    UNLOCK();
	    }

	return *this;
	}


// Replaces N bytes or to the end of string (whichever comes first) starting at byte position pos, which must be no
// greater than length(), with a copy of the string str. Returns a reference to self.
XString&
XString::replace(int pos, int n, const XString &str)
	{
	return replace(pos, n, str._strobj->_buf, str.length());
	}


// Replaces N1 bytes or to the end of string (whichever comes first) starting at position pos, which must be no
// greater than length(), with a copy of the first N2 bytes, or the length of str (whichever is less), from str.
XString&
XString::replace(int pos, int n1, const XString &str, int n2)
	{
	return replace(pos, n1, str._strobj->_buf, n2<str.length()?n2:str.length());
	}


// Pattern matching. Starting with index i, searches for the first occurrence of pat in self and returns the index of the
// start of the match. Returns RW_NPOS if there is no such pattern. Case sensitivity is according to the caseCompare
// argument; it defaults to XString::exact. If caseCompare is XString::exact, then this function works
// for all string types. Otherwise, this function is incompatible with MBCS strings.
int
XString::index(const TCHAR *pat, int pos, caseCompare cmp) const
	{
	return index(pat, (int)t_strlen(pat), pos, cmp);
	}

int
XString::index(const XString& pat, int pos, caseCompare cmp) const
	{
	return index(pat._strobj->_buf, pat.length(), pos, cmp);
	}

int
XString::index(const XString& pat, int patlen,int pos, caseCompare cmp) const
	{
	return index(pat._strobj->_buf, patlen, pos, cmp);
	}

int
XString::index(TCHAR ch, int pos, caseCompare cmp) const
	{
	TCHAR	tmp[2];

	tmp[0] = ch;
	tmp[1] = 0;

	return index(tmp, 1, pos, cmp);
	}


// Pattern matching. Starting with index i, searches for the first occurrence of the first patlen bytes from pat in self
// and returns the index of the start of the match. Returns RW_NPOS if there is no such pattern. Case sensitivity is
// according to the caseCompare argument. If caseCompare is XString::exact, then this function works for
// all string types. Otherwise, this function is incompatible with MBCS strings.
int
XString::index(const TCHAR *pat, int patlen, int pos, caseCompare cmp) const	
	{
	const TCHAR	*cp, *pp;
	int		c1, c2;
	int		strpos, patpos;

	strpos = pos;
	cp = _strobj->_buf + strpos;
	pp = pat;
	while (strpos+patlen <= length())
	    {
	    for (patpos=0;patpos<patlen;patpos++)
		{
		c1 = cp[patpos];
		c2 = pp[patpos];
		if (cmp != exact)
		    {
		    c1 = tolower(c1);
		    c2 = tolower(c2);
		    }

		if (c1 != c2) break;
		}

	    if (patpos == patlen)
		{
		// We've got a match, return the starting index
		return strpos;
		}

	    // Start again from next TCHAR
	    strpos++;
	    cp++;
	    }

	return -1;
	}



int
XString::rindex(TCHAR ch, int pos, caseCompare cmp) const
	{
	TCHAR	tmp[2];

	tmp[0] = ch;
	tmp[1] = 0;

	return rindex(tmp, 1, pos, cmp);
	}


int
XString::rindex(const XString &pat, int pos, caseCompare cmp) const
	{
	return rindex(pat._strobj->_buf, pat.length(), pos, cmp);
	}


int
XString::rindex(const TCHAR *pat, int pos, caseCompare cmp) const
	{
	return rindex(pat, (int)t_strlen(pat), pos, cmp);
	}


int
XString::rindex(const XString &pat, int patlen, int pos, caseCompare cmp) const
	{
	return rindex(pat._strobj->_buf, patlen, pos, cmp);
	}


// Pattern matching. Starting with index i, working backwards
// Searches for the first occurrence of the first patlen bytes from pat in self
// and returns the index of the start of the match.
// Returns -1 if there is no such pattern.
// Case sensitivity is according to the caseCompare argument.
// If caseCompare is XString::exact, then this function works for
// all string types. Otherwise, this function is incompatible with MBCS strings.
int
XString::rindex(const TCHAR *pat, int patlen, int pos, caseCompare cmp) const
	{
	const TCHAR	*cp, *pp;
	int		c1, c2;
	int		patpos;

	// Adjust pos before we start
	if (pos + patlen > length()) pos = length() - patlen;

	cp = _strobj->_buf + pos;
	pp = pat;
	while (pos >= 0)
	    {
	    for (patpos=0;patpos<patlen;patpos++)
		{
		c1 = cp[patpos];
		c2 = pp[patpos];
		if (cmp != exact)
		    {
		    c1 = tolower(c1);
		    c2 = tolower(c2);
		    }

		if (c1 != c2) break;
		}

	    if (patpos == patlen)
		{
		// We've got a match, return the starting index
		return pos;
		}

	    // Start again from next TCHAR
	    pos--;
	    cp--;
	    }

	return -1;
	}


// Returns the index of the first occurence of the character c in self. Returns RW_NPOS if there is no such character or
// if there is an embedded null prior to finding c. This function is incompatible with strings with embedded nulls.
// This function is incompatible with MBCS strings.
int
XString::first(TCHAR c) const
	{
	const TCHAR	*cp = t_strchr(_strobj->_buf, c);

	return cp?(int)(cp-_strobj->_buf):-1;
	}


// Returns the index of the first occurence of the character c in self. Continues to search past embedded nulls.
// Returns RW_NPOS if there is no such character. This function is incompatible with MBCS strings.
int
XString::first(TCHAR c, int) const
	{
	const TCHAR	*cp=NULL;
	
#if defined(UNICODE) || defined(_UNICODE)
	// We can't use memchr since we are operating on wide chars
	const TCHAR	*bp=_strobj->_buf;

	for (int i=0;i<length();i++)
	    {
	    if (*bp == c)
		{
		cp = bp;
		break;
		}
	    
	    bp++;
	    }
#else
	cp = (TCHAR *)memchr(_strobj->_buf, c, length());
#endif

	return cp?(int)(cp-_strobj->_buf):-1;
	}


/**
*** Returns the index of the first occurence in self of any character in str. Returns RW_NPOS if there is no match or if
*** there is an embedded null prior to finding any character from str. This function is incompatible with strings
*** with embedded nulls. This function may be incompatible with MBCS strings.
**/
int
XString::first(const TCHAR* str) const
	{
	const TCHAR	*dp, *sp;
	int		i;

	dp = _strobj->_buf;
	for (i=0;i<length();i++,dp++)
	    {
	    if (!*dp) break;
	    for (sp=str;*sp;sp++)
		{
		if (*dp == *sp) return i;
		}
	    }

	return -1;
	}


// Returns the index of the first occurence in self of any character in str. Exactly N bytes in str are checked
// including any embedded nulls so str must point to a buffer containing at least N bytes. Returns RW_NPOS if
// there is no match.
int
XString::first(const TCHAR *str, int n) const
	{
	const TCHAR	*dp, *sp;
	int		i, j;

	dp = _strobj->_buf;
	for (i=0;i<length();i++,dp++)
	    {
	    for (sp=str,j=0;j<n;j++,sp++)
		{
		if (*dp == *sp) return i;
		}
	    }

	return -1;
	}


// Returns the index of the last occurrence in the string of the character c. Returns RW_NPOS if there is no such
// character or if there is an embedded null to the right of c in self. This function is incompatible with strings with
// embedded nulls. This function may be incompatible with MBCS strings.
int
XString::last(TCHAR c) const
	{
	const TCHAR	*cp = t_strrchr(_strobj->_buf, c);

	return cp?(int)(cp-_strobj->_buf):-1;
	}


// Returns the index of the last occurrence in the string of the character c. Continues to search past embedded nulls.
// Returns RW_NPOS if there is no such character. This function is incompatible with MBCS strings.
int
XString::last(TCHAR c, int) const
	{
	return last(&c, 1);
	}


// Similar to first only backwards
int
XString::last(const TCHAR *str) const
	{
	return last(str, (int)t_strlen(str));
	}


// Similar to first only backwards
int
XString::last(const TCHAR *str, int n) const
	{
	const TCHAR	*dp, *sp;
	int		pos, j;

	pos = length()-1;
	dp = _strobj->_buf+pos;

	while (pos >= 0)
	    {
	    for (sp=str,j=0;j<n;j++,sp++)
		{
		if (*dp == *sp) return pos;
		}

	    dp--;
	    pos--;
	    }

	return -1;
	}


// Changes all upper-case letters in self to lower-case, using the standard C library facilities declared in <ctype.h>.
// This function is incompatible with MBCS strings.
void
XString::toLower()
	{
	TCHAR	*cp;
	int	i;

	LOCK();
	prepareToUpdate();
	for (cp=_strobj->_buf,i=0;i<length();i++,cp++) *cp = (TCHAR)tolower(*cp);
	UNLOCK();
	}


// Changes all lower-case letters in self to upper-case, using the standard C library facilities declared in <ctype.h>.
// This function is incompatible with MBCS strings.
void
XString::toUpper()
	{
	TCHAR	*cp;
	int	i;

	LOCK();
	prepareToUpdate();
	for (cp=_strobj->_buf,i=0;i<length();i++,cp++) *cp = (TCHAR)toupper(*cp);
	UNLOCK();
	}


// Returns TRUE if self contains no bytes with the high bit set.
bool
XString::isAscii() const
	{
	int		i;
	const TCHAR	*dp;

	for (i=0,dp=_strobj->_buf;i<length();i++,dp++)
	    {
	    if (*dp & 0x80) return false;
	    }

	return true;
	}


// Returns a substring of self where the character c has been stripped off the beginning, end, or both ends of the
// string. The first variant can be used as an lvalue. The enum stripType can take values:
// 
// stripType	Meaning
// leading	Remove characters at beginning
// trailing	Remove characters at end
// both		Remove characters at both ends
XString
XString::strip(stripType type, TCHAR c)
	{
	TCHAR	tmp[2];

	tmp[0] = c;
	tmp[1] = 0;
	return strip(type, tmp);
	}


XString
XString::strip(stripType type, const TCHAR *s)
	{
	XString	tmp;

	tmp = *this;
	tmp.stripChars(s, type);
	return tmp;
	}


// Strips off the chars specified from the specified end(s)
void
XString::stripChars(const TCHAR *str, stripType type)
	{
	int	n = (int)t_strlen(str);

	if (length() > 0 && (type == leading || type == both))
	    {
	    // Strip chars from the start
	    const TCHAR	*dp, *sp;
	    int		i, j, count;

	    count = 0;
	    dp = _strobj->_buf;
	    for (i=0;i<length();i++,dp++)
		{
		for (sp=str,j=0;j<n;j++,sp++)
		    {
		    if (*dp == *sp)
			{
			count++;
			break;
			}
		    }

		if (j == n) break;
		}

	    if (count > 0) remove(0, count);
	    }

	if (length() > 0 && (type == trailing || type == both))
	    {
	    // Strip chars from the end
	    const TCHAR	*dp, *sp;
	    int		pos, j;

	    pos = length()-1;
	    dp = _strobj->_buf+pos;

	    while (pos >= 0)
		{
		for (sp=str,j=0;j<n;j++,sp++)
		    {
		    if (*dp == *sp) break;
		    }

		if (j == n) break;

		dp--;
		pos--;
		}

	    remove(pos+1);
	    }
	}


// Returns a suitable hash value. If caseCompare is XString::ignoreCase then this function will be
// incompatible with MBCS strings.

// We use the following algorithm to produce a hash value
// Each TCHAR is folded into 6 bits assuming that letters and
// numbers are the most likely candidates. Therefore A-Z get values 0-25
// a-z get 26-51, 0-9 get 52-61, all others below 128 get 62 and all above
// 127 get 63.
// Each 6 bit value is pushed into the bottom of an unsigned value number
// for each TCHAR in the string. Therefore a zero length string will get a
// hash value of zero.
uint32
XString::hash(caseCompare cmp) const
	{
	unsigned	hv=0;
	int		pos, hlen, v;
	const TCHAR	*dp=_strobj->_buf;
	int		c;

	pos = 0;
	hlen = sizeof(hv)*8;
	while (hlen > 0 && pos < length())
	    {
	    c = *dp;
	    if (cmp != exact) c = tolower(c);

	    // Make it fit into 6 bits
	    if (c >= 'A' && c <= 'Z') v = c-'A';		// 00..25
	    else if (c >= 'a' && c <= 'z') v = 26+c-'a';	// 26..51
	    else if (c >= '0' && c <= '9') v = 52+c-'0';	// 52..61
	    else if (c < 128) v = 62;				// 62
	    else v = 63;					// 63

	    // printf("%c -> %c = %x", *dp, c, v);

	    if (hlen < 6)
		{
		hv <<= hlen;
		v >>= 6-hlen;
		}
	    else
		{
		hv <<= 6;
		hlen -= 6;
		}

	    // printf(" (%x -> ", hv);
	    hv |= (v&0x3f);
	    // printf("%x)\n", hv);

	    dp++;
	    pos++;
	    }

	return hv;
	}


uint32
hash(const XString& str)
	{
	return str.hash(XString::exact);
	}



// Pattern matching. Returns TRUE if str occurs in self. Case sensitivity is according to the caseCompare argument,
// and may be XString::exact or XString::ignoreCase. If caseCompare is XString::exact, then
// this function works for all string types. Otherwise, this function is incompatible with MBCS strings. This
// function is incompatible with const TCHAR* strings with embedded nulls. This function may be
// incompatible with const TCHAR* MBCS strings.
bool
XString::contains(const TCHAR* str, caseCompare cmp) const
	{
	int		i, j, r, len=(int)t_strlen(str);
	const TCHAR	*dp = _strobj->_buf, *sp;

	for (i=0;i<=length()-len;i++,dp++)
	    {
	    if (cmp == exact) r = memcmp(dp, str, len * sizeof(TCHAR));
	    else
		{
		r = 0;
		for (j=0,sp=str;j<len;j++,sp++)
		    {
		    if (tolower(*sp) != tolower(*(dp+j)))
			{
			r = -1;
			break;
			}
		    }
		}

	    if (r == 0) return true;
	    }

	return false;
	}

bool
XString::contains(const XString& cs, caseCompare cmp) const
	{
	return contains(cs._strobj->_buf, cmp);
	}


//Changes the length of self to n bytes, adding blanks or truncating as necessary.
void
XString::resize(int n)
	{
	if (n >= 0)
	    {
	    if (n < length())
		{
		// Truncate
		remove(n);
		}
	    else if (n > length())
		{
		// Grow (add blanks)
		append(' ', n-length()); 
		}
	    }
	}



/*******************************************************************************************
**  Microsoft CXString compatability
*******************************************************************************************/

// Set the character at the given position
void
XString::SetAt(int n, TCHAR c)
	{
	LOCK();
	if (n >= 0 || n < length())
	    {
	    prepareToUpdate();
	    _strobj->_buf[n] = c;
	    }
	UNLOCK();
	}


// Reverse the order of the chars in the string
void
XString::MakeReverse()
	{
	LOCK();
	int	l=length();
	TCHAR	c, *sp, *ep;

	prepareToUpdate();

	sp = &_strobj->_buf[0];
	ep = &_strobj->_buf[l-1];
	while (sp < ep)
	    {
	    c = *sp;
	    *sp = *ep;
	    *ep = c;
	    sp++;
	    ep--;
	    }

	UNLOCK();
	}


// Remove all occurences of the specified char and return the count
int
XString::Remove(TCHAR c)
	{
	LOCK();
	
	int	count=0, index;

	while ((index = first(c)) >= 0)
	    {
	    remove(index, 1);
	    count++;
	    }
	
	UNLOCK();

	return count;
	}


XString
XString::SpanIncluding(const TCHAR *charSet) const
	{
	XString		result;
	const TCHAR	*sp = _strobj->_buf;
	int		i=0, l=length();

	while (i < l)
	    {
	    if (t_strchr(charSet, *sp) == NULL) break;
	    result += *sp++;
	    i++;
	    }

	return result;
	}


XString
XString::SpanExcluding(const TCHAR *charSet) const
	{
	XString		result;
	const TCHAR	*sp = _strobj->_buf;
	int		i=0, l=length();

	while (i < l)
	    {
	    if (t_strchr(charSet, *sp) != NULL) break;
	    result += *sp++;
	    i++;
	    }

	return result;
	}


// Replace all occurences of oldchar with newchar
int
XString::replaceAll(TCHAR oldchar, TCHAR newchar)
	{
	LOCK();
	
	int	count=0;
	TCHAR	*sp = &_strobj->_buf[0];
	int	i=0, l=length();

	while (i < l)
	    {
	    if (*sp == oldchar)
		{
		if (count == 0)
		    {
		    prepareToUpdate();
		    sp = &_strobj->_buf[i];
		    }

		*sp = newchar;
		count++;
		}

	    i++;
	    sp++;
	    }

	UNLOCK();

	return count;
	}


// Replace all occurences of oldstr with newstr
int
XString::replaceAll(const TCHAR *oldstr, const TCHAR *newstr, caseCompare cmp)
		{
		LOCK();
		
		int	count=0;
		int	pos=0;
		int	oldlen = (int)t_strlen(oldstr);
		int	newlen = (int)t_strlen(newstr);

		while (pos < length() && pos >= 0)
			{
			pos = index(oldstr, pos, cmp);
			if (pos >= 0)
				{
				replace(pos, oldlen, newstr);
				count++;

				// Skip over the newly inserted string
				pos += newlen;
				}
			}

		UNLOCK();

		return count;
		}


#if defined(UNICODE) || defined(_UNICODE)
XString::operator const char *()
	{
	// Convert to a new char string (in _convBuf) and return a pointer to it.
	LOCK();
	delete [] _strobj->_convBuf;
	_strobj->_convBuf = x_string_newCharXString(_strobj->_buf);
	UNLOCK();

	return _strobj->_convBuf;
	}

#else

/**
** Return a pointer to unicode version of this string as a <i>const wchar_t *</i>
**/
XString::operator const wchar_t *()
	{
	// Convert to a new unicode string (in _convBuf) and return a pointer to it.
	LOCK();
	delete [] _strobj->_convBuf;
	_strobj->_convBuf = x_string_newUnicodeXString(_strobj->_buf);
	UNLOCK();

	return _strobj->_convBuf;
	}
#endif


// Read a line from the file
bool
XString::readLine(FILE *f, bool stripNL)
	{
	TCHAR	c;

	empty();
	for (;;)
	    {
		int		ic=t_getc(f);

	    c = (TCHAR)ic;
	    if (c == _T('\n'))
			{
			if (!stripNL) append(c);
			break;
			}

	    else if (ic == T_EOF)
			{
			if (isEmpty()) return false;
			break;
			}
	    else append(c);
	    }

	return true;
	}


// Return true if the prefix matches the string starting the comparsion at the offset given
bool
XString::startsWith(const XString &prefix, int toffset, bool ignore_case) const
	{
	return regionMatches(ignore_case, toffset, prefix, 0, prefix.length());
	}


// Return true if the suffix matches the end of the string
bool
XString::endsWith(const XString &suffix, bool ignore_case) const
	{
	return regionMatches(ignore_case, length()-suffix.length(), suffix, 0, suffix.length());
	}


// Tests if two string regions are equal.
bool		
XString::regionMatches(bool ignore_case, int thisOffset, const XString &other, int otherOffset, int len) const
	{
	int	r=0;

	// Some safety checks, since someone is bound to get it wrong and expect it to work
	if (otherOffset < 0) otherOffset = 0;
	if (thisOffset < 0) thisOffset = 0;
	if (len > (other.length() - otherOffset)) len = other.length() - otherOffset;

	// If len == 0 it's always true!
	if (!len) return true;

	// Check we are within the bounds of this string
	if (thisOffset + len > length()) return false;

	LOCK();
	if (ignore_case)
	    {
	    r = t_strncasecmp(&_strobj->_buf[thisOffset], &other._strobj->_buf[otherOffset], len);
	    }
	else
	    {
	    r = t_strncmp(&_strobj->_buf[thisOffset], &other._strobj->_buf[otherOffset], len);
	    }
	UNLOCK();
	
	return (r==0);
	}


// Tests if two string regions are equal.
bool		
XString::regionMatches(int toffset, const XString &other, int ooffset, int len) const
	{
	return regionMatches(false, toffset, other, ooffset, len);
	}




/**
*** This method provides direct access to the XString buffer.
***
*** @brief   Get direct access to string buffer (MFC CXString compatabililty)
***
*** @param minlen	Increases the capacity of the string to contain at least
***			minlen bytes.
**/
TCHAR *
XString::GetBuffer(int minlen)
	{
	// Make sure we have exclusive access to this string buffer
	LockBuffer();

	// Make sure we have enough space for the specified number of
	// chars plus one for the null terminator
	_strobj->ensureCapacity(minlen);

	return _strobj->_buf;
	}


/**
*** This method releases direct access to the XString buffer previously
*** set up by a call to GetBuffer or GetBufferSetLength. 
***
*** @brief		Release direct access to string buffer (MFC CXString compatabililty)
***
*** @param len		Specifies the new length of the string. If len is negative then the
***			length of the string is determined using strlen. Otherwise the length
***			the string is set to be that specified.
**/
void
XString::ReleaseBuffer(int len)
	{
	int	newlen = (len < 0)?(int)t_strlen(_strobj->_buf):len;

	_strobj->ensureCapacity(newlen);
	_strobj->_length = newlen;
	_strobj->_buf[newlen] = 0;

	UnlockBuffer();
	}


TCHAR *		GetBufferSetLength(int len);
void		FreeExtra();


/**
*** Provides exclusive access to the string buffer by disabling reference
*** counting
**/
void
XString::LockBuffer()
	{
	LOCK();

	if (_strobj->_refcount < 0)
	    {
	    // This string is already locked, just bump down the count
	    _strobj->_refcount--;
	    UNLOCK();
	    return;
	    }
	else prepareToUpdate();

	// Set the refcount to -1
	_strobj ->_refcount = -1;
	UNLOCK();
	}



/**
*** Stops exclusive access to the string buffer by enabling reference counting
**/
void
XString::UnlockBuffer()
	{
	LOCK();
	if (++(_strobj ->_refcount) == 0) _strobj ->_refcount = 1;
	UNLOCK();
	}




/**
*** @brief Adds quotes to the string according to parameters.
***
*** The quote method will modify the string by adding quotes to the beginning
*** and end of the string.
***
*** If the string does not already start and end with the quote
*** char (qchar) and it contains white space or onlyIfWhiteSpace is false
*** then the string is quoted with qchar.
***
*** @param qchar		The quote char to add to each end of the string.
*** @param onlyIfWhiteSpace	If true the string is only quoted if it contains spaces
*** @param backslash		The char to prefix any embedded quote char with
***
**/
void
XString::quote(TCHAR qchar, bool onlyIfWhiteSpace, TCHAR backslash)
	{
	LOCK();
	int	len = length();

	if (len > 0 && _strobj->_buf[0] == qchar && _strobj->_buf[len-1] == qchar)
	    {
	    // Already quoted - don't bother
	    UNLOCK();
	    return;
	    }

	if (onlyIfWhiteSpace && first(_T(" \t\n\r")) < 0)
	    {
	    // No white space and not always wanted - don't bother
	    UNLOCK();
	    return;
	    }

	// Now we know we must insert the quotes
	// but before we do check to see if we must quote
	// any embedded quotes with the given backslash char
	if (backslash && first(qchar) >= 0)
	    {
	    XString	qstr(qchar);

	    TCHAR	tmp[3];

	    tmp[0] = backslash;
	    tmp[1] = qchar;
	    tmp[2] = 0;

	    replaceAll(qstr, tmp);
	    }

	insert(0, qchar);
	append(qchar);

	UNLOCK();
	}


/**
*** @brief	Remove start and end quotes from the string.
***
*** If the string starts and ends with the given quote char (qchar)
*** the quotes are removed. If the backslash parameter is non-null all
*** instances of the backslash char followed by the quote char are replaced
*** with the quote char. E.g. if qchar is '"' and backslash is \ and the string 
*** is <b>"abc \"def\" ghi"</b> the result will be <b>abc "def" ghi</b>
***
*** @param qchar		The quote char
*** @param backslash		The char to prefix any embedded quote char with
***
**/
void
XString::unquote(TCHAR qchar, TCHAR backslash)
	{
	LOCK();
	int	len = length();

	if (len > 1 && _strobj->_buf[0] == qchar && _strobj->_buf[len-1] == qchar)
	    {
	    remove(len-1, 1);
	    remove(0, 1);
	    }

	// Now we know we must insert the quotes
	// but before we do check to see if we must quote
	// any embedded quotes with the given backslash char
	if (backslash)
	    {
	    XString	qstr(qchar);

	    TCHAR	tmp[3];

	    tmp[0] = backslash;
	    tmp[1] = qchar;
	    tmp[2] = 0;

	    replaceAll(tmp, qstr);
	    }

	UNLOCK();
	}


/**
*** @brief	Returns a quoted version of the string
***
*** Returns a quoted version of the string object without changing the current string.
***
*** @see XString::quote
***
*** @param qchar		The quote char
*** @param onlyIfWhiteSpace	If true the string is only quoted if it contains spaces
*** @param backslash		The char to prefix any embedded quote char with
***
**/
XString
XString::toQuotedXString(TCHAR qchar, bool onlyIfWhiteSpace, TCHAR backslash)
	{
	XString	s(*this);

	s.quote(qchar, onlyIfWhiteSpace, backslash);

	return s;
	}


/**
*** @brief	Returns a unquoted version of the string
***
*** Returns a unquoted version of the string object without changing the current string.
***
*** @see XString::unquote
***
*** @param qchar		The quote char
*** @param backslash		The char to prefix any embedded quote char with
***
**/
XString
XString::toUnquotedXString(TCHAR qchar, TCHAR backslash)
	{
	XString	s(*this);

	s.unquote(qchar, backslash);

	return s;
	}





#if defined(NEVER) && (defined(__AFX_H__) || defined(_AFXDLL) || defined(AFXAPI))
// MFC CXString Constructor
XString::XString(const CXString &s) :
    _strobj(0)
	{
	// Use the default constructor code
	_strobj = getEmptyXString();
	_strobj->addRef();

	// then assign from the CXString object
	*this = (const TCHAR *)s;
	}


// MFC Cstring cast
XString::operator CXString()
	{
	CXString tmp = data();
	return tmp;
	}


// MFC CXString assignment operator
const XString &	
XString::operator=(const CXString &s)
	{
	*this = (const TCHAR *)s;
	return *this;
	}


// MFC CXString append operator
XString &
XString::operator+=(const CXString &s)
	{
	*this += (const TCHAR *)s;
	return *this;
	}

#endif // defined(__AFX_H__) || defined(_AFXDLL) || defined(AFXAPI)



/*********************************************************************************

enum XString::scopeType { one, all }

     Used to specify whether regular expression replace replaces the first one substring matched by the regular
     expression or replaces all substrings matched by the regular expression.

RWCSubXString
operator()(const RWCRExpr& re, size_t start=0);
const RWCSubXString
operator()(const RWCRExpr& re, size_t start=0) const;
RWCSubXString
operator()(const RWCRegexp& re, size_t start=0);
const RWCSubXString
operator()(const RWCRegexp& re, size_t start=0) const;

     Returns the first substring starting after index start that matches the regular expression re. If there is no such
     substring, then the null substring is returned. The first variant can be used as an lvalue.

     Note that if you wish to use operator()(const RWCRExpr&...) you must instead use match(const
     RWCRExpr&...) described below. The reason for this is that we are presently retaining RWCRegexp but
     operator(const RWCRExpr&...) and operator(const RWCRegexp) are ambiguous in the case of
     XString::operator("string"). In addition, operator(const TCHAR *) and operator(size_t) are
     ambiguous in the case of XString::operator(0). This function maybe incompatible with strings with
     embedded nulls. This function is incompatible with MBCS strings.

Public Member Functions

size_t
index(const RWCRExpr& re, size_t i=0) const;
size_t
index(const RWCRegexp& re, size_t i=0) const;

     Regular expression matching. Returns the index greater than or equal to i of the start of the first pattern that
     matches the regular expression re. Returns RW_NPOS if there is no such pattern. This function is incompatible
     with MBCS strings.

size_t
index(const RWCRExpr& re,size_t* ext,size_t i=0) const;
size_t
index(const RWCRegexp& re,size_t* ext,size_t i=0) const;

     Regular expression matching. Returns the index greater than or equal to i of the start of the first pattern that
     matches the regular expression re. Returns RW_NPOS if there is no such pattern. The length of the matching pattern
     is returned in the variable pointed to by ext. This function is incompatible with strings with embedded nulls.
     This function may be incompatible with MBCS strings.

RWCSubXString
match(const RWCRExpr& re, size_t start=0);
const RWCSubXString
match(const RWCRExpr& re, size_t start=0) const;

     Returns the first substring starting after index start that matches the regular expression re. If there is no such
     substring, then the null substring is returned. The first variant can be used as an lvalue. Note that this is used in
     place of operator()(const RWCRegexp&...) if you want to use extended regular expressions. 

size_t
mbLength() const;

     Return the number of multibyte characters in self, according to the Standard C function ::mblen(). Returns
     RW_NPOS if a bad character is encountered. Note that, in general, mbLength() _ length(). Provided only on
     platforms that provide ::mblen().

istream&
readFile(istream& s);

     Reads characters from the input stream s, replacing the previous contents of self, until EOF is reached. Null
     characters are treated the same as other characters.

istream&
readLine(istream& s, RWBoolean skipWhite = TRUE);

     Reads characters from the input stream s, replacing the previous contents of self, until a newline (or an EOF) is
     encountered. The newline is removed from the input stream but is not stored. Null characters are treated the same
     as other characters. If the skipWhite argument is TRUE, then whitespace is skipped (using the iostream library
     manipulator ws) before saving characters.

istream&
readXString(istream& s);

     Reads characters from the input stream s, replacing the previous contents of self, until an EOF or null terminator is
     encountered. If the number of bytes remaining in the stream is large, you should resize the XString to
     approximately the number of bytes to be read prior to using this method. See "Implementation Details" in the
     User's Guide for more information. This function is incompatible with strings with embedded nulls. This
     function may be incompatible with MBCS strings.

istream&
readToDelim(istream& s, TCHAR delim='\n');

     Reads characters from the input stream s, replacing the previous contents of self, until an EOF or the delimiting
     character delim is encountered. The delimiter is removed from the input stream but is not stored. Null characters
     are treated the same as other characters. If delim is '\0' then this function is incompatible with strings with
     embedded nulls. If delim is '\0' then this function may be incompatible with MBCS strings.

istream&
readToken(istream& s);

     Whitespace is skipped before saving characters. Characters are then read from the input stream s, replacing
     previous contents of self, until trailing whitespace or an EOF is encountered. The whitespace is left on the input
     stream. Null characters are treated the same as other characters. Whitespace is identified by the standard C
     library function isspace(). This function is incompatible with MBCS strings. 

replace(const RWCRExpr& pattern, const TCHAR* replacement, 
        scopeType scope=one);
replace(const RWCRExpr& pattern, 
        const XString& replacement,scopeType scope=one);

     Replaces substring matched by pattern with replacement string. pattern is the new extended regular
     expression. scope is one of {one, all} and controls whether all matches of pattern are replaced with
     replacement or just the first one match is replaced. replacement is the replacement pattern for the string.
     Here's an example:

     XString s("hahahohoheehee");
     s.replace(RWCRExpr("(ho)+","HAR"); // s == "hahaHARheehee"

     This function is incompatible with const TCHAR* replacement strings with embedded nulls. This
     function may be incompatible with const TCHAR* replacement MBCS strings.

RWCSubXString
strip(stripType s = XString::trailing, TCHAR c = ' ');
const RWCSubXString
strip(stripType s = XString::trailing, TCHAR c = ' ') 
const;

RWCSubXString
subXString(const TCHAR* cs, size_t start=0,
          caseCompare = XString::exact);
const RWCSubXString
subXString(const TCHAR* cs, size_t start=0,
          caseCompare = XString::exact) const;

     Returns a substring representing the first occurence of the null-terminated string pointed to by "cs". The first
     variant can be used as an lvalue. Case sensitivity is according to the caseCompare argument; it defaults to
     XString::exact. If caseCompare is XString::ignoreCase then this function is incompatible with
     MBCS strings. This function is incompatible with cs strings with embedded nulls. This function may be
     incompatible with cs MBCS strings.

Static Public Member Functions

ostream&
operator<<(ostream& s, const XString&);

     Output an XString on ostream s.

istream&
operator>>(istream& s, XString& str);

     Calls str.readToken(s). That is, a token is read from the input stream s. This function is incompatible with
     MBCS strings.

RWvostream&
operator<<(RWvostream&, const XString& str);
RWFile&
operator<<(RWFile&,     const XString& str);

     Saves string str to a virtual stream or RWFile, respectively.

RWvistream&
operator>>(RWvistream&, XString& str);
RWFile&
operator>>(RWFile&,     XString& str);

     Restores a string into str from a virtual stream or RWFile, respectively, replacing the previous contents of str.

Related Global Functions

XString
strXForm(const XString&);

     Returns the result of applying ::strxfrm() to the argument string, to allow quicker collation than
     XString::collate(). Provided only on platforms that provide ::strxfrm(). This function is
     incompatible with strings with embedded nulls.

*********************************************************************************/



/*********************************************************************************
** Functions
*********************************************************************************/
// Returns a version of str where all upper-case characters have been replaced with lower-case characters. Uses
// the standard C library function tolower(). This function is incompatible with MBCS strings.
XString
toLower(const XString& str)
	{
	XString	tmp = str;

	tmp.toLower();

	return tmp;
	}


// Returns a version of str where all lower-case characters have been replaced with upper-case characters. Uses
// the standard C library function toupper(). This function is incompatible with MBCS strings. 
XString
toUpper(const XString& str)
	{
	XString	tmp = str;

	tmp.toUpper();

	return tmp;
	}


XString
x_string_quote(const XString &str, TCHAR qchar, bool onlyIfWhiteSpace, TCHAR backslash)
	{
	XString	s(str);

	s.quote(qchar, onlyIfWhiteSpace, backslash);

	return s;
	}


XString
x_string_unquote(const XString &str, TCHAR qchar, TCHAR backslash)
	{
	XString	s(str);

	s.unquote(qchar, backslash);

	return s;
	}


char *
XString::strnew(const char *s)
		{
		char	*sp;

		sp = new char[strlen(s)+1];
		strcpy(sp, s);
		return sp;
		}
