/****************************************************************************
**	Date.h	A class to represent a date
****************************************************************************/


#ifndef __base_XDate_h__
#define __base_XDate_h__

// Forward declarations
class XTime;

/**
***	A generic data class that represents dates as julian day numbers internally giving an
***	enormous date range.
***
*** @ingroup core
*/
class XDate
		{
public:
		/// An enumerator for days
		enum DayNumber
			{
			Sunday=0,
			Monday=1,
			Tuesday=2,
			Wednesday=3,
			Thursday=4,
			Friday=5,
			Saturday=6
			};

		/// An enumerator for months
		enum MonthNumber
			{
			January=1,
			February=2,
			March=3,
			April=4,
			May=5,
			June=6,
			July=7,
			August=8,
			September=9,
			October=10,
			November=11,
			December=12
			};

public: 
		/// Construct a XDate object with the current date
		XDate();

		/// Virtual destructor
		virtual ~XDate();

		/**
		*** Construct a XDate object with the date based on the given day, month and year.
		***
		*** @param	day					The day (1-31)
		*** @param	month				The month (1-12)
		*** @param	year				The year (1-9999)
		*** @param	throwOnInvalidDMY	If true and the date is invalid throw a XDateInvalidException
		*** @return						true if the conversion was successful, false otherwise.
		*** @throws XDateInvalidException
		**/
		XDate(int day, int month, int year, bool throwOnInvalidDMY=true);

		/// Copy constructor
		XDate(const XDate &other);

		/// Construct a XDate object with the date based on the given unix time (time_t)
		XDate(time_t t);

		/// Construct a XDate object with the date based on the given Time object
		XDate(const XTime &ut);

		/// Return true if this object contains a valid date
		bool	isValid() const										{ return m_valid; }

		/// Return true if this object represents a leap year
		bool	isLeapYear() const									{ return isLeapYear(m_year); }

		/// Assignment operator from XTime object
		XDate &		operator=(const XTime &ut);

		/// Assignment operator from XDate object
		XDate &		operator=(const XDate &other);

		/**
		*** Set this date from the given day, month and year.
		***
		*** If an invalid date is specified this method will return false and
		*** the object will remain unchanged.
		***
		*** @param	day					The day (1-31)
		*** @param	month				The month (1-12)
		*** @param	year				The year (1-9999)
		*** @param	throwOnInvalidDMY	If true and the date is invalid throw a XDateInvalidException
		*** @return						true if the conversion was successful, false otherwise.
		*** @throws XDateInvalidException
		**/
		bool			set(int day, int month, int year, bool throwOnInvalidDMY=false);

		/// Get the Julian Day Number
		int				julian() const								{ return m_julian; }

		/// Return the day of month (1-31)
		int				day() const									{ return m_day; }

		/// Return the month (1=Jan - 12=Dec)
		int				month() const								{ return m_month; }

		/// Get the year
		int				year() const								{ return m_year; }

		/// Return the day of week (0=Sunday - 6=Saturday)
		int				dayOfWeek() const							{ return (m_julian+1)%7; }

		/// Return the day of month (1-31)
		int				dayOfMonth() const							{ return m_day; }

		/// Return the day of year (1-366 if dayOneIsZero is false) or (0-365 if dayOneIsZero is true)
		int				dayOfYear(bool dayOneIsZero=false) const	{ return m_julian - m_julianYearStart + (dayOneIsZero?0:1); }

		/**
		*** Get the week number of year as a decimal number (0-53),
		***
		*** Returns the week number of the year taking into account the day
		*** which is considered the first day of the week. This defines when
		*** week 1 starts. Week 0 is the first few days of the year before the first
		*** day of the week. Week 53 is the last few days of the year. Both week 0 and week
		*** 53 will be less than 7 days.
		***
		*** @param	firstDayOfWeek1		Specifies which day is considered the first day of week 1 (default=Sunday)
		**/
		int				weekOfYear(DayNumber firstDayOfWeek1=Sunday) const;

		/// Set with todays date
		void			update();

public: // STATIC

		/// Convert a day/month/year to a julian day number
		static void		dayMonthYearToJulian(int d, int m, int y, int &j);

		/// Convert a  julian day number to a day/month/year
		static void		julianToDayMonthYear(int j, int &d, int &m, int &y);

		/// Check to see if the given year is a leap year
		static bool		isLeapYear(int year);

private:
		/// Internal function to calculate the value of the non-julian fields
		void			calculateFields();

private:
		bool	m_valid;			///< Set to true if the calculated fields are valid
		int		m_julian;			///< The julian day number
		int		m_julianYearStart;	///< Calculated field: The julian number for the start of the year
		int		m_day;				///< Calculated field: The day (1-31)
		int		m_month;			///< Calculated field: The month (1-12)
		int		m_year;				///< Calculated field: The year
		};


#endif
