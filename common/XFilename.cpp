/****************************************************************************
** XFilename.cpp	A data I/O block
****************************************************************************/


#include "Xtypes.h"
#include "XString.h"
#include "XIntegerArray.h"
#include "XFilename.h"


// Default constructor
XFilename::XFilename(const XString &path, PathType type)
		{
		if (type != PathTypeUnix && type != PathTypeWindows)
			{
#if defined(WIN32) || defined(_WIN32)
			m_type = PathTypeWindows;
#else
			m_type = PathTypeUnix;
#endif
			}

		m_path = fix(path, m_type);
		}


// Copy constructor
XFilename::XFilename(const XFilename &other)
		{
		*this = other;
		}


/*
** Destructor
*/
XFilename::~XFilename()
		{
		}


// Assignment operator
XFilename &
XFilename::operator=(const XFilename &other)
		{
		m_path = other.m_path;
		m_type = other.m_type;

		return *this;
		}


// Assignment operator
XFilename &
XFilename::operator=(const XString &path)
		{
		m_path = fix(path, PathTypeNoChange);
		m_type = PathTypeNoChange;

		return *this;
		}


// Test to see if the given path is relative
bool
XFilename::isRelativePath() const
		{
		return isRelativePath(m_path);
		}


// Test to see if the given path is relative (STATIC VERSION)
bool
XFilename::isRelativePath(const XString &path)
		{
		if (path.length() > 0) return !isAbsolutePath(path);

		return false;
		}


// Test to see if the given path is absolute
bool
XFilename::isAbsolutePath() const
		{
		return isAbsolutePath(m_path);
		}


// Test to see if the given path is absolute (STATIC VERSION)
bool
XFilename::isAbsolutePath(const XString &path)
		{
		if (path.length() > 0)
			{
			if (path[0] == '/' || path[0] == '\\') return true;

			if (path.length() > 2 && path[1] == ':' && (path[2] == '\\' || path[2] == '/')) return true;
			}

		return false;
		}



// Fix the given path (STATIC)
XString
XFilename::fix(const XString &filename, PathType type)
		{
		XString	f=filename, sep;
		int			l=(int)f.GetLength(), p;

		switch (type)
			{
			case PathTypeNoChange:
			case PathTypePlatform:
#if defined(WIN32) || defined(_WIN32)
				sep = "\\";
				if (type != PathTypeNoChange) f.Replace('/', '\\');
#else
				sep = "/";
				if (type != PathTypeNoChange) f.Replace('\\', '/');
#endif
				break;

			case PathTypeWindows:
				sep = "\\";
				f.Replace('/', '\\');
				break;

			case PathTypeUnix:
				sep = "/";
				f.Replace('\\', '/');
				break;
			}

		// Only work on non null strings
		if (l > 0)
			{
			// Check for C: case (append \)
			if (l == 2 && f[1] == _T(':'))
				{
				f += sep;
				l++;
				}
			else
				{
				// Check for trailing path separator
				while (l > 1 && (f[l-1] == _T('/') || f[l-1] == _T('\\')))
					{
					if (l == 3 && f[1] == _T(':')) break;

					if (l > 3 || (l > 2 && f[1] != _T(':'))) f.Delete(--l, 1);
					}
				//if (l > 3 && f[l-1] == _T('\\')) f.Delete(--l, 1);

				// Check for double separators
				while ((p = f.index(_T("//"))) >= 0)
					{
					f.remove(p, 1);
					}

				// Check for double separators but not in 1st position
				// which is allowed for Dos paths (e.g. \\server\...)
				while ((p = f.index(_T("\\\\"), 1)) >= 0)
					{
					f.remove(p, 1);
					}
				}
			}

		return f;
		}



/**
*** Split a path into it's component parts. (STATIC)
**/
int
XFilename::splitPath(const XString &path, XStringArray &sa)
		{
		const TCHAR	*sp, *tp;
		int			idx=0;
		bool		root=false;

		sa.clear();

		XString	tmppath(path);
		tp = tmppath;

		if (path.startsWith(_T("/")))
			{
			// Absolute Unix path
			sa[idx++] = _T("/");
			tp++;
			root = true;
			}
		else if (path.startsWith(_T("\\\\")))
			{
			// UNC Path \\server\xxx.....
			sa[idx++] = _T("\\\\");
			tp += 2;
			root = true;
			}
		else if (path.length() > 2 && path[1] == ':' && (path[2] == '\\' || path[2] == '/'))
			{
			// Dos path x:\....
			sa[idx++] = path(0, 3);
			tp += 3;
			root = true;
			}

		while (*tp)
			{
			// Ignore duplicate separators
			while (*tp == '/' || *tp == '\\') tp++;

			sp = tp;
			while (*tp && *tp != '/' && *tp != '\\') tp++;

			XString	tmp(sp, (int)(tp-sp));

			if (*tp) tp++;

			if (tmp == _T(".")) continue;
			if (tmp == _T(".."))
				{
				if (idx > 0)
					{
					// Make sure we don't go above root!
					if ((root && idx == 1) || sa[idx-1] == _T("..")) continue;
					idx--;
					continue;
					}
				}

			sa[idx++] = tmp;
			}

		// truncate the array to the correct size
		if (idx > sa.size()) sa.RemoveAt(idx, sa.size()-idx);

		return idx;
		}


/**
*** Optimize a path by reducing duplicate path separators
*** . and .. where possible
**/
XString	
XFilename::optimizePath(const XString &path, PathType type)
		{
		XStringArray	sa;

		splitPath(path, sa);
		return joinPath(sa, type);
		}


/**
*** Joins path components to make a path
**/
XString	
XFilename::joinPath(XStringArray &sa, PathType type)
		{
		TCHAR		c;
		int		i, n=0;
		XString	path;
		bool		root=false;
		XIntegerArray	ia;

		switch (type)
			{
			case PathTypeUnix:	c = _T('/');		break;
			case PathTypeWindows:	c = _T('\\');		break;
			default:	c = X_PATH_SEPARATOR;	break;
			}
		
		// First analyse the path to remove .. and .
		// and build up an index list in ia
		for (i=0;i<sa.size();i++)
			{
			if (i == 0 && isAbsolutePath(sa[i])) root = true;

			// Ignore references to .
			if (sa[i] == _T(".")) continue;

			if (sa[i] == _T(".."))
				{
				if (n > 0 && sa[ia[n-1]] != _T(".."))
					{
					--n;
					continue;
					}
				}

			// Add this position to the array
			ia[n++] = i;
			}

		// Now simply join the parts together
		for (i=0;i<n;i++)
			{
			if (i > (root?1:0)) path += c;
			path += sa[ia[i]];
			}

		// Make sure we have a valid path
		if (path.isEmpty()) path = _T(".");

		return path;
		}


/**
*** Returns the absolute path name for the given path
*** in relation to the specified directory (default current)
***
*** If the neither the directory nor the path passed to this function are absolute
*** it is impossible to generate an absolute path and the result will be the same
*** as calling concatPaths.
***
*** @param path		The path name
*** @param dir		The directory name used to generate a relative path
*** @param type		The path type (Default: 0 - platform dependent, 1 - Unix, 2 - Dos/Windows)
**/
XString
XFilename::absolutePath(const XString &path, const XString &dir, PathType type)
		{
		XString	result;

		if (isAbsolutePath(path)) result = path;
		else result = concatPaths(dir, path, type);

		return optimizePath(result, type);
		}


/**
*** Returns the relative path name for the given path
*** in relation to the specified directory (default .)
***
*** There are certain conditions when it is impossible to calculate a relative
*** path. These are described below:
***
*** When the path is absolute and the directory is relative it is impossible
*** to determine the relative path between them. Under these circumstances the
*** orginal path is returned.
***
*** When the path and the directory are on different physical drives (e.g. c: and d:)
*** there is no relative path between them. Under these circumstances the
*** orginal path is returned.
***
*** @param p		The path name (can be absolute)
*** @param d		The directory name used to generate an absolute path
*** @param type		The path type (Default: 0 - platform dependent, 1 - Unix, 2 - Dos/Windows)
**/
XString
XFilename::relativePath(const XString &p, const XString &d, PathType type)
		{
		// Check for impossible situations which require special handling
		if (isAbsolutePath(p) && isRelativePath(d)) return p;
		if (p.length() > 1 && d.length() > 1 && p[1] == ':' && d[1] == ':' && p[0] != d[0]) return p;

		XString	dir=d;
		XString	path=p;

		// Now path AND dir are both absolute
		XStringArray	pa, da, ra;
		int		i, j, npa, nda;

		npa = splitPath(path, pa);
		nda = splitPath(dir, da);

		i = 0;
		while (i < npa && i < nda)
			{
			// Ignore case on dos paths
			if (pa[i].compareTo(da[i], type==PathTypeWindows?XString::ignoreCase:XString::exact) != 0) break;
			i++;
			}

		for (j=i;j<nda;j++)
			{
			if (!isAbsolutePath(da[j])) ra.add(_T(".."));
			}

		for (j=i;j<npa;j++) 
			{
			if (!isAbsolutePath(pa[j])) ra.add(pa[j]);
			}

		if (ra.size() == 0) return _T(".");

		return joinPath(ra, type);
		}



XString
XFilename::extension() const
		{
		return extension(m_path);
		}


// This function returns the extension of the given file name.
XString
XFilename::extension(const XString &file)
		{
		XString	extension;
		int		p;

		extension = basename(file);

		if ((p = extension.ReverseFind(_T('.'))) >= 0) extension.Delete(0, p+1);
		else extension.empty();

		return extension;
		}


void
XFilename::split(XString &base, XString &extension) const
		{
		split(m_path, base, extension);
		}


void
XFilename::split(const XString &file, XString &base, XString &extension)
		{
		int	p;

		base = basename(file);
		extension.Empty();

		if ((p = base.ReverseFind(_T('.'))) >= 0)
			{
			extension = base;
			extension.Delete(0, p+1);
			base.Delete(p, (int)base.GetLength()-p);
			}
		}


XString
XFilename::concatPaths(const XString &path, const XString &path2, PathType type)
		{
		XString	result=path;
		XString	p2=path2;
		TCHAR		c;

		switch (type)
			{
			case PathTypeUnix:		c = _T('/');		break;
			case PathTypeWindows:	c = _T('\\');		break;
			default:				c = X_PATH_SEPARATOR;	break;
			}
		
		// First strip off any trailing slashes
		result.TrimRight(c);

		// Strip off slashes from start of second path
		p2.TrimLeft(c);

		if (!p2.isEmpty())
			{
			// Next add the separator and the second path

			// can't use result to test here since a root path spec '/' will become empty!
			// if (!result.isEmpty()) result += c;
			if (path.length()) result += c;

			result += p2;
			}
		else
			{
			// Check we didn't zap a root spec (i.e. /)
			if (path.length() && result.length() == 0) result = c;
			}

		return result;
		}


XString
XFilename::dirname() const
		{
		return dirname(m_path);
		}


XString
XFilename::dirname(const XString &path)
		{
		XString	v(path);
		int			p, l=(int)v.length();

		// Remove all the trailing slashes (but not if it it the one and only char)
		while (l > 1 && (v[l-1] == _T('/') || v[l-1] == _T('\\')))
			{
			if (l == 3 && v[1] == _T(':'))
				{
				// This is a DOS path with a drive
				// i.e. X:\ so just return it
				return v;
				}

			v.remove(l--, 1);
			}

		if (v != _T("/") && v != _T("\\") && !(v.length() == 2 && v[1] == _T(':')))
			{
			if (v.IsEmpty() || (p = v.last(_T("/\\"))) < 0) v = _T(".");
			else
				{
				v.Delete(p+1, (int)v.GetLength()-p-1);
				l = (int)v.GetLength();

				// Remove all the trailing slashes (but not if it it the one and only char)
				while (l > 1 && (v[l-1] == _T('/') || v[l-1] == _T('\\')))
					{
					if (l == 3 && v[1] == _T(':'))
						{
						// This is a DOS path with a drive
						// i.e. X:\ so just return it
						return v;
						}

					v.Delete(--l, 1);
					}

				}
			}

		return v;
		}


XString
XFilename::basename() const
		{
		return basename(m_path);
		}


XString
XFilename::basename(const XString &path)
		{
		XString	v(path);
		int			p;

		if (!v.IsEmpty() 
			&& v != _T("/") 
			&&  v != _T("\\")
			&& !(v.length() == 3 && v[1] == _T(':') && (v[2] == _T('/') || v[2] == _T('\\')))
			)
			{
			if ((p = v.last(_T("/\\"))) >= 0) v.Delete(0, p+1);
			}

		return v;
		}


XString
XFilename::drivename() const
		{
		return drivename(m_path);
		}


// This function returns the drivename of the given file name.
XString
XFilename::drivename(const XString &filename)
		{
		XString	v;
		XString	path=filename;

		// Case 1: \\server\share\...
		if (path.GetLength() > 2 && path[0] == _T('\\') && path[1] == _T('\\'))
			{
			XString	tmp;

			tmp = sharename(path);
			if (tmp.GetLength() == 2 && tmp[1] == _T('$')) v = tmp[0];
			}
		// Case 2: X:...
		else if (path.GetLength() > 1 && path[1] == _T(':'))
			{
			v = path[0];
			}

		return v;
		}


XString
XFilename::sharename() const
		{
		return sharename(m_path);
		}


// This function returns the sharename of the given file name.
XString
XFilename::sharename(const XString &filename)
		{
		XString	v;
		XString	path=filename;

		// Case 1: \\server\share\...
		if (path.GetLength() > 2 && path[0] == _T('\\') && path[1] == _T('\\'))
			{
			XString	tmp;
			int			p;

			tmp = path;
			tmp.Delete(0, 2);

			// NOW server\share\....
			if ((p = tmp.Find(_T('\\'))) >= 0)
				{
				// NOW share\....
				tmp.Delete(0, p+1);

				if ((p = tmp.Find(_T('\\'))) >= 0) tmp.Delete(p, (int)tmp.GetLength()-p);

				// NOW share
				v = tmp;
				}
			}

		return v;
		}
