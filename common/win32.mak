PROJROOT	= ..

include ../win32.mak.tools
include Makefile.defs

LIBRARY		= $(OBJDIR)\common.lib

all: $(LIBRARY)

!if [$(TOOLSBIN)\mkobjnames $(OBJDIR) $(OBJDIR)\objnames.inc OBJS $(SRCS)]
!endif
!include $(OBJDIR)\objnames.inc

clean:
	-if exist $(LIBRARY) del /f /q $(LIBRARY) *.*~
	-rd /s /q $(PLATFORM)

$(LIBRARY): $(OBJS)
	lib -nologo -out:$(LIBRARY) $(OBJS)
