/****************************************************************************
** Array.cpp	A generic array object
****************************************************************************/


#include <string.h>

#include "XArray.h"


XArray::XArray(int size, int elemsize, int incsize) :
    _data(size*elemsize, incsize*elemsize),
    _nelems(0),
    _elemsize(elemsize)
		{
		ensureCapacity(size);
		_nelems = size;
		}


XArray::~XArray()
		{
		}


// Make sure our _data array has enough space!
void
XArray::ensureCapacity(size_t wanted)
		{
		wanted *= _elemsize;
		if (wanted > _data.size()) _data.setSize(wanted);
		}


// Init back to zero
void
XArray::clear()
		{
		_nelems = 0;
		_data.clearContents();
		}


/**
*** Removes count elements from the position specified
**/
void
XArray::removeElements(int pos, int count)
		{
		if (count <= 0) return;
		if (pos < 0 || pos >= _nelems) throw 1;

		// Make sure we don't remove too much
		if ((pos + count) > _nelems) count = _nelems - pos;
		if (count > 0)
			{
			if (pos + count != _nelems)
			{
			// We must shift the data down to fill the gap.
			int	from, to, amount;

			to = pos * _elemsize;
			from = to + (count * _elemsize);
			amount = (_nelems - count - pos) * _elemsize;

			memmove((char *)_data + to, (char *)_data + from, amount);
			}

			// Adjust the element count and reflect this
			// in the underlying buffer too so that when it re-grows
			// it gets zeroed out again.
			_nelems -= count;
			_data.setSize(_nelems * _elemsize);
			}
		}


/**
*** Inserts count elements at the position specified
**/
void
XArray::insertElements(int pos, int count)
		{
		if (count <= 0) return;
		if (pos < 0) throw 1;

		if (count > 0)
			{
			// Make sure we have the capacity for the new elements
			ensureCapacity(_nelems + count);

			if (pos < _nelems)
			{
			// We must shift the data down to fill the gap.
			int	from, to, amount;

			from = pos * _elemsize;
			to = from + (count * _elemsize);
			amount = (_nelems - pos) * _elemsize;

			memmove((char *)_data + to, (char *)_data + from, amount);

			// Zero out the "new" data
			memset((char *)_data + from, 0, count * _elemsize);
			}

			// Adjust the element count and reflect this
			// in the underlying buffer too so that when it re-grows
			// it gets zeroed out again.
			_nelems += count;
			_data.setSize(_nelems * _elemsize);
			}
		}
