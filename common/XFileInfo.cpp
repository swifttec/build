/****************************************************************************
**	XFileInfo.cpp	A class representing a single directory entry
****************************************************************************/


#include "Xtypes.h"
#include "XString.h"
#include "XFilename.h"
#include "XFileInfo.h"

#if defined(WIN32) || defined(_WIN32)
	#include <winioctl.h>
	#include <io.h>

	#pragma warning(disable: 4996)
#else
	#include <unistd.h>
#endif

#include <sys/stat.h>

#if defined(linux)
	#include <sys/time.h>
	#include <utime.h>
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


XFileInfo::XFileInfo(const XString &filename)
		{
		reset();
		if (!filename.isEmpty()) getInfoForFile(filename);
		}


// Copy constructor
XFileInfo::XFileInfo(const XFileInfo &v)
		{
		*this = v;
		}


XFileInfo::~XFileInfo()
		{
		}


// Assignment operator
const XFileInfo &
XFileInfo::operator=(const XFileInfo &v)
		{
#define COPY(x)	x = v.x
		COPY(_name);
		COPY(_dosname);
		COPY(_directory);

		COPY(_attributes);
		COPY(_size);
		COPY(_inode);
		COPY(_devno);
		COPY(_dirdevno);
		COPY(_linkcount);
		COPY(_createTime);
		COPY(_lastAccessTime);
		COPY(_lastModificationTime);
		COPY(_lastStatusChangeTime);

		COPY(_validData);
#undef COPY

		return *this;
		}

// PRIVATE:
// Reset all attributes
void
XFileInfo::reset()
		{
#define ZERO(x) x = 0

		_name = _T("");
		_dosname = _T("");
		_directory = _T("");
		_attributes.clear();

		ZERO(_size);
		ZERO(_inode);
		ZERO(_devno);
		ZERO(_dirdevno);
		ZERO(_linkcount);
		ZERO(_validData);

		_createTime = 0.0;
		_lastAccessTime = 0.0;
		_lastModificationTime = 0.0;
		_lastStatusChangeTime = 0.0;
#undef ZERO
		}


/**
*** Ensure the file info is valid.
***
*** This method is not required for the XFileInfo class, but provided for 
*** derived classes like XDirectoryEntry which postpone reading file attributes
*** to improve performance.
**/
void
XFileInfo::validate(ValidateType type)
		{
		if ((_validData & (int)type) == 0)
			{
			// We don't yet have the data we need - so get it
			if ((type & FileAttributes) != 0 && ((_validData & FileAttributes) == 0)) getAttributeInfo(getFullPathname());
			if ((type & FileTime) != 0 && ((_validData & FileTime) == 0)) getTimeInfo(getFullPathname());
			if ((type & FileSize) != 0 && ((_validData & FileSize) == 0)) getSizeInfo(getFullPathname());
			if ((type & FileName) != 0 && ((_validData & FileName) == 0)) getNameInfo(getFullPathname());
			}
		}



// Refresh the info for this object
void
XFileInfo::refresh()
		{
		_validData = 0;
		validate(FileAll);
		}


/*
** Get the mode as an ls-like string
** This is a 10 or 11 character string
**
** First char
** d     the entry is a directory;
** D     the entry is a door;
** l     the entry is a symbolic link;
** b     the entry is a block special file;
** c     the entry is a character special file;
** p     the entry is a fifo (or "named pipe") special file;
** s     the entry is an AF_UNIX address family socket;
** -     the entry is an ordinary file;
**
** Next 3 groups of 3 are Read/Write/Execute permissions for
** Owner, Group, and World respectivly
**
** Finally a plus character is appended if there is an acl
** associated with the entry
*/
XString
XFileInfo::getUnixModeString()
		{
		XString	s;

		validate(FileAttributes);
		if (isValid()) s = _attributes.getUnixModeString();

		return s;
		}


XString
XFileInfo::getAttributesString()
		{
		XString	s;

		validate(FileAttributes);
		if (isValid()) s = _attributes.getAttributesString();
		
		return s;
		}


/*
** Read the value of the symbolic link (if we are one)
*/
XString
XFileInfo::getLinkValue()
		{
		XString	result;

		if (isSymbolicLink())
			{
#if defined(WIN32) || defined(_WIN32)
			/*******************************************************************************************
			HANDLE	hFile=0; 
			
			hFile = CreateFile(getFullPathname(), GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_FLAG_OPEN_REPARSE_POINT, NULL);
			if (hFile != INVALID_HANDLE_VALUE)
				{
				char						buf[MAXIMUM_REPARSE_DATA_BUFFER_SIZE];
				DWORD						sizebuf=sizeof(buf);

				if (DeviceIoControl(hFile, FSCTL_GET_REPARSE_POINT, NULL, 0, buf, &sizebuf, NULL))
					{
					REPARSE_GUID_DATA_BUFFER 	*rp=(REPARSE_GUID_DATA_BUFFER  *)buf;

					if (rp->ReparseTag == IO_REPARSE_TAG_MOUNT_POINT)
						{
						}
					}

				CloseHandle(hFile);
				}
			*******************************************************************************************/		
#endif
			/*
			char	tmp[1024];
			int		nbytes;

			if ((nbytes = ACE_OS::readlink(getFullPathname(), tmp, sizeof(tmp))) > 0) 
				{
				tmp[nbytes] = 0;
				result = tmp;
				}
			*/
			}
		
		return result;
		}


XString
XFileInfo::getFullPathname() const
		{
		XString	path;

		if (_name == _T("/") || (_name.length() == 3 && _name[1] == ':' && (_name[2] == '/' || _name[2] == '\\')))
			{
			path = _name;
			}
		else
			{
			path = XFilename::concatPaths(_directory, _name);
			}

		return path;
		}


XString
XFileInfo::getDosFullPathname() const
		{
		XString	path;

		if (_dosname == _T("/") || (_dosname.length() == 3 && _dosname[1] == ':' && (_dosname[2] == '/' || _dosname[2] == '\\')))
			{
			path = _dosname;
			}
		else
			{
			path = XFilename::concatPaths(_dosdir, _dosname);
			}

		return path;
		}


#if defined(WIN32) || defined(_WIN32)
	// These O/S have the stat64 and lstat64 interface
	typedef struct _stati64 x_stat_t;
	#define x_stat		::_tstati64
	#define x_lstat	::_tstati64
#else
	// These O/S have the stat and lstat interface
	typedef struct stat x_stat_t;
	#define x_stat		::stat
	#define x_lstat	::lstat
#endif


bool
XFileInfo::getAttributeInfo(const XString &filename)
		{
		bool		ok=false;
		uint32_t	unixmode=0, winattrs=0, symlink=0;
		x_stat_t	st;

		// First get the info on the file
		if (x_lstat(XString(filename), &st) >= 0)
			{
#ifdef S_IFLNK
			if ((st.st_mode & S_IFMT) == S_IFLNK)
				{
				x_stat_t	st2;

				if (x_stat(XString(filename), &st2) >= 0)
					{
					// This is a symbolic link, BUT we want the info on the file it points to!
					// as we regard a symlink as an attribute not a file type
					symlink = 1;
					st = st2;
					}
				else
					{
					// this is a symbolic link, BUT it points to an invalid file
					// so we return the information on the link itself.
					symlink = 0;
					}
				}
#endif

			unixmode = st.st_mode;

			_size = st.st_size;
			_inode = (uint64_t)st.st_ino;
			_devno = st.st_rdev;
			_dirdevno = st.st_dev;
			_linkcount = st.st_nlink;
			_lastAccessTime = st.st_atime;
			_lastModificationTime = st.st_mtime;

			// The mtime and ctime values have subtle differences on windows
#if defined(_WIN32) || defined(WIN32)
			_lastStatusChangeTime = st.st_mtime;
			_createTime = st.st_ctime;
#else
			_lastStatusChangeTime = st.st_ctime;
			_createTime = st.st_mtime;
#endif

			ok = true;
			}

		if (ok) _validData |= (int)(FileSize | FileTime);

#if defined(_WIN32) || defined(WIN32)
		if (ok)
			{
			winattrs = ::GetFileAttributes(XString(filename));
			ok = (winattrs != 0);
			}
#endif

		if (ok)
			{
			_attributes.set(filename, unixmode, winattrs, symlink);
			_validData |= FileAttributes;
			}

		return ok;
		}


bool
XFileInfo::getTimeInfo(const XString &filename)
		{
		bool	ok=false;

#if defined(_WIN32) || defined(WIN32)
		// use getTimes for improved accuracy over stat.
		getFileTimes(filename, &_lastModificationTime, &_createTime, &_lastAccessTime); 
		_lastStatusChangeTime = _lastModificationTime;
		ok = true;
#else
		X_UNREFERENCED_PARAMETER(filename);
		ok = true;
#endif

		if (ok) _validData |= FileTime;

		return ok;
		}


bool
XFileInfo::getSizeInfo(const XString &filename)
		{
		bool	ok=false;

		// Currently we take the file size from the attribute info.
		if ((_validData & FileAttributes) == 0) ok = getAttributeInfo(filename);
		else ok = true;

		if (ok) _validData |= (int)FileSize;

		return ok;
		}



bool
XFileInfo::getNameInfo(const XString &filename)
		{
		bool	ok=false;

#if defined(_WIN32) || defined(WIN32)
		XString	tmpname;
		DWORD		r, n;

		n = (DWORD)filename.length()+1;
		for (;;)
			{
			r = GetShortPathName(XString(filename), tmpname.GetBuffer(n), n);
			tmpname.ReleaseBuffer();
			if (r == 0)
				{
				_dosname = XFilename::basename(filename);
				_dosdir = XFilename::dirname(filename);
				break;
				}
			else if (r <= n)
				{
				_dosname = XFilename::basename(tmpname);
				_dosdir = XFilename::dirname(tmpname);
				break;
				}
			else
				{
				n = r;
				}
			}
		
		n = (DWORD)filename.length()+1;
		for (;;)
			{
			r = GetLongPathName(XString(filename), tmpname.GetBuffer(n), n);
			tmpname.ReleaseBuffer();
			if (r == 0)
				{
				_name = XFilename::basename(filename);
				_directory = XFilename::dirname(filename);
				break;
				}
			else if (r <= n)
				{
				_name = XFilename::basename(tmpname);
				_directory = XFilename::dirname(tmpname);
				break;
				}
			else
				{
				n = r;
				}
			}
		
		ok = true;
#else
		_dosname = _name = XFilename::basename(filename);
		_dosdir = _directory = XFilename::dirname(filename);
		ok = true;
#endif

		if (ok) _validData |= (int)FileName;

		return ok;
		}


uint_t
XFileInfo::getInfoForFile(const XString &filename)
		{
		bool		ok=true;

		reset();

		if (ok && (_validData & FileAttributes) == 0) ok = getAttributeInfo(filename);
		if (ok && (_validData & FileTime) == 0) ok = getTimeInfo(filename);
		if (ok && (_validData & FileSize) == 0) ok = getSizeInfo(filename);
		if (ok && (_validData & FileName) == 0) ok = getNameInfo(filename);

#if defined(WIN32) || defined(_WIN32)
		return (ok && isValid()?0:GetLastError());
#else
		return (ok && isValid()?0:errno);
#endif
		}




// The following file types we take from the attributes in preference to the mode.
// because under unix we derive the attributes from the mode and windows gives us
// the attributes directly. Therefore we are less likely to make a system call
// to get the information



bool
XFileInfo::hasAttributes(x_fileattr_t wanted, bool wantAny)
		{
		validate(FileAttributes);
		return (isValid() && _attributes.hasAttributes(wanted, wantAny));
		}


/*
bool
XFileInfo::isFile()
		{
		return (isValid() && !hasAttributes(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_DEVICE|FILE_ATTRIBUTE_REPARSE_POINT));
		}
*/



// Some useful shortcuts
bool
XFileInfo::isValid()
		{
		return ((_validData & FileAttributes) != 0);
		}




/**
*** Returns a list of allocated ranges in the file.
***
*** If the file is empty no ranges will be returned. If the file is not empty and not sparse
*** or alloacted ranges are not supported by the underlying O/S a single range for the entire file
*** will be returned. Otherwise a number of ranges are returned.
***
*** @return If the file cannot be opened this method will be return false othewise it will be return true
uint_t
XFileInfo::getAllocatedRanges(XObjectArray &oa)
		{
		uint_t	res=0;
		uint64_t		len=getSize();

		if (len > 0)
			{
#if defined(WIN32) || defined(_WIN32)
			HANDLE	fh;

			DWORD	dwDesiredAccess=GENERIC_READ;
			DWORD	dwShareMode=FILE_SHARE_READ;
			DWORD	dwCreationDisposition=OPEN_EXISTING;
			DWORD	dwFlagsAndAttributes=FILE_ATTRIBUTE_NORMAL;

			fh = CreateFile(getFullPathname(), dwDesiredAccess, dwShareMode, NULL, dwCreationDisposition, dwFlagsAndAttributes, NULL);
			if (fh != INVALID_HANDLE_VALUE)
				{
				FILE_ALLOXTED_RANGE_BUFFER		*pRange, inbuf;
				FILE_ALLOXTED_RANGE_BUFFER		outbuf[32];
				DWORD							dw;
				BOOL							b;

				inbuf.FileOffset.QuadPart = 0;
				inbuf.Length.QuadPart = len;

				for (;;)
					{
					dw = 0;
					b = DeviceIoControl(fh, FSCTL_QUERY_ALLOXTED_RANGES, &inbuf, sizeof(inbuf), outbuf, sizeof(outbuf), &dw, NULL);
					if (b || GetLastError() == ERROR_MORE_DATA)
						{
						pRange = outbuf;
						while (dw > 0)
							{
							oa.add(new XFileInfoRange(pRange->FileOffset.QuadPart, pRange->Length.QuadPart));
							inbuf.FileOffset.QuadPart = pRange->FileOffset.QuadPart + pRange->Length.QuadPart;
							pRange++;
							dw -= sizeof(FILE_ALLOXTED_RANGE_BUFFER);
							}

						inbuf.Length.QuadPart = len - inbuf.FileOffset.QuadPart;

						if (b)
							{
							res = true;
							break;
							}
						}
					else break;
					}

				CloseHandle(fh);
				res = 0;
				}
			else res = ::GetLastError();

#else
			// For now assume only one range
			oa.add(new XFileInfoRange(0, len));
			res = 0;
#endif
			}

		return res;
		}
**/


/// Returns true if the specified filename is a file.
bool
XFileInfo::isFile(const XString &filename)
		{
		return XFileInfo(filename).isFile();
		}


/// Returns true if the specified filename is a folder/directory.
bool
XFileInfo::isDirectory(const XString &filename)
		{
		return XFileInfo(filename).isDirectory();
		}


/// Returns true if the specified filename is a folder/directory.
bool
XFileInfo::isFolder(const XString &filename)
		{
		return XFileInfo(filename).isDirectory();
		}


/// Returns true if the specified filename is a symbolic link.
bool
XFileInfo::isSymbolicLink(const XString &filename)
		{
		return XFileInfo(filename).isSymbolicLink();
		}


#if defined(WIN32) || defined(_WIN32)
	#define accces(x, y)	_access(x, y)
#endif

/// Returns true if the specified filename is readable.
bool
XFileInfo::isReadable(const XString &filename)
		{
		return (access(XString(filename), 4)==0);
		}


/// Returns true if the specified filename is writeable.
bool
XFileInfo::isWriteable(const XString &filename)
		{
		return (access(XString(filename), 2)==0);
		}


/// Returns true if the specified filename is executeable.
bool
XFileInfo::isExecutable(const XString &filename)	
		{
		return (access(XString(filename), 1)==0);
		}


uint64_t
XFileInfo::size(const XString &filename)
		{
		return XFileInfo(filename).getSize();
		}



uint_t
XFileInfo::getFileTimes(const XString &path, XTime *lastModTime, XTime *createTime, XTime *lastAccessTime)
		{
		uint_t	res=0;

	#if defined(WIN32) || defined(_WIN32)
		HANDLE		fh = CreateFile(XString(path), GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		FILETIME	*ct=NULL, *at=NULL, *wt=NULL;

		if (lastModTime != NULL) wt = new FILETIME;
		if (createTime != NULL) ct = new FILETIME;
		if (lastAccessTime != NULL) at = new FILETIME;

		if (fh)
			{
			if (!GetFileTime(fh, ct, at, wt)) res = GetLastError();
			CloseHandle(fh);
			}
		else res = GetLastError();

		if (res == 0)
			{
			if (lastModTime != NULL) *lastModTime = *wt;
			if (createTime != NULL) *createTime = *ct;
			if (lastAccessTime != NULL) *lastAccessTime = *at;
			}

		delete at;
		delete wt;
		delete ct;
	#else
		// Use stat to get the file times
		struct stat sb;
		if (stat(XString(path), &sb) == 0)
			{
			if (lastModTime != NULL) *lastModTime = sb.st_mtime;
			if (createTime != NULL) *createTime = sb.st_mtime;	    // There is no create time in unix so pretend
			if (lastAccessTime != NULL) *lastAccessTime = sb.st_atime;
			}
		else res = errno;
	#endif

		return res;
		}


// Sets the various file times for the specified file.
uint_t
XFileInfo::setFileTimes(const XString &path, XTime *lastModTime, XTime *createTime, XTime *lastAccessTime)
		{
		uint_t	    res=0;

	#if defined(WIN32) || defined(_WIN32)
		HANDLE		fh;
		FILETIME	*ct=NULL, *at=NULL, *wt=NULL;

		fh = CreateFile(XString(path), GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

		if (fh == INVALID_HANDLE_VALUE && isDirectory(path))

			{
			fh = CreateFile(XString(path), GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL|FILE_FLAG_BACKUP_SEMANTICS, NULL);

			}

		if (fh == INVALID_HANDLE_VALUE) res =  GetLastError();
		else
			{
			if (lastModTime != NULL)
				{
				wt = new FILETIME;
				lastModTime->toFILETIME(*wt);
				}

			if (createTime != NULL)
				{
				ct = new FILETIME;
				createTime->toFILETIME(*ct);
				}

			if (lastAccessTime != NULL)
				{
				at = new FILETIME;
				lastAccessTime->toFILETIME(*at);
				}

			if (!SetFileTime(fh, ct, at, wt)) res = GetLastError();
			CloseHandle(fh);
			}

		delete at;
		delete wt;
		delete ct;
	#else
		// Use stat to get the file times
		struct timeval	tv[2]; // 0=last access, 1=last mod
		struct stat	sb;
		if (stat(XString(path), &sb) == 0)
			{
			// There is no create time in unix so pretend
			// Do it before lastModTime so that lastModTime takes precedence
			if (createTime != NULL) createTime->toTimeVal(tv[1]);
			else
				{
				tv[1].tv_sec = sb.st_mtime;
				tv[1].tv_usec = 0;
				}

			if (lastModTime != NULL) lastModTime->toTimeVal(tv[1]);
			else
				{
				tv[1].tv_sec = sb.st_mtime;
				tv[1].tv_usec = 0;
				}
			
			if (lastAccessTime != NULL) lastAccessTime->toTimeVal(tv[0]);
			else
				{
				tv[0].tv_sec = sb.st_atime;
				tv[0].tv_usec = 0;
				}
			
			// if (utimes(XString(path), tv) != 0) res = errno;
			}
		else res = errno;
	#endif

		return res;
		}
