/****************************************************************************
**	XTokenizer.h	A string tokenizer class
****************************************************************************/


#ifndef __XTokenizer_h__
#define __XTokenizer_h__



#include "XPointerArray.h"
#include "XString.h"
#include "XStringArray.h"


/**
*** The XTokenizer class is a powerful and flexible string tokenizer class. It will
*** tokenize strings using any delimiters specified and allows the use of symmetric and
*** asymmetric quote pairs. It also supports meta chars (e.g. \) which allows prefixed
*** quotes to be in strings.
*** 
*** @brief	A powerful string tokenizer class
*** @author	Simon Sparkes
***/
class XTokenizer
		{
public:
		XTokenizer(const TCHAR *s=_T(""), const TCHAR *sepchars=_T(" \t\n"), bool skipdups=true, bool stdquotes=false, TCHAR meta=0);
		~XTokenizer();

		bool		hasMoreTokens();			// Returns true is has more tokens
		bool		nextToken(XString &str);		// Return the next token or null if no more
		XString	nextToken();				// Return the next token or null if no more
		XString	getToken(int n);			// Return the nth token
		void		reset();				// Reset the current pointer to the beginning
		void		clear();				// Clear the token vector
		void		clearQuotes();				// Clear the quote vector
		void		addQuotes(TCHAR qStart, TCHAR qEnd);	// Add a pair of delmiting quotes
		int		count();				// The number of tokens
		void		setString(const TCHAR *s);		// Set a new string in place for tokenizing

		/// Returns the next token
		XString	operator()()			{ XString tmp; nextToken(tmp); return tmp; }
		bool		operator()(XString &str)	{ return nextToken(str); }

		/// Sets the meta char
		void		setMetaChar(TCHAR m)		{ _meta = m; _doit = true; }

		/// Gets the meta char
		TCHAR		getMetaChar() const		{ return _meta; }

		/// Sets the skip flag (if true multiple delimiters are ignored)
		void		setSkipFlag(bool v)		{ _skip = v; _doit = true; }

		/// Gets the skip flag
		bool		getSkipFlag() const		{ return _skip; }

		/// Sets the delimiter chars
		void		setDelimiters(const TCHAR *str)	{ delete [] _chars; _chars = XString::strnew(str); _doit = true; }

		/// Returns the current delimiters
		const TCHAR *	getDelimiters() const		{ return _chars; }

		/**
		** Returns the nth token
		** @exception IndexOutOfBoundsException if n is less than zero or greater than the number of available tokens
		** @see getToken
		**/
		XString	operator[](int n)	{ return getToken(n); }

private:
		void		tokenize();

private:
		XStringArray	_tokenVec;	// A vector to hold the tokens
		XPointerArray	_quoteVec;	// A vector to hold the quote pairs
		TCHAR		_meta;		// The metachar which prefixes special chars
		TCHAR		*_string;	// The string to be tokenized
		TCHAR		*_chars;	// The chars regarded as separators for this instance
		bool		_skip;		// If true skip multiple separators
		bool		_doit;		// If true we need to tokenize _string
		int		_curr;		// The current position
		};

#endif
