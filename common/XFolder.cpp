/****************************************************************************
** Folder.cpp	A class to represent a handle to a directory.
****************************************************************************/


// This needs to be included first
#include "Xtypes.h"
#include "XFolder.h"
#include "XFileInfo.h"

#include "XFilename.h"
#include "XString.h"

#if !defined(WIN32) && !defined(_WIN32)
	#include <dirent.h>
	#include <unistd.h>
	#include <sys/types.h>
	#include <sys/stat.h>
#else
	#pragma warning(disable: 4996)
#endif


#undef USES_REGEXP

/*
** Default constructor.
*/
XFolder::XFolder(const XString &path) :
#if defined(WIN32) || defined(_WIN32)
	// Windows specific members
	m_hFile(0),
	m_haveData(false),
#else
	// Unix specific members
	m_dir(0),
	m_buf(0),
#endif
	m_openFlag(false)
		{
		open(path);
		}




/*
** Copy constructor
*/
XFolder::XFolder(const XFolder &other) :
#if defined(WIN32) || defined(_WIN32)
	// Windows specific members
	m_hFile(0),
	m_haveData(false),
#else
	// Unix specific members
	m_dir(0),
	m_buf(0),
#endif
	m_openFlag(false)
		{
		*this = other;
		}


/*
** Destructor
*/
XFolder::~XFolder()
		{
		close();
		}


/*
** Assignment operator
*/
XFolder &
XFolder::operator=(const XFolder &other)
		{
		close();
		open(other.m_path);

		return *this;
		}


/*
** Opens the directory.
*/
x_status_t
XFolder::open(const XString &path)
		{
		x_status_t	res=0;

		// Do a close just in case
		close();

#if defined(WIN32) || defined(_WIN32)
		// Windows specific code
		if (path.isEmpty() || !XFileInfo::isDirectory(path)) return false;

		XString search;

		if (path == "/")
			{
			// Windows doesn't like a single / for a path spec
			// so quietly change it to a backslash
			search = XFilename::concatPaths("\\", "*");
			}
		else 
			{
			search = XFilename::concatPaths(path, "*");
			}

		m_hFile = FindFirstFile(search, &m_findFileData);
		if (m_hFile != INVALID_HANDLE_VALUE)
			{
			m_openFlag = true;
			m_haveData = true;
			}
		else res = ::GetLastError();
#else
		// Unix specific code
		m_buf = new char[sizeof(struct dirent) + pathconf(m_path, _PC_NAME_MAX)];
		m_openFlag = ((m_dir = opendir(path)) != NULL);
		if (!m_openFlag) res = errno;
#endif

		if (m_openFlag)
			{
			// Record the path
			m_path = path;
			}

		return res;
		}


/*
** Read the next matching directory entry.
*/
x_status_t
XFolder::read(XFileInfo &entry)
		{
		x_status_t	res=0;

		if (m_openFlag)
			{
			bool	gotit=false;

			// We use a loop here to support filtered folders
			while (!gotit)
				{
#if defined(WIN32) || defined(_WIN32)
				// Windows specific code
				if (!m_haveData)
					{
					// We have no previously cached data so read the next entry
					m_haveData = FindNextFile(m_hFile, &m_findFileData);
					}

				if (m_haveData)
					{
					m_haveData = false;

					// If we have a filter, apply it
					if (excludeFile(m_findFileData.cFileName)) continue;

					// Otherwise store it.
					//entry.set(_path, de);
					entry.reset();
					
					entry._directory = m_path;
					entry._name = m_findFileData.cFileName;

					// it makes no sense to this in Unix (for now) so just use the normal name
					entry._dosname = m_findFileData.cAlternateFileName;
					entry._dosdir = entry._directory;

					entry._validData = XFileInfo::FileName;

					gotit = true;
					}
				else
					{
					res = 1;
					break;
					}
#else
				// Unix specific code
				struct dirent	*dp;

				dp = readdir(m_dir);

				if (dp != NULL)
					{
					// If we have a filter, apply it
					if (excludeFile(dp->d_name)) continue;

					// Otherwise store it.
					//entry.set(_path, de);
					entry.reset();
					
					entry._directory = m_path;
					entry._name = (char *)(dp->d_name);

					// it makes no sense to this in Unix (for now) so just use the normal name
					entry._dosname = entry._name;
					entry._dosdir = entry._directory;

					entry._validData = XFileInfo::FileName;

					gotit = true;
					}
				else
					{
					res = 1;
					break;
					}
#endif
				}
			}
		else res = 2;

		return res;
		}


/*
** Closes the handle to the directory
*/
x_status_t
XFolder::close()
		{
		x_status_t	res=0;

		if (m_openFlag)
			{
#if defined(WIN32) || defined(_WIN32)
			// Windows specific code
			if (m_hFile != INVALID_HANDLE_VALUE) FindClose(m_hFile);
#else
			// Unix specific code
			if (m_dir) closedir(m_dir);
			delete m_buf;
			m_dir = 0;
			m_buf = 0;
#endif

			m_openFlag = false;
			}
		else res = 2;

		return res;
		}


/*
** Check to see if the given file name would be included or excluded
** by the current file filter.
*/
bool
XFolder::includeFile(const XString & /*file*/)
		{
		return true;
		}


/*
** Check to see if the given file name would be included or excluded
** by the current file filter.
*/
bool
XFolder::excludeFile(const XString & /*file*/)
		{
		return false;
		}



x_status_t
XFolder::create(const XString &path, bool createParent, XFileAttributes *pfa)
		{
		x_status_t	r=0;

		if (createParent)
			{
			XString	parentDir = XFilename::dirname(path);

			if (!XFileInfo::isDirectory(parentDir) 
				&& parentDir != _T(".") 
				&& parentDir != _T("/")
				&& parentDir != _T("\\")
				)
				{
				r = create(parentDir, true, pfa);
				if (r != 0) return r;
				}
			}

		int		mode=0777;

		if (pfa != NULL)
			{
			// Since we know the bottom 9 bits match standard unix we can use a straight mask
			mode = pfa->attributes() & 0777;
			}

#if defined(_WIN32) || defined(WIN32)
		if (!::CreateDirectory(path, NULL))
			{
			r = GetLastError();
			}
#else
		if (::mkdir((const char *)path, (mode_t)mode) != 0)
			{
			r = errno;
			}
#endif

		return r;
		}


x_status_t
XFolder::remove(const XString &path, bool removeContents)
		{
		x_status_t	r, res=0;

		if (removeContents)
			{
			XFolder	folder(path);
			XFileInfo	info;
			XString	filename;
			
			while (folder.read(info) == 0)
				{
				if (info == "." || info == "..") continue;

				filename = XFilename::concatPaths(path, info.getName());

				if (info.isDirectory())	r = remove(filename, true);
				else r = unlink(filename);

				// Return the FIRST error, not the last
				if (r != 0 && res == 0) res = r;
				}
			}

#if defined(WIN32) || defined(_WIN32)
		if (!::RemoveDirectory(path))
			{
			res = GetLastError();
			}
#else
		if (::rmdir(XString(path)) != 0)
			{
			res = errno;
			}
#endif

		return res;
		}


XStringArray	XFolder::sm_dirstack;


/**
*** Pushes the current directory onto an internal stack
*** and attempts to change to the specified directory.
***
*** If the setCurrentDirectory fails the current directory
*** is NOT pushed onto the stack.
***
*** @return	The result of calling setCurrentDirectory on the specified directory
**/
x_status_t
XFolder::pushCurrentDirectory(const XString &dir)
		{
		x_status_t	res;
		XString	currdir;

		currdir = getCurrentDirectory();
		res = setCurrentDirectory(dir);
		if (res == 0) sm_dirstack.push(currdir);

		return res;
		}


/**
*** Pops the a previously remembered directory name from the internal
*** stack and changes to that directory.
***
*** @return	The result of calling setCurrentDirectory on the popped directory.
**/
x_status_t
XFolder::popCurrentDirectory()
		{
		return setCurrentDirectory(sm_dirstack.pop());
		}


/**
*** Set the current working directory
***
*** @return	true is successful, false otherwise.
**/
x_status_t
XFolder::setCurrentDirectory(const XString &dir)
		{
		x_status_t	result=0;

#if defined(WIN32) || defined(_WIN32)
		if (!SetCurrentDirectory(XString(dir))) result = GetLastError();
#else
		if (chdir(XString(dir)) != 0) result = errno;
#endif

		return result;
		}



/**
*** Gets the current working directory
**/
XString
XFolder::getCurrentDirectory()
		{
		XString	dir;

#if defined(WIN32) || defined(_WIN32)
		TCHAR	tmp[1024];

		if (GetCurrentDirectory(1024, tmp) != 0) dir = tmp;
#else
		char	tmp[1024];

		if (getcwd(tmp, sizeof(tmp)) != NULL) dir = tmp;
#endif

		return dir;
		}
