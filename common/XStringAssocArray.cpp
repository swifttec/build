/****************************************************************************
**	XStringAssocArray.cpp	Perl-like assoc. arrays for strings
****************************************************************************/

#include "XStringAssocArray.h"




bool
Iterator::hasNext()
		{
		return (m_curr != NULL); // && m_curr->m_next != NULL);
		}


XStringEntry *
Iterator::next()
		{
		XStringEntry *p=m_curr;

		if (m_curr != NULL)
			{
			m_curr = m_curr->m_next;
			}

		return p;
		}



XStringAssocArray::XStringAssocArray() :
	m_count(0),
	m_head(0),
	m_tail(0)
		{
		}

XStringAssocArray::~XStringAssocArray()
		{
		clear();
		}


/**
*** Array operator
**/
XString &	
XStringAssocArray::operator[](const XString &key)
		{
		XStringEntry	*sp;

		sp = get(key);
		if (!sp)
			{
			sp = new XStringEntry(key, "");

			sp->m_prev = m_tail;
			if (m_tail != NULL) m_tail->m_next = sp;
			if (m_head == NULL) m_head = sp;
			m_tail = sp;
			}

		return sp->m_value;
		}



// Copy constructor
XStringAssocArray::XStringAssocArray(XStringAssocArray &other)
		{
		// Do copy via assignment operator
		*this = other;
		}



XStringAssocArray &
XStringAssocArray::operator=(XStringAssocArray &other)
		{
		clear();

		Iterator	it = other.iterator();
		XStringEntry	*me;

		while (it.hasNext())
			{
			me = it.next();
			
			(*this)[me->key()] = me->value();
			}

		return *this;
		}


// Check to see if an element is defined
XStringEntry *
XStringAssocArray::get(const XString &key)				
		{
		XStringEntry	*sp=m_head;

		while (sp != NULL)
			{
			if (sp->key() == key)
				{
				break;
				}

			sp = sp->m_next;
			}

		return sp;
		}


bool
XStringAssocArray::defined(const XString &key)				
		{
		return (get(key) != NULL);
		}


// Remove a entry
void	
XStringAssocArray::remove(const XString &key)				
		{
		XStringEntry	*sp=m_head;

		while (sp != NULL)
			{
			if (sp->key() == key)
				{
				XStringEntry	*np, *pp;

				np = sp->m_next;
				pp = sp->m_prev;

				if (m_head == sp) m_head = np;
				if (m_tail == sp) m_tail = pp;
				if (pp != NULL) pp->m_next = np;
				if (np != NULL) np->m_prev = pp;
				delete sp;
				break;
				}

			sp = sp->m_next;
			}
		}


// Remove all entries
int	
XStringAssocArray::size()							
		{
		return m_count;
		}


// Remove all entries
void	
XStringAssocArray::clear()							
		{
		XStringEntry	*sp=m_head, *np;

		while (sp != NULL)
			{
			np = sp->m_next;
			delete sp;
			sp = np;
			}

		m_head = m_tail = 0;
		}


// Returns an iterator of MapEntry objects
Iterator
XStringAssocArray::iterator()						
		{
		Iterator	it(m_head);

		return it;
		}
