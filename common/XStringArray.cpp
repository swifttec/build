/****************************************************************************
**	XStringArray.cpp	An array of Strings
****************************************************************************/


#include "XTokenizer.h"
#include "XStringArray.h"


XStringArray::XStringArray(int initialSize, int incsize) :
    XArray(initialSize, sizeof(XString *), incsize)
		{
		}


/**
*** Copy constructor
**/
XStringArray::XStringArray(XStringArray &ua) :
    XArray(ua.size(), sizeof(XString *), ua._data.increment())
		{
		*this = ua;
		}


/**
*** Assignment operator
**/
const XStringArray &
XStringArray::operator=(const XStringArray &ua)
	{
	int	n = ua.size();

	clear();

	// Do it backwards to prevent ensureCapacity from
	// doing multiple reallocs
	while (n > 0)
	    {
	    --n;
	    (*this)[n] = ua.getElement(n);
	    }

	return *this;
	}


XStringArray::~XStringArray()
	{
	clear();
	}


/**
*** Clear needs special handling here since we
*** must delete the object in question.
**/
void
XStringArray::clear()
	{
	remove(0, _nelems);
	}


/**
*** Remove needs special handling here since we
*** must delete the object in question.
**/
void
XStringArray::remove(int pos, int count)
	{
	for (int i=pos;i<_nelems&&i<(pos+count);i++)
	    {
	    XString **us = (XString **)((char *)_data + (i * sizeof(XString *)));
	    delete *us;
	    *us = NULL;
	    }

	// Now call the remove method of the underlying Array
	XArray::remove(pos, count);
	}


/**
*** Get a reference to the nth element in the array.
***
*** If the index specified is negative an exception is thrown.
***
*** If the index specified is greater than the number of elements in the
*** array, the array is automatically extended to encompass the referenced
*** element.
**/
XString &	
XStringArray::operator[](int i)
	{
	if (i < 0) throw 1;

	ensureCapacity(i+1);
	if (i >= _nelems) _nelems = i+1;

	XString **us = (XString **)((char *)_data + (i * sizeof(XString *)));
	if (*us == NULL) *us = new XString();

	return **us;
	}


/**
*** Gets the nth element without growing the array. If the index is out of bounds
*** an exception is thrown rather than growing the array automatically. Provided
*** for use when a const operation is required.
**/
XString &	
XStringArray::getElement(int i) const
		{
		if (i < 0 || i >= _nelems) throw 1;

		XString **us = (XString **)((char *)_data + (i * sizeof(XString *)));
		if (*us == NULL) *us = new XString();

		return **us;
		}


/**
*** Pops the last element off of the array
**/
XString
XStringArray::pop()
		{
		if (_nelems < 1) throw 1;


		XString	s;
		XString	**us = (XString **)((char *)_data + ((--_nelems) * sizeof(XString *)));
		if (*us != NULL)
			{
			s = **us;
			delete *us;
			*us = NULL;
			}


		return s;
		}


#ifdef NEVER
/**
*** Turn the the string array into a string with the elements separated 
*** with the supplied separator character
***
*** @param sep		The separator char to be inserted between the elements
*** @param squote	The char to use to prefix each element
*** @param equote	The char to use to postfix each element
*** @param meta		The char to prefix any quote string with
***
*** @return The assembled string
**/
XString
XStringArray::convertToString(TCHAR sep, TCHAR squote, TCHAR equote, TCHAR meta)
		{
		XString	str, tmp;
		XString	rsquote, requote, rmeta, rsep;	// The replacement strings

		if (sep) rsep.format(_T("%c%c"), meta, sep);
		if (squote) rsquote.format(_T("%c%c"), meta, squote);
		if (equote) requote.format(_T("%c%c"), meta, equote);
		if (meta) rmeta.format(_T("%c%c"), meta, meta);

		for (int i=0;i<size();i++)
			{
			if (i) str += sep;

			tmp = GetAt(i);

			// First if we have a quote string replace all instances of itself
			// with a double version of itself.
			if (meta) tmp.replaceAll(XString(meta), rmeta);

			// if we have a separator string and don't have quote strings
			// then replace all the separator chars in the strings
			if (sep && meta && !squote && !equote) tmp.replaceAll(XString(sep), rsep);

			// Finally replace all the contained quotes
			if (squote && meta) tmp.replaceAll(XString(squote), rsquote);
			if (equote && meta && squote != equote) tmp.replaceAll(XString(equote), requote);

			// Now we can append to the result string
			if (squote) str += squote;
			str += tmp;
			if (equote) str += equote;
			}
		
		return str;
		}



/**
*** Take the string given and load the array from it by successive
*** calls to add.
***
*** @param	s	The string to be split.
*** @param	sep	The separator character (default ',')
*** @param	squote	The start-of-quoted string character (default '"')
*** @param	equote	The end-of-quoted string character (default '"')
*** @param	meta	The meta character which can be used to prefix separator and quote chars. (default '\')
*** @param	append	A flag to indicate if the strings should be appended to the array (true) or if the array 
***			should be cleared before starting the split (false). The default action is to clear the
***			array.
**/
void
XStringArray::convertFromString(const XString &s, TCHAR sep, TCHAR squote, TCHAR equote, TCHAR meta, bool append)
		{
		XString	tmp(s);
		TCHAR	    sepChars[2];

		sepChars[0] = sep;
		sepChars[1] = 0;
		XTokenizer tok(tmp, sepChars, false, false);

		tok.addQuotes(squote, equote);
		tok.setMetaChar(meta);

		if (!append) clear();
		while (tok.hasMoreTokens()) add(tok.nextToken());
		}


/**
*** Special version of convertFromString which converts a Microsoft style
*** multi string.
***
*** Microsoft multi-strings are a set of multiple strings in a single buffer
*** each of which is null terminated. The last string is terminated by a double
*** null.
***
*** @param	sp	A pointer to the start of the multi-string buffer.
*** @param	append	A flag to indicate if the strings should be appended to the array (true) or if the array 
***			should be cleared before starting the split (false). The default action is to clear the
***			array.
**/
void
XStringArray::convertFromMultiSz(const TCHAR *sp, bool append)
		{
		if (!append) clear();
		while (*sp)
			{
			add(sp);
			sp += t_strlen(sp) + 1;
			}
		}


/**
*** Special version of convertFromString which converts the array to
*** a Microsoft style multi string.
***
*** Microsoft multi-strings are a set of multiple strings in a single buffer
*** each of which is null terminated. The last string is terminated by a double
*** null.
**/
XString
XStringArray::convertToMultiSz()
		{
		XString	s, t;
		int		i;

		for (i=0;i<size();i++)
			{
			t = GetAt(i);
			// Must cast t here otherwise s.insert chops the length
			// to be the string length
			s.insert((int)s.length(), (const TCHAR *)t, (int)t.length()+1);
			}

		if (i==0) s.insert((int)s.length(), _T(""), 1);
		s.insert((int)s.length(), _T(""), 1);

		return s;
		}
#endif // NEVER
