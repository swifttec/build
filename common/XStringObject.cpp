/****************************************************************************
** XStringObject.cpp	The internal object used to represent a string
****************************************************************************/


#include "XStringObject.h"


int
x_math_max(int a, int b)
		{
		return a>b?a:b;
		}


//--------------------------------------------------------------------------------
// XStringObject implementation
//--------------------------------------------------------------------------------
// The XStringObject thread mutex

void
XStringObject::LOCK()		
	{
	}

void
XStringObject::UNLOCK()	
	{
	}


int XStringObject::__incsize = 16;

// We don't use initsize and maxwaste for now, but they are here for compatability
// with RW strings.
int XStringObject::__initsize = 16;
int XStringObject::__maxwaste = 16;


// Constructor - from another XStringObject
XStringObject::XStringObject(XStringObject *obj) :
    _buf(0),
    _length(0),
    _capacity(0),
    _refcount(1),
    _convBufLen(0),
    _convBuf(0)
		{
		LOCK();
		_length = obj->_length;
		ensureCapacity(_length);
		memcpy(_buf, obj->_buf, (obj->_length+1) * sizeof(TCHAR));
		obj->dropRef();
		UNLOCK();
		}


// Constructor - from a string
XStringObject::XStringObject(const TCHAR *s) :
    _buf(0),
    _length(0),
    _capacity(0),
    _refcount(1),
    _convBufLen(0),
    _convBuf(0)
		{
		_length = (int)t_strlen(s);
		ensureCapacity(_length);
		t_strcpy(_buf, s);
		}


// Constructor - set the length
XStringObject::XStringObject(int len, int ic) :
    _buf(0),
    _length(len),
    _capacity(0),
    _refcount(1),
    _convBufLen(0),
    _convBuf(0)
		{
		ensureCapacity(x_math_max(_length, ic));
		}


// Destructor
XStringObject::~XStringObject()
		{
		delete [] _buf;
		delete [] _convBuf;
		}


// Make sure our _data array has enough space!
void
XStringObject::ensureCapacity(int wanted)
		{
		// Make sure we have enough space for the nul TCHAR
		wanted++;

		LOCK();
		if (wanted > _capacity)
			{
			TCHAR *olddata = _buf;

			if (__incsize == 0) _capacity = wanted;
			else if (__incsize < 0)
				{
				// Double in size until capacity reached
				while (wanted > _capacity) _capacity *= 2;
				}
			else
				{
				if (wanted > _capacity) 
					{
					_capacity = ((wanted + __incsize - 1) / __incsize)*__incsize;
					}
				}

			_buf = new TCHAR[_capacity];
			if (olddata) memcpy(_buf, olddata, (_length+1) * sizeof(TCHAR));
			else _buf[0] = 0;
			delete [] olddata;
			}
		UNLOCK();
		}


int	
XStringObject::addRef()	
		{
		// If we are locked do nothing
		if (_refcount < 0) return 1;

		LOCK();
		int r = ++_refcount; 
		UNLOCK();
		return r;
		}


int	
XStringObject::dropRef()	
		{
		// If we are locked do nothing
		if (_refcount < 0) return 1;

		LOCK();
		int r =  --_refcount; 
		UNLOCK();
		return r;
		}


void
XStringObject::append(const TCHAR *s)
		{
		int	len = (int)t_strlen(s);

		LOCK();
		ensureCapacity(_length+len);
		t_strcpy(_buf+_length, s);
		_length += len;
		UNLOCK();
		}


void
XStringObject::append(const TCHAR *s, int len)
		{
		LOCK();
		ensureCapacity(_length+len);
		memcpy(_buf + _length, s, len * sizeof(TCHAR));
		_length += len;
		_buf[_length] = 0;
		UNLOCK();
		}


/**
*** Set chars to a preset value. Copes with chars (via memset) and
*** wchar (via code).
***
*** @param c	The char value to set
*** @param p	The initial position
*** @param n	The number of chars to set
**/
void
XStringObject::setChars(TCHAR c, int p, int n)
		{
		TCHAR	*bp = _buf + p;

	#if defined(UNICODE) || defined(_UNICODE)
		// Can't use memset as it works on chars
		for (int i=0;i<n;i++) *bp++ = c;
	#else
		memset(bp, c, n * sizeof(TCHAR));
	#endif
		}
