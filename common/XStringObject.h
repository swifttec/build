/****************************************************************************
** XStringObject.h	The internal object used to represent a string
****************************************************************************/

#ifndef __XStringObject_h__
#define __XStringObject_h__

#if defined(WIN32) || defined(_WIN32)
#include <windows.h>
#include <tchar.h>

#define strcasecmp	stricmp
#define strncasecmp	strnicmp

#endif

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <wchar.h>

#if defined(_AIX)
	#include <strings.h>
#endif

#if defined(UNICODE) || defined(_UNICODE)

#if !defined(WIN32) && !defined(_WIN32)
typedef wchar_t	TCHAR;
#define _T(x)	L x
#endif

#define T_EOF	WEOF

#define t_strlen(x)	wcslen(x)
#define t_strcpy(x, y)	wcscpy(x, y)
#define t_wcschr(x, y)	wcschr(x, y)
#define t_wcsrchr(x, y)	wcsrchr(x, y)

#else

#if !defined(WIN32) && !defined(_WIN32)
typedef char	TCHAR;
#define _T(x)	x
#endif

#define T_EOF	EOF

#define t_strlen(x)	strlen(x)
#define t_strcpy(x, y)	strcpy(x, y)
#define t_strchr(x, y)	strchr(x, y)
#define t_strrchr(x, y)	strrchr(x, y)
#define t_getc(x)	getc(x)
#define t_strncasecmp(x, y, z)	strncasecmp(x, y, z)
#define t_strncmp(x, y, z)	strncmp(x, y, z)
#endif

typedef unsigned int	UINT;
typedef unsigned int	uint32;

/**
*** The internal object used to represent a string.
***
*** The XStringObject is used internally by the XString class and holds
*** the actual data for the string. A XStringObject contains a reference count
*** and can be referenced by multiple XString objects simultaneously.
***
*** Referencing and dereferencing is protected by a global thread mutex.
***
**/
class XStringObject
	{
friend class XString;


protected:
	XStringObject(XStringObject *obj);
	XStringObject(const TCHAR *s);
	XStringObject(int len, int ic=0);
	~XStringObject();

	int	addRef();
	int	dropRef();
	void	ensureCapacity(int wanted);
	void	append(const TCHAR *s);
	void	append(const TCHAR *s, int n);

	// Set characters (rather like memset)
	void	setChars(TCHAR c, int p, int n);

protected: // STATIC members
	static void	LOCK();
	static void	UNLOCK();

protected: // STATIC members
	static int		__incsize;	// The size for increments
	static int		__initsize;	// The intial size for strings
	static int		__maxwaste;	// The size for increments

protected:
	TCHAR	*_buf;		// The string buffer of default char type via TCHAR
	int	_length;	// The length of the string
	int	_capacity;	// The size of the string buffer
	int	_refcount;	// The number of objects referencing this string (if -ve then object locked)
	int	_convBufLen;	// The conversion buffer length (for Unicode/MCBS conversions)
#if defined(UNICODE) || defined(_UNICODE)
	char	*_convBuf;	// The conversion buffer (wchar->char)
#else
	wchar_t	*_convBuf;	// The conversion buffer (char->wchar)
#endif
	};

#endif
