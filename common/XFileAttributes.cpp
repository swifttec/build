/****************************************************************************
**	XFileAttributes.cpp	A class representing a single directory entry
****************************************************************************/


#include "Xtypes.h"
#include "XString.h"
#include "XFileAttributes.h"

#if defined(WIN32) || defined(_WIN32)
	#include <winioctl.h>
#else
	#include <unistd.h>
	#include <sys/stat.h>
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


XFileAttributes::XFileAttributes() :
	m_type(FileTypeUnknown),
	m_attribs(0)
		{
		}


// Copy constructor
XFileAttributes::XFileAttributes(const XFileAttributes &other) :
	m_type(FileTypeUnknown),
	m_attribs(0)
		{
		*this = other;
		}


// Assignment operator
XFileAttributes &
XFileAttributes::operator=(const XFileAttributes &other)
		{
#define COPY(x)	x = other.x
		COPY(m_attribs);
#undef COPY

		return *this;
		}


XFileAttributes::XFileAttributes(FileType type, x_fileattr_t attribs, XFileSecurityDescriptor * /*psd*/) :
	m_type(type),
	m_attribs(attribs)
		{
		}


/*
** Get the mode as an ls-like string
** This is a 10 or 11 character string
**
** First char
** d     the entry is a directory;
** D     the entry is a door;
** l     the entry is a symbolic link;
** b     the entry is a block special file;
** c     the entry is a character special file;
** p     the entry is a fifo (or "named pipe") special file;
** s     the entry is an AF_UNIX address family socket;
** -     the entry is an ordinary file;
**
** Next 3 groups of 3 are Read/Write/Execute permissions for
** Owner, Group, and World respectivly
**
** Finally a plus character is appended if there is an acl
** associated with the entry
*/
XString
XFileAttributes::getUnixModeString() const
		{
		XString	result;
		char		perm[10] = "rwxrwxrwx";
		int			i, mask;

		if (isDirectory()) result = "d";
		else if (isDoor()) result = "D";
		else if (isSymbolicLink()) result = "l";
		else if (isBlockDevice()) result = "b";
		else if (isCharDevice()) result = "c";
		else if (isPipe()) result = "p";
		else if (isSocket()) result = "s";
		else result = "-";

		mask = 0400;
		for (i=0;i<9;i++)
			{
			if (!(m_attribs & mask)) perm[i] = '-';
			mask >>= 1;
			}
		
		if (isSetUserID()) perm[2] = isOwnerExecute()?'s':'S';
		if (isSetGroupID()) perm[5] = isGroupExecute()?'s':'S';
		if (isSticky()) perm[8] = isWorldExecute()?'t':'T';

		result += perm;

		return result;
		}


XString
XFileAttributes::getAttributesString() const
		{
		XString	s;

		switch (m_type)
			{
			// Directory is the same as Folder
			// case FileTypeDirectory:			s = "Directory";		break;
			case FileTypeFile:				s = "File";				break;
			case FileTypeFolder:			s = "Folder";			break;
			case FileTypeDevice:			s = "Device";			break;
			case FileTypeCharDevice:		s = "CharDevice";		break;
			case FileTypeBlockDevice:		s = "BlockDevice";		break;
			case FileTypeSocket:			s = "Socket";			break;
			case FileTypeDoor:				s = "Door";				break;
			case FileTypeFifo:				s = "Fifo";				break;
			case FileTypeXenixNamedFile:	s = "XenixNamedFile";	break;
			case FileTypeNetworkSpecial:	s = "NetworkSpecial";	break;
			case FileTypePipe:				s = "Pipe";				break;
			case FileTypeSymbolicLink:		s = "SymbolicLink";		break;
			default:						s = "Unknown";			break;
			}

		if ((m_attribs & FileAttrArchive) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Archive";
			}
		if ((m_attribs & FileAttrCompressed) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Compressed";
			}
		if ((m_attribs & FileAttrEncrypted) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Encrypted";
			}
		if ((m_attribs & FileAttrHidden) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Hidden";
			}
		if ((m_attribs & FileAttrNormal) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Normal";
			}
		if ((m_attribs & FileAttrOffline) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Offline";
			}
		if ((m_attribs & FileAttrReadOnly) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "ReadOnly";
			}
		if ((m_attribs & FileAttrReparsePoint) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "ReparsePoint";
			}
		if ((m_attribs & FileAttrSparse) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Sparse";
			}
		if ((m_attribs & FileAttrSystem) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "System";
			}
		if ((m_attribs & FileAttrTemporary) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Temporary";
			}
		
		return s;
		}


bool
XFileAttributes::hasAttributes(x_fileattr_t wanted, bool wantAny)
		{
		bool	result=false;

		if (m_attribs != 0 && m_type != FileTypeUnknown)
			{
			x_fileattr_t	tmp = (m_attribs & wanted);
			
			if (tmp)
				{
				result = wantAny?true:(tmp==wanted);
				}
			}

		return result;
		}


// Protected methods to set the file attributes
void
XFileAttributes::set(const XString &name, uint32 unixmode, uint32 winattrs, uint32 symlink)
		{
		m_type=FileTypeUnknown;
		m_attribs = 0;

#if defined(WIN32) || defined(_WIN32)
		X_UNREFERENCED_PARAMETER(symlink);
		X_UNREFERENCED_PARAMETER(unixmode);

		if ((winattrs & FILE_ATTRIBUTE_READONLY) != 0)				m_attribs |= FileAttrReadOnly;
		if ((winattrs & FILE_ATTRIBUTE_HIDDEN) != 0)				m_attribs |= FileAttrHidden;
		if ((winattrs & FILE_ATTRIBUTE_SYSTEM) != 0)				m_attribs |= FileAttrSystem;
		if ((winattrs & FILE_ATTRIBUTE_ARCHIVE) != 0)				m_attribs |= FileAttrArchive;
		if ((winattrs & FILE_ATTRIBUTE_TEMPORARY) != 0)				m_attribs |= FileAttrTemporary;
		if ((winattrs & FILE_ATTRIBUTE_SPARSE_FILE) != 0)			m_attribs |= FileAttrSparse;
		if ((winattrs & FILE_ATTRIBUTE_COMPRESSED) != 0)			m_attribs |= FileAttrCompressed;
		if ((winattrs & FILE_ATTRIBUTE_OFFLINE) != 0)				m_attribs |= FileAttrOffline;
		if ((winattrs & FILE_ATTRIBUTE_NOT_CONTENT_INDEXED) != 0)	m_attribs |= FileAttrNotIndexed;
		if ((winattrs & FILE_ATTRIBUTE_ENCRYPTED) != 0)				m_attribs |= FileAttrEncrypted;
		if ((winattrs & FILE_ATTRIBUTE_NORMAL) != 0)				m_attribs |= FileAttrNormal;

		if ((winattrs & FILE_ATTRIBUTE_REPARSE_POINT) != 0)
			{
			// Can we be more specific and distinguish between various reparse points?
			m_attribs |= FileAttrReparsePoint | FileAttrSymbolicLink;
			}

		if ((winattrs & FILE_ATTRIBUTE_DIRECTORY) != 0) m_type = FileTypeDirectory;
		else if ((winattrs & FILE_ATTRIBUTE_DEVICE) != 0) m_type = FileTypeDevice;
		else m_type = FileTypeFile;

		// Check for executable files (should really use PATHEXT)
		if (
			   name.endsWith(".exe", XString::ignoreCase)
			|| name.endsWith(".com", XString::ignoreCase)
			|| name.endsWith(".bat", XString::ignoreCase)
			|| name.endsWith(".cmd", XString::ignoreCase)
			)
			{
			m_attribs |= FileAttrOwnerExecute | FileAttrGroupExecute | FileAttrWorldExecute;
			}
#else
		X_UNREFERENCED_PARAMETER(winattrs);

		// Dos/Windows attributes must be simulated
		m_attribs = unixmode & 0x1ff;

		switch (unixmode & S_IFMT)
			{
			case S_IFDIR:	 m_type = FileTypeDirectory;	break;
			case S_IFREG:	 m_type = FileTypeFile;	break;
	#ifdef S_IFBLK
			case S_IFBLK:	 m_type = FileTypeBlockDevice;	break;
	#endif
	#ifdef S_IFCHR
			case S_IFCHR:	 m_type = FileTypeCharDevice;	break;
	#endif
	#ifdef S_IFIFO
			case S_IFIFO:	 m_type = FileTypeFifo;	break;
	#endif
	#ifdef S_IFSOCK
			case S_IFSOCK:	 m_type = FileTypeSocket;	break;
	#endif
	#ifdef S_IFDOOR
			case S_IFDOOR:	 m_type = FileTypeDoor;	break;
	#endif
	#ifdef S_IFNAM
			case S_IFNAM:	
				{
				//#define	S_IFNAM		0x5000  /* XENIX special named file */
				//#define	S_INSEM		0x1	/* XENIX semaphore subtype of IFNAM */
				//#define	S_INSHD		0x2	/* XENIX shared data subtype of IFNAM */
				m_type = FileTypeXenixNamedFile;
				break;
				}
	#endif
	#ifdef S_IFLNK
			case S_IFLNK:	 m_type = FileTypeSymbolicLink;	break;
	#endif
			}

#ifdef S_ISUID
		if (unixmode & (S_ISUID)) m_attribs |= FileAttrSetUserID;
#endif
#ifdef S_ISGID
		if (unixmode & (S_ISGID)) m_attribs |= FileAttrSetGroupID;
#endif
#ifdef S_ISVTX
		if (unixmode & (S_ISVTX)) m_attribs |= FileAttrSticky;
#endif
#ifdef S_ENFMT
		if (unixmode & (S_ENFMT)) m_attribs |= FileAttrRecordLocking;
#endif

		if (symlink != 0)
			{
			// Can we be more specific and distinguish between various reparse points?
			m_attribs |= FileAttrReparsePoint | FileAttrSymbolicLink;
			}

		// "dot" files are pseudo hidden
		if (name[0] == _T('.')) m_attribs |= FileAttrHidden;

		if (access(XString(name), W_OK) != 0) m_attribs |= FileAttrReadOnly;

		// We get this flag set only if no others are set
		if (m_type == FileTypeUnknown)
			{
			m_type = FileTypeFile;
			m_attribs |= FileAttrNormal;
			}
#endif
		}

