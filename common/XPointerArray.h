/****************************************************************************
**	PointerArray.h	An array of Pointers
****************************************************************************/


#ifndef __base_X_PointerArray_h__
#define __base_X_PointerArray_h__


#include "XArray.h"

/**
*** An array of pointers based on the Array class.
***
*** @see		XArray
*** @ingroup	core
**/
class XPointerArray : public XArray
		{
public:
		XPointerArray(int initialSize=0, int incsize=8);

		/// Virtual destructor
		virtual ~XPointerArray();

		// Copy constructor and assignment operator
		XPointerArray(const XPointerArray &ua);
		const XPointerArray &	operator=(const XPointerArray &ua);

		// Returns the nth element growing the array as required
		void * &	operator[](int i);

		/// Returns the nth element without growing the array
		void * &	getElement(int i) const;

		/// Add to the end of the array
		void		add(void *v)			{ (*this)[_nelems++] = v; }
		void		Add(void *v)			{ add(v); }

		/// Add (insert) at the specified position in the array
		void		insert(void *v, int pos)	{ insertElements(pos, 1); (*this)[pos] = v; }

		/// Returns the nth element growing the array as required
		void * &	GetAt(int i) const		{ return getElement(i); }

		/// Push a value onto the end of the array
		void		push(void *v)			{ add(v); }

		/// Pop a value from the end of the array
		void *		pop();
		};


#endif
