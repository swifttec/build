/****************************************************************************
**	XStringAssocArray.h	Perl-like assoc. arrays for strings
**
**						This is simple version which is not very efficient
**						but is fine for small data sets.
****************************************************************************/


#ifndef __XStringAssocArray_h__
#define __XStringAssocArray_h__

#include "XString.h"

// Forward declarations

class XStringEntry
		{
friend class Iterator;
friend class XStringAssocArray;

public:
		XStringEntry(const XString &k, const XString &v) : m_key(k), m_value(v), m_next(0), m_prev(0)	{ }

		const XString &	key() const		{ return m_key; }
		const XString &	value() const	{ return m_value; }

protected:
		XString		m_key;
		XString		m_value;
		XStringEntry	*m_next;
		XStringEntry	*m_prev;
		};


class Iterator
		{
public:
		Iterator(XStringEntry *p=0) : m_curr(p)	{ }

		bool			hasNext();
		XStringEntry *	next();

private:
		XStringEntry	*m_curr;
		};


/**
*** Perl-like assoc. arrays for strings
**/
class XStringAssocArray
		{
public:
		/// Default constructor
		XStringAssocArray();
		~XStringAssocArray();

		// Copy constructor and assignment operator
		XStringAssocArray(XStringAssocArray &other);
		XStringAssocArray & operator=(XStringAssocArray &other);

		// Array operator
		XString &	operator[](const XString &key);

		// Check to see if an element is defined
		bool		defined(const XString &key);

		// Remove a entry
		void		remove(const XString &key);

		// Remove all entries
		int			size();

		// Remove all entries
		void		clear();

		// Returns an iterator of MapEntry objects
		Iterator	iterator();

		XStringEntry *	get(const XString &key);

private:
		int			m_count;
		XStringEntry	*m_head;
		XStringEntry	*m_tail;
		};

#endif
