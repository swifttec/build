/****************************************************************************
**	XIntegerArray.cpp	An array of Integers
****************************************************************************/

#include "XIntegerArray.h"


XIntegerArray::XIntegerArray(int initialSize, int incsize) :
    XArray(initialSize, sizeof(int), incsize)
		{
		}


XIntegerArray::~XIntegerArray()
		{
		}


/**
*** Get a reference to the nth element in the array.
***
*** If the index specified is negative an exception is thrown.
***
*** If the index specified is greater than the number of elements in the
*** array, the array is automatically extended to encompass the referenced
*** element.
**/
int &	
XIntegerArray::operator[](int i)
	{
	if (i < 0) throw 1;
	ensureCapacity(i+1);
	if (i >= _nelems) _nelems = i+1;

	int	*ip = (int *)((char *)_data + (i * sizeof(int)));

	return *ip;
	}

	
/**
*** Get a reference to the nth element in the array.
***
*** Throws an exception in the referenced element
*** does not exist.
**/
int &	
XIntegerArray::getElement(int i) const
	{
	if (i < 0 || i >= _nelems) throw 1;

	int	*ip = (int *)((char *)_data + (i * sizeof(int)));

	return *ip;
	}

	
/**
*** Pops the last element off of the array
**/
int
XIntegerArray::pop()
	{
	if (_nelems < 1) throw 1;

	int	*ip = (int *)((char *)_data + ((--_nelems) * sizeof(int)));

	return *ip;
	}


/**
*** Copy constructor
**/
XIntegerArray::XIntegerArray(const XIntegerArray &ua) :
    XArray(ua.size(), sizeof(int), ua._data.increment())
	{
	*this = ua;
	}


/**
*** Assignment operator
**/
const XIntegerArray &
XIntegerArray::operator=(const XIntegerArray &ua)
	{
	int	n = ua.size();

	clear();

	// Do it backwards to prevent ensureCapacity from
	// doing multiple reallocs
	while (n > 0)
	    {
	    --n;
	    (*this)[n] = ua.getElement(n);
	    }

	return *this;
	}
