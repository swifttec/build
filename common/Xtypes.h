/****************************************************************************
**	Xtypes.h		Define data types
****************************************************************************/


#ifndef __Xtypes_h__
#define __Xtypes_h__

/**************************************************************** 
** Define the "standard" types
****************************************************************/
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include <time.h>
#include <wchar.h>

#if defined(__osf__)
	#include <sys/time.h>
#endif

#if defined(WIN32) || defined(_WIN32)
	#include <winsock2.h>
	#include <windows.h>
	#include <winnt.h>
	#include <tchar.h>

	#include <rpc.h>
	#include <rpcndr.h>
#else // defined(WIN32) || defined(_WIN32)
	#include <errno.h>
#endif // defined(WIN32) || defined(_WIN32)



/**************************************************************** 
** Boolean
****************************************************************/
#ifndef __cplusplus
	/* C does not have a bool type, only C++ */
	typedef unsigned char		bool;

	// Define true (Java compatibility)
	#ifndef true
		#define true	1
	#endif

	// Define false (Java compatibility)
	#ifndef false
		#define false	0
	#endif
#endif /* #ifndef __cplusplus */


// Define TRUE (fairly standard)
#ifndef TRUE
	#define TRUE	true
#endif

// Define FALSE (fairly standard)
#ifndef FALSE
	#define FALSE	false
#endif


/**************************************************************** 
** Character and string types
****************************************************************/
typedef unsigned char	uchar_t;

#if defined(UNICODE) || defined(_UNICODE)
typedef wchar_t			tchar_t;		///< Compile-time defined char - maps to wchar_t if UNICODE is defined or to char otherwise.
#else
typedef char			tchar_t;		///< Compile-time defined char - maps to wchar_t if UNICODE is defined or to char otherwise.
#endif

#if defined(WIN32) || defined(_WIN32) || defined(_AIX)
	#define X_SIZEOF_WCHAR		2		///< Define the size of a wchar_t on this platform
	typedef wchar_t		dchar_t;		///< A double-byte character string
	typedef int			qchar_t;		///< A quad-byte character string
#elif defined(sun) || defined(linux) || defined(__osf__) || defined(__MACH__)
	#define X_SIZEOF_WCHAR		4		///< Define the size of a wchar_t on this platform
	typedef short		dchar_t;		///< A double-byte character string
	typedef wchar_t		qchar_t;		///< A quad-byte character string
#else
	#error size of wchar_t unknown on this platform. Please update base/types.h
#endif


/**
*** A generic character type.
***
*** This type is not intented for declaring strings but a type to be able
*** to hold a single character that can be passed to a single, double or quad
*** byte string. This type is used extensively by the String class when
*** operations involving characters are declared.
**/
typedef int	char_t;


/**************************************************************** 
** Integer types
****************************************************************/

#if !defined(linux) && !defined(_AIX) && !defined(__MACH__)
typedef char					int8_t;		///< An 8-bit (1 byte) signed integer
typedef short					int16_t;		///< An 16-bit (2 byte) signed integer
typedef int						int32_t;		///< An 32-bit (4 byte) signed integer
#if defined(WIN32) || defined(_WIN32)
	typedef __int64				int64_t;		///< An 64-bit (8 byte) signed integer
#else
	typedef long long			int64_t;		///< An 64-bit (8 byte) signed integer
#endif
#endif // !defined(linux)

typedef unsigned char			uint8_t;		///< An 8-bit (1 byte) unsigned integer
typedef unsigned short			uint16_t;	///< An 16-bit (2 byte) unsigned integer
typedef unsigned int			uint32_t;	///< An 32-bit (4 byte) unsigned integer
#if defined(WIN32) || defined(_WIN32)
	typedef unsigned __int64	uint64_t;	///< An 64-bit (8 byte) signed integer
#else
	typedef unsigned long long	uint64_t;	///< An 64-bit (8 byte) signed integer
#endif

typedef int8_t				byte_t;		///< An 8-bit (1-byte) integer
typedef signed short			short_t;		///< A signed short integer 
typedef signed int				int_t;		///< A signed integer
typedef signed long				long_t;		///< A signed long integer

typedef unsigned short			ushort_t;	///< An unsigned short integer
typedef unsigned int			uint_t;		///< A unsigned integer
typedef unsigned long			ulong_t;		///< An unsigned long integer




/**************************************************************** 
** Integer Splits
****************************************************************/
typedef union uint64_split
		{
		struct
			{
#if defined(sparc)
			uint32_t	high32;
			uint32_t	low32;
#else
			uint32_t	low32;
			uint32_t	high32;
#endif
			} u;
		uint64_t	ui64;
		} X_SPLIT_UINT64;



/**************************************************************** 
** Time
****************************************************************/

#if (defined(WIN32) || defined(_WIN32)) && !defined(ACE_TIME_VALUE_H) && (_MSC_VER < 1900)
typedef struct timespec
	{
	time_t	tv_sec;		///< Seconds
	long	tv_nsec;	///< Nanoseconds
	} timespec_t;
#else
typedef struct timespec	timespec_t;	///< A high resolution time structure holding time in seconds and nanoseconds.
#endif

typedef struct timeval	timeval_t;	///< A high resolution time structure holding time in seconds and microseconds.
typedef struct tm		tm_t;		///< A time/date structure representing the different portions of the time and date.

#if !defined(WIN32) && !defined(_WIN32)
// Used for windows compatibilty and NOT provided by ACE

/**
*** The SYSTEMTIME structure as defined by Microsoft for windows.
***
*** We use the type uint16 instead of WORD here to avoid having to define
*** unnessecary microsoft types in the BDF.
**/
typedef struct X_WINDOWS_SYSTEMTIME
	{
	uint16_t	wYear;				///< The year (1602 - 30806)
	uint16_t	wMonth;				///< The month of the year (January=1 - December=12)
	uint16_t	wDayOfWeek;			///< The day of the week (Sunday=0 - Saturday=6)
	uint16_t	wDay;				///< The day of the month (1-31)
	uint16_t	wHour;				///< The hour (0-23)
	uint16_t	wMinute;			///< The minute (0-59)
	uint16_t	wSecond;			///< The second (0-61)
	uint16_t	wMilliseconds;		///< The milliseconds
	} SYSTEMTIME, *PSYSTEMTIME, *LPSYSTEMTIME;


/**
*** The FILETIME structure as defined by Microsoft for windows.
***
*** The FILETIME structure is a 64-bit value split in 2 halves (the low
*** and high 32-bit values) which together represent the number of
*** 100-nanosecond intervals since 1st January 1900.
***
*** We use the type uint32 instead of DWORD here to avoid having to define
*** unnessecary microsoft types in the BDF.
***
*** The order of the high and low 32-bit words is reversed depending on which platform
*** we are running on so that we can do a cast to a 64-bit integer directly as we can
*** under windows. This is checked as part of the tests built in to the BDF library builds.
**/
typedef struct X_WINDOWS_FILETIME
	{
	#if defined(sparc)
		uint32_t	dwHighDateTime;		///< The high 32 bits of the 64-bit filetime value
		uint32_t	dwLowDateTime;		///< The low 32 bits of the 64-bit filetime value
	#else
		uint32_t	dwLowDateTime;		///< The low 32 bits of the 64-bit filetime value
		uint32_t	dwHighDateTime;		///< The high 32 bits of the 64-bit filetime value
	#endif
	} FILETIME, *PFILETIME, *LPFILETIME;

#endif



/**************************************************************** 
** Files
****************************************************************/
typedef uint64_t		filesize_t;		///< The type used for file sizes
typedef int64_t		fileoffset_t;	///< The type used for offsets into files

#if defined(WIN32) || defined(_WIN32)
	typedef HANDLE		filehandle_t;	///< A native file handle type
#else
	typedef int			filehandle_t;	///< A native file handle type
#endif



/**************************************************************** 
** Miscellaneous
****************************************************************/

typedef uint32_t		thread_t;		///< Type used for thread IDs

#if defined(WIN32) || defined(_WIN32)
	typedef DWORD		pid_t;			///< Type used for process IDs
#else
	typedef pid_t		pid_t;			///< Type used for process IDs
#endif

typedef off_t			off_t;			///< Type used for sizes of files and offsets into files
typedef void *			handle_t;		///< A generic handle type
typedef uint32_t		x_status_t;		///< The type used for status codes returned from the BDF.
typedef uint32_t		resourceid_t;	///< The type used for resource IDs

/* define IN and OUT to be nothing (used in some code for marking parameters) */
#if !defined(WIN32) && !defined(_WIN32)
	#define IN
	#define OUT
#endif

#if defined(__LP64__) || defined(_LP64) || defined(__ia64) || defined(_M_IA64) || defined(_M_ALPHA)
	#define X_SIZEOF_POINTER	8
#else
	#define X_SIZEOF_POINTER	4
#endif


/**************************************************************** 
** Windows compatibilty types
****************************************************************/
#if !defined(WIN32) && !defined(_WIN32)
typedef unsigned int	UINT;
typedef tchar_t		TCHAR;
typedef uint8_t		BYTE;
typedef uint16_t		WORD;
typedef uint32_t		DWORD;
typedef uint64_t		QWORD;
#endif // !defined(WIN32) && !defined(_WIN32)

#define X_UNREFERENCED_PARAMETER(x)	(void)(x)
#endif // __types_h__
