/**
*** @file		XStdioFile.cpp
*** @brief		Basic file handling class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the XStdioFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



// This needs to be included first
#include "Xtypes.h"
#include "XStdioFile.h"



/*
** Default constructor
*/
XStdioFile::XStdioFile() :
	m_hFile(0)
		{
		}


/*
** Destructor
*/
XStdioFile::~XStdioFile()
		{
		close();
		}


int
XStdioFile::open(const XString &filename, const XString &mode)
		{
		int	res=0;

		if (isOpen())
			{
			res = -1;
			}
		else
			{
#if defined(WIN32) || defined(_WIN32)
	#pragma warning(disable: 4996)
#endif
			m_hFile = fopen(filename, mode);

#if defined(WIN32) || defined(_WIN32)
	#pragma warning(default: 4996)
#endif
			if (m_hFile == NULL)
				{
				res = errno;
				}
			}

		return res;
		}


/*
** Close the file
**
** @return true if successful, false otherwise
*/
int
XStdioFile::close()
		{
		int	res=0;

		if (isOpen())
			{
			fclose(*this);
			m_hFile = NULL;
			}

		return res;
		}



bool
XStdioFile::isOpen() const
		{
		return (m_hFile != NULL);
		}


void
XStdioFile::flush()
		{
		if (isOpen())
			{
			fflush(*this);
			}
		}




