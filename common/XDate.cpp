/****************************************************************************
**	Date.cpp	A representation of date based on Julian day numbers
****************************************************************************/


#include <stdio.h>
#include <time.h>
#include "Xtypes.h"
#include "XDate.h"
#include "XTime.h"


/*************************************************************************************************************************
Mathematicians and programmers have naturally interested themselves in mathematical and computational
algorithms to convert between Julian day numbers and Gregorian dates. The following conversion algorithm
is due to Henry F. Fliegel and Thomas C. Van Flandern: 

Days are integer values in the range 1-31, months are integers in the range 1-12, and years are positive or
negative integers. Division is to be understood as in integer arithmetic, with remainders discarded.

This algorithm is valid only in the Gregorian Calendar and the proleptic Gregorian Calendar (for non-negative
Julian day numbers). It does not correctly convert dates in the Julian Calendar.
*************************************************************************************************************************/


// The Julian day (jd) is computed from Gregorian day, month and year (d, m, y) as follows:
void
XDate::dayMonthYearToJulian(int d, int m, int y, int &jd)
		{
		jd = ( 1461 * ( y + 4800 + ( m - 14 ) / 12 ) ) / 4 +
			( 367 * ( m - 2 - 12 * ( ( m - 14 ) / 12 ) ) ) / 12 -
			( 3 * ( ( y + 4900 + ( m - 14 ) / 12 ) / 100 ) ) / 4 +
			d - 32075;
		}

// Converting from the Julian day number to the Gregorian date is performed thus:
void
XDate::julianToDayMonthYear(int jd, int &d, int &m, int &y)
		{
		int	l, n, j, i;

		l = jd + 68569;
		n = ( 4 * l ) / 146097;
		l = l - ( 146097 * n + 3 ) / 4;
		i = ( 4000 * ( l + 1 ) ) / 1461001;
		l = l - ( 1461 * i ) / 4 + 31;
		j = ( 80 * l ) / 2447;
		d = l - ( 2447 * j ) / 80;
		l = j / 11;
		m = j + 2 - ( 12 * l );
		y = 100 * ( n - 49 ) + i + l;
		}

extern struct tm *localtime_r(const time_t *, struct tm *);

XDate::~XDate()
		{
		}


// Construct with today's date
XDate::XDate() :
	m_valid(false),
	m_julian(0),
	m_julianYearStart(0),
	m_day(0),
	m_month(0),
	m_year(0)
		{
		XTime	ut;

		*this = ut;
		}


XDate::XDate(int day, int month, int year, bool throwOnInvalidDMY) :
	m_valid(false),
	m_julian(0),
	m_julianYearStart(0),
	m_day(0),
	m_month(0),
	m_year(0)
		{
		set(day, month, year, throwOnInvalidDMY);
		}


XDate::XDate(time_t t) :
	m_valid(false),
	m_julian(0),
	m_julianYearStart(0),
	m_day(0),
	m_month(0),
	m_year(0)
		{
		XTime	ut(t);

		m_julian = ut.toJulian();
		calculateFields();
		}


XDate::XDate(const XTime &ut) :
	m_valid(false),
	m_julian(0),
	m_julianYearStart(0),
	m_day(0),
	m_month(0),
	m_year(0)
		{
		m_julian = ut.toJulian();
		calculateFields();
		}


XDate::XDate(const XDate &other)
		{
		*this = other;
		}


// Assignment from a Time
XDate &
XDate::operator=(const XTime &ut)
		{
		m_julian = ut.toJulian();
		calculateFields();

		return *this;
		}


// Assignment from another XDate
XDate &
XDate::operator=(const XDate &other)
		{
#define COPY(x) x = other.x
		COPY(m_valid);
		COPY(m_julian);
		COPY(m_julianYearStart);
		COPY(m_day);
		COPY(m_month);
		COPY(m_year);
#undef COPY

		return *this;
		}


void
XDate::update()
		{
		XTime	t;

		t.update();
		set(t.day(), t.month(), t.year(), false);
		}


bool
XDate::set(int day, int month, int year, bool throwOnInvalidDMY)
		{
		int		d, m, y, j;

		// First convert to a julian
		XDate::dayMonthYearToJulian(day, month, year, j);

		// And then back again
		XDate::julianToDayMonthYear(j, d, m, y);

		if (d != day || m != month || y != year)
			{
			// We got back a different result so the date was invalid
			// (or I supposed our calculations went wrong, but of course that's impossible right,
			// after all this is bug-free software!)

			// The caller wanted an exception so throw one.
			if (throwOnInvalidDMY) throw 1;

			// The caller wanted a bool
			return false;
			}

		m_julian = j;
		m_day = (uint8_t)d;
		m_month = (uint8_t)m;
		m_year = (int16_t)y;

		// Calculate the julian day number of the year start
		XDate::dayMonthYearToJulian(1, 1, y, m_julianYearStart);

		// Finally
		m_valid = true;

		return true;
		}


// PRIVATE
// calculate internal fields from _julian
void
XDate::calculateFields()
		{
		int		d, m, y;

		XDate::julianToDayMonthYear(m_julian, d, m, y);
		m_day = (uint8_t)d;
		m_month = (uint8_t)m;
		m_year = (int16_t)y;

		// Calculate the julian day number of the year start
		XDate::dayMonthYearToJulian(1, 1, y, m_julianYearStart);

		// Finally
		m_valid = true;
		}


// Calculate the week number
// if firstSunday is true the first Sunday in the year is the start of week 1
// otherwise the first Monday in the year is the start of week 1
int
XDate::weekOfYear(DayNumber firstDayOfWeek1) const
		{
		// Today's day number (zero-based)
		int	yday = dayOfYear()-1;

		// The day number for the 1st of the year
		int	day1 = (m_julianYearStart+1)%7;

		int	week = yday + day1 - firstDayOfWeek1;
		if (day1 <= firstDayOfWeek1) week += 7;
		week /= 7;

		return week;
		}


// Check to see if the given year is a leap year
bool
XDate::isLeapYear(int year)
		{
		return ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0));
		}
