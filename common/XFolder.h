/****************************************************************************
** Folder.h	A class to represent a handle to a directory.
****************************************************************************/



#ifndef __Folder_h__
#define __Folder_h__

#include "Xtypes.h"
#include "XFileInfo.h"
#include "XStringArray.h"


/**
*** A class to represent a handle to a folder/directory.
***
*** This class represents a handle to a folder/directory which can
*** operate on a single folder/directory entry at a time. This makes it
*** efficient on folder/directories with very large numbers of files.
***
*** No filtering is done on folder entries so all entries in the folder
*** are returned including hidden/system entries. To pre-filter the 
*** entries returned use a XFilteredFolder.
***
*** @ingroup io
**/
class XFolder
		{
public:
		/**
		*** Default constructor.
		***
		*** If the path is specified the directory will be opened by
		*** calling the open method.
		***
		*** @param	path		The directory to be opened
		*** @see	open
		**/
		XFolder(const XString &path=_T(""));

		/**
		*** Copy constructor - opens a new handle to the directory
		*** represented by other.
		**/
		XFolder(const XFolder &other);

		/**
		*** Assignment operator - opens a new handle to the directory
		*** represented by other.
		***
		*** If the XFolder object is currently open it will be closed.
		**/
		XFolder &	operator=(const XFolder &other);

		/**
		*** Destructor - will close the directory handle if open.
		**/
		virtual ~XFolder();

		/**
		*** Opens the specified directory.
		***
		*** If the folder is currently opened it will be automatically closed.
		***
		*** @param	path	The directory to be opened
		*** @return			If successful X_STATUS_SUCCESS is returned, otherwise
		***					an error code is returned.
		**/
		virtual x_status_t		open(const XString &path);

		/**
		*** Read the next matching folder entry.
		***
		*** @param	entry	The XFileInfo object to be filled in if an entry
		***					is found. The entry is not changed if a matching
		***					entry is not found.
		***
		*** @return			If successful X_STATUS_SUCCESS is returned, otherwise
		***					an error code is returned.
		**/
		virtual x_status_t		read(XFileInfo &entry);

		/**
		*** Rewind the handle to the directory.
		***
		*** Causes the directory to start reading entries from the start.
		***
		*** @return			If successful X_STATUS_SUCCESS is returned, otherwise
		***					an error code is returned.
		virtual x_status_t		rewind();
		**/

		/**
		*** Closes the handle to the directory
		***
		*** @return			If successful X_STATUS_SUCCESS is returned, otherwise
		***					an error code is returned.
		**/
		virtual x_status_t		close();

		/// Determine if this directory is currently open
		bool				isOpen() const			{ return m_openFlag; }

		/// Return the path for this directory handle
		const XString &	path() const			{ return m_path; }


public: // STATIC methods
		/**
		*** Creates the specified folder.
		***
		*** @param path			The pathname of the directory to be created.
		*** @param createParent	This flag indicates if parent directories should be created to
		***						accomodate the folder creation.
		*** @param	pfa			A pointer to a file attribute object which is used when a folder
		***						is created. If not supplied default file attributes are used.
		***						The file attributes define how a folder is created and includes
		***						security information.
		***
		*** @return			If successful X_STATUS_SUCCESS is returned, otherwise
		***					an error code is returned.
		**/
		static x_status_t	create(const XString &path, bool createParent=false, XFileAttributes *pfa=0);

		/**
		*** Removes the specified folder.
		***
		*** @param path				The pathname of the folder to be removed.
		*** @param removeContents	If true the contents of the directory are removed
		***							to enable the directory to be deleted. If false
		***							this method will fail if folder is not empty.
		***
		*** @return			If successful X_STATUS_SUCCESS is returned, otherwise
		***					an error code is returned.
		**/
		static x_status_t	remove(const XString &path, bool removeContents=false);

		/**
		*** Pushes the current directory onto an internal stack
		*** and attempts to change to the specified directory.
		***
		*** If the setCurrentFolder fails the current directory
		*** is NOT pushed onto the stack.
		***
		*** @return	The result of calling getCurrentFolder
		**/
		static x_status_t	pushCurrentDirectory(const XString &dir);


		/**
		*** Pops the a previously remembered directory name from the internal
		*** stack and changes to that directory.
		***
		*** If the setCurrentFolder fails the current directory
		*** is NOT pushed onto the stack.
		***
		*** @return	The result of calling getCurrentFolder
		**/
		static x_status_t	popCurrentDirectory();

		/**
		*** Set the current working directory
		***
		*** @return	true is successful, false otherwise.
		**/
		static x_status_t	setCurrentDirectory(const XString &dir);

		/**
		*** Gets the current working directory
		**/
		static XString		getCurrentDirectory();

protected: // Methods
		// Test for file inclusion against current filter
		virtual bool		includeFile(const XString &file);
		virtual bool		excludeFile(const XString &file);


protected: // Static Members
		static XStringArray	sm_dirstack;	///< The directory stack for push and pop operations

protected: // Members
		XString			m_path;		///< The pathname of the directory
#if defined(WIN32) || defined(_WIN32)
		// Windows specific members
		HANDLE			m_hFile;
		WIN32_FIND_DATA	m_findFileData;
		BOOL			m_haveData;
#else
		// Unix specific members
		DIR				*m_dir;
		char			*m_buf;
#endif
		bool			m_openFlag;
		};

#endif
