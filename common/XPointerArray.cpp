/****************************************************************************
**	XPointerArray.cpp	An array of Pointers
****************************************************************************/


#include "XPointerArray.h"

XPointerArray::XPointerArray(int initialSize, int incsize) :
    XArray(initialSize, sizeof(void *), incsize)
		{
		}


XPointerArray::~XPointerArray()
		{
		}


/**
*** Get a reference to the nth element in the array.
***
*** If the index specified is negative an exception is thrown.
***
*** If the index specified is greater than the number of elements in the
*** array, the array is automatically extended to encompass the referenced
*** element.
**/
void * &	
XPointerArray::operator[](int i)
	{
	if (i < 0) throw 1;
	ensureCapacity(i+1);
	if (i >= _nelems) _nelems = i+1;

	void **ip = (void **)((char *)_data + (i * sizeof(void *)));

	return *ip;
	}

	
/**
*** Get a reference to the nth element in the array.
***
*** Throws an exception in the referenced element
*** does not exist.
**/
void * &	
XPointerArray::getElement(int i) const
	{
	if (i < 0 || i>= _nelems) throw 1;

	void **ip = (void **)((char *)_data + (i * sizeof(void *)));

	return *ip;
	}
	

/**
*** Pops the last element off of the array
**/
void *
XPointerArray::pop()
	{
	if (_nelems < 1) throw 1;

	void **ip = (void **)((char *)_data + ((--_nelems) * sizeof(void *)));

	return *ip;
	}

	
/**
*** Copy constructor
**/
XPointerArray::XPointerArray(const XPointerArray &ua) :
    XArray(ua.size(), sizeof(void *), ua._data.increment())
	{
	*this = ua;
	}


/**
*** Assignment operator
**/
const XPointerArray &
XPointerArray::operator=(const XPointerArray &ua)
	{
	int	n = ua.size();

	clear();

	// Do it backwards to prevent ensureCapacity from
	// doing multiple reallocs
	while (n > 0)
	    {
	    --n;
	    (*this)[n] = ua.getElement(n);
	    }

	return *this;
	}
