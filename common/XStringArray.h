/****************************************************************************
**	XStringArray.h	An array of XStrings
****************************************************************************/


#ifndef __base_X_StringArray_h__
#define __base_X_StringArray_h__

#include "XString.h"
#include "XArray.h"


/**
*** An array of Strings based on the Array class
***
*** @see XArray
*** @ingroup core
**/
class XStringArray : public XArray
		{
public:
		XStringArray(int initialSize=0, int incsize=8);
		virtual ~XStringArray();

		// Copy constructor and assignment operator
		XStringArray(XStringArray &ua);
		const XStringArray &	operator=(const XStringArray &ua);

		/// Returns the nth element growing the array as required
		XString &	GetAt(int i) const			{ return getElement(i); }

		/// Returns the nth element growing the array as required
		XString &	operator[](int i);

		/// Returns the nth element without growing the array
		XString &	getElement(int i) const;

		/// Clears the array
		virtual void	clear();

		// Remove the one or more elements
		virtual void	remove(int pos, int count=1);

		/// Add to the end of the array
		void		add(const XString &v)			{ (*this)[_nelems++] = v; }
		void		Add(const XString &v)			{ add(v); }

		/// Add (insert) at the specified position in the array
		void		insert(const XString &v, int pos)	{ insertElements(pos, 1); (*this)[pos] = v; }

		/// Push a value onto the end of the array
		void		push(const XString &v)			{ add(v); }

		/// Pop a value from the end of the array
		XString	pop();


		// Conversion to/from string
		//virtual XString	toString()			{ return convertToString(); }

		//XString	convertToString(TCHAR sep=',', TCHAR squote='"', TCHAR equote='"', TCHAR meta='\\');
		//void		convertFromString(const XString &s, TCHAR sep=',', TCHAR squote='"', TCHAR equote='"', TCHAR meta='\\', bool append=false);
		//void		convertFromMultiSz(const TCHAR *sp, bool append=false);
		//XString	convertToMultiSz();
		};

#endif
