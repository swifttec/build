/****************************************************************************
**	IntegerArray.h	An array of Integers
****************************************************************************/


#ifndef __base_XIntegerArray_h__
#define __base_XIntegerArray_h__

#include "XArray.h"


/**
*** An array of ints based on the Array class
***
*** @see		Array
*** @ingroup	core
**/
class XIntegerArray : public XArray
		{
public:
		XIntegerArray(int initialSize=0, int incsize=8);
		virtual ~XIntegerArray();

		// Copy constructor and assignment operator
		XIntegerArray(const XIntegerArray &ua);
		const XIntegerArray &	operator=(const XIntegerArray &ua);

		// Returns the nth element growing the array as required
		int &	operator[](int i);

		/// Returns the nth element without growing the array
		int &	getElement(int i) const;

		/// Add to the end of the array
		void		add(int v)		{ (*this)[_nelems++] = v; }

		/// Add (insert) at the specified position in the array
		void		insert(int v, int pos)	{ insertElements(pos, 1); (*this)[pos] = v; }

		/// Push a value onto the end of the array
		void		push(int v)		{ add(v); }

		/// Pop a value from the end of the array
		int		pop();
		};


#endif
