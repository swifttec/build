@echo off

if defined SW_BUILD_ENV goto env_already_set

set SW_BUILD_ARCH=%PROCESSOR_ARCHITECTURE%
set SW_BUILD_PLATFORM=%PROCESSOR_ARCHITECTURE%

if "%1" == "x86" goto :set_x86
if "%1" == "X86" goto :set_x86
if "%1" == "x64" goto :set_x64
if "%1" == "X64" goto :set_x64
if "%1" == "ia64" goto :set_ia64
if "%1" == "ia64" goto :set_ia64
if %PROCESSOR_ARCHITECTURE% == x86 goto :set_x86
if %PROCESSOR_ARCHITECTURE% == X86 goto :set_x86
if %PROCESSOR_ARCHITECTURE% == amd64 goto :set_x64
if %PROCESSOR_ARCHITECTURE% == AMD64 goto :set_x64

echo Can't determine architecture
exit /b 1

:set_x86
echo Setting the x86 build environment
set SW_BUILD_ARCH=x86
set SW_BUILD_PLATFORM=x86
set VCVARSARG=x86
goto :find_vc

:set_x64
echo Setting the x64 build environment
set SW_BUILD_ARCH=x64
set SW_BUILD_PLATFORM=x64
set VCVARSARG=%PROCESSOR_ARCHITECTURE%
if %PROCESSOR_ARCHITECTURE% == x86 set VCVARSARG=x86_amd64
if %PROCESSOR_ARCHITECTURE% == X86 set VCVARSARG=x86_amd64
goto :find_vc

:set_ia64
echo Setting the ia64 build environment
set SW_BUILD_ARCH=ia64
set SW_BUILD_PLATFORM=ia64
set VCVARSARG=%PROCESSOR_ARCHITECTURE%
if %PROCESSOR_ARCHITECTURE% == x86 set VCVARSARG=x86_ia64
if %PROCESSOR_ARCHITECTURE% == X86 set VCVARSARG=x86_ia64
goto :find_vc


:find_vc
rem set SW
rem set VC
rem goto :eof

rem Use VC14 if available
if exist "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" goto setenv_VC14
if exist "C:\Program Files\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" goto setenv_VC14

rem Use VC12 if available
if exist "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" goto setenv_VC12
if exist "C:\Program Files\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" goto setenv_VC12

rem Use VC10 if available
if exist "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\vcvarsall.bat" goto setenv_VC10
if exist "C:\Program Files\Microsoft Visual Studio 10.0\VC\vcvarsall.bat" goto setenv_VC10

rem Use VC9 if available
if defined VS90COMNTOOLS goto setenv_VC9

rem Use VC7 if available
if defined VS71COMNTOOLS goto setenv_VC7

rem Check for VC6
if not defined MSVCdir call :find_VC6
if defined MSVCdir goto setenv_VC6

echo Can't work out which C/C++ build environment to setup.
exit /b 1


:nomsvc
echo Can't find Visual Studio - try running vcvars32
exit /b 1

rem ###########################################################################
rem ## VC6
rem ##
rem ## We must make sure we have the Platform SDK and VC98, then call the VC98
rem ## setup script. Then insert the Platform SDK before VC98 in the path, include
rem ## and lib environment variables
rem ###########################################################################
:setenv_VC6
if not defined MSVCdir call :find_VC6
if not defined MSVCdir goto no_VC6
if not defined MStools goto no_sdk

echo Using Visual Studio 6
set SW_BUILD_ENV=VC6

rem First use vcvars32.batto setup basic paths
call %MSVCdir%\bin\vcvars32

rem We must put the platform SDK before the VC98 stuff
PATH=%mstools%\bin;%PATH%
set INCLUDE=%mstools%\include;%INCLUDE%
set LIB=%mstools%\lib;%LIB%
goto :do_common


:find_VC6
if not exist "%SystemDrive%\Program Files\Microsoft Visual Studio\VC98\bin\vcvars32.bat" goto :eof
call "%SystemDrive%\Program Files\Microsoft Visual Studio\VC98\bin\vcvars32.bat"
goto :eof


:no_VC6
echo MSVCdir is not set
exit /b 1


:no_sdk
echo The VC6 build envirnoment requires the platform SDK to be installed

echo Please make sure that the environment variable MSVCdir
exit /b 1





rem ###########################################################################
rem ## VC14
rem ##
rem ## VC14 seems to be simply a matter of calling vsvarsall and then it all works
rem ###########################################################################
:setenv_VC14

set vcvarsbat=C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat
if exist "%vcvarsbat%" goto :call_vc14_bat

set vcvarsbat=C:\Program Files\Microsoft Visual Studio 14.0\VC\vcvarsall.bat
if exist "%vcvarsbat%" goto :call_vc14_bat

echo Can't set VC14 environment
exit /b 1


:call_vc14_bat
echo Using Visual Studio 14

call "%vcvarsbat%" %VCVARSARG%
set SW_BUILD_ENV=VC14
goto :do_common


:env_already_set
echo The build environment has already been set for %SW_BUILD_ENV%
goto :eof


rem ###########################################################################
rem ## VC12
rem ##
rem ## VC12 seems to be simply a matter of calling vsvarsall and then it all works
rem ###########################################################################
:setenv_VC12

set vcvarsbat=C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat
if exist "%vcvarsbat%" goto :call_vc12_bat

set vcvarsbat=C:\Program Files\Microsoft Visual Studio 12.0\VC\vcvarsall.bat
if exist "%vcvarsbat%" goto :call_vc12_bat

echo Can't set VC12 environment
exit /b 1


:call_vc12_bat
echo Using Visual Studio 12

call "%vcvarsbat%" %VCVARSARG%
set SW_BUILD_ENV=VC12
goto :do_common


:env_already_set
echo The build environment has already been set for %SW_BUILD_ENV%
goto :eof


rem ###########################################################################
rem ## VC10
rem ##
rem ## VC10 seems to be simply a matter of calling vsvarsall and then it all works
rem ###########################################################################
:setenv_VC10

set vcvarsbat=C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\vcvarsall.bat
if exist "%vcvarsbat%" goto :call_vc10_bat

set vcvarsbat=C:\Program Files\Microsoft Visual Studio 10.0\VC\vcvarsall.bat
if exist "%vcvarsbat%" goto :call_vc10_bat

echo Can't set VC10 environment
exit /b 1


:call_vc10_bat
echo Using Visual Studio 10

call "%vcvarsbat%" %VCVARSARG%
set SW_BUILD_ENV=VC10
goto :do_common


:env_already_set
echo The build environment has already been set for %SW_BUILD_ENV%
goto :eof


rem ###########################################################################
rem ## VC9
rem ##
rem ## VC9 seems to be simply a matter of calling vsvars32 and then it all works
rem ###########################################################################
:setenv_VC9
if not defined VS90COMNTOOLS goto no_VC9

echo Using Visual Studio 9
call "%vs90comntools%\vsvars32.bat"
set SW_BUILD_ENV=VC9
goto :do_common


:no_VC9
echo VS90COMNTOOLS is not set
exit /b 1

:env_already_set
echo The build environment has already been set for %SW_BUILD_ENV%
goto :eof



rem ###########################################################################
rem ## VC7
rem ##
rem ## VC7 seems to be simply a matter of calling vsvars32 and then it all works
rem ###########################################################################
:setenv_VC7
if not defined VS71COMNTOOLS goto no_VC7

echo Using Visual Studio 7
call "%vs71comntools%\vsvars32.bat"
set SW_BUILD_ENV=VC7
goto :do_common


:no_VC7
echo VS71COMNTOOLS is not set
exit /b 1





rem ###########################################################################
rem ## Common build settings
rem ###########################################################################
:do_common
set SW_BUILD_COMPILER=%SW_BUILD_ENV%

if exist "%ProgramFiles%\Inno Setup 5" PATH=%PATH%;%ProgramFiles%\Inno Setup 5
