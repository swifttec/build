@echo off

setlocal

if not defined SW_BUILD_ENV call setbuildenv

rem Pre-build the build tools
call :do_make build

for /d %%d in (*) do call :do_makein %%d
goto :eof


:do_makein
set folder=%1

if "%folder%" == "build" goto :eof

if exist %folder%\mk.bat call :do_mk %folder%
if exist %folder%\win32.mak call :do_make %folder%
if exist %folder%\%folder%.sln call :do_sln %folder%
goto :eof

:do_mk
pushd %1
call mk
popd
goto :eof

:do_make
pushd %1
nmake -nologo -f win32.mak
popd
goto :eof

:do_sln
set SOLUTION=%1.sln

pushd %1
devenv %SOLUTION% /build "Release|Win32"
devenv %SOLUTION% /build "Debug|Win32"
devenv %SOLUTION% /build "Release|x64"
devenv %SOLUTION% /build "Debug|x64"
popd
goto :eof
