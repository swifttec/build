@echo off

setlocal

set gitrootdir=\\mediaserver\git
set gitrooturl=//mediaserver/git

:loop
if "%1" == "" goto :eof

call :do_folder %1
shift
goto :loop


:do_folder
set folder=%1
if not exist %folder% goto :not_folder
if exist %folder%/.git goto :already_git
if not exist %gitrootdir%\%folder% call :make_remote %folder%
if not exist %gitrootdir%\%folder% goto :not_remote

echo Processing %folder%
ren %folder% %folder%.tmp
git clone %gitrooturl%/%folder%
mv %folder%\.git %folder%.tmp\.git
rd %folder%
ren %folder%.tmp %folder%
if not exist %folder%\.gitignore copy build\git\dotgitignore %folder%\.gitignore


goto :eof




:not_folder
echo %folder% is not a folder
goto :eof

:already_git
echo %folder% is already under git control
goto :eof

:not_remote
echo %gitrootdir%\%folder% does not exist
goto :eof

:make_remote
echo Creating central repository %gitrootdir%\%1
git init --bare --shared=all %gitrootdir%\%1
goto :eof
