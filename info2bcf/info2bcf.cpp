#include <stdio.h>

#include <XString.h>
#include <XStdioFile.h>
#include <XBinaryConfigFile.h>

void
usage()
		{
		printf("Usage: info2bcf infofile bcffile\n");
		exit(1);
		}


int
main(int argc, char **argv)
		{
		SWString	inifile, bcffile;

		if (argc < 3) usage();

		inifile = argv[1];
		bcffile = argv[2];


		SWStdioFile			ifile;

		if (ifile.open(inifile, "r") == SW_STATUS_SUCCESS)
			{
			printf("Converting %s to %s\n", inifile.c_str(), bcffile.c_str());

			SWBinaryConfigFile	bfile(bcffile);
			SWString			line;

			while (line.readLine(ifile))
				{
				int		p;

				p = line.first('=');
				if (p > 0)
					{
					SWString	n, v;

					n = line.left(p);
					v = line.mid(p+1);

					printf("    %s = %s\n", n.c_str(), v.c_str());

					if (v.startsWith("0x"))
						{
						// Hex value
						bfile.findOrCreateValue(n)->setValue(v.mid(2).toUInt32(16));
						}
					else if (v.count("0123456789") == v.length())
						{
						// Decimal value
						bfile.findOrCreateValue(n)->setValue(v.toInt32(10));
						}
					else
						{
						// String value
						bfile.findOrCreateValue(n)->setValue(v);
						}
					}
				}

			ifile.close();
			}
		else
			{
			printf("Failed to open %s\n", inifile.c_str());
			}

		return 0;
		}
