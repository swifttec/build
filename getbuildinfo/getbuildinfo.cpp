/*
**	getbuildinfo.cpp	A program to maintain a build.info file.
*/

#include <stdio.h>
#include <stdlib.h>

#if !defined(WIN32) && !defined(_WIN32)
#include <unistd.h>
#endif

#include <XTokenizer.h>
#include <XString.h>
#include <XStringAssocArray.h>
#include <XDate.h>

XStringAssocArray	buildparams, paramtype;
XString				outPrefix;
XString				rmPrefix;
XString				csName;	// The class name (VB & C#)
XString				nsName;	// The namespace name (VB & C#)
bool				update=false;
bool				noautoyear=false;
bool				noversion=false;
int					verbose=0;
XString				todayString;
XString				thisyearString;


// Predefined constants for versions

XString MAJOR;
XString MINOR;
XString BUILD;
XString REVISION;
XString VERSION;
XString VERSION_FORMAT;


void
setPrefix(const XString &prefix)
		{
		char	tmp[512];

		sprintf(tmp, "%s_MAJOR_VERSION", prefix.data());
		MAJOR = tmp;
		sprintf(tmp, "%s_MINOR_VERSION", prefix.data());
		MINOR = tmp;
		sprintf(tmp, "%s_BUILD", prefix.data());
		BUILD = tmp;
		sprintf(tmp, "%s_REVISION", prefix.data());
		REVISION = tmp;
		sprintf(tmp, "%s_VERSION", prefix.data());
		VERSION = tmp;
		sprintf(tmp, "%s_VERSION_FORMAT", prefix.data());
		VERSION_FORMAT = tmp;

		if (!noversion)
			{
			if (!buildparams.defined(MAJOR))
				{
				buildparams[MAJOR]= "0";
				update = true;
				}
			if (!buildparams.defined(MINOR)) 
				{
				buildparams[MINOR]= "0";
				update = true;
				}
			if (!buildparams.defined(BUILD)) 
				{
				buildparams[BUILD]= "0";
				update = true;
				}
			if (!buildparams.defined(REVISION)) 
				{
				buildparams[REVISION]= "0";
				update = true;
				}
			if (!buildparams.defined(VERSION_FORMAT)) 
				{
				buildparams[VERSION_FORMAT]= "%m.%n.%r.%b";
				update = true;
				}

			paramtype[MAJOR]= "n";
			paramtype[MINOR]= "n";
			paramtype[BUILD]= "n";
			paramtype[REVISION]= "n";
			}
		}


// File formats
enum FileFormat
	{
	FILE_FORMAT_UNKNOWN,
	FILE_FORMAT_MAKE,
	FILE_FORMAT_BATCH,
	FILE_FORMAT_CSH_SETENV,
	FILE_FORMAT_CSH,
	FILE_FORMAT_SH,
	FILE_FORMAT_SH_NO_EXPORT,
	FILE_FORMAT_C,
	FILE_FORMAT_CPP,
	FILE_FORMAT_CS,
	FILE_FORMAT_VB
	};


void
readInfoFile(const XString &filename)
		{
		FILE		*f;
		XString	line, key, value;

		if ((f = fopen(filename, "r")) != NULL)
			{
			while (line.readLine(f))
				{
				// puts(line);
				if (line.isEmpty() || line[0] == '#') continue;

				// Fix DOS format issues
				line.replaceAll("\r", "");

				if (line.startsWith("setenv ", 0, true))
					{
					line.remove(0, 7);
					line.stripChars(" \t", XString::leading);

					// there wil be no = to search for so use spaces
					int	p1=line.first(' ');
					int	p2=line.first('\t');
					int	p;

					if (p1 < 0 && p2 < 0) continue;

					if (p1 < 0) p = p2;
					else if (p2 < 0) p = p1;
					else p = (p1<p2)?p1:p2;

					key = line.substring(0, p);
					key.trim();
					value = line.substring(p+1);
					value.stripChars(" \t", XString::leading);
					}
				else 
					{
					if (line.startsWith("set ", 0, true)) line.remove(0, 4);

					int p = line.first('=');
					if (p < 0) continue;

					key = line.substring(0, p);
					key.trim();
					value = line.substring(p+1);
					}

				value.trim();
				if (value.length() > 0 && value[0] == '"' && value[value.length()-1] == '"')
					{
					value.remove(0, 1);
					value.remove(value.length()-1);
					}

				if (!key.isEmpty() && key.first(' ') < 0 && key.first('\t') < 0)
					{
					if (!noautoyear && key.endsWith("COPYRIGHT"))
						{
						XTokenizer	tok(value, " \t");
						XString		s, tmp;
						char		year4[5], year2[3];
						XDate		today;

						today.update();
						sprintf(year4, "%04d", today.year());
						sprintf(year2, "%02d", today.year() % 100);

						while (tok.hasMoreTokens())
							{
							int		c, nchars, type=1;

							s = tok.nextToken();
							nchars = s.length();

							for (int i=0;type!=0&&i<nchars;i++)
								{
								c = s[i];
								if (c == '-')
									{
									if (type == 1 && i > 0) type = 2;
									else type = 0;
									}
								else if (c < 0 || c > '9') type = 0;
								}

							if (!tmp.isEmpty()) tmp += " ";
							switch (type)
								{
								case 1: // Single number
									if (s.length() == 4) tmp += year4;
									else tmp += s;
									break;

								case 2:
									{
									int		pos=s.first('-');

									if (pos == 4 && s.length() == 9)
										{
										tmp += s.left(5);
										tmp += year4;
										}
									else if (pos == 4 && s.length() == 7)
										{
										tmp += s.left(5);
										tmp += year2;
										}
									else
										{
										tmp += s;
										}
									}
									break;

								default:
									tmp += s;
									break;
								}
							}

						value = tmp;
						}

					//printf("%s = %s\n", (const char *)key, (const char *)value);
					buildparams[key] = value;
					}
				
				if (!value.isEmpty())
					{
					bool	numeric=true, hex=false;
					int		pos=0;
					int		nchars=value.length();
					int		c;

					if (value.startsWith("0x") || value.startsWith("0X"))
						{
						pos = 2;
						hex = true;
						}

					while (numeric && pos < nchars)
						{
						c = value[pos++];
						if (c < '0' || c > '9')
							{
							if (!hex || !((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F')))
								{
								numeric = false;
								}
							}
						}

					if (numeric)
						{
						paramtype[key]= "n";
						}
					}
				}
			//printf("Done\n");
			fclose(f);
			}
		else update = true;
		}


void
writeInfoFile(const XString &filename, FileFormat ff)
		{
		FILE	*f;

		if (!noversion)
			{
			XString	version = buildparams[VERSION_FORMAT];

			version.ReplaceAll("%m", buildparams[MAJOR]);
			version.ReplaceAll("%n", buildparams[MINOR]);
			version.ReplaceAll("%b", buildparams[BUILD]);
			version.ReplaceAll("%r", buildparams[REVISION]);
			buildparams[VERSION] = version;
			}

		if (verbose) printf("Creating file %s\n", filename.data());

		if (filename == "-") f = stdout;
		else if ((f = fopen(filename, "w")) == NULL)
			{
			fprintf(stderr, "**** getbuildinfo - Can't open %s for writing ****\n", filename.data());
			return;
			}

		XString		section_start="", section_end="";
		const char	*comment_start="", *comment_end="", *set_prefix="", *value_prefix="", *value_suffix="", *format="Unknown", *equals="=", *as_string_before="", *as_integer_before="", *as_string_after="", *as_integer_after="", *indent="", *line_end="";
		bool		allstrings=true;

		switch (ff)
			{
			case FILE_FORMAT_BATCH:
				comment_start = "@rem ";
				comment_end = "";
				set_prefix = "set ";
				value_prefix = "";
				value_suffix = "";
				equals = "=";
				format = "batch/cmd";
				break;

			case FILE_FORMAT_CSH:
				comment_start = "# ";
				comment_end = "";
				set_prefix = "set ";
				value_prefix = "\"";
				value_suffix = "\"";
				equals = "=";
				format = "csh - set";
				break;

			case FILE_FORMAT_CSH_SETENV:
				comment_start = "# ";
				comment_end = "";
				set_prefix = "setenv ";
				value_prefix = "\"";
				value_suffix = "\"";
				equals = " ";
				format = "csh - setenv";
				break;

			case FILE_FORMAT_SH:
			case FILE_FORMAT_SH_NO_EXPORT:
				comment_start = "# ";
				comment_end = "";
				set_prefix = "";
				value_prefix = "\"";
				value_suffix = "\"";
				equals = "=";
				format = "sh/ksh/zsh";
				break;

			case FILE_FORMAT_C:
			case FILE_FORMAT_CPP:
				comment_start = "/* ";
				comment_end = " */";
				set_prefix = "#define ";
				value_prefix = "\"";
				value_suffix = "\"";
				equals = "\t";
				format = "C/C++";
				allstrings = false;
				break;

			case FILE_FORMAT_CS:
				comment_start = "/* ";
				comment_end = " */";
				set_prefix = "public const ";
				value_prefix = "\"";
				value_suffix = "\"";
				equals = " = ";
				format = "C#";
				as_string_before = "string ";
				as_integer_before = "int ";
				line_end = ";";
				allstrings = false;

				if (!nsName.isEmpty())
					{
					section_start += "\nnamespace ";
					section_start += nsName;
					section_start += "\n\t{\n";
					}

				if (!csName.isEmpty())
					{
					section_start += "\tpublic class ";
					section_start += csName;
					section_start += "\n\t\t{\n";

					section_end += "\t\t} // end class ";
					section_end += csName;
					section_end += "\n";
					}

				if (!nsName.isEmpty())
					{
					section_end += "\t} // end namespace ";
					section_end += nsName;
					section_end += "\n\n";
					}

				indent = "\t\t";
				break;

			case FILE_FORMAT_VB:
				comment_start = "' ";
				comment_end = "";
				set_prefix = "Public Const ";
				value_prefix = "\"";
				value_suffix = "\"";
				equals = " = ";
				format = "VB";
				as_string_after = " As String";
				as_integer_after = " As Integer";
				allstrings = false;

				if (!nsName.isEmpty())
					{
					section_start += "\nPublic Module ";
					section_start += nsName;
					section_start += "\n\n";
					}

				if (!csName.isEmpty())
					{
					section_start += "Public Class ";
					section_start += csName;
					section_start += "\n\n";

					section_end += "\nEnd Class\n";
					}

				if (!nsName.isEmpty())
					{
					section_end += "\nEnd Module\n\n";
					}
				break;

			case FILE_FORMAT_MAKE:
			default:
				comment_start = "# ";
				comment_end = "";
				set_prefix = "";
				value_prefix = "";
				value_suffix = "";
				equals = "=";
				format = "make";
				break;
			}


		fprintf(f, "%sStart of generated build info (%s format)%s\n", comment_start, format, comment_end);

		if (!section_start.isEmpty())
			{
			fprintf(f, "%s", section_start.data());
			}

		Iterator	it = buildparams.iterator();
		XString	key, value;

		while (it.hasNext())
			{
			bool		numeric=false;
			const char	*as_type_before=as_string_before;
			const char	*as_type_after=as_string_after;

			key = it.next()->key();
			value = buildparams[key];

				//printf("key = %s\n", (const char *)key);
				//printf("value = %s\n", (const char *)value);
			if (!allstrings)
				{
				if (paramtype[key] == "n")
					{
					// This is a numeric type, don't show quotes
					numeric = true;
					as_type_before = as_integer_before;
					as_type_after = as_integer_after;
					}
				}

			if (!rmPrefix.isEmpty() && key.startsWith(rmPrefix))
				{
				key.remove(0, rmPrefix.length());
				}

			fprintf(f, "%s%s%s%s%s%s%s%s%s%s%s\n", indent, set_prefix, (const TCHAR *)outPrefix, as_type_before, key.data(), as_type_after, equals, numeric?"":value_prefix, value.data(), numeric?"":value_suffix, line_end);
			if (numeric) fprintf(f, "%s%s%s%s%s_STR%s%s%s%s%s%s\n", indent, set_prefix, (const TCHAR *)outPrefix, as_string_before, key.data(), as_string_after, equals, value_prefix, value.data(), value_suffix, line_end);

			if (ff == FILE_FORMAT_SH) fprintf(f, "export %s\n", key.data());
			}

		if (!section_end.isEmpty())
			{
			fprintf(f, "%s", section_end.data());
			}

		fprintf(f, "%sEnd of generated build info%s\n", comment_start, comment_end);
		if (f != stdout) fclose(f);
		}


void
showInfo()
		{
		Iterator	it = buildparams.iterator();
		XString	key, value;
		int		w=0;

		while (it.hasNext())
			{
			key = it.next()->key();
			w = (w>key.length())?w:key.length();
			}

		it = buildparams.iterator();
		while (it.hasNext())
			{
			key = it.next()->key();
			value = buildparams[key];

			printf("%-*s = %s\n", w, key.data(), value.data());
			}
		}


void
incXString(XString &v)
		{
		int	i = atoi(v);
		char	tmp[32];

		sprintf(tmp, "%d", i+1);
		v = tmp;
		}

/*
** Produce a usage message and exit
*/
void
usage(int exitcode=1)
	{
	printf(
///////////////////////////////////////////////////////////////////////////////
"Usage: getbuildinfo [options]\n"
"\n"
"The getbuildinfo maintains a build.info file which is in a format tha can be\n"
"directly include in a makefile (Unix and windows). Options are available to\n"
"produce a format suitable for batch files (Dos/Windows) and scripts (Unix sh,\n"
"csh, ksh, etc).\n"
"\n"
"The options are:\n"
"\n"
"    --inc-major        Increment the major version number (minor and\n"
"                       revision numbers are set to zero).\n"
"    --inc-minor        Increment the minor version number (revision\n"
"                       number is set to zero, the major version number\n"
"                       remains unchanged).\n"
"    --inc-build        Increment the build number (other numbers remain\n"
"                       unchanged).\n"
"    --inc-revision     Increment the revision number (the major and minor\n"
"                       version numbers remain unchanged).\n"
"\n"
"    --qa-build         Make sure this is a QA build number (even)\n"
"                       incrementing only if necessary\n"
"    --int-build        Make sure this is an internal build number (odd)\n"
"                       incrementing only if necessary\n"
"    --inc-build-even   Increment the build number to the next even number\n"
"    --inc-build-odd    Increment the build number to the next odd number\n"
"\n"
"    --set param=value  Set the specified parameter to the given value\n"
"    --get param        Get the specified parameter and display it on stdout\n"
"    --remove param     Remove the specified parameter\n"
"\n"
"    --make		Write out in make format (default)\n"
"    --bat		Write out in dos/windows batch/cmd format\n"
"    --cmd		Same as --bat\n"
"    --csh		Write out in csh set format\n"
"    --csh-env		Write out in csh setenv format\n"
"    --sh		Write out in sh format (also suitable for ksh and zsh)\n"
"    --ksh		Same as --sh\n"
"    --zsh		Same as --sh\n"
"    --C		Write out in C #define format\n"
"    --C++		Same as --C\n"
"\n"
"    --file xxxx        Use the specified file xxxx. By default build.info is\n"
"                       updated.\n"
"    --out fmt:file     Write to the specified file in the requested format\n"
"                       Can be combined with other options to produce\n"
"                       interesting results. If fmt: is omitted current format is\n"
"                       used. Valid formats: make, bat, cmd, .... (see above)\n"
"\n"
"    --show             Show the contents of the file\n"
"    --readonly         Don't update the file\n"
"    --class xxxx       Set the output class to xxxx (C# and VB)\n"
"    --namespace xxxx   Set the output namespace to xxxx (C# and VB)\n"
"    --module xxxx      Set the output module to xxxx (VB)\n"
"    --prefix xxxx      Set build parameter prefix to xxxx\n"
"    --out-prefix x     Set the output parameter prefix to xxxx (used with --out)\n"
"    --rm-prefix x      Set the part of the parameter prefix that is removed if matched (used with --out)\n"
"\n"
"    --no-autoyear      Disable auto-year update on COPYRIGHT entry.\n"
"    --help             Display this help text.\n"
///////////////////////////////////////////////////////////////////////////////
	);

	exit(exitcode);
	}

	
int
//ca_main(XString &prog, XStringArray &args)
main(int argc, char **argv)
		{
		XDate	today;
		char	s[64];

		today.update();

		sprintf(s, "%04d", today.year());
		thisyearString = s;

		sprintf(s, "%02d/%02d/%04d", today.day(), today.month(), today.year());
		todayString = s;

		XString	prog(argv[0]);
		XString	cmd, filename("build.info");
		int		i;
		FileFormat	format=FILE_FORMAT_MAKE;
		bool		readonly=false, showflag=false, prefixSet=false;

		for (i=1;i<argc;i++)
			{
			cmd = argv[i];
			if (cmd == "--file")
				{
				if (++i < argc) filename = argv[i];
				}
			else if (cmd == "--prefix")
				{
				if (++i < argc)
					{
					setPrefix(argv[i]);
					prefixSet = true;
					}
				else usage();
				}
			else if (cmd == "-v" || cmd == "--verbose")
				{
				verbose = 1;
				}
			else if (cmd == "-q" || cmd == "--quiet")
				{
				verbose = 0;
				}
			else if (cmd == "--no-autoyear") noautoyear = true;
			else if (cmd == "--no-version") noversion = true;
			}

		if (!prefixSet) setPrefix("BUILD");

		// printf("Reading file %s\n", (const char *)filename);
		readInfoFile(filename);
		// printf("Done reading file\n");

		for (i=1;i<argc;i++)
			{
			cmd = argv[i];
			if (cmd == "--inc-major")
				{
				incXString(buildparams[MAJOR]);
				buildparams[MINOR] = "0";
				buildparams[REVISION] = "0";
				update = true;
				}
			else if (cmd == "--inc-minor")
				{
				incXString(buildparams[MINOR]);
				buildparams[REVISION] = "0";
				update = true;
				}
			else if (cmd == "--inc-build" || cmd == "--qa-build" || cmd == "--int-build" || cmd == "--inc-build-odd" || cmd == "--inc-build-even")
				{
				XString	v = buildparams[BUILD];

				if (cmd == "--qa-build") // Even build number (inc if not already even)
					{
					while ((atoi(v) % 2) != 0)
					{
					incXString(v);
					}
					}
				else if (cmd == "--int-build") // Odd build number (inc if not already odd)
					{
					while ((atoi(v) % 2) != 1) 
					{
					incXString(v);
					}
					}
				else if (cmd == "--inc-build-even") // NEXT Even build number
					{
					incXString(v);
					while ((atoi(v) % 2) != 0) incXString(v);
					}
				else if (cmd == "--inc-build-odd") // NEXT Odd build number
					{
					incXString(v);
					while ((atoi(v) % 2) != 1) incXString(v);
					}
				else
					{
					incXString(v);
					}

				if (v != buildparams[BUILD])
					{
					buildparams[BUILD] = v;
					update = true;
					}
				}
			else if (cmd == "--inc-revision")
				{
				incXString(buildparams[REVISION]);
				update = true;
				}
			else if (cmd == "--help") usage(0);
			else if (cmd == "--show")
				{
				showflag = true;
				}
			else if (cmd == "--file")
				{
				// Already processed - just ignore
				i++;
				}
			else if (cmd == "--set")
				{
				XString	arg, key, value;

				if (++i < argc)
					{
					arg = argv[i];
					int p = arg.first('=');
					if (p < 0) usage();
					key = arg.substring(0, p);
					value = arg.substring(p+1);

					buildparams[key] = value;
					update = true;
					}
				else usage();
				}
			else if (cmd == "--class")
				{
				if (++i < argc)
					{
					csName = argv[i];
					}
				else usage();
				}
			else if (cmd == "--namespace" || cmd == "--module")
				{
				if (++i < argc)
					{
					nsName = argv[i];
					}
				else usage();
				}
			else if (cmd == "--get" || cmd == "--remove")
				{
				XString	key, value;

				if (++i < argc)
					{
					key = argv[i];

					if (cmd == "--get")
						{
						if (buildparams.defined(key)) value = buildparams[key];

						printf("%s=%s\n", key.data(), value.data());
						}
					else if (cmd == "--remove")
						{
						buildparams.remove(key);
						update = true;
						}
					}
				else usage();
				}
			else if (cmd == "--make") format = FILE_FORMAT_MAKE;
			else if (cmd == "--bat") format = FILE_FORMAT_BATCH;
			else if (cmd == "--cmd") format = FILE_FORMAT_BATCH;
			else if (cmd == "--csh") format = FILE_FORMAT_CSH;
			else if (cmd == "--csh-env") format = FILE_FORMAT_CSH_SETENV;
			else if (cmd == "--sh") format = FILE_FORMAT_SH_NO_EXPORT;
			else if (cmd == "--ksh") format = FILE_FORMAT_SH_NO_EXPORT;
			else if (cmd == "--zsh") format = FILE_FORMAT_SH_NO_EXPORT;
			else if (cmd == "--sh-export") format = FILE_FORMAT_SH;
			else if (cmd == "--ksh-export") format = FILE_FORMAT_SH;
			else if (cmd == "--zsh-export") format = FILE_FORMAT_SH;
			else if (cmd.CompareNoCase("--C") == 0) format = FILE_FORMAT_C;
			else if (cmd.CompareNoCase("--C++") == 0) format = FILE_FORMAT_CPP;
			else if (cmd.CompareNoCase("--CS") == 0 || cmd.CompareNoCase("--C#") == 0 || cmd.CompareNoCase("--c-sharp") == 0) format = FILE_FORMAT_CS;
			else if (cmd.CompareNoCase("--VB") == 0) format = FILE_FORMAT_VB;
			else if (cmd == "--out")
				{
				if (++i < argc)
					{
					XString	file(argv[i]);
					FileFormat	ff = FILE_FORMAT_UNKNOWN;
					int		p = file.first(':');

					if (p > 0)
						{
						XString	fmt = file(0, p);

						if (fmt == "make") ff = FILE_FORMAT_MAKE;
						else if (fmt == "bat") ff = FILE_FORMAT_BATCH;
						else if (fmt == "cmd") ff = FILE_FORMAT_BATCH;
						else if (fmt == "csh") ff = FILE_FORMAT_CSH;
						else if (fmt == "csh-env") ff = FILE_FORMAT_CSH_SETENV;
						else if (fmt == "sh-export") ff = FILE_FORMAT_SH;
						else if (fmt == "ksh-export") ff = FILE_FORMAT_SH;
						else if (fmt == "zsh-export") ff = FILE_FORMAT_SH;
						else if (fmt == "sh") ff = FILE_FORMAT_SH_NO_EXPORT;
						else if (fmt == "ksh") ff = FILE_FORMAT_SH_NO_EXPORT;
						else if (fmt == "zsh") ff = FILE_FORMAT_SH_NO_EXPORT;
						else if (fmt.CompareNoCase("c") == 0) ff = FILE_FORMAT_C;
						else if (fmt.CompareNoCase("c++") == 0) ff = FILE_FORMAT_CPP;
						else if (fmt.CompareNoCase("c#") == 0) ff = FILE_FORMAT_CS;
						else if (fmt.CompareNoCase("cs") == 0) ff = FILE_FORMAT_CS;
						else if (fmt.CompareNoCase("vb") == 0) ff = FILE_FORMAT_VB;

						if (ff != FILE_FORMAT_UNKNOWN) file.remove(0, p+1);
						}

					if (ff == FILE_FORMAT_UNKNOWN) ff = format;

					if (ff == FILE_FORMAT_VB && nsName.isEmpty())
						{
						printf("ERROR: VB output must have a class or module defined\n");
						exit(1);
						}

					if (ff == FILE_FORMAT_CS && (nsName.isEmpty() || csName.isEmpty()))
						{
						printf("ERROR: C# output must have both a class and namespace defined\n");
						exit(1);
						}

					writeInfoFile(file, ff);
					}
				else usage();
				}
			else if (cmd == "--readonly") readonly = true;
			else if (cmd == "--version")
				{
		// If this is the first build of getbuildinfo we have to fake it
		#ifndef BUILD_VERSION
		#define BUILD_VERSION	"0.0.0.0"
		#endif
				printf("%s version %s\n", prog.data(), BUILD_VERSION);
				}
			else if (cmd == "--prefix")
				{
				if (++i < argc) setPrefix(argv[i]);
				else usage();
				}
			else if (cmd == "--rm-prefix")
				{
				if (++i < argc) rmPrefix = argv[i];
				else usage();
				}
			else if (cmd == "--out-prefix")
				{
				if (++i < argc) outPrefix = argv[i];
				else usage();
				}
			else if (cmd == "-v" || cmd == "--verbose")
				{
				verbose = 1;
				}
			else if (cmd == "-q" || cmd == "--quiet")
				{
				verbose = 0;
				}
			else if (cmd == "--no-autoyear") noautoyear = true;
			else if (cmd == "--no-version") noversion = true;
			else usage();
			}

		if (!readonly && update) writeInfoFile(filename, format);
		if (showflag) showInfo();

		return 0;
		}
